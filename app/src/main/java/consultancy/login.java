package consultancy;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import job.job.consaltancy.BuildConfig;
import job.job.consaltancy.ForgotPassword;
import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;


/**
 * Created by Dinesh Kukreja on 7/4/2016.
 */

public class login extends AppCompatActivity {

    int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    EditText email, password;
    TextView login;
    TextView signup;
    String url = "";
    private Pattern pattern;
    private Matcher matcher;
    Toolbar toolbar;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    SharedPreferences user_info;
    SharedPreferences.Editor editor;

    NetworkConnection nw;
    private TextView forgot;
    private TextView fmf;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        OneSignal.startInit(this).init();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user_info = getSharedPreferences("user_info", Context.MODE_PRIVATE);
        nw = new NetworkConnection(login.this);
        email = (EditText) findViewById(R.id.input_email);
        password = (EditText) findViewById(R.id.input_password);
        login = (TextView) findViewById(R.id.btn_login);
        signup = (TextView) findViewById(R.id.link_signup);
        forgot = (TextView) findViewById(R.id.forgot);
        fmf = (TextView) findViewById(R.id.fmf);
        pattern = Pattern.compile(EMAIL_PATTERN);

        Typeface BalooBhaiRegular = Typeface.createFromAsset(getAssets(),  "fonts/BalooBhai-Regular.ttf");
        fmf.setTypeface(BalooBhaiRegular);

        Constants.VersionCode = BuildConfig.VERSION_CODE;
        Constants.VersionName = BuildConfig.VERSION_NAME;

        if (user_info.getBoolean("login_bool", false)) {
            Intent in = new Intent(login.this, HomeActivity.class);
            startActivity(in);
            finish();
        } else {


            if (Build.VERSION.SDK_INT >= 23) {
                //do your check here
                getDummyContactWrapper();
            }


            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (validateFields())
                        if (nw.isConnected()) {
                            login_volley();
                        } else
                            showSnackbar("Oops! Not Connected to Internet");

                }
            });

            signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("clicked on signup", "entered");
                    Intent in = new Intent(login.this, registeration.class);
                    startActivity(in);
                }
            });

            forgot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(login.this, ForgotPassword.class);
                    startActivity(intent);
                }
            });

        }

    }

    private void showSnackbar(String text) {

        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.notify);

        final Snackbar snackbar = Snackbar.make(login, "  " + text, Snackbar.LENGTH_LONG);
//        snackbar.setAction("OK", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                    }
//                });
//        snackbar.setActionTextColor(Color.DKGRAY);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
        textView.setGravity(Gravity.CENTER);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
        textView.setCompoundDrawablePadding(0);

        snackbar.show(); // Don’t forget to show!
        mp.start();

    }

    private boolean validateFields() {

        matcher = pattern.matcher(email.getText());

        if (email.getText().length() < 1 && password.getText().length() < 1) {
            showSnackbar("Please enter Email/UserId & Password");

        } else if (email.getText().length() < 1) {
            showSnackbar("Please enter Email or UserId");

        } else if (email.getText().length() != 10 && !matcher.matches()) {
                showSnackbar("It seems not a valid Email or UserId");

        } else if (password.getText().length() < 1) {
            showSnackbar("Please enter your Password");

        } else if (password.getText().length() < 4) {
            showSnackbar("Please enter correct Password");

        } else {
            return true;
        }
        return false;
    }

    private void login_volley() {

        final ProgressDialog dialog = new ProgressDialog(login.this);
        dialog.setMessage("Logging in");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.show();

        String mail = "", id = "", pass;
        if (matcher.matches())
            mail = email.getText().toString();
        else
            id = email.getText().toString();
        pass = password.getText().toString();

        url = Constants.url + "login.php?email=" + mail + "&unique_id=" + id + "&password=" + pass;
        url = URLDecoder.decode(url);
        url = url.replace(" ", "%20");
        Log.e("url", url);
        RequestQueue queue = Volley.newRequestQueue(login.this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject res) {
                dialog.dismiss();
                Log.e("response", res + "");
                try {
                    JSONArray jsonArray = new JSONArray();
                    JSONObject obj;

                    if (res.has("wrong")) {
                        showSnackbar("Oh! Wrong Username or Password");
//                        Toast.makeText(login.this, "Wrong Username and Password", Toast.LENGTH_LONG).show();
                    } else {

                        editor = user_info.edit();

//                        if (res.has("login")) {
                        jsonArray = res.getJSONArray("login");
                        obj = jsonArray.getJSONObject(0);

                        Log.e("login_object", obj.toString());

                        editor.putString("email", obj.getString("email"));
                        editor.putString("unique_id", obj.getString("unique_id"));
                        editor.putString("state", obj.getString("state"));
                        editor.putString("district", obj.getString("district"));
                        editor.putBoolean("login_bool", true);

                        //  }
                        if (res.has("basic")) {

                            JSONObject jobj = new JSONObject();
                            jsonArray = res.getJSONArray("basic");
                            obj = jsonArray.getJSONObject(0);

                            Log.e("basic_object", obj.toString());

                            jobj.put("c_fname", obj.getString("candidate"));
                            jobj.put("c_mname", "");
                            jobj.put("c_lname", "");
                            jobj.put("f_fname", obj.getString("father"));
                            jobj.put("f_mname", "");
                            jobj.put("f_lname", "");
                            jobj.put("m_fname", obj.getString("mother"));
                            jobj.put("m_mname", "");
                            jobj.put("m_lname", "");
                            jobj.put("h_fname", obj.getString("husband"));
                            jobj.put("h_mname", "");
                            jobj.put("h_lname", "");

                            String dob = obj.getString("dob");
                            String darr[] = new String[0];
                            if (dob.contains("-")) {
                                darr = dob.split("-");

                                jobj.put("dob_d", darr[2]);
                                jobj.put("dob_m", darr[1]);
                                jobj.put("dob_y", darr[0]);
                            }


                            jobj.put("dob_proof", obj.getString("dob_proof"));
                            jobj.put("area", obj.getString("area"));
                            jobj.put("aadhar_no", obj.getString("aadhar_number"));
                            jobj.put("visible_id_mark", obj.getString("mark"));
                            jobj.put("criminal_case_yn", obj.getString("criminal_case"));
                            jobj.put("criminal_case", obj.getString("case"));
                            jobj.put("domicile_state", obj.getString("domicil"));

                            editor.putString("basic_info", jobj.toString());

                        }
                        if (res.has("category")) {

                            JSONObject jobj = new JSONObject();

                            jsonArray = res.getJSONArray("category");
                            obj = jsonArray.getJSONObject(0);

                            Log.e("category_object", obj.toString());


                            jobj.put("gender", obj.getString("gender"));
                            jobj.put("category", obj.getString("category"));
                            jobj.put("date_of_issue", obj.getString("date_of_issue"));
                            jobj.put("sub_cate", obj.getString("sub_cat"));
                            jobj.put("disability", obj.getString("disability"));
                            jobj.put("type_disability", obj.getString("type_disability"));
                            jobj.put("per_disability", obj.getString("percentage"));
                            jobj.put("pwd_subcat", obj.getString("pwd_sub_cat"));
                            jobj.put("pwd_date", obj.getString("pwd_date"));
                            jobj.put("cerabral", obj.getString("cerabral"));
                            jobj.put("palsy_extra", obj.getString("palsy_extra_time"));
                            jobj.put("dominant_writing", obj.getString("dominat_writing"));
                            jobj.put("dominant_extra", obj.getString("dominat_extra_time"));
                            jobj.put("judicially_relax", obj.getString("relaxation"));
                            jobj.put("domicile_jk", obj.getString("jk_domicile"));
                            jobj.put("religion", obj.getString("religion"));
                            jobj.put("religious_minor", obj.getString("religious_minor"));
                            jobj.put("ex_serviceman", obj.getString("ex_service"));
                            jobj.put("disable_exman", obj.getString("disable_ex_service"));
                            jobj.put("period_of_service", obj.getString("pof_service"));
                            jobj.put("ser_start_date", obj.getString("sstart_date"));
                            jobj.put("ser_to_date", obj.getString("sto_date"));
                            jobj.put("ser_discharge_date", obj.getString("send_date"));
                            jobj.put("dep_serman_killed", obj.getString("killed_in_action"));
                            jobj.put("dep_serman_state", obj.getString("dept_of_ex"));
                            jobj.put("relation_with_exman", obj.getString("relation_with_exman"));
                            jobj.put("freedom_fighter", obj.getString("ffighter"));
                            jobj.put("relation_with_fighter", obj.getString("relation_ffighter"));

                            editor.putString("category_info", jobj.toString());

                        }
                        if (res.has("address")) {

                            JSONObject jobj = new JSONObject();

                            jsonArray = res.getJSONArray("address");
                            obj = jsonArray.getJSONObject(0);

                            Log.e("address_object", obj.toString());

                            jobj.put("email_alt", obj.getString("alternat_mail"));
                            jobj.put("mobile_no", obj.getString("mobile_no"));
                            jobj.put("mobile_alt", obj.getString("alternate_mob"));
                            jobj.put("landline", obj.getString("landline_no"));

                            jobj.put("address1", obj.getString("address1"));
                            jobj.put("address2", obj.getString("address2"));
                            jobj.put("address3", obj.getString("address3"));
                            jobj.put("town", obj.getString("town_city"));
                            jobj.put("district", obj.getString("district"));
                            jobj.put("state", obj.getString("state"));
                            jobj.put("pincode", obj.getString("pincode"));
                            jobj.put("near_rail", obj.getString("near_rail_station"));
                            jobj.put("near_land", obj.getString("near_land_mark"));

                            jobj.put("address_p1", obj.getString("p_address1"));
                            jobj.put("address_p2", obj.getString("p_address2"));
                            jobj.put("address_p3", obj.getString("p_address3"));
                            jobj.put("town_p", obj.getString("p_town_city"));
                            jobj.put("district_p", obj.getString("p_district"));
                            jobj.put("state_p", obj.getString("p_state"));
                            jobj.put("pincode_p", obj.getString("p_pincode"));
                            jobj.put("near_rail_p", obj.getString("p_near_rail_station"));
                            jobj.put("near_land_p", obj.getString("p_near_land_mark"));

                            editor.putString("address_info", jobj.toString());
                        }
                        if (res.has("education")) {

                            JSONObject jobj = new JSONObject();

                            jsonArray = res.getJSONArray("education");
                            obj = jsonArray.getJSONObject(0);

                            Log.e("education_object", obj.toString());

                            jobj.put("matric_yn", obj.getString("matric_yn"));

                            jobj.put("sub10", obj.getString("all_sub"));
                            jobj.put("board10", obj.getString("nameofboard"));
                            jobj.put("boardstate10", obj.getString("stateboard"));
                            jobj.put("school10", obj.getString("name_of_school"));
                            jobj.put("result10", obj.getString("result"));
                            jobj.put("rollno10", obj.getString("roll_number"));
                            jobj.put("obtainmark10", obj.getString("obtanined_mrks"));
                            jobj.put("totalmark10", obj.getString("total_marks"));
                            jobj.put("percentagemark10", obj.getString("percentage_mrks"));
                            jobj.put("passingdate10", obj.getString("date_of_passing"));
                            jobj.put("grade10", obj.getString("grade"));
                            jobj.put("mode10", obj.getString("mode"));

                            jobj.put("intermed_yn", obj.getString("intermed_yn"));

                            jobj.put("stream12", obj.getString("stream"));
                            jobj.put("sub12", obj.getString("all_sb12"));
                            jobj.put("board12", obj.getString("name_baord"));
                            jobj.put("boardstate12", obj.getString("state_of_board12"));
                            jobj.put("school12", obj.getString("name_of_school12"));
                            jobj.put("result12", obj.getString("result12"));
                            jobj.put("rollno12", obj.getString("roll_number12"));
                            jobj.put("obtainmark12", obj.getString("obtanied_mrks12"));
                            jobj.put("totalmark12", obj.getString("total_mrks12"));
                            jobj.put("percentagemark12", obj.getString("p_marks"));
                            jobj.put("passingdate12", obj.getString("d_of_pass"));
                            jobj.put("grade12", obj.getString("division"));
                            jobj.put("mode12", obj.getString("mode12"));

                            jobj.put("d_holder", obj.getString("d_holder"));
                            jobj.put("d_deploma", obj.getString("deploma"));
                            jobj.put("d_stream", obj.getString("d_stream"));
                            jobj.put("d_duretion", obj.getString("dur_of_dep"));
                            jobj.put("d_board", obj.getString("nam_of_board"));
                            jobj.put("d_board_state", obj.getString("d_state"));
                            jobj.put("d_school", obj.getString("d_name_school"));
                            jobj.put("d_result", obj.getString("d_res"));
                            jobj.put("d_rollno", obj.getString("roll_no"));
                            jobj.put("d_obt_mark", obj.getString("obt_marks"));
                            jobj.put("d_total_mark", obj.getString("tot_marks"));
                            jobj.put("d_per_mark", obj.getString("per_marksd"));
                            jobj.put("d_date_passing", obj.getString("d_of_pass_d"));
                            jobj.put("d_grade", obj.getString("d_grade"));
                            jobj.put("d_mode", obj.getString("d_mode"));


                            jobj.put("graduation_yn", obj.getString("graduation_yn"));
                            jobj.put("g_graduate", obj.getString("graduate"));
                            jobj.put("g_stream", obj.getString("g_stream"));
                            jobj.put("g_duration", obj.getString("g_duration"));
                            jobj.put("g_univercity", obj.getString("g_name_uni"));
                            jobj.put("g_univercity_place", obj.getString("g_place"));
                            jobj.put("g_univercity_state", obj.getString("g_state"));
                            jobj.put("g_collage", obj.getString("g_name_of_clg"));
                            jobj.put("g_result", obj.getString("g_result"));
                            jobj.put("g_obt_mark", obj.getString("g_marks_obt"));
                            jobj.put("g_total_mark", obj.getString("g_total_mrks"));
                            jobj.put("g_percentage", obj.getString("g_percent"));
                            jobj.put("g_date_passing", obj.getString("g_d_of_pass"));
                            jobj.put("g_grade", obj.getString("g_grade"));
                            jobj.put("g_mode", obj.getString("g_mode"));
                            jobj.put("g_cgpa", obj.getString("cgpa"));
                            jobj.put("g_max_cgpa", obj.getString("mrks_cgpa"));

                            jobj.put("postgraduation_yn", obj.getString("postgraduation_yn"));
                            jobj.put("pg_graduate", obj.getString("postgraduate"));
                            jobj.put("pg_stream", obj.getString("pg_streamv"));
                            jobj.put("pg_duration", obj.getString("pg_dur"));
                            jobj.put("pg_univercity", obj.getString("pg_name_of_uni"));
                            jobj.put("pg_univercity_place", obj.getString("pg_uni_place"));
                            jobj.put("pg_univercity_state", obj.getString("pg_uni_state"));
                            jobj.put("pg_collage", obj.getString("pg_clgname"));
                            jobj.put("pg_result", obj.getString("pg_result"));
                            jobj.put("pg_obt_mark", obj.getString("pg_obt_marks"));
                            jobj.put("pg_total_mark", obj.getString("pg_total_marks"));
                            jobj.put("pg_percentage", obj.getString("pg_perc_of_mrks"));
                            jobj.put("pg_date_passing", obj.getString("pd_d_pass"));
                            jobj.put("pg_grade", obj.getString("pg_garde"));
                            jobj.put("pg_mode", obj.getString("pg_modee"));
                            jobj.put("pg_cgpa", obj.getString("pg_cgpa"));
                            jobj.put("pg_max_cgpa", obj.getString("pg_max"));

                            jobj.put("hin_read", obj.getString("hindi_read"));
                            jobj.put("hin_write", obj.getString("hindi_write"));
                            jobj.put("hin_speak", obj.getString("hindi_speak"));

                            jobj.put("eng_read", obj.getString("eng_read"));
                            jobj.put("eng_write", obj.getString("eng_write"));
                            jobj.put("eng_speak", obj.getString("eng_speak"));

                            jobj.put("oth_lang", obj.getString("other_lang"));
                            jobj.put("oth_read", obj.getString("other_read"));
                            jobj.put("oth_write", obj.getString("other_write"));
                            jobj.put("oth_speak", obj.getString("other_speak"));

                            editor.putString("education_info", jobj.toString());


                        }
                        if (res.has("experience")) {

                            JSONObject jobj = new JSONObject();
                            jsonArray = res.getJSONArray("experience");
                            obj = jsonArray.getJSONObject(0);

                            Log.e("experience_object", obj.toString());

                            jobj.put("go_exp", obj.getString("go_exp"));
                            jobj.put("go_name", obj.getString("go_name"));
                            jobj.put("go_type", obj.getString("go_type"));
                            jobj.put("go_place", obj.getString("go_place"));
                            jobj.put("go_empid", obj.getString("go_empid"));
                            jobj.put("go_post", obj.getString("go_post"));
                            jobj.put("go_duty", obj.getString("go_duty"));
                            jobj.put("go_ser_start", obj.getString("go_ser_start"));

                            jobj.put("we_exp", obj.getString("we_exp"));
                            jobj.put("we_name", obj.getString("we_name"));
                            jobj.put("we_type", obj.getString("we_type"));
                            jobj.put("we_place", obj.getString("we_place"));
                            jobj.put("we_empid", obj.getString("we_empid"));
                            jobj.put("we_post", obj.getString("we_post") + "");
                            jobj.put("we_duty", obj.getString("we_duty") + "");
                            jobj.put("we_ser_from", obj.getString("we_ser_from") + "");
                            jobj.put("we_ser_to", obj.getString("we_ser_to") + "");
                            jobj.put("we_ser_start", obj.getString("we_ser_time") + "");

                            jobj.put("otr_exp", obj.getString("otr_exp"));
                            jobj.put("otr_name", obj.getString("otr_name") + "");
                            jobj.put("otr_type", obj.getString("otr_type") + "");
                            jobj.put("otr_place", obj.getString("otr_place") + "");
                            jobj.put("otr_empid", obj.getString("otr_empid") + "");
                            jobj.put("otr_post", obj.getString("otr_post") + "");
                            jobj.put("otr_duty", obj.getString("otr_duty") + "");
                            jobj.put("otr_ser_from", obj.getString("otr_ser_from") + "");
                            jobj.put("otr_ser_to", obj.getString("otr_ser_to") + "");
                            jobj.put("otr_ser_start", obj.getString("otr_ser_time") + "");

                            editor.putString("experience_info", jobj.toString());

                        }
                        editor.commit();

                        Intent in = new Intent(login.this, HomeActivity.class);
                        startActivity(in);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("jsonexception", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();

                Log.e("error", volleyError + "");

                String err = "UnknownHostException";
                String vol_err = volleyError.toString();
                if (vol_err.contains(err))
                    showSnackbar("Server Error, Try After Sometime");
//                    Toast.makeText(login.this, "Server Error Please Try After Sometime", Toast.LENGTH_LONG).show();


            }
        });

        queue.add(request);

    }


    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private void getDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.GET_ACCOUNTS))
            permissionsNeeded.add("GET_ACCOUNTS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("ACCESS_NETWORK_STATE");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("INTERNET");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ_EXTERNAL_STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("ACCESS_NETWORK_STATE");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_WIFI_STATE))
            permissionsNeeded.add("ACCESS_WIFI_STATE");

//        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS))
//            permissionsNeeded.add("WRITE_CONTACTS");
//        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
//            permissionsNeeded.add("READ_SMS");
//        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
//            permissionsNeeded.add("RECEIVE_SMS");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
//            permissionsNeeded.add("ACCESS_COARSE_LOCATION");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("ACCESS_FINE_LOCATION");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (Build.VERSION.SDK_INT >= 23) {
                                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            }
                        });
                return;
            }
            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(login.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
