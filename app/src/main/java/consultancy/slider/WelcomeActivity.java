package consultancy.slider;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import consultancy.login;
import consultancy.registeration;
import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by Dinesh Kukreja on 7/2/2016.
 */
public class WelcomeActivity extends AppCompatActivity {

    int  REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS=124;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private PrefManager prefManager;
    String possibleEmail;
    SharedPreferences user_info;
    SharedPreferences.Editor editor;
    private TextView tvSignIn;
    private TextView tvSignup;
    private TextView tvSignInfb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Checking for first time launch - before calling setContentView()


        Constants.statelist.clear();
        Constants.statelist.add("Select state");
        Constants.statelist.add("Andaman and Nicobar Islands");
        Constants.statelist.add("Andhra Pradesh");
        Constants.statelist.add("Arunachal Pradesh");
        Constants.statelist.add("Assam");
        Constants.statelist.add("Bihar");
        Constants.statelist.add("Chandigarh");
        Constants.statelist.add("Chhattisgarh");
        Constants.statelist.add("Dadra and Nagar Haveli");
        Constants.statelist.add("Daman and Diu");
        Constants.statelist.add("Delhi – National Capital Territory");
        Constants.statelist.add("Goa");
        Constants.statelist.add("Gujarat");
        Constants.statelist.add("Haryana");
        Constants.statelist.add("Himachal Pradesh");
        Constants.statelist.add("Jammu & Kashmir");
        Constants.statelist.add("Jharkhand");
        Constants.statelist.add("Karnataka");
        Constants.statelist.add("Kerala");
        Constants.statelist.add("Lakshadweep");
        Constants.statelist.add("Madhya Pradesh");
        Constants.statelist.add("Maharashtra");
        Constants.statelist.add("Manipur");
        Constants.statelist.add("Meghalaya");
        Constants.statelist.add("Mizoram");
        Constants.statelist.add("Nagaland");
        Constants.statelist.add("Odisha");
        Constants.statelist.add("Puducherry");
        Constants.statelist.add("Punjab");
        Constants.statelist.add("Rajasthan");
        Constants.statelist.add("Sikkim");
        Constants.statelist.add("Tamil Nadu");
        Constants.statelist.add("Telangana");
        Constants.statelist.add("Tripura");
        Constants.statelist.add("Uttar Pradesh");
        Constants.statelist.add("Uttarakhand");
        Constants.statelist.add("West Bengal");


        Constants.districtlist.clear();
        Constants.districtlist.add("Select district");
        Constants.districtlist.add("Ambala");
        Constants.districtlist.add("Bhiwani");
        Constants.districtlist.add("Charkhi Dadri");
        Constants.districtlist.add("Faridabad");
        Constants.districtlist.add("Fatehabad");
        Constants.districtlist.add("Gurgaon");
        Constants.districtlist.add("Hisar");
        Constants.districtlist.add("Jhajjar");
        Constants.districtlist.add("Jind");
        Constants.districtlist.add("Kaithal");
        Constants.districtlist.add("Karnal");
        Constants.districtlist.add("Kurukshetra");
        Constants.districtlist.add("Mahendragarh");
        Constants.districtlist.add("Mewat");
        Constants.districtlist.add("Palwal");
        Constants.districtlist.add("Panchkula");
        Constants.districtlist.add("Panipat");
        Constants.districtlist.add("Rewari");
        Constants.districtlist.add("Rohtak");
        Constants.districtlist.add("Sirsa");
        Constants.districtlist.add("Sonipat");
        Constants.districtlist.add("Yamuna Nagar");



        prefManager = new PrefManager(this);
        Log.e("pref",String.valueOf(prefManager.isFirstTimeLaunch()));
        if (!prefManager.isFirstTimeLaunch()) {
            gotoSignIn();
            finish();
        }
        if (Build.VERSION.SDK_INT >= 23) {
            //do your check here
            getDummyContactWrapper();
        }
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        tvSignIn = (TextView) findViewById(R.id.signin);
        tvSignup = (TextView) findViewById(R.id.signup);
        tvSignInfb = (TextView) findViewById(R.id.signin_fb);

        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3,
               };

        // adding bottom dots

        addBottomDots(0);
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoSignIn();
            }
        });

        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManager.setFirstTimeLaunch(false);
                Intent intent = new Intent(WelcomeActivity.this,registeration.class);
                startActivity(intent);
                finish();
            }
        });

        tvSignInfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoSignIn();
            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#ffffff"));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    public void gotoSignIn() {
        prefManager.setFirstTimeLaunch(false);
//        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
//        Account[] accounts = AccountManager.get(WelcomeActivity.this).getAccounts();
//        for (Account account : accounts) {
//            if (emailPattern.matcher(account.name).matches()) {
//                possibleEmail = account.name;
//            }
//        }

//        user_info=getSharedPreferences("user_info",1);
//        Log.e("default_email",possibleEmail+"sdghfk");
//        editor=user_info.edit();
//        editor.putString("email",possibleEmail);
//        editor.commit();
//        possibleEmail=getEmail(WelcomeActivity.this);

//        possibleEmail="hardeepmonga@gmail.com";
        Intent in=new Intent(WelcomeActivity.this, login.class);
//        in.putExtra("Default_Email",possibleEmail);

        startActivity(in);
        finish();
    }

    String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.get(WelcomeActivity.this).getAccounts();
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }
    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void getDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.GET_ACCOUNTS))
            permissionsNeeded.add("GET_ACCOUNTS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("ACCESS_NETWORK_STATE");
//        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
//            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("INTERNET");
//        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
//            permissionsNeeded.add("Read External Storage");
//        if (!addPermission(permissionsList, Manifest.permission.CALL_PHONE))
//            permissionsNeeded.add("Call Phone");
//        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
//            permissionsNeeded.add("CAMERA");
//        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS))
//            permissionsNeeded.add("WRITE_CONTACTS");
//        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
//            permissionsNeeded.add("READ_SMS");
//        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
//            permissionsNeeded.add("RECEIVE_SMS");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
//            permissionsNeeded.add("ACCESS_COARSE_LOCATION");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("ACCESS_FINE_LOCATION");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(WelcomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
