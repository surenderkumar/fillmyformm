package consultancy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 8/31/2016.
 */
public class Term_and_Condition extends AppCompatActivity {

    WebView wv;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_confirmation_file);

//        wv=(WebView)findViewById(R.id.webview_id);
//        header=(TextView)findViewById(R.id.tv_header_wv_id);
//
//        header.setText("Terms &amp; Conditions");
//
//        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        wv.loadData(Constants.Term_and_Cond, "text/html; charset=UTF-8", null);
//        wv.loadDataWithBaseURL(null, Constants.Term_and_Cond, "text/html", "utf-8", null);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Terms \u0026 Conditions");

        wv = (WebView) findViewById(R.id.webview_id);

        int myInt = getIntent().getIntExtra("clas", 0);

        if (myInt == 1) {
            getSupportActionBar().setTitle("My Resume");

            wv.setWebViewClient(new MyBrowser());
            wv.getSettings().setJavaScriptEnabled(true);
            String pdf = Constants.url + "resume/" + Constants.Unique_Id + "_resume.pdf";
            wv.loadUrl("http://docs.google.com/gview?embedded=true&url=" + pdf);
        } else {

            wv.loadData(Constants.Term_and_Cond, "text/html; charset=UTF-8", null);
            wv.loadDataWithBaseURL(null, Constants.Term_and_Cond, "text/html", "utf-8", null);
        }

    }

    private class MyBrowser extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("should Override url", url);

            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
