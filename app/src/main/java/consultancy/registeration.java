package consultancy;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;

/**
 * Created by Dinesh Kukreja on 7/4/2016.
 */
public class registeration extends AppCompatActivity {

    Spinner spinner;

    TextView login, term_condition;
    EditText email, password, confirm_password, referral_code,user_name,et_district;
    TextView create_account;
    CheckBox checkBox;
    String url = "", mail, pass, state, district,nameuser;
    SharedPreferences user_info;
    SharedPreferences.Editor editor;
    boolean b = false;
    NetworkConnection nw;
//    private Spinner spinner2;
    private Toolbar toolbar;
    private Pattern pattern;
    private Matcher matcher;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        OneSignal.startInit(this).init();
        user_info = getSharedPreferences("user_info", Context.MODE_PRIVATE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pattern = Pattern.compile(EMAIL_PATTERN);
        user_name=(EditText)findViewById(R.id.user_name);
        email = (EditText) findViewById(R.id.user_email);
        et_district=(EditText)findViewById(R.id.user_district);
        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);
        referral_code = (EditText) findViewById(R.id.et_referal_id);
        create_account = (TextView) findViewById(R.id.btn_signup);
        term_condition = (TextView) findViewById(R.id.tv_term_cond_id);
        checkBox = (CheckBox) findViewById(R.id.cb_term_cond_id);

        b = user_info.getBoolean("login_bool", false);
        if (b) {
            Intent in = new Intent(registeration.this, HomeActivity.class);
            startActivity(in);
            finish();
        }

        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(registeration.this).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                mail = account.name;
            }
        }

        email.setText(mail);
//        Intent in=getIntent();
//        user_email=in.getStringExtra("default_email");
        Log.e("user_emmail", user_info.getString("email", "") + "emiallll");
//        email.setText(in.getStringExtra("Default_Email"));
//        email.setKeyListener(null);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.statelist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Constants.districtlist);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
//        spinner2 = (Spinner) findViewById(R.id.spinner2);
//        spinner2.setAdapter(adapter2);


        login = (TextView) findViewById(R.id.link_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(registeration.this, login.class);
                startActivity(i);
                finish();
            }
        });


        nw = new NetworkConnection(registeration.this);


        term_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(registeration.this, Term_and_Condition.class);
                startActivity(in);
            }
        });

        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateFields())
                    if (nw.isConnected()) {
                        signup_volley();
                    } else
                        showSnackbar("Oops! Not Connected to Internet");

            }

        });

    }

    private void showSnackbar(String text) {

        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.notify);

        final Snackbar snackbar = Snackbar.make(login, "  " + text, Snackbar.LENGTH_LONG);
//        snackbar.setAction("OK", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                    }
//                });
        snackbar.setActionTextColor(Color.DKGRAY);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
        textView.setGravity(Gravity.CENTER);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
        textView.setCompoundDrawablePadding(0);
//        textView.setMaxHeight(165);

        snackbar.show(); // Don’t forget to show!
        mp.start();

    }

    private boolean validateFields() {

        matcher = pattern.matcher(email.getText());

        if (email.getText().length() < 1 && password.getText().length() < 1) {
            showSnackbar("Please fill all the blank fields");
        }
        else if(user_name.getText().length()<1)
        {
            showSnackbar("Please enter your name");
        }else if (email.getText().length() < 1) {
            showSnackbar("Please enter Email address");
        } else if (!matcher.matches()) {
            showSnackbar("Please enter a valid Email address");
        } else if (password.getText().length() < 1) {
            showSnackbar("Enter New Password minimum 4 letters");
        } else if (password.getText().length() < 4) {
            showSnackbar("Password must 4 characters long");
        } else if (spinner.getSelectedItem().toString().equals("Select state")) {
            showSnackbar("Please select your State");
        } else if (et_district.getText().toString().length()<1) {
            showSnackbar("Please enter your District");
        } else if (referral_code.getText().length() > 0 && referral_code.getText().length() != 10) {
            showSnackbar("Enter correct Referral Id or leave it");
        } else if (!checkBox.isChecked()) {
            showSnackbar("You must accept Terms and Conditions");

        } else {
            return true;
        }
        return false;
    }

    private void signup_volley() {

        final ProgressDialog pdialog = new ProgressDialog(registeration.this);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pdialog.setMessage("Loading...Please wait");
        pdialog.show();

        nameuser=user_name.getText().toString();
        nameuser=nameuser.replace(" ","%20");
        mail = email.getText().toString();
        pass = password.getText().toString();
        state = spinner.getSelectedItem().toString();
        district = et_district.getText().toString();

        url = Constants.url + "registration.php?email=" + mail + "&password=" + pass + "&district=" + district + "&state=" + state + "&code=" + referral_code.getText().toString()+"&name="+nameuser;
        url = URLDecoder.decode(url);
        url = url.replace(" ", "%20");
        Log.e("url", url);
        RequestQueue queue = Volley.newRequestQueue(registeration.this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("response", jsonObject + "");
                try {

                    editor = user_info.edit();

                    final String message = jsonObject.getString("scalar");
                    if (message.equals("-1")) {
                        Log.e("Message", message);
                        showSnackbar("Something went wrong.\n Please try again later.");
//                        Toast.makeText(registeration.this, "Something went wrong.\n Please try again later", Toast.LENGTH_SHORT).show();
                    } else {
                        pdialog.dismiss();
                        final Dialog dialog = new Dialog(registeration.this);
                        dialog.setContentView(R.layout.unique_id_dialog);
                        dialog.setCanceledOnTouchOutside(false);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        dialog.getWindow().setAttributes(lp);
                        TextView unique_id, ok;
                        unique_id = (TextView) dialog.findViewById(R.id.unique_id);
                        ok = (TextView) dialog.findViewById(R.id.dialog_ok);
                        unique_id.setText(message);
                        dialog.show();

                        ok.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                editor.putString("email", email.getText().toString());
                                editor.putString("unique_id", message);
                                editor.putString("password", pass);
                                editor.putString("district", district);
                                editor.putString("state", state);
                                editor.putBoolean("login_bool", true);
                                editor.commit();
                                dialog.dismiss();
                                Log.e("Message", message);
                                Intent in = new Intent(registeration.this, HomeActivity.class);
                                startActivity(in);
                                finish();
                                Log.e("Login successful", "login succesful");
                                Log.e("login_bool", b + "");
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("volley error", volleyError.getMessage() + "");

                String err = "UnknownHostException";
                String vol_err = volleyError.toString();
                if (vol_err.contains(err))
                    showSnackbar("Server Error Please Try After Sometime.");
//                    Toast.makeText(registeration.this, "Server Error Please Try After Sometime", Toast.LENGTH_LONG).show();


                pdialog.dismiss();
            }
        });
        queue.add(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
