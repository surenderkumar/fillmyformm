package job.job.consaltancy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 10/15/2016.
 */
public class Link_Webview extends AppCompatActivity{

    LinearLayout back_layout;
    WebView wv;
    String url;
    TextView header;
    ProgressBar bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_detail);

        back_layout=(LinearLayout)findViewById(R.id.back_layout);
        header=(TextView)findViewById(R.id.tv_heading_id);
        bar=(ProgressBar)findViewById(R.id.progressBar);

        if(getIntent().getStringExtra("link").equalsIgnoreCase("how_to_pay")){
         url=Constants.url+"app_link/how_to_pay.pdf";
            header.setText("How To Pay");
        }
        else if(getIntent().getStringExtra("link").equalsIgnoreCase("how_to_use")){
            url=Constants.url+"app_link/how_to_use.pdf";
            header.setText("How To Use APP");
        }
        else {
            url=Constants.url+"app_link/your_offer.pdf";
            header.setText("Your Offer");
        }

        wv=(WebView)findViewById(R.id.webview_id);
        wv.setWebViewClient(new Callback());
        wv.getSettings().setJavaScriptEnabled(true);
            Log.e("applink",url+" url");

        wv.loadUrl("http://docs.google.com/gview?embedded=true&url="+url);

        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            bar.setVisibility(View.GONE);

        }
    }
}
