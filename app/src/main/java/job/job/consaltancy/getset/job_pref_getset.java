package job.job.consaltancy.getset;

import java.util.ArrayList;

/**
 * Created by surender on 8/6/2016.
 */
public class job_pref_getset {

    int id,fees,ourcharge,paid_wallet;
    String job_id,post_name,last_date,basic,category,address,experience;
    ArrayList<String> preference,exam_center,education;

    public job_pref_getset(int id, int fees, String job_id, String post_name,
                           String last_date,String basic, String category, String address, String experience,
                           ArrayList<String> preference, ArrayList<String> exam_center, ArrayList<String> education,
                           int ourcharge,int paid_wallet) {
        this.id = id;
        this.fees=fees;
        this.job_id = job_id;
        this.post_name = post_name;
        this.last_date = last_date;
        this.basic = basic;
        this.category = category;
        this.address = address;
        this.experience = experience;
        this.preference = preference;
        this.exam_center = exam_center;
        this.education = education;
        this.ourcharge=ourcharge;
        this.paid_wallet=paid_wallet;
    }


    public int getOurcharge() {
        return ourcharge;
    }

    public void setOurcharge(int ourcharge) {
        this.ourcharge = ourcharge;
    }

    public int getPaid_wallet() {
        return paid_wallet;
    }

    public void setPaid_wallet(int paid_wallet) {
        this.paid_wallet = paid_wallet;
    }

    public int getFees() {
        return fees;
    }

    public void setFees(int fees) {
        this.fees = fees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

    public String getBasic() {
        return basic;
    }

    public void setBasic(String basic) {
        this.basic = basic;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public ArrayList<String> getPreference() {
        return preference;
    }

    public void setPreference(ArrayList<String> preference) {
        this.preference = preference;
    }

    public ArrayList<String> getExam_center() {
        return exam_center;
    }

    public void setExam_center(ArrayList<String> exam_center) {
        this.exam_center = exam_center;
    }

    public ArrayList<String> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<String> education) {
        this.education = education;
    }
}
