package job.job.consaltancy.getset;

/**
 * Created by surender on 8/17/2016.
 */
public class other_info_user_getset {

    int id;
    String psid,info,value_type,info_type;

    public other_info_user_getset(int id, String psid, String info, String value_type, String info_type) {
        this.id = id;
        this.psid = psid;
        this.info = info;
        this.value_type = value_type;
        this.info_type = info_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPsid() {
        return psid;
    }

    public void setPsid(String psid) {
        this.psid = psid;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getValue_type() {
        return value_type;
    }

    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public String getInfo_type() {
        return info_type;
    }

    public void setInfo_type(String info_type) {
        this.info_type = info_type;
    }
}
