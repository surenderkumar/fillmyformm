package job.job.consaltancy.getset;

/**
 * Created by Rohan on 2/21/2017.
 */
public class get_slide
{
    int id;
    String image;

    public get_slide(int id, String image) {
        this.id = id;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
