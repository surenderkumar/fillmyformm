package job.job.consaltancy.getset;

/**
 * Created by surender on 8/10/2016.
 */
public class job_processing_getset {

    int id;
    String job_id,post_name,fees,exam_center,preference,company_name,job_type,download_link,fee_receipt,date;

    public job_processing_getset(int id, String job_id, String post_name, String fees, String exam_center, String preference,
                                 String company_name, String job_type, String download_link,String fee_receipt,String date) {
        this.id = id;
        this.job_id = job_id;
        this.post_name = post_name;
        this.fees = fees;
        this.exam_center = exam_center;
        this.preference = preference;
        this.company_name = company_name;
        this.job_type = job_type;
        this.download_link = download_link;
        this.fee_receipt=fee_receipt;
        this.date=date;
    }

    public String getFee_receipt() {
        return fee_receipt;
    }

    public void setFee_receipt(String fee_receipt) {
        this.fee_receipt = fee_receipt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getExam_center() {
        return exam_center;
    }

    public void setExam_center(String exam_center) {
        this.exam_center = exam_center;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getJob_type() {
        return job_type;
    }

    public void setJob_type(String job_type) {
        this.job_type = job_type;
    }

    public String getDownload_link() {
        return download_link;
    }

    public void setDownload_link(String download_link) {
        this.download_link = download_link;
    }
}
