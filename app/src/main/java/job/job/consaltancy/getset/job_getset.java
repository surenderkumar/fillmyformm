package job.job.consaltancy.getset;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by surender on 7/29/2016.
 */
public class job_getset {

    int id;
    String job_type,company_name,detail,notification,hint,center_hint;
    ArrayList<String> post_name=new ArrayList<>();
    String lastdate;
    Boolean expire;
    ArrayList<String> examcenter=new ArrayList<>();
    ArrayList<String> category=new ArrayList<>();
    ArrayList<String> category_fee=new ArrayList<>();
    ArrayList<String> our_charge=new ArrayList<>();
    ArrayList<String> paid_wallet=new ArrayList<>();

    public job_getset(int id, String job_type, String company_name, String detail,
                      String notification, ArrayList<String> our_charge,ArrayList<String> paid_wallet,
                      ArrayList<String> post_name,String lastdate,ArrayList<String> examcenter,Boolean expire,ArrayList<String> category,String hint,String center_hint,ArrayList<String> category_fee) {

        this.id = id;
        this.job_type = job_type;
        this.company_name = company_name;
        this.detail = detail;
        this.notification = notification;
        this.our_charge=our_charge;
        this.paid_wallet=paid_wallet;
        this.post_name=post_name;
        this.lastdate=lastdate;
        this.examcenter=examcenter;
        this.expire=expire;
        this.category=category;
        this.hint=hint;
        this.center_hint=center_hint;
        this.category_fee=category_fee;
   }

    public Boolean getExpire() {
        return expire;
    }

    public void setExpire(Boolean expire) {
        this.expire = expire;
    }

    public ArrayList<String> getExamcenter() {
        return examcenter;
    }

    public void setExamcenter(ArrayList<String> examcenter) {
        this.examcenter = examcenter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getJob_type() {
        return job_type;
    }

    public void setJob_type(String job_type) {
        this.job_type = job_type;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public ArrayList<String> getPost_name() {
        return post_name;
    }

    public void setPost_name(ArrayList<String> post_name) {
        this.post_name = post_name;
    }

    public String getLastdate() {
        return lastdate;
    }

    public void setLastdate(String lastdate) {
        this.lastdate = lastdate;
    }

    public ArrayList<String> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<String> category) {
        this.category = category;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getCenter_hint() {
        return center_hint;
    }

    public void setCenter_hint(String center_hint) {
        this.center_hint = center_hint;
    }

    public ArrayList<String> getCategory_fee() {
        return category_fee;
    }

    public void setCategory_fee(ArrayList<String> category_fee) {
        this.category_fee = category_fee;
    }

    public ArrayList<String> getOur_charge() {
        return our_charge;
    }

    public void setOur_charge(ArrayList<String> our_charge) {
        this.our_charge = our_charge;
    }

    public ArrayList<String> getPaid_wallet() {
        return paid_wallet;
    }

    public void setPaid_wallet(ArrayList<String> paid_wallet) {
        this.paid_wallet = paid_wallet;
    }
}
