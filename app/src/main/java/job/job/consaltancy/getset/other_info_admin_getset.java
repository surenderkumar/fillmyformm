package job.job.consaltancy.getset;

/**
 * Created by surender on 8/17/2016.
 */
public class other_info_admin_getset {

    int id;
    String heading,value_selection,value_type,info_type;

    public other_info_admin_getset(int id, String heading, String value_selection, String value_type, String info_type) {
        this.id = id;
        this.heading = heading;
        this.value_selection = value_selection;
        this.value_type = value_type;
        this.info_type = info_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getValue_selection() {
        return value_selection;
    }

    public void setValue_selection(String value_selection) {
        this.value_selection = value_selection;
    }

    public String getValue_type() {
        return value_type;
    }

    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public String getInfo_type() {
        return info_type;
    }

    public void setInfo_type(String info_type) {
        this.info_type = info_type;
    }
}
