package job.job.consaltancy.getset;

/**
 * Created by surender on 8/10/2016.
 */

public class job_apply_data {

    int postfees,ourchage,pay_wallet,total;
    String post_name;

    public job_apply_data(int postfees, int ourchage, int pay_wallet, int total, String post_name) {
        this.postfees = postfees;
        this.ourchage = ourchage;
        this.pay_wallet = pay_wallet;
        this.total = total;
        this.post_name = post_name;
    }

    public int getPostfees() {
        return postfees;
    }

    public void setPostfees(int postfees) {
        this.postfees = postfees;
    }

    public int getOurchage() {
        return ourchage;
    }

    public void setOurchage(int ourchage) {
        this.ourchage = ourchage;
    }

    public int getPay_wallet() {
        return pay_wallet;
    }

    public void setPay_wallet(int pay_wallet) {
        this.pay_wallet = pay_wallet;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }
}
