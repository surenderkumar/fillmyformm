package job.job.consaltancy;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import job.job.consaltancy.adapter.Exam_center_adapter;
import job.job.consaltancy.utils.Constants;


@SuppressLint("NewApi")
public class Transaction extends Activity{
	
	WebView w;
	LinearLayout main_layout;
	WebSettings set;
	int order_id;
	String url,url1,testname,email;
	SharedPreferences paymentpref,pref;
	Editor paymenteditor;
	int count=0;
	SharedPreferences other_pref;
	SharedPreferences.Editor other_edit;
	int job_id,paid_from_wallet;
	String unique_id,apply_post,apply_fees,center,preference,companyname,jobcategory,category;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.transaction);

		other_pref=getSharedPreferences("other_pref", Context.MODE_PRIVATE);
		main_layout=(LinearLayout)findViewById(R.id.main_layout);
        w = (WebView)findViewById(R.id.webView1);
		w.setWebViewClient(new MyBrowser());

		Intent in=getIntent();
		job_id=in.getIntExtra("job_id",0);
		unique_id=in.getStringExtra("unique_id");
		apply_post=in.getStringExtra("post_name");
		apply_fees=in.getStringExtra("amount");
		center=in.getStringExtra("exam_center");
		preference=in.getStringExtra("preference");
		companyname=in.getStringExtra("company");
		jobcategory=in.getStringExtra("job_type");
		paid_from_wallet=in.getIntExtra("paid_wallet",0);
		category=in.getStringExtra("category");
		Log.e("category",category);

		url=in.getStringExtra("url");
		url1=in.getStringExtra("url1");

		Log.e("transaction_url",url);
		Log.e("transaction_url1",url1);

		Random r = new Random();
		order_id = r.nextInt(999999 - 100001) + 100001;

		set = w.getSettings();
		set.setJavaScriptEnabled(true);

		w.loadUrl(url);

		//applyjob_volley();
	
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		String successurl=w.getUrl();
	    Log.e("successurl",successurl);
	    // http://mechanicallive.com/Payment_Online_Exam/CareerMuskanResponseHandler.php
	    //http://mechanicallive.com/Payment_Online_Exam/CareerMuskanResponseHandler.php

	    if(successurl.equals(Constants.url+"Payment_fillmyform/success.php"))
		    {


				Transaction.this.finish();

		    }
	    
		if ((keyCode == KeyEvent.KEYCODE_BACK) && w.canGoBack()) {
	        w.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

	private class MyBrowser extends WebViewClient {

	        @Override
	        public void onPageStarted(WebView view, String url, Bitmap favicon) {
	        	  Log.e("onPage startedurl",url);
	        	  
	        super.onPageStarted(view, url, favicon);


				if(url.equals(Constants.url+"Payment_fillmyform/success.php"))
				{
					Log.e("count_count111",count+" count1111");
					if(count==0)
					applyjob_volley();
				}
				else
				{
					Log.e("count_count2222",count+" count2222");
				}


			}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);

			if(url.equals(Constants.url+"Payment_fillmyform/success.php"))
			{
				count++;
			}


		}

		@Override
		      public boolean shouldOverrideUrlLoading(WebView view, String url) {
		         view.loadUrl(url);

		         Log.e("should Override url",url);
		         
		         return false;
		      }
	   }

	public void applyjob_volley1()
	{

		final ProgressDialog pd = new ProgressDialog(Transaction.this);
		pd.setCancelable(true);
		pd.setMessage("Payment in Progressing Don't Press Back or Close App");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		RequestQueue queue= Volley.newRequestQueue(Transaction.this);

		url1=url1+"&order_id="+order_id;

		url1=url1.replaceAll(" ","%20");

		Log.e("emailcheckvolleyurl", url1);

		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url1, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {

				pd.dismiss();
				Log.e("count_count",count+" count_count");

				Log.e("loginres", res.toString());

				try {

					JSONObject obj=new JSONObject();
					String resp=res.getString("scalar");
					if(resp.equalsIgnoreCase("Apply Successfully")==true) {
							final Snackbar snackbar = Snackbar.make(main_layout, "Apply Successfully.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
//							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//						Toast.makeText(Transaction.this, "Apply Successfully", Toast.LENGTH_LONG).show();
						thanks_dialog();
					}
					else if(resp.equalsIgnoreCase("Can't Apply")==true)
					{
						final Snackbar snackbar = Snackbar.make(main_layout, " Can't Apply.", Snackbar.LENGTH_INDEFINITE);

						snackbar.setActionTextColor(Color.WHITE);
						snackbar.setDuration(3500);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

						TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
						textView.setTextColor(Color.WHITE);//change Snackbar's text color;
						textView.setGravity(Gravity.CENTER_VERTICAL);
						textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
						textView.setCompoundDrawablePadding(0);

						snackbar.show();
					}
//						Toast.makeText(Transaction.this,"Can't Apply",Toast.LENGTH_LONG).show();

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("exception_apply_job",e.toString());
					pd.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError e) {

				pd.dismiss();
				final Snackbar snackbar = Snackbar.make(main_layout, " Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();
//				Toast.makeText(Transaction.this,"Please check your internet connection", Toast.LENGTH_SHORT).show();
				Log.e("error_apply_job", e.toString());
			}
		});

		queue.add(jsonreq);
	}

	public void applyjob_volley() {

		final ProgressDialog pd = new ProgressDialog(Transaction.this);
		pd.setCancelable(true);
		pd.setMessage("Uploading Please Wait...");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		pd.show();

		RequestQueue queue = Volley.newRequestQueue(Transaction.this);
		String url="";
		url = Constants.url+"apply_job.php?";
		url = url.replace(" ", "%20");
		Log.e("name", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Log.e("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject obj=new JSONObject(s);
						s=obj.getString("scalar");
						if(s.equalsIgnoreCase("Apply Successfully")==true)
						{
							final Snackbar snackbar = Snackbar.make(main_layout, "Apply Successfully.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
//							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//							Toast.makeText(Transaction.this,"Applly Successfully",Toast.LENGTH_LONG).show();
							thanks_dialog();
						}
						else if(s.equalsIgnoreCase("Can't Apply")==true)
						{
							final Snackbar snackbar = Snackbar.make(main_layout, " Somthing is Wrong Can't Upload, Please Try Again.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//							Toast.makeText(Transaction.this,"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
						}

						pd.dismiss();
						Log.e("response_string",s+" response");
					}
				}

				catch (Exception e)
				{
					pd.dismiss();
					Log.e("e","e",e);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {

				Log.e("imagevolley_error",volleyError.toString());
				pd.dismiss();
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("job_id",job_id+"");
				params.put("unique_id",unique_id);
				params.put("post_name",apply_post);
				params.put("amount",apply_fees);
				params.put("exam_center",center);
				params.put("preference",preference);
				params.put("company",companyname);
				params.put("job_type",jobcategory);
				params.put("paid_wallet",paid_from_wallet+"");
				params.put("order_id",order_id+"");
				params.put("category",category);
				return params;
			}
		};

		queue.add(request);
	}

	public void thanks_dialog() {

		final Dialog dialog=new Dialog(Transaction.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.thankyou);
		dialog.setCancelable(false);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.show();

		TextView close=(TextView)dialog.findViewById(R.id.tv_close_id);
		TextView orderid=(TextView)dialog.findViewById(R.id.tv_orderid_id);

		orderid.setText("Now What?\nYour Form Id "+order_id);

		LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

		layout.setMinimumWidth(Constants.Width - 20);

		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Constants.other_count=0;
				Constants.other_adapter_count=0;
				other_edit=other_pref.edit();
				other_edit.clear();
				other_edit.commit();
                Intent in=new Intent(Transaction.this,HomeActivity.class);
                startActivity(in);
				finish();
			}
		});
	}

}
