package job.job.consaltancy;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import consultancy.slider.WelcomeActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import job.job.consaltancy.fragment.Aboutus;
import job.job.consaltancy.fragment.Applied_job_fragment;
import job.job.consaltancy.fragment.MyProfileActivity;
import job.job.consaltancy.fragment.Job_set_notification;
import job.job.consaltancy.fragment.Jobs_in_processing;
import job.job.consaltancy.fragment.Share_and_Earn;
import job.job.consaltancy.fragment.Supports;
import job.job.consaltancy.fragment.Tab_Fragment;
import job.job.consaltancy.utils.Constants;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    String imagepath = "drawable://" + R.drawable.image;
    FrameLayout FrameLayout;
    Toolbar toolbar;
    Tab_Fragment home_frag;
    DrawerLayout drawer;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    CircleImageView userimage;
    //    public static SearchView et_search;
    public static TextView tv_point;
    public static TextView tv_appname;
    SharedPreferences detailprf, user_info;
    SharedPreferences.Editor detailedt;
    SharedPreferences.Editor editor;
    TextView dr_name, dr_email, dr_unique_id, dr_signout, dr_info, dr_activity, dr_resume, dr_sharearn, dr_notification, dr_aboutus;
    TextView dr_termcond, dr_support, dr_processingjob, dr_appliedjob;

    int count_processing_job = 0, count_applied_job = 0, last_ind_proc, last_ind_appl;
    private long backPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setLogo(R.drawable.homelogo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        //getSupportActionBar().setLogo(null);

        detailprf = getSharedPreferences("detailprf", Context.MODE_PRIVATE);
        user_info = getSharedPreferences("user_info", Context.MODE_PRIVATE);

        // et_search=(SearchView) findViewById(R.id.et_searchid);
        tv_appname = (TextView) findViewById(R.id.tv_appnameid);
        tv_point = (TextView) findViewById(R.id.tv_pointid);

        FrameLayout = (FrameLayout) findViewById(R.id.FrameLayout);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Constants.Height = displaymetrics.heightPixels;
        Constants.Width = displaymetrics.widthPixels;

        PushService.setDefaultPushCallback(this, HomeActivity.class);
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        new ParseApplication();
        parseInstallation.saveInBackground();

        Log.e("parse_call", "parse_call");

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        final View headerView = navigationView.inflateHeaderView(R.layout.header_layout);

        userimage = (CircleImageView) headerView.findViewById(R.id.profile);
        dr_name = (TextView) headerView.findViewById(R.id.dr_nameid);
        dr_email = (TextView) headerView.findViewById(R.id.dr_emailid);
        dr_unique_id = (TextView) headerView.findViewById(R.id.dr_uniqueid);
        dr_info = (TextView) headerView.findViewById(R.id.dr_infoid);
        dr_signout = (TextView) headerView.findViewById(R.id.dr_signoutid);
        dr_aboutus = (TextView) headerView.findViewById(R.id.dr_aboutusid);
        dr_activity = (TextView) headerView.findViewById(R.id.dr_activityid);
        dr_resume = (TextView) headerView.findViewById(R.id.dr_resumeid);
        dr_sharearn = (TextView) headerView.findViewById(R.id.dr_share_earnid);
        dr_notification = (TextView) headerView.findViewById(R.id.dr_notificationid);
        dr_support = (TextView) headerView.findViewById(R.id.dr_supportid);
        dr_termcond = (TextView) headerView.findViewById(R.id.dr_term_conditionid);
        dr_processingjob = (TextView) headerView.findViewById(R.id.dr_processingjobid);
        dr_appliedjob = (TextView) headerView.findViewById(R.id.dr_appliedjobid);

        dr_email.setText(user_info.getString("email", ""));
        dr_unique_id.setText(user_info.getString("unique_id", ""));
        Constants.Unique_Id = user_info.getString("unique_id", "");
        last_ind_appl = user_info.getInt("applied_job_lastid", 0);
        last_ind_proc = user_info.getInt("processing_job_lastid", 0);
        dr_name.setText(user_info.getString("name", ""));
        Log.e("username",user_info.getString("name", ""));
        Picasso.with(HomeActivity.this)
                .load(Constants.url+"user_image/img/"+user_info.getString("image", ""))
                .error(R.drawable.logoappnew).placeholder(R.drawable.logoappnew)
                .into(userimage);
//        String st_basicinfo = user_info.getString("basic_info", "");
//        try {
//            JSONObject jobj = new JSONObject(st_basicinfo);
//            dr_name.setText(jobj.getString("c_fname") + " " + jobj.getString("c_mname") + " " + jobj.getString("c_lname"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        volley();

        tv_point.setText("₹ " + String.valueOf(Constants.Wallet_Money));

        Typeface BalooBhaiRegular = Typeface.createFromAsset(getAssets(), "fonts/BalooBhai-Regular.ttf");
        tv_appname.setTypeface(BalooBhaiRegular);

//        if(last_ind_appl>0|last_ind_proc>0)
//            volley();
//        else{
//            home_frag=new Tab_Fragment();
//            getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,home_frag).commit();
//        }


//        Parse.initialize(new Parse.Configuration.Builder(HomeActivity.this)
//                .applicationId("YOUR_APP_ID")
//                .server("http://YOUR_PARSE_SERVER:1337/parse").build());


//        options = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.loading)
//                .showImageForEmptyUri(R.drawable.nullimage)
//                .showImageOnFail(R.drawable.nullimage)
//                .cacheInMemory(false)
//                .cacheOnDisk(true)
//                .considerExifParams(true)
//                .displayer(new CircleBitmapDisplayer(Color.WHITE, 2))
//                .build();
//
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        imageLoader.init(ImageLoaderConfiguration.createDefault(HomeActivity.this));
//        imageLoader.getInstance().displayImage(imagepath, userimage, options, animateFirstListener);


        dr_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Snackbar snackbar = Snackbar.make(drawer, " Realy want to Sign out?", Snackbar.LENGTH_INDEFINITE);

                snackbar.setAction("SIGN OUT", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        doSignOut();
                    }
                });
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.setDuration(3500);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                textView.setGravity(Gravity.CENTER_VERTICAL);
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                textView.setCompoundDrawablePadding(0);

                snackbar.show();

            }
        });

        dr_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Your_info", "Click");
                drawer.closeDrawer(GravityCompat.START);

                Intent in = new Intent(HomeActivity.this, MyProfileActivity.class);
                startActivity(in);

            }
        });

        dr_appliedjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Your_info", "Click");
                drawer.closeDrawer(GravityCompat.START);
                Constants.fragment = 3;
                Applied_job_fragment home_frag = new Applied_job_fragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, home_frag).addToBackStack("home").commit();
            }
        });

        dr_processingjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Your_info", "Click");
                drawer.closeDrawer(GravityCompat.START);
                Constants.fragment = 3;
                Jobs_in_processing home_frag = new Jobs_in_processing();
                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, home_frag).addToBackStack(null).commit();
            }
        });

        dr_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Your_resume", "Click");
                drawer.closeDrawer(GravityCompat.START);

                Intent in = new Intent(HomeActivity.this, consultancy.Term_and_Condition.class);
                in.putExtra("clas", 1);
                startActivity(in);

//               Term_and_Condition home_frag=new Term_and_Condition();
//                Bundle bundle = new Bundle();
//                bundle.putInt("clas", 1);
//                home_frag.setArguments(bundle);
//                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,home_frag).addToBackStack(null).commit();
            }
        });

        dr_sharearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Share and Earn", "Click");
                drawer.closeDrawer(GravityCompat.START);

                Intent in = new Intent(HomeActivity.this, Share_and_Earn.class);
                startActivity(in);

//                Share_and_Earn home_frag=new Share_and_Earn();
//                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,home_frag).addToBackStack(null).commit();

            }
        });

        dr_termcond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Term and Condition", "Click");
                drawer.closeDrawer(GravityCompat.START);

                Intent in = new Intent(HomeActivity.this, consultancy.Term_and_Condition.class);
                in.putExtra("clas", 0);
                startActivity(in);

//                Term_and_Condition home_frag=new Term_and_Condition();
//                Bundle bundle = new Bundle();
//                bundle.putInt("clas", 0);
//                home_frag.setArguments(bundle);
//                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,home_frag).addToBackStack(null).commit();
            }
        });

        dr_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("About Us", "Click");
                drawer.closeDrawer(GravityCompat.START);
                Intent in = new Intent(HomeActivity.this, Aboutus.class);
                startActivity(in);

//                Aboutus home_frag=new Aboutus();
//                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,home_frag).addToBackStack(null).commit();
            }
        });

        dr_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("notification manager", "Click");
                drawer.closeDrawer(GravityCompat.START);
                Constants.fragment = 3;
                Job_set_notification home_frag = new Job_set_notification();
                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, home_frag).addToBackStack(null).commit();
            }
        });

        dr_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("help and support", "Click");
                drawer.closeDrawer(GravityCompat.START);
                Intent in = new Intent(HomeActivity.this, Supports.class);
                startActivity(in);
//                Supports home_frag=new Supports();
//                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,home_frag).addToBackStack(null).commit();
            }
        });

        tv_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //et_search.setVisibility(View.GONE);
                tv_appname.setVisibility(View.VISIBLE);

                Intent in = new Intent(HomeActivity.this, Share_and_Earn.class);
                startActivity(in);

//              Share_and_Earn acc_frag=new Share_and_Earn();
//                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,acc_frag).addToBackStack(null).commit();

            }
        });
    }

    @Override
    public void onBackPressed() {

        if (Constants.fragment < 3){
            if (this.drawer.isDrawerOpen(GravityCompat.START)) {
                this.drawer.closeDrawer(GravityCompat.START);
            } else {
                // to prevent irritating accidental logouts
                long curTime = System.currentTimeMillis();
                if (curTime - backPressedTime > 2500) {    // 2.5 secs
                    backPressedTime = curTime;
                    showSnackbar("Press back again to exit");
                } else {

                    moveTaskToBack(true);
                }
            }
    } else {
            try {
                super.onBackPressed();
            } catch (Exception e) {
                Log.e("Exception",e.getMessage()+" abc");
            }
        }

    }

    private void showSnackbar(String text) {

        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(drawer.getWindowToken(), 0);

        final Snackbar snackbar = Snackbar.make(drawer, "    " + text, Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
        textView.setCompoundDrawablePadding(0);

        snackbar.show(); // Don’t forget to show!

    }

    private void doSignOut() {

        editor = user_info.edit();
        editor.clear();
        editor.commit();
        Intent in = new Intent(HomeActivity.this, WelcomeActivity.class);
        startActivity(in);
        finish();
    }

    public void exit_dialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Do you really want to exit?");

        alertDialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1)

            {

                moveTaskToBack(true);

                // Toast.makeText(Jobsandalert_activity.this,"You clicked yes button",Toast.LENGTH_LONG).show();
            }


        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }


    public void volley() {
        final ProgressDialog dialog = new ProgressDialog(HomeActivity.this);
        dialog.setMessage("Loading.. please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);

        String url = Constants.url + "get_job_notification.php?unique_id=" + Constants.Unique_Id + "&processing_id=" + last_ind_proc + "&applied_id=" + last_ind_appl;
        Log.e("url= ", url);
        url = url.replaceAll(" ", "%20");

        //jobarray=new ArrayList<job_getset>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                try {

                    Log.e("job_homeactivity=", res.toString());

                    if (res.has("processing")) {

                        JSONArray array = res.getJSONArray("processing");
                        int len = array.length();
                        Constants.Process_Job_Noti = len;

                        Log.e("length_processing=", String.valueOf(len));
                    }
                    if (res.has("applied")) {
                        JSONArray array = res.getJSONArray("applied");
                        int len = array.length();
                        Constants.Applied_Job_Noti = len;
                        Log.e("length_applied=", String.valueOf(len));
                    }

                    if (res.has("walletmoney")) {
                        JSONArray array = res.getJSONArray("walletmoney");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            Constants.Wallet_Money = obj.getInt("wallet");
                        }
                        tv_point.setText("₹ " + String.valueOf(Constants.Wallet_Money));
                    }
                    if (res.has("version")) {
                        JSONArray array = res.getJSONArray("version");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            Constants.VersionCode = obj.getInt("versioncode");
                            Constants.VersionName = obj.getString("versionname");
                            Constants.VersionDesc = obj.getString("descriptio");
                        }

                        if (Constants.VersionCode > BuildConfig.VERSION_CODE)
                            if (!Constants.CheckVersion)
                                versiondialog();
                    }

                    home_frag = new Tab_Fragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, home_frag).commit();


                    dialog.dismiss();

                } catch (JSONException e) {
                    Log.e("catch exp= ", e.toString());
                    home_frag = new Tab_Fragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, home_frag).commit();

                    dialog.dismiss();
                    e.printStackTrace();
                }
            }

        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                dialog.dismiss();
                home_frag = new Tab_Fragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, home_frag).commit();

                String error = arg0.toString();
                if (error.contains("NoConnectionError")) {
                    showSnackbar("No Internet Connection found");
//                    Toast.makeText(HomeActivity.this, "No Network Connection", Toast.LENGTH_LONG).show();
                }
//                else
//                    Toast.makeText(HomeActivity.this,"No any job in processing", Toast.LENGTH_LONG).show();

                Log.e("error", arg0.toString());
            }
        });
        queue.add(request);
    }

    public void versiondialog() {


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.versiondialog_layout);
        dialog.show();

        TextView notnow = (TextView) dialog.findViewById(R.id.notnowid);
        TextView update = (TextView) dialog.findViewById(R.id.updateid);
        TextView title = (TextView) dialog.findViewById(R.id.titleid);
        TextView description = (TextView) dialog.findViewById(R.id.descriptionid);
        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(width);

        title.setText("New Version Available");
        description.setText(Constants.VersionDesc);

        Constants.CheckVersion = true;

        notnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String PACKAGE_NAME = getPackageName();
                Intent in = new Intent(Intent.ACTION_VIEW);

                in.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + PACKAGE_NAME));
                startActivity(in);
            }
        });
    }

}
