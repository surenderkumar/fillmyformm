package job.job.consaltancy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import job.job.consaltancy.adapter.Exam_center_adapter;
import job.job.consaltancy.fragment.Address_Info_fragment;
import job.job.consaltancy.fragment.Basic_Info_fragment;
import job.job.consaltancy.fragment.Category_Info_fragment;
import job.job.consaltancy.fragment.Education_Info_fragment;
import job.job.consaltancy.fragment.Experience_Info_fragment;
import job.job.consaltancy.getset.job_apply_data;
import job.job.consaltancy.getset.job_pref_getset;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 8/6/2016.
 */
public class Apply_job_activity extends Fragment implements View.OnClickListener {

    LinearLayout ly_container;
    RelativeLayout main_layout;
    ArrayList<String> center_lt=new ArrayList<>();
    ArrayList<job_apply_data> apply_data;
    ArrayList<String> pref_lt,edu_lt,check_lt,stor_pref;
    ArrayList<ArrayList<String>> stor_pref_array;
    ArrayList<job_pref_getset> data;
    FloatingActionButton submit;
    String st_status="true",clas_nm="",apply_post,apply_fees;
    SharedPreferences detailprf;
    Boolean bool=false;
    EditText et_pref;
    String description="Dear User," +
            " You don’t fill the required details for this Form (as mention in Form Details ).\n" +
            "Please fill the required details";
    String selectcate="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.apply_job_activity, container,false);

        detailprf=getActivity().getSharedPreferences("detailprf", Context.MODE_PRIVATE);

        main_layout=(RelativeLayout)v.findViewById(R.id.main_layout);
        ly_container=(LinearLayout)v.findViewById(R.id.ly_container_id);
        submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);

        volley();

        submit.setOnClickListener(this);

        return v;
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.apply_job_activity);
//
//        job_id=getIntent().getIntExtra("job_id",0);
//
//        detailprf=getSharedPreferences("detailprf", 1);
//
//        ly_container=(LinearLayout)findViewById(R.id.ly_container_id);
//
////        post_lt.add("postone");
////        post_lt.add("postone");
////        post_lt.add("postone");
//
//        volley();
//
//
//    }

    public void setview(){

        if ( data.size()>0){

            submit.setVisibility(View.VISIBLE);
            check_lt=new ArrayList<>();
            stor_pref_array=new ArrayList<>();

            ArrayList<String> clist;
            //ArrayList<String> stor_pref;

            for (int k=0;k<data.size();k++) {
                check_lt.add(k, "false");
            }

            for(int i=0;i<data.size();i++) {

                pref_lt=new ArrayList<>();
                center_lt=new ArrayList<>();
                edu_lt=new ArrayList<>();

                View child = getActivity().getLayoutInflater().inflate(R.layout.apply_job_view, null);

                CheckBox cb_postname=(CheckBox)child.findViewById(R.id.cb_postname_id);
                TextView tv_ourchages=(TextView)child.findViewById(R.id.tv_ourcharge_id);
                TextView tv_paywallet=(TextView)child.findViewById(R.id.tv_paid_wallet_id);
                TextView tv_postfees=(TextView)child.findViewById(R.id.tv_postfees_id);
                TextView tv_total=(TextView)child.findViewById(R.id.tv_total_id);
                Button hint_btn=(Button)child.findViewById(R.id.hint_id);

                //final Spinner sp_examcenter=(Spinner)child.findViewById(R.id.sp_examcenter_id);

                cb_postname.setText(data.get(i).getPost_name());
//                tv_postfees.setText(String.valueOf(data.get(i).getFees()));
                tv_postfees.setText(Constants.fee_cate);
                tv_ourchages.setText(Constants.our_charge);
                tv_paywallet.setText(Constants.paid_wallet);
                tv_total.setText(String.valueOf(Integer.parseInt(Constants.our_charge)+Integer.parseInt(Constants.paid_wallet)+Integer.parseInt(Constants.fee_cate)));
                pref_lt=data.get(i).getPreference();
                center_lt=data.get(i).getExam_center();

                final int finalI = i;

//                sp_examcenter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        examcenter_lt.set(finalI, sp_examcenter.getSelectedItem().toString());
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });

                hint_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (Constants.hint.length() > 1) {
                                Hint_Dialog();
                            }
                        }catch (Exception e)
                        {
                            Log.e("Exception",e.getMessage()+" ");
                        }
                    }
                });

                cb_postname.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {

                            check_lt.set(finalI, "true");

                            if (data.get(finalI).getBasic().equalsIgnoreCase("yes") == true & (!detailprf.getBoolean("basic", false))) {
                                if (!detailprf.getBoolean("basic", false)) {
                                    st_status = "false";
                                    clas_nm = "basic";
                                    checkdetail("Basic",description , "basic");
                                } else
                                    st_status = "true";

                            } else if (data.get(finalI).getCategory().equalsIgnoreCase("yes") == true & (!detailprf.getBoolean("category", false))) {
                                if (!detailprf.getBoolean("category", false)) {
                                    st_status = "false";
                                    clas_nm = "category";
                                    checkdetail("Category", description, "category");
                                } else
                                    st_status = "true";
                            } else if (data.get(finalI).getAddress().equalsIgnoreCase("yes") == true & (!detailprf.getBoolean("address", false))) {
                                if (!detailprf.getBoolean("address", false)) {
                                    st_status = "false";
                                    Log.e("address call", "address call " + detailprf.getBoolean("address", false));
                                    clas_nm = "address";
                                    checkdetail("Address", description, "address");
                                } else
                                    st_status = "true";
                            } else if (data.get(finalI).getAddress().equalsIgnoreCase("yes") == true & (!detailprf.getBoolean("experience", false))) {
                                if (!detailprf.getBoolean("experience", false)) {
                                    st_status = "false";
                                    Log.e("experince call", "experience call");
                                    clas_nm = "experience";
                                    checkdetail("Experience", description, "experience");
                                } else
                                    st_status = "true";
                            }

                            edu_lt = data.get(finalI).getEducation();

                            for (int i = 0; i < edu_lt.size(); i++) {
                                if (edu_lt.get(i).equalsIgnoreCase("metric") == true) {
                                    if (!detailprf.getBoolean("metric", false)) {
                                        st_status = "false";
                                        clas_nm = "education";
                                        checkdetail("Education", description, "education");
                                        break;
                                    }
                                } else if (edu_lt.get(i).equalsIgnoreCase("intermediate") == true) {
                                    if (!detailprf.getBoolean("intermed", false)) {
                                        st_status = "false";
                                        clas_nm = "education";
                                        checkdetail("Education", description, "education");
                                        break;
                                    }
                                } else if (edu_lt.get(i).equalsIgnoreCase("diploma") == true) {
                                    if (!detailprf.getBoolean("diploma", false)) {
                                        st_status = "false";
                                        clas_nm = "education";
                                        checkdetail("Education", description, "education");
                                        break;
                                    }
                                } else if (edu_lt.get(i).equalsIgnoreCase("graduation") == true) {
                                    if (!detailprf.getBoolean("graduation", false)) {
                                        st_status = "false";
                                        clas_nm = "education";
                                        checkdetail("Education", description, "education");
                                        break;
                                    }
                                } else if (edu_lt.get(i).equalsIgnoreCase("postgraduation") == true) {
                                    if (!detailprf.getBoolean("postgraduation", false)) {
                                        st_status = "false";
                                        clas_nm = "education";
                                        checkdetail("Education", description , "education");
                                        break;
                                    }
                                }
                            }

                            Log.e("applydialog call", "applydialog call");
//                        if (st_status.equalsIgnoreCase("true")==true)
//                            //applydialog(p);

                        } else
                            check_lt.set(finalI, "false");
                    }
                });

//                ArrayAdapter<String> proofAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, center_lt);
//                proofAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                sp_examcenter.setAdapter(proofAdapter);

                LinearLayout ly_prefer=(LinearLayout)child.findViewById(R.id.ly_preference_id);

                for(int j=0;j<pref_lt.size();j++) {

                    View vw_pref = getActivity().getLayoutInflater().inflate(R.layout.apply_job_preference_item, null);
                    et_pref = (EditText) vw_pref.findViewById(R.id.et_preference_id);
                    TextView tv_serial = (TextView) vw_pref.findViewById(R.id.tv_serial_id);
                    TextView tv_postname = (TextView) vw_pref.findViewById(R.id.tv_postname_id);
                    et_pref.setText(String.valueOf(j + 1));
                    tv_postname.setText(pref_lt.get(j));
                    tv_serial.setText(j + 1+".");

                    ly_prefer.addView(vw_pref);
                }

                ly_container.addView(child);
            }
        }
    }


    private void Hint_Dialog() {
        final Dialog dialog=new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.hint_dialog);
        TextView hintt=(TextView)dialog.findViewById(R.id.text);
        hintt.setText(Constants.hint);
        TextView Ok=(TextView)dialog.findViewById(R.id.ok);
        Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public void category_dialog() {

        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);

        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView submit=(TextView)dialog.findViewById(R.id.tv_submit_id);
        ListView listView = (ListView)dialog.findViewById(R.id.list_examcenter_id);

        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width - 20);
        layout.setMinimumHeight(Constants.Height);

        //Log.e("examcenter_list",Constants.ExamCenterList.toString()+"  center_list");
        Log.e("categoryy",Constants.CategoryList+"");




        RadioGroup rgp= (RadioGroup)dialog.findViewById(R.id.radio_group);
        RadioGroup.LayoutParams rprms;

        for(int i=0;i<Constants.CategoryList.size();i++){
            RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setText(Constants.CategoryList.get(i));
            radioButton.setId(i);
            rprms= new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            rgp.addView(radioButton, rprms);
        }

//        Category_adapter adap=new Category_adapter(getActivity(),Constants.CategoryList);
//        listView.setAdapter(adap);

        rgp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup rg, int checkedId) {
                for(int i=0; i<rg.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) rg.getChildAt(i);
                    if(btn.getId() == checkedId) {
                        selectcate = (String) btn.getText();
                        Log.e("selectedd",selectcate+","+checkedId);
                        Constants.fee_cate=Constants.CategoryFeeList.get(checkedId);
                        Constants.our_charge=Constants.OurchargesList.get(checkedId);
                        Constants.paid_wallet=Constants.PaidWalletList.get(checkedId);
                        Log.e("selectedcatefee",selectcate+","+Constants.fee_cate+","+Constants.our_charge+","+Constants.paid_wallet);
                        // do something with text
                        return;
                    }
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(selectcate.length()<1)
                {
                    final Snackbar snackbar = Snackbar.make(main_layout, " Please select your category.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
                }
//                    Toast.makeText(getActivity(),"Please select your category.",Toast.LENGTH_LONG).show();
                else {
                    dialog.dismiss();
                    Log.e("select_your_category", selectcate);
                    setview();
                }

            }
        });
    }






    public void examcenter_dialog() {

        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.exam_center_layout);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView heading_id=(TextView)dialog.findViewById(R.id.heading_id);
        TextView submit=(TextView)dialog.findViewById(R.id.tv_submit_id);
        ListView listView = (ListView)dialog.findViewById(R.id.list_examcenter_id);
        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);
        layout.setMinimumWidth(Constants.Width - 20);
        layout.setMinimumHeight(Constants.Height);

        try {
            if (Constants.center_hint.length() > 1) {
                heading_id.setText(Constants.center_hint);
            }
        }catch (Exception e)
        {
            Log.e("Exception",e.getMessage()+"");
        }

        //Log.e("examcenter_list",Constants.ExamCenterList.toString()+"  center_list");

        Exam_center_adapter adap=new Exam_center_adapter(getActivity(),Constants.ExamCenterList);
        listView.setAdapter(adap);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Exam_center_adapter.selectcenter.size()<1)
                {
                    final Snackbar snackbar = Snackbar.make(main_layout, " Please select minimum One center.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);
                    snackbar.show();
                }
//                    Toast.makeText(getActivity(),"Please select minimum One center",Toast.LENGTH_LONG).show();
                else {
                    dialog.dismiss();
                    Log.e("select_exam_center", Exam_center_adapter.selectcenter.toString());
                    applydialog();
                }

            }
        });
    }

    public void applydialog() {

        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.apply_now_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        int tot_fs=0,tot_ourcharge=0,tot_wallet=0,total=0;
        TextView tv_head=(TextView)dialog.findViewById(R.id.tv_heading_id);
        TextView notnow=(TextView)dialog.findViewById(R.id.not_nowid);
        TextView apply=(TextView)dialog.findViewById(R.id.apply_id);
        TextView fees=(TextView)dialog.findViewById(R.id.tv_fees_id);
        TextView our_charge=(TextView)dialog.findViewById(R.id.tv_ourcharge_id);
        TextView paid_wallet=(TextView)dialog.findViewById(R.id.tv_paid_wallet_id);
        TextView total_fees=(TextView)dialog.findViewById(R.id.tv_total_fees_id);
         apply_post="";

        for(int i=0;i<apply_data.size();i++){
            tot_fs=tot_fs+apply_data.get(i).getPostfees();
            tot_ourcharge=tot_ourcharge+apply_data.get(i).getOurchage();
            tot_wallet=tot_wallet+apply_data.get(i).getPay_wallet();
            total=total+apply_data.get(i).getTotal();

            if(i==(apply_data.size()-1))
                apply_post=apply_post+apply_data.get(i).getPost_name();
                else
                apply_post=apply_post+apply_data.get(i).getPost_name()+",";
        }


        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width - 20);

        //tv_head.setText("You are applying for the Post "+data.get(p).getPost_name()+" in "+data.get(p).getCompany_name());


        if(Constants.Wallet_Money>=tot_wallet) {

            total=tot_ourcharge+tot_fs;
            total=total-tot_wallet;

            our_charge.setText("Rs. " + String.valueOf(tot_ourcharge));
            paid_wallet.setText("Rs. " + String.valueOf(tot_wallet));
            fees.setText("Rs. " + String.valueOf(tot_fs));
            total_fees.setText("Rs. " + String.valueOf(total));

            Constants.Paid_from_Wallet=tot_wallet;
        }else if(Constants.Wallet_Money==0){

          //  int w=tot_wallet-Constants.Wallet_Money;
           // tot_ourcharge=tot_ourcharge+w;

            total=tot_ourcharge+tot_fs;
           // total=total-Constants.Wallet_Money;


            our_charge.setText("Rs. "+String.valueOf(tot_ourcharge));
            paid_wallet.setText("Rs. " + String.valueOf(Constants.Wallet_Money));
            fees.setText("Rs. " + String.valueOf(tot_fs));
            total_fees.setText("Rs. " + String.valueOf(total));

            Constants.Paid_from_Wallet=Constants.Wallet_Money;
        }
        else {
            int w=tot_wallet-Constants.Wallet_Money;
            tot_ourcharge=tot_ourcharge+w;

            total=tot_ourcharge+tot_fs;
            total=total-Constants.Wallet_Money;


            our_charge.setText("Rs. "+String.valueOf(tot_ourcharge));
            paid_wallet.setText("Rs. " + String.valueOf(Constants.Wallet_Money));
            fees.setText("Rs. " + String.valueOf(tot_fs));
            total_fees.setText("Rs. " + String.valueOf(total));

            Constants.Paid_from_Wallet=Constants.Wallet_Money;
        }

        apply_fees=String.valueOf(total);

        notnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //applyjob_volley();

                String pre="", cen="";
                for(int i=0;i<Exam_center_adapter.selectcenter.size();i++)
                {
                    cen=cen+Exam_center_adapter.selectcenter.get(i)+",";
                }
                for(int j=0;j<stor_pref_array.size();j++) {

                    stor_pref=stor_pref_array.get(j);

                    for (int i = 0; i < stor_pref.size(); i++) {
                        pre = pre + stor_pref.get(i) + ",";

                    }
//                    demo_pref.add(pre);
                    Log.e("demo_pref"," aaaa"+pre);
                }

//                apply_post=apply_post.replace(" ","%20");
//                pre=pre.replace(" ","%20");
//                cen=cen.replace(" ","%20");
               String url= Constants.url+"Payment_fillmyform/CareerMuskandataForm.php?job_id="+
                       Constants.Job_id+"&unique_id="+Constants.Unique_Id+"&post_name="+apply_post+
                        "&amount="+apply_fees+"&exam_center="+cen+"&preference="+pre+
                        "&company="+Constants.Company_Name+"&job_type="+Constants.Job_Category;

                String url1=Constants.url+"apply_job.php?job_id="+
                        Constants.Job_id+"&unique_id="+Constants.Unique_Id+"&post_name="+apply_post+
                        "&amount="+apply_fees+"&exam_center="+cen+"&preference="+pre+
                        "&company="+Constants.Company_Name+"&job_type="+Constants.Job_Category+"&paid_wallet="+Constants.Paid_from_Wallet+"&category="+selectcate;

                url = url.replaceAll(" ", "%20");

                Log.e("urll",url+"");
                Intent in =new Intent(getActivity(),Transaction.class);
                in.putExtra("url",url);
                in.putExtra("url1",url1);
                in.putExtra("job_id",Constants.Job_id);
                in.putExtra("unique_id",Constants.Unique_Id);
                in.putExtra("post_name",apply_post);
                in.putExtra("amount",apply_fees);
                in.putExtra("exam_center",cen);
                in.putExtra("preference",pre);
                in.putExtra("company",Constants.Company_Name);
                in.putExtra("job_type",Constants.Job_Category);
                in.putExtra("paid_wallet",Constants.Paid_from_Wallet);
                in.putExtra("category",selectcate);
                getActivity().startActivity(in);
            }
        });
    }

    public void thanks_dialog() {

        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.thankyou);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView close=(TextView)dialog.findViewById(R.id.tv_close_id);

        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width - 20);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent in=new Intent(getActivity(),UIActivity.class);
//                startActivity(in);
                getActivity().finish();
            }
        });
    }

    public void checkdetail(String head,String des, final String cls)
    {

        final Dialog dialog=new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_detail);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView notnow=(TextView)dialog.findViewById(R.id.not_nowid);
        TextView apply=(TextView)dialog.findViewById(R.id.apply_id);

        TextView tv_heading=(TextView)dialog.findViewById(R.id.tv_heading_id);
        TextView tv_desc=(TextView)dialog.findViewById(R.id.tv_description_id);

        tv_heading.setText(head);
        tv_desc.setText(des);

        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width-20);

        notnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cls.equalsIgnoreCase("basic")==true) {
                    Basic_Info_fragment basic_frag = new Basic_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("category")==true) {
                    Category_Info_fragment basic_frag = new Category_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("address")==true) {
                    Address_Info_fragment basic_frag = new Address_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("experience")==true) {
                    Experience_Info_fragment basic_frag = new Experience_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("education")==true) {
                    Education_Info_fragment basic_frag = new Education_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }

                dialog.dismiss();

            }
        });
    }

    public void volley() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue= Volley.newRequestQueue(getActivity());

        String url= Constants.url+"get_job_preference.php?job_id="+Constants.Job_id;
        Log.e("url= ", url);

        data=new ArrayList<job_pref_getset>();

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST,url, null,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res)
            {

                try {
                    JSONArray array=res.getJSONArray("job");

                    int len=array.length();

                    Log.e("length=", String.valueOf(len));

                    for(int i=0;i<array.length();i++)
                    {
                        JSONObject obj=array.getJSONObject(i);
                        ArrayList<String> pref=new ArrayList<>();
                        ArrayList<String> examcenter=new ArrayList<>();
                        ArrayList<String> education=new ArrayList<>();

                        String bc=obj.getString("preference");
                        if(bc.contains(","))
                        {
                            String[] bcarr=bc.split(",");
                            for(int j=0;j<bcarr.length;j++)
                                pref.add(bcarr[j]);
                        }
                        else
                            pref.add(bc);

                        String ec=obj.getString("exam_center");
                        if(ec.contains(",")) {
                            String[] prarr=ec.split(",");
                            for(int j=0;j<prarr.length;j++)
                                examcenter.add(prarr[j]);
                        }
                        else
                            examcenter.add(ec);

                        String edc=obj.getString("education");
                        if(edc.contains(",")) {
                            String[] prarr=edc.split(",");
                            for(int j=0;j<prarr.length;j++)
                                education.add(prarr[j]);
                        }
                        else
                            education.add(edc);


                        int fes = 0;
                        if(Constants.Gender.equalsIgnoreCase("Female")&obj.getInt("female_fees")>=0){
                            fes=obj.getInt("female_fees");
                        }
                        else {
                            if (Constants.Category.equalsIgnoreCase("ST")) {
                                fes = obj.getInt("st_fees");
                            } else if (Constants.Category.equalsIgnoreCase("SC")) {
                                fes = obj.getInt("sc_fees");
                            } else if (Constants.Category.equalsIgnoreCase("OBC")) {
                                fes = obj.getInt("obc_fees");
                            } else if (Constants.Category.equalsIgnoreCase("General")) {
                                fes = obj.getInt("gen_fees");
                            }
                        }

                        data.add(new job_pref_getset(obj.getInt("id"),fes,obj.getString("job_id"),obj.getString("post_name")," ",
                                obj.getString("basic"),obj.getString("category"),obj.getString("address"),obj.getString("experience"),
                                pref,examcenter,education,obj.getInt("ourcharge"),obj.getInt("paid_wallet")));

                        Log.e("job_type=", obj.getString("post_name")+" fees "+fes);
                    }

                    category_dialog();

//                    setview();
//                    adapter=new Job_home_adapter(getActivity(),jobarray);
//                    recyclerView.setAdapter(adapter);

                    dialog.dismiss();

                } catch (JSONException e) {
                    Log.e("catch exp= ", e.toString());
                    dialog.dismiss();
                    e.printStackTrace();
                }

            }

        }
                ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                dialog.dismiss();

                String error=arg0.toString();
                if (error.contains("NoConnectionError")) {
                    final Snackbar snackbar = Snackbar.make(main_layout, " No Connection Error.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
//                    Toast.makeText(getActivity(), "NoConnectionError", Toast.LENGTH_LONG).show();
                }
                else
                {
                    final Snackbar snackbar = Snackbar.make(main_layout, " Can't data, We Update Soon.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
                }
//                    Toast.makeText(getActivity(), "Can't data, We Update Soon", Toast.LENGTH_LONG).show();

                Log.e("error", arg0.toString());
            }
        });

        queue.add(request);
    }

    public void applyjob_volley()
    {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(true);
        pd.setMessage("Loading Please Wait");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        pd.show();

        RequestQueue queue= Volley.newRequestQueue(getActivity());
        String url;


        url= Constants.url+"apply_job.php?job_id="+Constants.Job_id+"&unique_id="+Constants.Unique_Id+"&post_name="+apply_post+
                          "&fees="+apply_fees+"&exam_center="+Exam_center_adapter.selectcenter+"&preference="+stor_pref_array+
                          "&company="+Constants.Company_Name+"&job_type="+Constants.Job_Category;


        url = url.replace(" ", "%20");

        //com.citrus.sample.Constants.BILL_URL=url;

        Log.e("emailcheckvolleyurl", url);
        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                pd.dismiss();

                Log.e("loginres", res.toString());

                try {

                    thanks_dialog();

                   JSONObject obj=new JSONObject();
                    String resp=res.getString("scalar");
                    if(resp.equalsIgnoreCase("Apply Successfully")==true)
                    {
                        final Snackbar snackbar = Snackbar.make(main_layout, " Apply Successfully.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
//                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                    }
//                        Toast.makeText(getActivity(),"Apply Successfully",Toast.LENGTH_LONG).show();
                    else if(resp.equalsIgnoreCase("Can't Apply")==true)
                    {
                        final Snackbar snackbar = Snackbar.make(main_layout, " Can't Apply.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                    }
//                        Toast.makeText(getActivity(),"Can't Apply",Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("exception_apply_job",e.toString());
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {

                pd.dismiss();
                    final Snackbar snackbar = Snackbar.make(main_layout, " Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
//                Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
                Log.e("error_apply_job", e.toString());
            }
        });
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.submitbtn){
            apply_data=new ArrayList<>();
            int count=ly_container.getChildCount();

           for(int i=0;i<count;i++)
           {
               stor_pref=new ArrayList<>();

               View row = ly_container.getChildAt(i);
               CheckBox cb=(CheckBox)row.findViewById(R.id.cb_postname_id);
               LinearLayout lay=(LinearLayout)row.findViewById(R.id.ly_preference_id);
               TextView tv_fees=(TextView)row.findViewById(R.id.tv_postfees_id);
               TextView tv_ourchages=(TextView)row.findViewById(R.id.tv_ourcharge_id);
               TextView tv_paywallet=(TextView)row.findViewById(R.id.tv_paid_wallet_id);
               TextView tv_total=(TextView)row.findViewById(R.id.tv_total_id);

               int ct=lay.getChildCount();
               for(int j=0;j<ct;j++)
               {
                   View subrow = lay.getChildAt(j);
                   EditText et_pr=(EditText)subrow.findViewById(R.id.et_preference_id);
                   TextView tv_pr=(TextView)subrow.findViewById(R.id.tv_postname_id);
                   try {
                       if(et_pr.getText().toString().length()>0)
                       stor_pref.add(et_pr.getText().toString() + " " + tv_pr.getText().toString());
                   }catch (Exception e)
                   {
                       Log.e("Exception",e.getMessage()+"");
                   }
               }

               if(cb.isChecked()) {
                   bool=true;
                   apply_data.add(new job_apply_data(Integer.parseInt(tv_fees.getText().toString()),Integer.parseInt(tv_ourchages.getText().toString()),
                                   Integer.parseInt(tv_paywallet.getText().toString()),Integer.parseInt(tv_total.getText().toString()),cb.getText().toString()));
                   stor_pref_array.add(stor_pref);
                   Log.e("size store",stor_pref_array.size()+"abc");
                   Log.e("stor_pref_list", stor_pref_array.get(i).toString() + " stor_pref");
               }
           }

            if(bool) {

                    examcenter_dialog();

            }
            else
            {
                final Snackbar snackbar = Snackbar.make(main_layout, " Please Select Atleast One Job.", Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                snackbar.setDuration(3500);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                textView.setGravity(Gravity.CENTER_VERTICAL);
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                textView.setCompoundDrawablePadding(0);
                snackbar.show();
            }
//                Toast.makeText(getActivity(),"Please Select Atleast One Job",Toast.LENGTH_LONG).show();
        }
    }
}
