package job.job.consaltancy.utils;

import java.util.ArrayList;

/**
 * Created by surender on 6/9/2016.
 */
public class Constants {

    public static int Width=0;
    public static int Height=0;
    public static int VersionCode=0;
    public static String VersionName=" ";
    public static String VersionDesc="";
    public static Boolean CheckVersion=false;
    public static String url="http://inkit.online/webservices/";

    public static String Job_Query="";

    public static String fee_cate="";
    public static String our_charge="";
    public static String paid_wallet="";

    public static int other_count=0;
    public static int other_adapter_count=0;

    public static String Fee_Url="";
    public static String Apply_Url="";
    public static String Unique_Id="";
    public static String Email_Id="";
    public static String District="";
    public static String State="";

    public static String Job_Detail="";
    public static String Category="";
    public static String Gender="";

    public static int Job_id=0;
    public static String Company_Name="";
    public static String Job_Category="";
    public static int fragment=0;

    public static String hint="";
    public static String center_hint="";

    public static int Wallet_Money=0;
    public static int Paid_from_Wallet=0;


    public static String userimage="";
    public static String username="";
    public static String Value="";
    public static String Value_Type="";
    public static String Info_Type="";
    public static String PSID="";
    public static String Value_Selection="";
    public static String Heading="";

    public static String About_US="<p><strong>Welcome</strong></p>" +
            "<p>&nbsp;</p>" +
            "<p><strong>&nbsp;</strong><strong>Fill My Form (By SheoSoft Pvt. Ltd.)</strong></p>" +
            "<p><strong>&nbsp;</strong></p>" +
            "<p>We are the team of professional. Our team is a <strong>multilingual team which have a lot of experience in different professional areas so </strong>we consider the high standard of service and the professional team. <strong>There is no matter too small, big, stressful or difficult for our team. &nbsp;</strong></p>\n" +
            "<p><strong>At this time our team working on &ldquo;fill MY form&rdquo; an Android APP. This is the India&rsquo;s First Android APP in which user can request for Apply their &lsquo;FORM&rsquo; from home (give permission to APP to do work on the behalf of them). &ldquo;fill MY form&rdquo; team give their service just within 24 to 48 hr(maximum).</strong></p>\n" +
            "<p><strong>&nbsp; </strong></p>\n" +
            "<p><strong>DON&rsquo;T STRESS!! WE ARE HERE TO HELP.</strong></p>\n" +
            "<p><strong>The services we provide are:-</strong></p>\n" +
            "<p><strong>&ldquo;fill MY form&rdquo; APP Team fill the form on the behalf of APP user. These are the following form for which APP Team give services to their user are:-</strong></p>\n" +
            "<ol>\n" +
            "<li><strong>UPSC Jobs FORM:-</strong></li>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<li>Civil Services Examination</li>\n" +
            "<li>Engineering Services Examination(IES)</li>\n" +
            "<li>Combined Defence Services Examination(CDSE)</li>\n" +
            "<li>National Defence Academy Examination(NDA)</li>\n" +
            "<li>Naval Academy Examination</li>\n" +
            "<li>Combined Medical Services Examination</li>\n" +
            "<li>Special Class Railway Apprentice(SCRA)</li>\n" +
            "<li>Indian Economic Service/Indian Statistical Service Examination</li>\n" +
            "<li>Geologists&rsquo; Examination(GE)</li>\n" +
            "<li>Central Armed Police Forces(AC)</li>\n" +
            "</ul>\n" +
            "<p><strong>&nbsp;</strong></p>\n" +
            "<ol start=\"2\">\n" +
            "<li><strong>SSC Jobs FORM:-</strong></li>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<li>Delhi Subordinate Services Selection Board DSSSB</li>\n" +
            "<li>Special Class Railway Apprentices SCRA Examination</li>\n" +
            "<li>SSC Central Police Organizations CPO Exam</li>\n" +
            "<li>SSC Combined Graduate Level Examination</li>\n" +
            "<li>SSC Combined Matric Level Examination</li>\n" +
            "<li>SSC Data Entry Operator DEO Exam</li>\n" +
            "<li>SSC Junior Engineers Examination</li>\n" +
            "<li>SSC Junior Translator CSOLS Examination</li>\n" +
            "<li>SSC Tax Assistant Examination</li>\n" +
            "<li>Staff Selection Commission (SSC) Section Officer Audit Examination</li>\n" +
            "<li>Staff Selection Commission SSC Section Officer Commercial Audit Examination</li>\n" +
            "<li>Statistical Investigator Examination</li>\n" +
            "</ul>\n" +
            "<ol start=\"3\">\n" +
            "<li><strong> Banking Jobs FORM:-</strong></li>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<li>IBPS-PO/Clerk</li>\n" +
            "<li>SBI-PO/Clerk</li>\n" +
            "<li>RRB-PO/Clerk</li>\n" +
            "<li>SO and other exams</li>\n" +
            "</ul>\n" +
            "<p>&nbsp;</p>\n" +
            "<p><strong>Other Jobs FORM:-</strong></p>\n" +
            "<p>&nbsp;</p>\n" +
            "<table>\n" +
            "<tbody>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>4&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Railway Job </strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>5&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Engineering Jobs </strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>6&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PSUs Jobs </strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>7&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State Govt. Jobs</strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>8&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Teaching Job</strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>9&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Research &amp; &nbsp;Development Jobs</strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>IT Sector Jobs</strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>11&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Diploma Jobs</strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Police Jobs</strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>13&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Medical and Health Jobs </strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>14&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Defence Jobs</strong></p>\n" +
            "<p><strong>&nbsp;</strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>15&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Army Jobs</strong></p>\n" +
            "<p><strong>&nbsp;</strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td width=\"293\">\n" +
            "<p><strong>16&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Navy Jobs</strong></p>\n" +
            "</td>\n" +
            "<td width=\"298\">\n" +
            "<p><strong>17&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Other Govt. Jobs</strong></p>\n" +
            "</td>\n" +
            "</tr>\n" +
            "</tbody>\n" +
            "</table>\n" +
            "<p><strong>&nbsp;</strong></p>";

    public static String Term_and_Cond="<p><strong><span style=\"text-decoration: underline;\">TERMS</span></strong><span style=\"text-decoration: underline;\">&nbsp;</span><strong><span style=\"text-decoration: underline;\">AND CONDITIONS OF USE</span></strong></p>\n" +
            "<p><strong>1 About our terms and conditions</strong></p>" +
            "<ul>" +
            "<li>These terms of use explain how you may use this mobile APP.</li>\n" +
            "<li>References in these terms to the mobile APP include the following websites: www.fillmyformapp.com, fill MY form APP, all associated web and social media pages.</li>\n" +
            "<li>You should read these terms and conditions carefully before using the mobile APP.</li>\n" +
            "<li>By accessing or using mobile APP or otherwise indicating your consent, you agree to be bound by these terms and conditions and the documents referred to in them.</li>\n" +
            "<li>If you do not agree with or accept any of these terms, you should stop using this mobile APP immediately.</li>\n" +
            "<li>If you have any questions about this mobile APP, please contact us by:</li>\n" +
            "</ul>\n" +
            "<ol>\n" +
            "<li>fillmyformapp@gmail.com (Emails are normally responded to during Mon-San and the hours of 9am-5pm), or</li>\n" +
            "<li>Telephone 07015889866(Telephone calls are normally responded to during Mon-Sat and the hours of 9am-5pm). We may record calls for quality and training purposes.</li>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<li>Definitions<br /> Content means any text, images, video, audio or other multimedia content, software or other information or material submitted to or on the Site; Unwanted Submission has the meaning given to it in clause below;</li>\n" +
            "<li>This mobile APP acceptable use policy means the policy (see below) which governs your permitted use of this mobile APP.</li>\n" +
            "<li>This mobile APP cookie policy means the policy (see below), which governs how we use cookies in this mobile APP.</li>\n" +
            "<li>This mobile APP privacy policy means the policy (see below), which governs how we process any personal data collected from you.</li>\n" +
            "<li>We, us or our means, fill MY form - Mobile APP, www.fillmyformapp.com,SheoSoft Pvt. Ltd.<br /> You or your means the person accessing or using the Site or its Content.<br /> </li>\n" +
            "<li><strong>Regulatory Matters: The fill my forms APP team member(s) who are Qualified Worker. SheoSoft Pvt. Ltd. itself is Registered and Regulated by the individual (Directors) Qualified Worker(s).</strong></li>\n" +
            "</ul>\n" +
            "<p><strong>&nbsp;</strong></p>\n" +
            "<p><strong>&nbsp;</strong></p>\n" +
            "<p><strong>2&nbsp;&nbsp;&nbsp; Who is user and Membership Eligibility</strong></p>\n" +
            "<ul>\n" +
            "<li>Use of the mobile APP is available only to persons who can form legally binding&nbsp;contracts under Indian Contract Act, 1872. Persons who are \"incompetent to contract\" within the meaning of the Indian Contract Act, 1872 including minors,&nbsp;un-discharged insolvents etc. are not eligible to use the mobile APP. If you are&nbsp;a minor i.e. under the age of 18 years, you shall not register as a User of this mobile APP and shall not transact on or use the mobile APP. As a minor if you wish to use or transact on mobile APP, such use or transaction may be made by your&nbsp;legal guardian or parents on the mobile APP. The mobile APP reserves the right to terminate&nbsp;your membership and / or refuse to provide you with access to the mobile APP if it is&nbsp;brought to ours notice or if it is discovered that you are under the age of 18&nbsp;years.</li>\n" +
            "</ul>\n" +
            "<p><strong>&nbsp;</strong></p>\n" +
            "<h1>3&nbsp;&nbsp; Your Account and Registration Obligations</h1>\n" +
            "<p>&nbsp;</p>\n" +
            "<ul>\n" +
            "<li>If You use the mobile APP, You shall be responsible for maintaining the&nbsp;confidentiality of your Display Name and Password and You shall be responsiblefor all activities that occur under your Display Name and Password. You agree&nbsp;that if You provide any information that is untrue, inaccurate, not current or&nbsp;incomplete or We have reasonable grounds to suspect that such information is&nbsp;untrue, inaccurate, not current or incomplete, or not in accordance with the this&nbsp;Terms of Use, We shall have the right to indefinitely suspend or terminate or&nbsp;block access of your membership on the mobile APP and refuse to provide You with&nbsp;access to the mobile APP.</li>\n" +
            "</ul>\n" +
            "<h1>4&nbsp;&nbsp;&nbsp; Charges</h1>\n" +
            "<p>&nbsp;</p>\n" +
            "<ul>\n" +
            "<li>Membership on the mobile APP is free for buyers SheoSoft Pvt. Ltd. does not charge any fee for browsing and buying on the Website. SheoSoft Pvt. Ltd. reserves the right to change its Fee Policy from time to time. In particular, SheoSoft Pvt. Ltd. may at its sole discretion introduce new services and modify some or all of the existing services offered on the mobile APP. In such an event SheoSoft Pvt. Ltd. reserves the right to introduce fees for the new services offered or amend/introduce fees for existing services, as the case may be. Changes to the Fee Policy shall be posted on the mobile APP and such changes shall automatically become effective immediately after they are posted on the mobile APP. Unless otherwise stated, all fees shall be quoted in Indian Rupees. You shall be solely responsible for compliance of all applicable laws including those in India for making payments SheoSoft Pvt. Ltd.</li>\n" +
            "</ul>\n" +
            "<p>&nbsp;</p>\n" +
            "<p><strong>5&nbsp;&nbsp;&nbsp; Payment</strong></p>\n" +
            "<ul>\n" +
            "<li>While availing any of the payment method/s available on the mobile APP, we will&nbsp;not be responsible or assume any liability, whatsoever in respect of any loss arising directly or indirectly to You due to following:</li>\n" +
            "</ul>\n" +
            "<p>&nbsp;</p>\n" +
            "<p>&nbsp;</p>\n" +
            "<ol>\n" +
            "<li>Lack of authorization for any transaction/s, or</li>\n" +
            "<li>Exceeding the preset limit mutually agreed by You and between\"Bank/s\", or</li>\n" +
            "<li>Any payment issues arising out of the transaction, or</li>\n" +
            "<li>Decline of transaction for any other reason/s</li>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<li>All payments made against the purchases/services on mobile APP by you shall be&nbsp;compulsorily in Indian Rupees acceptable in the Republic of India. Website will not facilitate transaction with respect to any other form of currency with respect&nbsp;to the purchases made on Website..</li>\n" +
            "</ul>\n" +
            "<p>&nbsp;</p>\n" +
            "<p><strong>6&nbsp;&nbsp; Refund Policy</strong></p>\n" +
            "<ul>\n" +
            "<li>Facility of request for Refund of payment is available for user but company have right to reject your request for any service and refund your payment if company release any incorrect or incomplete information is given by you , or due to any emergency condition(s).&nbsp; For electronics payments, refund shall be made through payment facility&nbsp;using NEFT / RTGS or any other online banking / electronic funds transfer&nbsp;system approved by Reserve Bank India (RBI).</li>\n" +
            "</ul>\n" +
            "<p>&nbsp;</p>\n" +
            "<p>&nbsp;</p>\n" +
            "<p><strong>7&nbsp; Using </strong><strong>this mobile APP</strong></p>\n" +
            "<ul>\n" +
            "<li>This mobile APP is for your personal and non-commercial use only.</li>\n" +
            "<li>Your use of this mobile APP means that you must also comply with our mobile APP acceptable use policy, our Privacy policy and our Cookie policy, where applicable.</li>\n" +
            "<li>You agree that you are solely responsible for:</li>\n" +
            "</ul>\n" +
            "<ol>\n" +
            "<li>All costs and expenses you may incur in relation to your use this mobile APP and</li>\n" +
            "<li>Keeping, where applicable, your password and other account details confidential.</li>\n" +
            "<li>When you make/do a request of Job Form Apply or Exam Form Apply and all other Form Apply to &ldquo;fill MY form APP&rdquo;, you give a permission to &ldquo;fill MY form&rdquo; APP Team . By this permission APP Team will do work on the behalf of you and fill your requested FORM.</li>\n" +
            "<li>Request for the Service(s) on the last date is only and only responsibility of user(s) (this type of request is on 100% user(s) risk. Company is not obligate or responsible to give the service for this type of Request(s).User(s) cannot do any claim on company for this type of request(s).</li>\n" +
            "</ol>\n" +
            "<p>&nbsp;</p>\n" +
            "<ul>\n" +
            "<li>This mobile APP is intended for use only by those who can access it from within the INDIA. If you choose to access this mobile APP from locations outside the INDIA, you are responsible for compliance with local laws where they are applicable.</li>\n" +
            "<li>We seek to make the mobile APP as accessible as possible. If you have any difficulties using the mobile APP, please contact us at fillmyformapp@gmail.com and/or use the contact us forms on our mobile APP<br /> </li>\n" +
            "<li>We may prevent or suspend your access to this mobile APP if you do not comply with any part of these mobile APP terms and conditions, any terms or policies to which they refer or any applicable law.</li>\n" +
            "</ul>\n" +
            "<p><strong>8<strong>&nbsp;&nbsp; Ownership, use and intellectual property rights:</strong></strong></p>\n" +
            "<ul>\n" +
            "<li>This mobile APP and all intellectual property rights in it including but not limited to any Content are owned by us. Intellectual property rights means rights such as: copyright, trademarks, domain names, design rights, database rights, patents and all other intellectual property rights of any kind whether or not they are registered or unregistered (anywhere in the world). We reserve all of our rights in any intellectual property in connection with these terms and conditions. This means, for example, that we remain owners of them and free to use them as we see fit.</li>\n" +
            "<li>Nothing in these terms and conditions grants you any legal rights in the mobile APP other than as necessary to enable you to access the mobile APP. You agree not to adjust to try to circumvent or delete any notices contained on the mobile APP (including any intellectual property notices) and in particular in any digital rights or other security technology embedded or contained within the mobile APP.<br /> </li>\n" +
            "</ul>\n" +
            "<p><strong>9&nbsp;&nbsp; Submitting information to the Site</strong></p>\n" +
            "<ul>\n" +
            "<li>While we try to make sure that the mobile APP is secure, we cannot guarantee the security of any information that you supply to us and therefore we cannot guarantee that it will be kept confidential. For that reason, you should not let us have any patentable ideas or patent applications, advertising or marketing suggestions, prototypes, or any other information that you regard as confidential, commercially sensitive or valuable (Unwanted Submissions). While we value your feedback, you agree not to submit any Unwanted Submissions.</li>\n" +
            "<li>We may use any Unwanted Submissions as we see reasonably fit on a free-of-charge basis (bear in mind that we have no way of knowing whether such information is confidential, commercially sensitive or valuable because we do not monitor the mobile APP to check for these matters). Therefore, we will not be legally responsible for keeping any Unwanted Submissions confidential nor will we be legally responsible to you or anybody else for any use of such Unwanted Submissions.<br /> </li>\n" +
            "</ul>\n" +
            "<p><strong>10&nbsp; &nbsp;Accuracy of information and availability of the Site</strong></p>\n" +
            "<ul>\n" +
            "<li>While we try to make sure that the mobile APP is accurate, up-to-date and free from bugs, we cannot promise that it will be. Furthermore, we cannot promise that the mobile APP will be fit or suitable for any purpose. Any reliance that you may place on the information on this mobile APP is at your own risk.</li>\n" +
            "<li>We may suspend or terminate operation of the mobile APP at any time as we see fit.</li>\n" +
            "<li>Any Content is provided for your general information purposes only and to inform you about us and our products and news, features, services and other websites that may be of interest. It does not constitute technical, financial or legal advice or any other type of advice and should not be relied on for any purposes.</li>\n" +
            "<li>While we try to make sure that the mobile APP is available for your use, we do not promise that the mobile APP is available at all times nor do we promise the uninterrupted use by you of the mobile APP.</li>\n" +
            "</ul>\n" +
            "<p><strong>11&nbsp; &nbsp;Hyperlinks and third party sites</strong></p>\n" +
            "<ul>\n" +
            "<li>The Site may contain hyperlinks or references to third party websites other than the mobile APP. Any such hyperlinks or references are provided for your convenience only. We have no control over third party websites and accept no legal responsibility for any content, material or information contained in them. The display of any hyperlink and reference to any third party website does not mean that we endorse that third party&rsquo;s website, products or services. Your use of a third party site may be governed by the terms and conditions of that third party site.</li>\n" +
            "</ul>\n" +
            "<p><strong>12&nbsp; </strong>&nbsp;<strong>Limitation on our liability</strong></p>\n" +
            "<ul>\n" +
            "<li>Except for any legal responsibility that we cannot exclude in law (such as for death or personal injury), we are not legally responsible for any</li>\n" +
            "</ul>\n" +
            "<ol>\n" +
            "<li>losses that:<br /> (a) were not foreseeable to you and us when this contract was formed<br /> (b) that were not caused by any breach on our part<br /> 2. business losses<br /> 3. losses to non-consumers<br /> </li>\n" +
            "</ol>\n" +
            "<p><strong>13 </strong>&nbsp; <strong>Events beyond our control</strong></p>\n" +
            "<ul>\n" +
            "<li>We shall have no liability to you for any breach of these terms caused by any event or circumstance beyond our reasonable control including, without limitation, strikes, lock-outs or other industrial disputes; breakdown of systems or network access; or flood, fire, explosion or accident.</li>\n" +
            "</ul>\n" +
            "<p><strong>14 &nbsp;&nbsp;Rights of third parties</strong></p>\n" +
            "<ul>\n" +
            "<li>No one other than a party to these terms and conditions has any right to enforce any of these terms and conditions.</li>\n" +
            "</ul>\n" +
            "<p><strong>15 &nbsp;&nbsp;Variation</strong></p>\n" +
            "<ul>\n" +
            "<li>These terms are dated 13.9.2016. No changes to these terms are valid or have any effect unless agreed by us in writing. We reserve the right to vary these terms and conditions from time to time. Our new terms will be displayed on the mobile APP and by continuing to use and access the mobile APP following such changes, you agree to be bound by any variation made by us. It is your responsibility to check these terms and conditions from time to time to verify such variations.</li>\n" +
            "</ul>\n" +
            "<p><strong>11&nbsp; Disputes</strong></p>\n" +
            "<ul>\n" +
            "<li>We will try to resolve any disputes with you quickly and efficiently.</li>\n" +
            "<li>If you are unhappy with us under this contract please contact us as soon as possible.</li>\n" +
            "<li>If you and we cannot resolve a dispute using our internal complaint handling procedure, we will:</li>\n" +
            "</ul>\n" +
            "<ol>\n" +
            "<li>let you know that we cannot settle the dispute with you, and<br /> 2. give you certain information required by law about our alternative dispute resolution matters.</li>\n" +
            "</ol>\n" +
            "<ul>\n" +
            "<li>If you want to take court proceedings, the relevant courts of the India will have exclusive jurisdiction in relation to this contract.</li>\n" +
            "<li>Relevant Indian law will apply to this contract.</li>\n" +
            "</ul>\n" +
            "<p><strong>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;<span style=\"text-decoration: underline;\">Mobile APP</span></strong><span style=\"text-decoration: underline;\">&nbsp;</span><strong><span style=\"text-decoration: underline;\">Privacy Policy</span></strong></p>\n" +
            "<ul>\n" +
            "<li>This mobile APP is brought to you by SheoSoft Pvt. Ltd. We take the privacy of our mobile APP users very seriously. We ask that you read this Privacy Policy (&lsquo;the Policy&rsquo;) carefully as it contains important information about how we will use your personal data.<br /> For the purposes of the Data Protection SheoSoft Pvt. Ltd. (&lsquo;we&rsquo; or &lsquo;us&rsquo;) is the &lsquo;data controller&rsquo; (i.e. the company who is responsible for, and controls the processing of, your personal data).</li>\n" +
            "<li><strong>Personal data we may collect about you</strong><br /> We will obtain personal data about you (such as your name, email address, enquiry whenever you complete an online contact us form.<br /> For example, we will obtain your personal data when you contact us, instruct our services, complete the online contact us form etc. We may also obtain sensitive personal data about you if you volunteer it during the completion of an online form. If you volunteer such information, you will be consenting to our processing it for the purpose of completing the task you are instructing us for.<br /> We may monitor your use of this mobile APP through the use of cookies and similar tracking devices. For example, we may monitor how many times you visit, which pages you go to, traffic data, location data and the originating domain name of a user&rsquo;s internet service provider. This information helps us to build a profile of our users. Some of this data will be aggregated or statistical, which means that we will not be able to identify you individually. For further information on our use of cookies, please see our Cookie policy.<br /> Occasionally we may receive information about you from other sources, which we will add to the information we already hold about you in order to help us to personalize our service to you.</li>\n" +
            "<li><strong>How we use your personal data</strong><br /> We will use your personal data for the purposes described in the data protection notice that was given to you at the time your data were obtained. These purposes include:<br /> to help us identify you and any files you hold with us<br /> &bull; administration<br /> &bull; research, statistical analysis and behavioral analysis<br /> &bull; customer profiling and analyzing your request preferences<br /> &bull; marketing&mdash;see &lsquo;Marketing and opting out&rsquo;, below<br /> &bull; fraud prevention and detection<br /> &bull; billing and order fulfillment<br /> &bull; customizing this mobile APP and its content to your particular preferences<br /> &bull; to notify you of any changes to this mobile APP or to our services that may affect you<br /> &bull; security vetting<br /> &bull; improving our services</li>\n" +
            "<li><strong>Marketing and opting out</strong><br /> If you have given permission, we may contact you by email, telephone, SMS, Text/Picture, Video message, fax about products, services, promotions and special offers that may be of interest to you. If you prefer not to receive any direct marketing communications from us, you can opt out at any time. See further &lsquo;Your rights&rsquo;, below.<br /> Disclosure of your personal data<br /> We may disclose your personal data to:<br /> our agents and service providers<br /> &bull; law enforcement agencies in connection with any investigation to help prevent unlawful activity<br /> &bull; our business partners in accordance with the &lsquo;Marketing and opting out&rsquo; section above[, and]</li>\n" +
            "<li><strong>Keeping your data secure</strong><br /> We will use technical and organizational measures to safeguard your personal data, for example:<br /> access to your account, where applicable, is controlled by a password and username that are unique to you<br /> &bull; we store your personal data securely<br /> &bull; payment details, where applicable, are encrypted using technology<br /> While we will use all reasonable efforts to safeguard your personal data, you acknowledge that the use of the internet is not entirely secure and for this reason we cannot guarantee the security or integrity of any personal data that are transferred from you or to you via the internet.</li>\n" +
            "<li><strong>Monitoring</strong><br /> We may monitor and record communications with you (such as telephone conversations and emails) for the purpose of quality assurance.</li>\n" +
            "<li><strong>Information about other individuals</strong><br /> If you give us information on behalf of someone else, you confirm that the other person has appointed you to act on his/her behalf and has agreed that you can:<br /> give consent on his/her behalf to the processing of his/her personal data<br /> &bull; receive on his/her behalf any data protection notices<br /> &bull; give consent to the transfer of his/her personal data abroad, and<br /> &bull; give consent to the processing of his/her personal data in relation to the instructed task.</li>\n" +
            "<li><strong>Your rights</strong><br /> You have the right, subject to the payment of a small fee (currently Free), to request access to personal data that we may process about you. If you wish to exercise this right, you should:<br /> put your request in writing;<br /> &bull; include proof of your identity and address (e.g. a copy of your driving license or passport, and a recent utility or credit card bill);<br /> &bull; specify the personal data you want access to, including any account or reference numbers where applicable.<br /> You have the right to require us to correct any inaccuracies in your data free of charge. If you wish to exercise this right, you should:<br /> &bull; put your request in writing;<br /> &bull; provide us with enough information to identify you (e.g. account number, username, registration details); and<br /> &bull; specify the information that is incorrect and what it should be replaced with.<br /> You also have the right to ask us to stop processing your personal data for direct marketing purposes. If you wish to exercise this right, you should:<br /> &bull; put your request in writing (an email sent to fillmyformapp@gmail.com with a header that says &lsquo;Unsubscribe from Marketing&rsquo; is acceptable)],<br /> &bull; provide us with enough information to identify you (e.g. account number, username, registration details), and<br /> &bull; if your objection is not to direct marketing in general, but to direct marketing by a particular channel (e.g. email or telephone), please specify the channel you are objecting to</li>\n" +
            "<li><strong>Our contact details</strong><br /> We welcome your feedback and questions. If you wish to contact us, please send an email to fillmyformapp@gmail.com or call us on 07015889866.</li>\n" +
            "<li>We may change this privacy policy from time to time. You should check this policy occasionally to ensure you are aware of the most recent version that will apply each time you access this mobile APP.</li>\n" +
            "</ul>\n" +
            "<p><strong><span style=\"text-decoration: underline;\">Cookie Policy</span></strong></p>\n" +
            "<ul>\n" +
            "<li><strong>Use of cookies </strong><br /> A cookie is a small text file which is placed onto your mobile (or other electronic device) when you access our mobile APP. We use cookies on this mobile APP to:<br /> recognize you whenever you visit this mobile APP (this speeds up your access to the website as you do not have to log on each time)<br /> &bull; obtain information about your preferences, online movements and use of the internet<br /> &bull; carry out research and statistical analysis to help improve our content and to help us better understand our visitor requirements and interests<br /> &bull; target our marketing and advertising campaigns more effectively by providing interest-based advertisements that are personalized to your interests, and<br /> &bull; make your online experience more efficient and enjoyable.<br /> In most cases we will need your consent in order to use cookies on this mobile APP. The exception is where the cookie is essential in order for us to provide you with a service you have requested.<br /> If you visit our mobile APP when your browser is set to accept cookies, we will interpret this as an indication that you consent to our use of cookies and other similar technologies as described in this Privacy Policy. If you change your mind in the future about letting us use cookies, you can modify the settings of your browser to reject cookies or disable cookies completely.</li>\n" +
            "<li><strong>Third-party cookies</strong><br /> We may work with third-party suppliers who may also set cookies on our website. These third-party suppliers are responsible for the cookies they set on our site. If you want further information please go to the website for the relevant third party. You will find additional information in the table below.</li>\n" +
            "<li><strong>How to turn off cookies</strong><br /> If you do not want to accept cookies, you can change your browser settings so that cookies are not accepted. If you do this, please be aware that you may lose some of the functionality of this mobile APP. For further information about cookies and how to disable them please go to: www.aboutcookies.org or www.allaboutcookies.org.</li>\n" +
            "</ul>\n" +
            "<p><strong><span style=\"text-decoration: underline;\">ACCEPTABLE USE POLICY</span></strong></p>\n" +
            "<ul>\n" +
            "<li><strong>Introduction</strong></li>\n" +
            "<li>1 Together with our General mobile APP terms and conditions of use, this acceptable use policy governs how you may access and use the mobile APP.<br /> </li>\n" +
            "<li><strong>2 Definitions</strong>: Mobile APP includes the following website[s]: www.fillmyformapp.com and all associated web pages; Submission means any text, images, video, audio or other multimedia content, software or other information or material submitted by you or other users to the mobile APP;</li>\n" +
            "<li>We, us or our means, fill MY form - Mobile APP, www.fillmyformapp.com,SheoSoft Pvt. Ltd.<br /> or your means the person accessing or using the mobile APP or its Content.</li>\n" +
            "<li><strong>Acceptable use</strong><br /> We permit you to use the mobile APP only for personal, non-commercial purposes and primarily for accessing information about us. Use of the mobile APP in any other way, including in contravention of any restriction on use set out in this policy, is not permitted. If you do not agree with the terms of this policy, you may not use the mobile APP.</li>\n" +
            "<li><strong>Restrictions on use</strong></li>\n" +
            "<li>As a condition of your use of the mobile APP, you agree:<br /> 1 not to use the mobile APP for any purpose that is unlawful under any applicable law or prohibited by this policy or our General mobile APP terms and conditions of use (see above).<br /> 2 not to use the mobile APP to commit any act of fraud; <br /> 3 not to use the mobile APP to distribute viruses or malware or other similar harmful software code<br /> 4 not to use the mobile APP for purposes of promoting unsolicited advertising or sending spam;<br /> 5 not to use the mobile APP to simulate communications from us or another service or entity in order to collect identity information, authentication credentials, or other information (&lsquo;phishing&rsquo;);<br /> 6 not to use the mobile APP in any manner that disrupts the operation of our mobile APP or business or the website or business of any other entity;<br /> 7 not to use the mobile APP in any manner that harms minors;<br /> 8 not to promote any unlawful activity;<br /> 9 not to represent or suggest that we endorse any other business, product or service unless we have separately agreed to do so in writing;<br /> 10 not to use the mobile APP to gain unauthorized access to or use of computers, data, systems, accounts or networks;<br /> 11 not to attempt to circumvent password or user authentication methods; and<br /> 12 to comply with the provisions relating to our intellectual property rights and software contained in our General mobile APP terms and conditions of use (see above)<br /> </li>\n" +
            "<li><strong>Using the SheoSoft Pvt. Ltd. name and logo</strong><br /> You may not use, where applicable, our trademarks, logos or trade names except in accordance with this policy and our General mobile APP terms and conditions of use.</li>\n" +
            "<li><strong>Breach</strong><br /> We shall apply the terms of this policy in our absolute discretion. In the event of your breach of these terms we may terminate or suspend your use of the mobile APP, remove or edit Submissions, disclose Submissions to law enforcement authorities or take any action we consider necessary to remedy the breach.</li>\n" +
            "<li><strong>Disputes</strong><br /> 1 We will try to resolve any disputes with you quickly and efficiently.<br /> 2 If you are unhappy with us under this contract please contact us as soon as possible.<br /> 3 If you and we cannot resolve a dispute using our internal complaint handling procedure, we will:<br />1 let you know that we cannot settle the dispute with you, and<br /> 3.2 give you certain information required by law about our alternative dispute resolution matters. You may also use the online dispute resolution (ODR) platform to resolve the dispute with us.<br /> 4 If you want to take court proceedings, the relevant courts of the INDIA will have exclusive jurisdiction in relation to this contract.<br /> 5 Relevant INDIAN law will apply to this contract.</li>\n" +
            "</ul>\n" +
            "<p><strong><span style=\"text-decoration: underline;\">COMPLAINTS HANDLING STATEMENT</span></strong></p>\n" +
            "<ul>\n" +
            "<li>SheoSoft Pvt. Ltd. is committed to high quality service and client care. If you are unhappy about any aspect of the service you have received please contact on 07015889866, A copy of our complaints policy is available on request via: fillmyformapp@gmail.com</li>\n" +
            "</ul>";




    public static String Support="<p><strong>We welcome your feedback and questions.</strong></p>\n" +
            "<p><strong>If you wish to contact us,</strong></p>\n" +
            "<p>&nbsp;Please send an email to &ldquo; <a href=\"mailto:fillmyformapp@gmail.com\">fillmyformapp@gmail.com</a> &rdquo;</p>\n" +
            "<p>Or</p>\n" +
            "<p>Contact us on WhatsApp Number- 07015889866</p>\n" +
            "<p>&nbsp;</p>\n" +
            "<p>Please do not hesitate to contact us for any queries.</p>";

    public static String Share_and_Earn_Text="<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>How to Earn Money</strong></p>" +
            "<p>&nbsp;</p>" +
            "<p>Step 1: Share this APP by <strong>Clicking</strong> on below <strong>colour bar.</strong></p>" +
            "<p>&nbsp;</p>" +
            "<p>Step 2: Your friend will get play store<strong> link of \"fill MY form\" </strong>APP with a&nbsp;<strong>Referral Code.</strong></p>" +
            "<p>&nbsp;</p>" +
            "<p>Step 3: Ask your friend to install&nbsp;<strong>\"fill MY form\" </strong>APP<strong>&nbsp;</strong>&amp; Sign Up using your Referral Code.</p>" +
            "<p>&nbsp;</p>" +
            "<p>Step 4: <strong>Done!&nbsp;</strong>Each of you will get <strong>Rs.50 extra</strong> in your wellats.</p>";

    public static String Share_and_Earn1="I invite you to download Fill MY Form APP with my referral coupon code: ";


    public static String Share_and_Earn2="\nBy using my referral code you will get Rs. 50 in your Wallet. It is a very useful APP for every student who wants to apply any government job form from your Home without wandering to cafe shops. APP give service within 48 hours (maximum).";

    public static int Job_Noti=0;
    public static int Process_Job_Noti=0;
    public static int Applied_Job_Noti=0;
    public static int Count=0;

    public static ArrayList<String> statelist=new ArrayList<>();
    public static ArrayList<String> districtlist=new ArrayList<>();
    public static ArrayList<String> ExamCenterList=new ArrayList<>();
    public static ArrayList<String> CategoryList=new ArrayList<>();
    public static ArrayList<String> CategoryFeeList=new ArrayList<>();
    public static ArrayList<String> OurchargesList=new ArrayList<>();
    public static ArrayList<String> PaidWalletList=new ArrayList<>();
// Citrus  Variable

    //    public static String BILL_URL = "https://salty-plateau-1529.herokuapp.com/billGenerator.production.wallet.php";
    public static String BILL_URL = "https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.php";
    //public static String BILL_URL_BN = "https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.bn.php";
    public static String BILL_URL_BC = "https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.bc.php";
    public static String RETURN_URL_LOAD_MONEY = "https://salty-plateau-1529.herokuapp.com/redirectUrlLoadCash.php";

    // Sand wallet PG
    public static String SIGNUP_ID = "9hh5re3r5q-signup";
    public static String SIGNUP_SECRET = "3be4d7bf59c109e76a3619a33c1da9a8";
    public static String SIGNIN_ID = "9hh5re3r5q-signin";
    public static String SIGNIN_SECRET = "ffcfaaf6e6e78c2f654791d9d6cb7f09";
    public static String VANITY = "nativeSDK";

    public static boolean enableLogging = true;
    public static boolean ENABLE_ONE_TAP_PAYMENT = true;

    public static String colorPrimaryDark = "#E7961D";
    public static String colorPrimary = "#F9A323";
    public static String textColor = "#ffffff";

    public static String INTENT_EXTRA_ENVIRONMENT_CHANGED = "INTENT_EXTRA_ENVIRONMENT_CHANGED";
    public static String INTENT_EXTRA_MERCHANT_DETAILS_CHANGED = "INTENT_EXTRA_MERCHANT_DETAILS_CHANGED";

}
