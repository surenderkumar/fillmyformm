package job.job.consaltancy.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;

import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.fragment.View_confirmation_file;
import job.job.consaltancy.fragment.Webview_details;
import job.job.consaltancy.getset.job_processing_getset;
import job.job.consaltancy.utils.Constants;

/**
 * Created by Surender on 4/2/2016.
 */

public class Applied_job_adapter extends RecyclerView.Adapter<Applied_job_adapter.recycleviewholder> {

    ArrayList<job_processing_getset> data;
    Context c;
    TextView progtext,okbtn;
    NumberProgressBar bar;
    String URL = "";
    Dialog dialog;
    int pos;

    public Applied_job_adapter(Context c, ArrayList<job_processing_getset> data) {
        this.c = c;
        this.data = data;
    }

    @Override
    public recycleviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.applied_job_list_item, null);

        recycleviewholder recycleviewholder = new recycleviewholder(v);

        return recycleviewholder;
    }

    @Override
    public void onBindViewHolder(recycleviewholder holder, final int p) {

        holder.heading.setText(data.get(p).getCompany_name());
        holder.postname.setText(data.get(p).getPost_name());
        holder.fees.setText(data.get(p).getFees());
        holder.date.setText(data.get(p).getDate());


        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pos = p;
                Log.e("position",pos+" abc"+data.get(p).getFee_receipt()+" "+data.get(pos).getDownload_link());
                dialog();

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class recycleviewholder extends RecyclerView.ViewHolder {
        public TextView heading, postname, fees, date, download, view;
        public LinearLayout layout;

        public recycleviewholder(View v) {
            super(v);

            heading = (TextView) v.findViewById(R.id.tv_heading_id);
            postname = (TextView) v.findViewById(R.id.tv_postname_id);
            fees = (TextView) v.findViewById(R.id.tv_postfees_id);
            date = (TextView) v.findViewById(R.id.tv_date_id);
            download = (TextView) v.findViewById(R.id.tv_download_id);
//            view=(TextView)v.findViewById(R.id.tv_view_id);
        }
    }

    public void dialog() {

        dialog = new Dialog(c);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.view_download_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView download_fee = (TextView) dialog.findViewById(R.id.tv_down_fee);
        TextView view_fee = (TextView) dialog.findViewById(R.id.tv_view_fee);
        TextView download_file = (TextView) dialog.findViewById(R.id.tv_down_file);
        TextView view_file = (TextView) dialog.findViewById(R.id.tv_view_file);

        download_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.url + "fee_receipt/" + data.get(pos).getFee_receipt();
                url=url.replace(" ","%20");
                Log.e("urlll",url);
                download_dialog(url);
            }
        });

        download_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.url + "fee_receipt/" + data.get(pos).getDownload_link();
                url=url.replace(" ","%20");
                Log.e("urlll",url);
                download_dialog(url);
            }
        });

        view_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.url + "fee_receipt/" + data.get(pos).getFee_receipt();
                Log.e("confirmation_url", url);
                Intent in = new Intent(c, View_confirmation_file.class);
                in.putExtra("url", url);
                c.startActivity(in);

//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                c.startActivity(i);
            }
        });
        view_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.url + "fee_receipt/" + data.get(pos).getDownload_link();
                Log.e("confirmation_url", url);
                Intent in = new Intent(c, View_confirmation_file.class);
                in.putExtra("url", url);
                c.startActivity(in);

//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                c.startActivity(i);
            }
        });


        dialog.show();
    }

    public void download_dialog(String url) {

        dialog = new Dialog(c);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.file_download);

        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bar = (NumberProgressBar) dialog.findViewById(R.id.numberbar1);
        progtext = (TextView) dialog.findViewById(R.id.textView2);
        okbtn=(TextView)dialog.findViewById(R.id.textView3);
        dialog.show();

        //URL=data.get(p).getDownload_link();

        URL = url;
        //URL=data.get(pos).getDownload_link();

        String[] urlarr = URL.split("/");
        int len = urlarr.length;
        //Toast.makeText(c,"len "+len,200).show();
        URL = urlarr[len - 1];

        new DownloadFile().execute(url);

    }

    private class DownloadFile extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//
//                }
//            }, 5000);

        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                String filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
                //.getPath();

                File myDir = new File(filepath + "/FillMyForm");
                myDir.mkdirs();
                File file = new File(myDir, URL);
                if (file.exists())
                    file.delete();

                // Download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Save the downloaded file

                OutputStream output = new FileOutputStream(file);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // Publish the progress
                    publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }

                // Close connection
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                // Error Log
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            bar.setProgress(progress[0]);

            if (progress[0] > 99)
                progtext.setVisibility(View.VISIBLE);
                okbtn.setVisibility(View.VISIBLE);
                okbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }
    }
}
