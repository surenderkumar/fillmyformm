package job.job.consaltancy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import job.job.consaltancy.R;

/**
 * Created by surender on 6/7/2016.
 */
public class Exam_center_adapter extends BaseAdapter {

    Context c;
    private LayoutInflater inflater;
    ArrayList<String> centerlist=new ArrayList<>();
  public static  ArrayList<String> selectcenter;
     ArrayList<Boolean> check_center;
    int count=0;

    public Exam_center_adapter(Context c,ArrayList<String> centerlist) {
        this.c = c;
        this.centerlist=centerlist;
        selectcenter= new ArrayList<>();
        check_center= new ArrayList<>();
        for (int i=0;i<centerlist.size();i++)
            check_center.add(false);
    }

    @Override
    public int getCount() {
        return centerlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.exam_center_list_item, parent, false);
        final LinearLayout main_layout=(LinearLayout)view.findViewById(R.id.main_layout);
        final CheckBox cb=(CheckBox)view.findViewById(R.id.cb_examcenter_id);
        cb.setText(centerlist.get(position));

        if(check_center.get(position))
            cb.setChecked(true);
        else
        cb.setChecked(false);

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (selectcenter.size() == 5&(isChecked)) {
                    Log.e("selectcentersize if",selectcenter.size()+" ");
                    final Snackbar snackbar = Snackbar.make(main_layout, "      You Select Maximum Five Center.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
//                    Toast.makeText(c, "You Select Maximum Five Center", Toast.LENGTH_LONG).show();
                    cb.setChecked(false);
                } else if (isChecked) {
                    selectcenter.add(centerlist.get(position));
                    check_center.set(position,true);
                    count++;
//                    Toast.makeText(c, "You Select Reference No. "+count, Toast.LENGTH_LONG).show();
                    Log.e("selectcentersiz else if", selectcenter.size() + " "+selectcenter.get(selectcenter.indexOf(centerlist.get(position))));
                } else {
                    Log.e("selectcentersize else", selectcenter.size() + " " + selectcenter.get(selectcenter.indexOf(centerlist.get(position))));
                    selectcenter.remove(centerlist.get(position));
                    check_center.set(position,false);
                    count--;
                }
            }
        });

        return view;
    }
}
