package job.job.consaltancy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import job.job.consaltancy.R;

/**
 * Created by Anil Godara on 19-01-2017.
 */

public class ExamAdapter extends BaseAdapter {

    Context context;


    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

            context = viewGroup.getContext();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.txtlist, viewGroup, false);

        TextView textView = (TextView) view.findViewById(R.id.txtlst);
        if (i % 2 == 0)
            textView.setText("This is even List Item Testing");
        else
            textView.setText("My Odd List Test Item");


        return view;
    }
}
