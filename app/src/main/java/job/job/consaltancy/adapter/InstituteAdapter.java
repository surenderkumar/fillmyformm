package job.job.consaltancy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import job.job.consaltancy.R;

/**
 * Created by Anil Godara on 19-01-2017.
 */

public class InstituteAdapter extends BaseAdapter {

Context context;


    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        context = viewGroup.getContext();
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view1 = layoutInflater.inflate(R.layout.institute_list,viewGroup,false);
        ImageView image = (ImageView)view1.findViewById(R.id.ins_image);

        if (i% 2 == 0)
        image.setImageResource(R.drawable.institute2);
        return view1;
    }
}
