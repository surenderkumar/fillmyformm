package job.job.consaltancy.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import job.job.consaltancy.fragment.Home_Job_Fragment;
import job.job.consaltancy.fragment.Home_fragment;
import job.job.consaltancy.fragment.Other_fragment;

public class PagerAdapter_tab extends FragmentStatePagerAdapter {
	int mNumOfTabs;

	public PagerAdapter_tab(FragmentManager fm, int NumOfTabs) {
		super(fm);
		this.mNumOfTabs = NumOfTabs;
	}

	@Override
	public Fragment getItem(int position) {

		if(position==0) {
			Home_fragment tab1 = new Home_fragment();
//			Bundle b = new Bundle();
//			b.putInt("key", position);
//			tab1.setArguments(b);
			return tab1;
		}
		else if(position==1)
		{
			Home_Job_Fragment tab1 = new Home_Job_Fragment();
//			Bundle b = new Bundle();
//			b.putInt("key", position);
//			tab1.setArguments(b);
			return tab1;
		}
		else
		{
			Other_fragment tab1 = new Other_fragment();
//			Bundle b = new Bundle();
//			b.putInt("key", position);
//			tab1.setArguments(b);
			return tab1;
		}

	}

	@Override
	public int getCount() {
		return mNumOfTabs;
	}
}