package job.job.consaltancy.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

import job.job.consaltancy.R;

/**
 * Created by surender on 6/7/2016.
 */
public class Category_adapter extends BaseAdapter {

    Context c;
    private LayoutInflater inflater;
    ArrayList<String> catelist=new ArrayList<>();
    public static  String selectcategory;
    int count=0;

    public Category_adapter(Context c,ArrayList<String> catelist) {
        this.c = c;
        this.catelist = catelist;
    }

    @Override
    public int getCount() {
        return catelist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.category_list_item, parent, false);
        RadioGroup rgp= (RadioGroup)view.findViewById(R.id.radio_group);
        RadioGroup.LayoutParams rprms;

            RadioButton radioButton = new RadioButton(c);
            radioButton.setText(catelist.get(position));
            radioButton.setId(position);
            rprms= new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            rgp.addView(radioButton, rprms);

        return view;
    }
}
