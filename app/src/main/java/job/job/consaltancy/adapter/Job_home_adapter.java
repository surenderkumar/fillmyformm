package job.job.consaltancy.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import job.job.consaltancy.Apply_job_activity;
import job.job.consaltancy.R;
import job.job.consaltancy.fragment.Address_Info_fragment;
import job.job.consaltancy.fragment.Basic_Info_fragment;
import job.job.consaltancy.fragment.Category_Info_fragment;
import job.job.consaltancy.fragment.Education_Info_fragment;
import job.job.consaltancy.fragment.Experience_Info_fragment;
import job.job.consaltancy.fragment.Other_info_activity;
import job.job.consaltancy.fragment.View_confirmation_file;
import job.job.consaltancy.fragment.Webview_details;
import job.job.consaltancy.getset.job_getset;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;

/**
 * Created by Surender on 4/2/2016.
 */

public class Job_home_adapter extends RecyclerView.Adapter<Job_home_adapter.recycleviewholder> {
    TextView progtext,okbtn;
    NumberProgressBar bar;
    String URL = "";
    Dialog dialog;
    int pos;
    ArrayList<job_getset> data=new ArrayList<>();
    Context c;
    SharedPreferences detailprf;
    String st_status="true",clas_nm="";
    ArrayList<String> post_lt=new ArrayList<>();
    ArrayList<String> date_lt=new ArrayList<>();
    int clas_id;

    NetworkConnection nw;

    public Job_home_adapter( Context c, ArrayList<job_getset> data) {
        this.c = c;
        this.data=data;
        nw=new NetworkConnection(c);
    }

    @Override
    public recycleviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.job_home_listitem,null);

        recycleviewholder holder=new recycleviewholder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(final recycleviewholder holder, final int p) {

        detailprf=c.getSharedPreferences("detailprf",Context.MODE_PRIVATE);

        holder.job_type.setText(data.get(p).getJob_type());
        holder.company_name.setText(data.get(p).getCompany_name());


        if(data.get(p).getExpire()) {
            holder.lastdate.setText("Last Date "+data.get(p).getLastdate());
            holder.applynow.setClickable(true);
        }
        else {
            holder.lastdate.setText("Last Date Expire");
            holder.lastdate.setTextColor(Color.RED);
            holder.applynow.setClickable(false);
        }

       post_lt=data.get(p).getPost_name();

        String st_post="";
        if(post_lt.size()>0) {
            st_post = post_lt.get(0);
            for (int i = 1; i < post_lt.size(); i++)
                st_post=st_post+","+post_lt.get(i);
        }

        holder.sp_post.setText(st_post);

        if(data.get(p).getExpire())
        holder.applynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("adapter category",data.get(p).getCategory()+"");

                Constants.CategoryList=data.get(p).getCategory();
                Constants.CategoryFeeList=data.get(p).getCategory_fee();
                Constants.ExamCenterList = data.get(p).getExamcenter();
                Constants.OurchargesList=data.get(p).getOur_charge();
                Constants.PaidWalletList = data.get(p).getPaid_wallet();

                if(Constants.Count>199)
                {
                    final Snackbar snackbar = Snackbar.make(holder.main_layout, "      Today Limit of Applied Job Complete, You Can Try Next Day.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
                }
//                    Toast.makeText(c,"Today Limit of Applied Job Complete, You Can Try Next Day",Toast.LENGTH_LONG).show();
                else
                {
                if (Constants.Category.length() < 1) {
                    final Snackbar snackbar = Snackbar.make(holder.main_layout, "      Please select your Reservation Cota (Category).", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
//                    Toast.makeText(c, "Please select your Reservation Cota (Category)", Toast.LENGTH_LONG).show();
                    Constants.fragment=3;
                    Category_Info_fragment basic_frag = new Category_Info_fragment();
                    ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
                            addToBackStack(null).commit();
                } else {

                    if (nw.isConnected()) {
                        Constants.Job_id = data.get(p).getId();
                        Constants.Job_Category = data.get(p).getJob_type();
                        Constants.Company_Name = data.get(p).getCompany_name();
                        Constants.hint=data.get(p).getHint();
                        Constants.center_hint=data.get(p).getCenter_hint();
                        Constants.fragment=3;

                        Other_info_activity basic_frag = new Other_info_activity();
                        ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
                                addToBackStack(null).commit();



//                        Apply_job_activity basic_frag = new Apply_job_activity();
//                        ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
//                                addToBackStack(null).commit();
                    } else
                    {
                        final Snackbar snackbar = Snackbar.make(holder.main_layout, "      No Internet Connection.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                    }
//                        Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();

                }
            }

            }
        });

        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.Job_Detail=data.get(p).getDetail();
                Constants.fragment=3;
                Webview_details acc_frag=new Webview_details();
                FragmentManager manager = ((AppCompatActivity)c).getSupportFragmentManager();

                manager.beginTransaction().replace(R.id.FrameLayout,acc_frag).addToBackStack(null).commit();

                Log.e("detail click", "details click");
            }
        });

        holder.notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("notification click","notification click");

                if(holder.lastdate.equals("Last Date Expire"))
                {

                }
                else {
                    pos = p;
                    dialog();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class recycleviewholder extends RecyclerView.ViewHolder
    {

        public ImageView pic;
        public TextView job_type,company_name,lastdate,detail,notification;
        public LinearLayout layout;
        Button applynow;
        TextView sp_post;
        LinearLayout main_layout;

        public recycleviewholder(View v) {
            super(v);

            main_layout=(LinearLayout)v.findViewById(R.id.main_layout);
            applynow=(Button)v.findViewById(R.id.apply_id);
            job_type=(TextView)v.findViewById(R.id.tv_job_type_id);
            company_name=(TextView)v.findViewById(R.id.tv_companyname_id);
            lastdate=(TextView)v.findViewById(R.id.tv_lastdate_id);
            detail=(TextView)v.findViewById(R.id.tv_detail_id);
            notification=(TextView)v.findViewById(R.id.tv_notification_id);
            sp_post=(TextView)v.findViewById(R.id.sp_postname_id);

        }

    }


    public void setFilter(ArrayList<job_getset> countryModels) {
        data = new ArrayList<>();
        data.addAll(countryModels);
        notifyDataSetChanged();
    }

    public void checkdetail(String head,String des, final String cls)
    {

        final Dialog dialog=new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_detail);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView notnow=(TextView)dialog.findViewById(R.id.not_nowid);
        TextView apply=(TextView)dialog.findViewById(R.id.apply_id);

        TextView tv_heading=(TextView)dialog.findViewById(R.id.tv_heading_id);
        TextView tv_desc=(TextView)dialog.findViewById(R.id.tv_description_id);

        tv_heading.setText(head);
        tv_desc.setText(des);

        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width-20);

        notnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cls.equalsIgnoreCase("basic")==true) {
                    Basic_Info_fragment basic_frag = new Basic_Info_fragment();
                    ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
               else if(cls.equalsIgnoreCase("category")==true) {
                    Category_Info_fragment basic_frag = new Category_Info_fragment();
                    ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("address")==true) {
                    Address_Info_fragment basic_frag = new Address_Info_fragment();
                    ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("experience")==true) {
                    Experience_Info_fragment basic_frag = new Experience_Info_fragment();
                    ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
                else if(cls.equalsIgnoreCase("education")==true) {
                    Education_Info_fragment basic_frag = new Education_Info_fragment();
                    ((FragmentActivity) c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }

                dialog.dismiss();

            }
        });
    }


    public void applydialog(int p) {

        final Dialog dialog=new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.apply_now_dialog);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView tv_head=(TextView)dialog.findViewById(R.id.tv_heading_id);
        TextView notnow=(TextView)dialog.findViewById(R.id.not_nowid);
        TextView apply=(TextView)dialog.findViewById(R.id.apply_id);
        TextView fees=(TextView)dialog.findViewById(R.id.tv_fees_id);
        TextView our_charge=(TextView)dialog.findViewById(R.id.tv_ourcharge_id);
        TextView paid_wallet=(TextView)dialog.findViewById(R.id.tv_paid_wallet_id);
        TextView total_fees=(TextView)dialog.findViewById(R.id.tv_total_fees_id);

        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width - 20);

        String fes = "0";

        tv_head.setText("You are applying for the Post "+data.get(p).getPost_name()+" in "+data.get(p).getCompany_name());


        our_charge.setText("Rs. "+String.valueOf(data.get(p).getOur_charge()));
        paid_wallet.setText("Rs. "+String.valueOf(data.get(p).getPaid_wallet()));

//        int tf=Integer.valueOf(fes)+data.get(p).getOur_charge();
//
//        tf=tf-data.get(p).getPaid_wallet();

//        total_fees.setText("Rs. "+String.valueOf(tf));

        notnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checksavedetail_dialog();

            }
        });
    }

    public void checksavedetail_dialog()
    {

        final Dialog dialog=new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_save_detaile);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        TextView yes=(TextView)dialog.findViewById(R.id.yesid);
        TextView no=(TextView)dialog.findViewById(R.id.noid);
        LinearLayout layout=(LinearLayout)dialog.findViewById(R.id.layoutid);
        Spinner sp_pref=(Spinner)dialog.findViewById(R.id.sp_preference_id);

        layout.setMinimumWidth(Constants.Width - 20);

//        if(pref_lt.get(0).equalsIgnoreCase("no")==true)
//            sp_pref.setVisibility(View.GONE);
//        else {
//            sp_pref.setVisibility(View.VISIBLE);
//            ArrayAdapter<String> proofAdapter = new ArrayAdapter<String>(c, android.R.layout.simple_spinner_item, pref_lt);
//            proofAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            sp_pref.setAdapter(proofAdapter);
//        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    public void dialog() {

        dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.offical_notification_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView download_file = (TextView) dialog.findViewById(R.id.tv_down_file);
        TextView view_file = (TextView) dialog.findViewById(R.id.tv_view_file);


        download_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Constants.Job_Detail=Constants.url+"notification/"+data.get(pos).getNotification();
                String url = Constants.Job_Detail;
                Log.e("urll",url);
                download_dialog(url);
            }
        });


        view_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                Constants.Job_Detail=data.get(pos).getNotification();

                Constants.fragment=3;
                Webview_details acc_frag=new Webview_details();
                FragmentManager manager = ((AppCompatActivity)c).getSupportFragmentManager();

                manager.beginTransaction().replace(R.id.FrameLayout,acc_frag).addToBackStack(null).commit();

//                String url = Constants.url + "fee_receipt/" + data.get(pos).getDownload_link();
//                Log.e("confirmation_url", url);
//                Intent in = new Intent(c, View_confirmation_file.class);
//                in.putExtra("url", url);
//                c.startActivity(in);

//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                c.startActivity(i);
            }
        });


        dialog.show();


    }

    public void download_dialog(String url) {

        dialog = new Dialog(c);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.file_download);

        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bar = (NumberProgressBar) dialog.findViewById(R.id.numberbar1);
        progtext = (TextView) dialog.findViewById(R.id.textView2);
        okbtn=(TextView)dialog.findViewById(R.id.textView3);
        dialog.show();

        //URL=data.get(p).getDownload_link();

        URL = url;
        //URL=data.get(pos).getDownload_link();

        String[] urlarr = URL.split("/");
        int len = urlarr.length;
        //Toast.makeText(c,"len "+len,200).show();
        URL = urlarr[len - 1];

        new DownloadFile().execute(url);

    }

    private class DownloadFile extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//
//                }
//            }, 5000);

        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                java.net.URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                String filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
                //.getPath();

                File myDir = new File(filepath + "/FillMyForm");
                myDir.mkdirs();
                File file = new File(myDir, URL);
                if (file.exists())
                    file.delete();

                // Download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Save the downloaded file

                OutputStream output = new FileOutputStream(file);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // Publish the progress
                    publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }

                // Close connection
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                // Error Log
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            bar.setProgress(progress[0]);

            if (progress[0] > 99)
                progtext.setVisibility(View.VISIBLE);
                okbtn.setVisibility(View.VISIBLE);
                okbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }
    }


}
