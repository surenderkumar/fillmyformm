package job.job.consaltancy.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.getset.job_processing_getset;

/**
 * Created by Surender on 4/2/2016.
 */

public class Job_in_processing_adapter extends RecyclerView.Adapter<Job_in_processing_adapter.recycleviewholder> {

    ArrayList<job_processing_getset> data;
    Context c;
    public Job_in_processing_adapter(Context c,ArrayList<job_processing_getset> data) {
        this.c = c;
        this.data=data;
    }

    @Override
    public recycleviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.processing_job_list_item,null);

        recycleviewholder recycleviewholder=new recycleviewholder(v);

        return recycleviewholder;
    }

    @Override
    public void onBindViewHolder(recycleviewholder holder, final int p) {

        holder.heading.setText(data.get(p).getCompany_name());
        holder.postname.setText(data.get(p).getPost_name());
        holder.fees.setText(data.get(p).getFees());
        holder.time.setText(data.get(p).getDate());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class recycleviewholder extends RecyclerView.ViewHolder
    {

        public TextView heading,postname,fees,time;

        public recycleviewholder(View v) {
            super(v);

            heading=(TextView)v.findViewById(R.id.tv_heading_id);
            postname=(TextView)v.findViewById(R.id.tv_postname_id);
            fees=(TextView)v.findViewById(R.id.tv_postfees_id);
            time=(TextView)v.findViewById(R.id.tv_time_id);

        }
    }

}
