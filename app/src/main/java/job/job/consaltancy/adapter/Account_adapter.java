package job.job.consaltancy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import job.job.consaltancy.R;

/**
 * Created by surender on 6/7/2016.
 */

public class Account_adapter extends BaseAdapter {

    Context c;
    private LayoutInflater inflater;

    public Account_adapter(Context c) {
        this.c = c;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.account_summary_listitem, parent, false);

        return view;
    }
}
