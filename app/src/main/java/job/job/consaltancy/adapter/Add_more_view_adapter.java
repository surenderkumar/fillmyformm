package job.job.consaltancy.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Add_more_view_adapter extends BaseAdapter {

    Context c;
    private LayoutInflater inflater;
    ArrayList<String> infolist;
    ArrayList<String> infotype;

    public Add_more_view_adapter(Context c, ArrayList<String> infolist,ArrayList<String> infotype) {
        this.c = c;
        this.infolist=infolist;
        this.infotype=infotype;
    }

    @Override
    public int getCount() {
        return infolist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int p, View convertView, ViewGroup parent) {
        inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.add_more_list_item, parent, false);
        TextView text=(TextView)view.findViewById(R.id.tv_admore_id);
        TextView imagetext=(TextView)view.findViewById(R.id.tv_imagetext_id);
        ImageView img=(ImageView)view.findViewById(R.id.iv_admore_id);
        String arr[];
        if(infotype.get(p).equalsIgnoreCase("text")) {
            text.setText(infolist.get(p));
            text.setVisibility(View.VISIBLE);
        }
        else {
            img.setVisibility(View.VISIBLE);
            imagetext.setVisibility(View.VISIBLE);

            if(infolist.get(p).contains("_")) {
                arr = infolist.get(p).split("_");
                imagetext.setText(arr[1]);
            }

            String url= Constants.url + "other_document/" + infolist.get(p)+ ".png";
            url=url.replace(" ","%20");
            Log.e("other_image_url "+p,url);
            Picasso.with(c)
                    .load(url)
                    .into(img);
        }

        return view;
    }
}
