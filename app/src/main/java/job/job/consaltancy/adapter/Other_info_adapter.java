package job.job.consaltancy.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.fragment.Other_info_insert;
import job.job.consaltancy.getset.other_info_admin_getset;
import job.job.consaltancy.getset.other_info_user_getset;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Other_info_adapter extends BaseAdapter {

    Context c;
    private LayoutInflater inflater;

    ArrayList<other_info_user_getset> userdata;
    ArrayList<other_info_admin_getset> admindata;
    ArrayList<String> psid_arr;
    ImageView iv_other;

    public Other_info_adapter(Context c, ArrayList<other_info_user_getset> userdata,ArrayList<other_info_admin_getset> admindata,
                              ArrayList<String> psid_arr) {
        this.c = c;
        this.userdata=userdata;
        this.admindata=admindata;
        this.psid_arr=psid_arr;
    }

    @Override
    public int getCount() {
        return admindata.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int p, View convertView, ViewGroup parent) {
        inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.other_info_list_item, parent, false);

        TextView tv_head=(TextView)view.findViewById(R.id.tv_heading_id);
        final EditText et_other=(EditText)view.findViewById(R.id.et_other_id);
        iv_other=(ImageView)view.findViewById(R.id.iv_other_id);
        ImageView iv_submit=(ImageView)view.findViewById(R.id.iv_submit_id);
        Spinner sp_other=(Spinner)view.findViewById(R.id.sp_other_id);
        LinearLayout lay_layout=(LinearLayout)view.findViewById(R.id.lay_layout);

        tv_head.setText(admindata.get(p).getHeading());

        if(admindata.get(p).getValue_type().equalsIgnoreCase("selection")) {
            sp_other.setVisibility(View.VISIBLE);
            ArrayList<String> value=new ArrayList<>();
            value.add("Select");
            if(admindata.get(p).getValue_selection().contains(",")) {
                String[] arr = admindata.get(p).getValue_selection().split(",");
                for(int i=0;i<arr.length;i++)
                    value.add(arr[i]);
            }
            else
            value.add(admindata.get(p).getValue_selection());

            ArrayAdapter<String> sp_Adapter = new ArrayAdapter<String>(c, android.R.layout.simple_spinner_item, value);
            sp_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_other.setAdapter(sp_Adapter);

            if(psid_arr.contains(String.valueOf(admindata.get(p).getId()))){

                int ind=psid_arr.indexOf(String.valueOf(admindata.get(p).getId()));

               sp_other.setSelection(value.indexOf(userdata.get(ind).getInfo()));

               // sp_other.setSelected(false);

            }
            sp_other.setEnabled(false);
        }
        else if(admindata.get(p).getValue_type().equalsIgnoreCase("image")) {
            iv_other.setVisibility(View.VISIBLE);

            if(psid_arr.contains(String.valueOf(admindata.get(p).getId()))){

                int ind=psid_arr.indexOf(String.valueOf(admindata.get(p).getId()));

                String url=Constants.url + "other_document/" + userdata.get(ind).getInfo()+ ".png";
                url=url.replace(" ","%20");
                Log.e("other_image_url "+p,url);
                Picasso.with(c)
                        .load(url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(iv_other);
            }
            //Toast.makeText(c,"else if image",Toast.LENGTH_LONG).show();
        }
        else {
            et_other.setVisibility(View.VISIBLE);

            if(psid_arr.contains(String.valueOf(admindata.get(p).getId()))){

                int ind=psid_arr.indexOf(String.valueOf(admindata.get(p).getId()));
                et_other.setText(userdata.get(ind).getInfo());
                //sp_other.setSelection(value.indexOf(userdata.get(ind).getInfo()));
            }

            //Toast.makeText(c,"else text",Toast.LENGTH_LONG).show();
        }

        iv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.Value_Type=admindata.get(p).getValue_type();
                Constants.PSID=String.valueOf(admindata.get(p).getId());
                Constants.Info_Type=admindata.get(p).getInfo_type();
                Constants.Value_Selection=admindata.get(p).getValue_selection();
                Constants.Heading=admindata.get(p).getHeading();

                Other_info_insert basic_frag = new Other_info_insert();
                ((FragmentActivity)c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();

            }
        });
        lay_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.Value_Type=admindata.get(p).getValue_type();
                Constants.PSID=String.valueOf(admindata.get(p).getId());
                Constants.Info_Type=admindata.get(p).getInfo_type();
                Constants.Value_Selection=admindata.get(p).getValue_selection();
                Constants.Heading=admindata.get(p).getHeading();

                Other_info_insert basic_frag = new Other_info_insert();
                ((FragmentActivity)c).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();

            }
        });

        return view;
    }

}
