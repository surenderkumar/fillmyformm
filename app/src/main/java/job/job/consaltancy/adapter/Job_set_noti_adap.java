package job.job.consaltancy.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.fragment.Job_set_notification;

/**
 * Created by surender on 6/7/2016.
 */

public class Job_set_noti_adap extends BaseAdapter {

    Context c;

    SharedPreferences pref;
    SharedPreferences.Editor edit;

    private LayoutInflater inflater;
    ArrayList<String> joblist;

    public Job_set_noti_adap(Context c,ArrayList<String> joblist) {
        this.c = c;
        this.joblist=joblist;
        pref=c.getSharedPreferences("notification",1);
    }

    @Override
    public int getCount() {
        return joblist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int p, View convertView, ViewGroup parent) {
        inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.job_set_notification_listitem, parent, false);
        CheckBox cb=(CheckBox)v.findViewById(R.id.checkbox_id);

        cb.setText(joblist.get(p));

        if(Job_set_notification.selection.contains(String.valueOf(p)))
            cb.setChecked(true);

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    Job_set_notification.selection.add(String.valueOf(p));
                    Job_set_notification.selectionname.add(joblist.get(p));

                }
                else {
                    Job_set_notification.selection.remove(String.valueOf(p));
                    Job_set_notification.selectionname.remove(joblist.get(p));

                }
            }
        });

        return v;
    }
}
