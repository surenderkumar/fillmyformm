package job.job.consaltancy.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;


public class Category_Info_fragment extends Fragment implements View.OnClickListener {

	TextView tv_gender,tv_category,tv_disability,tv_suffering,tv_handwriting,tv_seeking,tv_domiciled,tv_religion,tv_religion_govt,tv_ex_ser_man,tv_dependent_serman,tv_dep_ex_serman,tv_child_freedomf;
	LinearLayout ly_gender,ly_category,ly_disability,ly_suffering,ly_handwriting,ly_seeking,ly_domiciled,ly_religion,ly_religion_govt,ly_ex_ser_man,ly_dependent_serman,ly_dep_ex_serman,ly_child_freedomf;
	LinearLayout lys_disability,lys_suffering,lys_handwriting,lys_ex_serman,lys_dep_ex_serman_detail,lys_child_freedom_detail;
	RelativeLayout main_layout;

	Spinner sp_gender,sp_category,sp_disability_cate,sp_disability_perc,sp_pwd_day,sp_pwd_month,sp_pwd_year,sp_religion;
	Spinner sp_serv_from_day,sp_serv_from_month,sp_serv_from_year,sp_serv_to_day,sp_serv_to_month,sp_serv_to_year;
	Spinner sp_servdis_day,sp_servdis_month,sp_servdis_year;
	EditText et_dateof_issue,et_sub_category,et_pwd_subcate,et_service_time,et_religion,et_relation_fighter;
    RadioButton rb_disability_yes,rb_disability_no,rb_suffering_yes,rb_suffering_no,rb_comp_time_yes,rb_comp_time_no;
    RadioButton rb_handwr_yes,rb_handwr_no,rb_seeking_yes,rb_seeking_no,rb_domicile_yes,rb_domicile_no,rb_religion_govt_yes,rb_religion_govt_no;
	RadioButton rb_ex_serman_yes,rb_ex_serman_no,rb_dis_ex_serman_yes,rb_dis_ex_serman_no,rb_dep_serman_yes,rb_dep_serman_no;
	RadioButton rb_dep_exservman_yes,rb_dep_exservman_no,rb_child_freedf_yes,rb_child_freedf_no;
	RadioButton rb_writing_time_yes,rb_writing_time_no;
	ArrayList<String> category_list,disability_cate_list,disability_perc_list,pwd_day_list,pwd_month_list,pwd_year_list;
	ArrayList<String> religion_list,gender_list;

	boolean b_gender=false,b_category=false,b_disable=false,b_sufferin=false,b_handwr=false,b_seeking=false,b_domicil=false;
	boolean b_religion=false,b_religion_gt=false,b_ex_serman=false,b_dep_serman=false,b_dep_exserman=false,b_child_freed=false;

	FloatingActionButton submit;
	EditText relation_xman;

	//--------Strings---------//

	String gender="",category="",sub_category="",disability="",type_disability="",per_disability="",pwd_sub_category="",pwd_date="",pwd_month="",pwd_year="";
	String cerebral_palsy,palsy_extra_time="",dominant_writing_hand="",dominant_extra_time="",relaxation_judicially="",domicile_jk="";
	String religion="",religious_minor="",ex_serviceman="",disable_exman="",period_of_service="",start_service_date="",start_service_month="",start_service_year="";
	String service_to_date="",service_to_month="",service_to_year="",end_date="",end_month="",end_year="";
	String killed_in_action="",dependent_on_exman="",relation_with_exman="",freedom_fighter="",relation_with_fighter="";
	String pwd_datee="",start_datee="",start_to_datee="",end_datee="",st_status="false";

	SharedPreferences user_info,detailprf;
	SharedPreferences.Editor editor,detailedt;

	NetworkConnection nw;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.fill_category_info, container,false);


		nw=new NetworkConnection(getActivity());

		user_info=getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		detailprf=getActivity().getSharedPreferences("detailprf", Context.MODE_PRIVATE);

		sp_gender=(Spinner)v.findViewById(R.id.sp_gender_id);
		sp_category=(Spinner)v.findViewById(R.id.sp_categoryid);   // sc,obc etc
		sp_disability_cate=(Spinner)v.findViewById(R.id.sp_disability_cate_id);
		sp_disability_perc=(Spinner)v.findViewById(R.id.sp_disability_percentage_id);
		sp_pwd_day=(Spinner)v.findViewById(R.id.sp_pwd_dayid);
		sp_pwd_month=(Spinner)v.findViewById(R.id.sp_pwd_monthsid);
		sp_pwd_year=(Spinner)v.findViewById(R.id.sp_pwd_yearid);
		sp_religion=(Spinner)v.findViewById(R.id.sp_religion_id);
		sp_serv_from_day=(Spinner)v.findViewById(R.id.sp_servicefrom_dayid);
		sp_serv_from_month=(Spinner)v.findViewById(R.id.sp_servicefrom_monthsid);
		sp_serv_from_year=(Spinner)v.findViewById(R.id.sp_servicefrom_yearid);
		sp_serv_to_day=(Spinner)v.findViewById(R.id.sp_serviceto_dayid);
		sp_serv_to_month=(Spinner)v.findViewById(R.id.sp_serviceto_monthsid);
		sp_serv_to_year=(Spinner)v.findViewById(R.id.sp_serviceto_yearid);
		sp_servdis_day=(Spinner)v.findViewById(R.id.sp_servicedischarge_dayid);
		sp_servdis_month=(Spinner)v.findViewById(R.id.sp_servicedischarge_monthsid);
		sp_servdis_year=(Spinner)v.findViewById(R.id.sp_servicedischarge_yearid);


		et_dateof_issue=(EditText)v.findViewById(R.id.et_dateofissueid);
		et_sub_category=(EditText)v.findViewById(R.id.et_subcategoryid);  // sc,bca etc
		et_pwd_subcate=(EditText)v.findViewById(R.id.et_pwd_sub_cateid);
		et_service_time=(EditText)v.findViewById(R.id.et_service_period_id);
		et_religion=(EditText)v.findViewById(R.id.et_religiion_id);
		et_relation_fighter=(EditText)v.findViewById(R.id.et_child_freedomf_id);
		relation_xman=(EditText)v.findViewById(R.id.et_relation_serviceman_id);

		rb_disability_yes=(RadioButton)v.findViewById(R.id.rb_disability_yesid);
		rb_disability_no=(RadioButton)v.findViewById(R.id.rb_disability_noid);
		rb_suffering_yes=(RadioButton)v.findViewById(R.id.rb_suffering_yesid);
		rb_suffering_no=(RadioButton)v.findViewById(R.id.rb_suffering_noid);
		rb_comp_time_yes=(RadioButton)v.findViewById(R.id.rb_comp_time_yesid);
		rb_comp_time_no=(RadioButton)v.findViewById(R.id.rb_comp_time_noid);
		rb_handwr_yes=(RadioButton)v.findViewById(R.id.rb_handwriting_yesid);
		rb_handwr_no=(RadioButton)v.findViewById(R.id.rb_handwriting_noid);
		rb_seeking_yes=(RadioButton)v.findViewById(R.id.rb_seeking_yesid);
		rb_seeking_no=(RadioButton)v.findViewById(R.id.rb_seeking_noid);
		rb_domicile_yes=(RadioButton)v.findViewById(R.id.rb_domicile_yesid);
		rb_domicile_no=(RadioButton)v.findViewById(R.id.rb_domicile_noid);
		rb_religion_govt_yes=(RadioButton)v.findViewById(R.id.rb_religios_govt_yesid);
		rb_religion_govt_no=(RadioButton)v.findViewById(R.id.rb_religios_govt_noid);
		rb_ex_serman_yes=(RadioButton)v.findViewById(R.id.rb_ex_serviceman_yesid);
		rb_ex_serman_no=(RadioButton)v.findViewById(R.id.rb_ex_serviceman_noid);
		rb_dis_ex_serman_yes=(RadioButton)v.findViewById(R.id.rb_disabled_ex_serviceman_yesid);
		rb_dis_ex_serman_no=(RadioButton)v.findViewById(R.id.rb_disabled_ex_serviceman_noid);
		rb_dep_serman_yes=(RadioButton)v.findViewById(R.id.rb_dependent_serviceman_yesid);
		rb_dep_serman_no=(RadioButton)v.findViewById(R.id.rb_dependent_serviceman_noid);
		rb_dep_exservman_yes=(RadioButton)v.findViewById(R.id.rb_dependent_ex_serviceman_yesid);
		rb_dep_exservman_no=(RadioButton)v.findViewById(R.id.rb_dependent_ex_serviceman_noid);
		rb_child_freedf_yes=(RadioButton)v.findViewById(R.id.rb_child_freedomf_yesid);
		rb_child_freedf_no=(RadioButton)v.findViewById(R.id.rb_child_freedomf_noid);
		rb_writing_time_yes=(RadioButton)v.findViewById(R.id.rb_handwriting_time_yesid);
		rb_writing_time_no=(RadioButton)v.findViewById(R.id.rb_handwriting_time_noid);

		tv_gender=(TextView)v.findViewById(R.id.gender_textviewid);
		tv_category=(TextView)v.findViewById(R.id.category_textviewid);
		tv_disability=(TextView)v.findViewById(R.id.disability_textviewid);
		tv_suffering=(TextView)v.findViewById(R.id.suffering_textviewid);
		tv_handwriting=(TextView)v.findViewById(R.id.handwriting_textviewid);
		tv_seeking=(TextView)v.findViewById(R.id.seeking_textviewid);
		tv_domiciled=(TextView)v.findViewById(R.id.domicile_textviewid);
		tv_religion=(TextView)v.findViewById(R.id.religion_textviewid);
		tv_religion_govt=(TextView)v.findViewById(R.id.religios_govt_textviewid);
		tv_ex_ser_man=(TextView)v.findViewById(R.id.ex_serviceman_textviewid);
		tv_dependent_serman=(TextView)v.findViewById(R.id.dependent_serviceman_textviewid);
		tv_dep_ex_serman=(TextView)v.findViewById(R.id.dependent_ex_serviceman_textviewid);
		tv_child_freedomf=(TextView)v.findViewById(R.id.child_freedomf_textviewid);

		main_layout=(RelativeLayout)v.findViewById(R.id.main_layout);
		ly_gender=(LinearLayout)v.findViewById(R.id.gender_layoutid);
		ly_category=(LinearLayout)v.findViewById(R.id.category_layoutid);
		ly_disability=(LinearLayout)v.findViewById(R.id.disability_layoutid);
		ly_suffering=(LinearLayout)v.findViewById(R.id.suffering_layoutid);
		ly_handwriting=(LinearLayout)v.findViewById(R.id.handwriting_layoutid);
		ly_seeking=(LinearLayout)v.findViewById(R.id.seeking_layoutid);
		ly_domiciled=(LinearLayout)v.findViewById(R.id.domicile_layoutid);
		ly_religion=(LinearLayout)v.findViewById(R.id.religion_layoutid);
		ly_religion_govt=(LinearLayout)v.findViewById(R.id.religios_govt_layoutid);
		ly_ex_ser_man=(LinearLayout)v.findViewById(R.id.ex_serviceman_layoutid);
		ly_dependent_serman=(LinearLayout)v.findViewById(R.id.dependent_serviceman_layoutid);
		ly_dep_ex_serman=(LinearLayout)v.findViewById(R.id.dependent_ex_serviceman_layoutid);
		ly_child_freedomf=(LinearLayout)v.findViewById(R.id.child_freedomf_layoutid);

		lys_disability=(LinearLayout)v.findViewById(R.id.disability_detaile_layoutid);
		lys_suffering=(LinearLayout)v.findViewById(R.id.suffering_detail_layoutid);
		lys_handwriting=(LinearLayout)v.findViewById(R.id.handwriting_detail_layoutid);
		lys_ex_serman=(LinearLayout)v.findViewById(R.id.ex_serviceman_detaile_layoutid);
		lys_dep_ex_serman_detail=(LinearLayout)v.findViewById(R.id.detail_relation_serviceman_layoutid);
		lys_child_freedom_detail=(LinearLayout)v.findViewById(R.id.detail_child_freedomf_layoutid);

		submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);



		gender_list=new ArrayList<>();
		gender_list.add("Select");
		gender_list.add("Male");
		gender_list.add("Female");

		ArrayAdapter<String> gend_dap = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, gender_list);
		gend_dap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_gender.setAdapter(gend_dap);

		category_list=new ArrayList<>();
		category_list.add("Select");
		category_list.add("General");
		category_list.add("OBC");
		category_list.add("SC");
		category_list.add("ST");

		ArrayAdapter<String> cate_dap = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, category_list);
		cate_dap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_category.setAdapter(cate_dap);

		sp_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				if(position>1)
					et_dateof_issue.setVisibility(View.VISIBLE);
				else
					et_dateof_issue.setVisibility(View.GONE);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});




		disability_cate_list=new ArrayList<>();
		disability_cate_list.add("Select");
		disability_cate_list.add("OS");
		disability_cate_list.add("VI");
		disability_cate_list.add("SI");

		ArrayAdapter<String> disability_adap = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, disability_cate_list);
		disability_adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_disability_cate.setAdapter(disability_adap);

		disability_perc_list=new ArrayList<>();
		disability_perc_list.add("30");
		disability_perc_list.add("40");
		disability_perc_list.add("More than 40");

		ArrayAdapter<String> disab_per_adap = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, disability_perc_list);
		disab_per_adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_disability_perc.setAdapter(disab_per_adap);

		religion_list=new ArrayList<>();
		religion_list.add("Select");
		religion_list.add("Buddhist");
		religion_list.add("Christian");
		religion_list.add("Hindu");
		religion_list.add("Jain");
		religion_list.add("Muslim");
		religion_list.add("Sikh");
		religion_list.add("Other");

		ArrayAdapter<String> religion_adap = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, religion_list);
		religion_adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_religion.setAdapter(religion_adap);


		sp_religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(religion_list.get(position).equalsIgnoreCase("Other")){
					et_religion.setVisibility(View.VISIBLE);
				}
				else
					et_religion.setVisibility(View.GONE);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});


		pwd_day_list=new ArrayList<>();
		pwd_month_list=new ArrayList<>();
		pwd_year_list=new ArrayList<>();

		for (int i=1;i<32;i++)
			pwd_day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			pwd_month_list.add(String.valueOf(i));
		for (int i=1970;i<2020;i++)
			pwd_year_list.add(String.valueOf(i));

		ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, pwd_day_list);
		dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_pwd_day.setAdapter(dayAdapter);
		sp_serv_from_day.setAdapter(dayAdapter);
		sp_serv_to_day.setAdapter(dayAdapter);
		sp_servdis_day.setAdapter(dayAdapter);

		ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, pwd_month_list);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_pwd_month.setAdapter(monthAdapter);
		sp_serv_from_month.setAdapter(monthAdapter);
		sp_serv_to_month.setAdapter(monthAdapter);
		sp_servdis_month.setAdapter((monthAdapter));

		ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, pwd_year_list);
		yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_pwd_year.setAdapter(yearAdapter);
		sp_serv_from_year.setAdapter(yearAdapter);
		sp_serv_to_year.setAdapter(yearAdapter);
		sp_servdis_year.setAdapter(yearAdapter);


		tv_gender.setOnClickListener(this);
		tv_category.setOnClickListener(this);
		tv_disability.setOnClickListener(this);
		tv_suffering.setOnClickListener(this);
		tv_handwriting.setOnClickListener(this);
		tv_seeking.setOnClickListener(this);
		tv_domiciled.setOnClickListener(this);
		tv_religion.setOnClickListener(this);
		tv_religion_govt.setOnClickListener(this);
		tv_ex_ser_man.setOnClickListener(this);
		tv_dependent_serman.setOnClickListener(this);
		tv_dep_ex_serman.setOnClickListener(this);
		tv_child_freedomf.setOnClickListener(this);

//		ly_h_gender.setOnClickListener(this);
//		ly_h_category.setOnClickListener(this);
//		ly_h_disability.setOnClickListener(this);
//		ly_h_suffering.setOnClickListener(this);
//		ly_h_handwriting.setOnClickListener(this);
//		ly_h_seeking.setOnClickListener(this);
//		ly_h_domiciled.setOnClickListener(this);
//		ly_h_religion.setOnClickListener(this);
//		ly_h_religion_govt.setOnClickListener(this);
//		ly_h_ex_ser_man.setOnClickListener(this);
//		ly_h_dependent_serman.setOnClickListener(this);
//		ly_h_dep_ex_serman.setOnClickListener(this);
//		ly_h_child_freedomf.setOnClickListener(this);


		rb_disability_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_disability.setVisibility(View.VISIBLE);

			}
		});
		rb_disability_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_disability.setVisibility(View.GONE);
			}
		});
		rb_suffering_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_suffering.setVisibility(View.VISIBLE);

			}
		});
		rb_suffering_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_suffering.setVisibility(View.GONE);
			}
		});

		rb_suffering_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_suffering.setVisibility(View.VISIBLE);

			}
		});
		rb_suffering_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_suffering.setVisibility(View.GONE);
			}
		});
		rb_handwr_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_handwriting.setVisibility(View.VISIBLE);

			}
		});
		rb_handwr_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_handwriting.setVisibility(View.GONE);
			}
		});

		rb_ex_serman_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_ex_serman.setVisibility(View.VISIBLE);

			}
		});
		rb_ex_serman_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_ex_serman.setVisibility(View.GONE);
			}
		});
		rb_dep_exservman_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_dep_ex_serman_detail.setVisibility(View.VISIBLE);
			}
		});
		rb_dep_exservman_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_dep_ex_serman_detail.setVisibility(View.GONE);
			}
		});
		rb_child_freedf_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
					lys_child_freedom_detail.setVisibility(View.VISIBLE);
			}
		});
		rb_child_freedf_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_child_freedom_detail.setVisibility(View.GONE);
			}
		});


		String cate_info=user_info.getString("category_info","");
		Log.e("disability", cate_info);
		if(cate_info.length()>0) {

			String gen,cate,type_disable,per_disable,pwd_dat,relign,ser_st_dt,ser_to_dt,ser_dis_dt;
            String[] dt_ar;
			int ind=0;
			try {
				JSONObject jobj=new JSONObject(cate_info);

				gen=jobj.getString("gender");
				if(gender_list.contains(gen)) {
				ind=gender_list.indexOf(gen);
					sp_gender.setSelection(ind);
				}

				cate=jobj.getString("category");
				if(category_list.contains(cate)) {
					ind=category_list.indexOf(cate);
					sp_category.setSelection(ind);
				}

				type_disable=jobj.getString("type_disability");
				if(disability_cate_list.contains(type_disable)) {
					ind=disability_cate_list.indexOf(type_disable);
					sp_disability_cate.setSelection(ind);
				}

				per_disable=jobj.getString("per_disability");
				if(disability_perc_list.contains(per_disable)) {
					ind=disability_perc_list.indexOf(per_disable);
					sp_disability_perc.setSelection(ind);
				}

				relign=jobj.getString("religion");
				if(religion_list.contains(relign)) {
					ind=religion_list.indexOf(relign);
					sp_religion.setSelection(ind);
				}

				pwd_dat=jobj.getString("pwd_date");
				dt_ar=pwd_dat.split("-");

				ind=pwd_year_list.indexOf(dt_ar[0]);
				sp_pwd_year.setSelection(ind);

				ind=pwd_month_list.indexOf(dt_ar[1]);
				sp_pwd_month.setSelection(ind);

				ind=pwd_day_list.indexOf(dt_ar[2]);
				sp_pwd_day.setSelection(ind);


				ser_st_dt=jobj.getString("ser_start_date");
				dt_ar=ser_st_dt.split("-");

				ind=pwd_year_list.indexOf(dt_ar[0]);
				sp_serv_from_year.setSelection(ind);

				ind=pwd_month_list.indexOf(dt_ar[1]);
				sp_serv_from_month.setSelection(ind);

				ind=pwd_day_list.indexOf(dt_ar[2]);
				sp_serv_from_day.setSelection(ind);


				ser_to_dt=jobj.getString("ser_to_date");
				dt_ar=ser_to_dt.split("-");

				ind=pwd_year_list.indexOf(dt_ar[0]);
				sp_serv_to_year.setSelection(ind);

				ind=pwd_month_list.indexOf(dt_ar[1]);
				sp_serv_to_month.setSelection(ind);

				ind=pwd_day_list.indexOf(dt_ar[2]);
				sp_serv_to_day.setSelection(ind);


				ser_dis_dt=jobj.getString("ser_discharge_date");
				dt_ar=ser_dis_dt.split("-");

				ind=pwd_year_list.indexOf(dt_ar[0]);
				sp_servdis_year.setSelection(ind);

				ind=pwd_month_list.indexOf(dt_ar[1]);
				sp_servdis_month.setSelection(ind);

				ind=pwd_day_list.indexOf(dt_ar[2]);
				sp_servdis_day.setSelection(ind);

				et_dateof_issue.setText(jobj.getString("date_of_issue"));
				et_sub_category.setText(jobj.getString("sub_cate"));
				et_pwd_subcate.setText(jobj.getString("pwd_subcat"));
				et_religion.setText(jobj.getString("religion"));
				et_service_time.setText(jobj.getString("period_of_service"));
				relation_xman.setText(jobj.getString("relation_with_exman"));
				et_relation_fighter.setText(jobj.getString("relation_with_fighter"));

				Log.e("disability", "glob ");

				if(jobj.getString("disability").equalsIgnoreCase("yes")==true){
					Log.e("disability","yes "+jobj.getString("disability"));
					rb_disability_yes.setChecked(true);
				}
				else {
					Log.e("disability","no "+jobj.getString("disability"));
					rb_disability_no.setChecked(true);
				}
				if(jobj.getString("cerabral").equalsIgnoreCase("yes")==true){
					rb_suffering_yes.setChecked(true);
				}
				else
					rb_suffering_no.setChecked(true);

				if(jobj.getString("palsy_extra").equalsIgnoreCase("yes")==true){
					rb_comp_time_yes.setChecked(true);
				}
				else
					rb_comp_time_no.setChecked(true);

				if(jobj.getString("dominant_writing").equalsIgnoreCase("yes")==true){
					rb_handwr_yes.setChecked(true);
				}
				else
					rb_handwr_no.setChecked(true);

				if(jobj.getString("dominant_extra").equalsIgnoreCase("yes")==true){
					rb_writing_time_yes.setChecked(true);
				}
				else
					rb_writing_time_no.setChecked(true);

				if(jobj.getString("judicially_relax").equalsIgnoreCase("yes")==true){
					rb_seeking_yes.setChecked(true);
				}
				else
					rb_seeking_no.setChecked(true);

				if(jobj.getString("domicile_jk").equalsIgnoreCase("yes")==true){
					rb_domicile_yes.setChecked(true);
				}
				else
					rb_domicile_no.setChecked(true);

				if(jobj.getString("religious_minor").equalsIgnoreCase("yes")==true){
					rb_religion_govt_yes.setChecked(true);
				}
				else
					rb_religion_govt_no.setChecked(true);

				if(jobj.getString("ex_serviceman").equalsIgnoreCase("yes")==true){
					rb_ex_serman_yes.setChecked(true);
				}
				else
					rb_ex_serman_no.setChecked(true);

				if(jobj.getString("disable_exman").equalsIgnoreCase("yes")==true){
					rb_dis_ex_serman_yes.setChecked(true);
				}
				else
					rb_dis_ex_serman_no.setChecked(true);

				if(jobj.getString("dep_serman_killed").equalsIgnoreCase("yes")==true){
					rb_dep_serman_yes.setChecked(true);
				}
				else
					rb_dep_serman_no.setChecked(true);

				if(jobj.getString("dep_serman_state").equalsIgnoreCase("yes")==true){
					rb_dep_exservman_yes.setChecked(true);
				}
				else
					rb_dep_exservman_no.setChecked(true);

				if(jobj.getString("freedom_fighter").equalsIgnoreCase("yes")==true){
					rb_child_freedf_yes.setChecked(true);
				}
				else
					rb_child_freedf_no.setChecked(true);




			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("jsonexception",e.getMessage().toString());
			}
		}

		return v;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==R.id.gender_textviewid) {
			if(b_gender) {
				ly_gender.setVisibility(View.GONE);
				b_gender=false;
				tv_gender.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_gender.setVisibility(View.VISIBLE);
				b_gender=true;
				tv_gender.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.category_textviewid) {
			if(b_category) {
				ly_category.setVisibility(View.GONE);
				b_category=false;
				tv_category.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_category.setVisibility(View.VISIBLE);
				b_category=true;
				tv_category.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}

			//ly_category.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.disability_textviewid) {
			if(b_disable) {
				ly_disability.setVisibility(View.GONE);
				b_disable=false;
				tv_disability.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_disability.setVisibility(View.VISIBLE);
				b_disable=true;
				tv_disability.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_disability.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.suffering_textviewid) {
			if(b_sufferin) {
				ly_suffering.setVisibility(View.GONE);
				b_sufferin=false;
				tv_suffering.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_suffering.setVisibility(View.VISIBLE);
				b_sufferin=true;
				tv_suffering.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_suffering.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.handwriting_textviewid) {
			if(b_handwr) {
				ly_handwriting.setVisibility(View.GONE);
				b_handwr=false;
				tv_handwriting.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_handwriting.setVisibility(View.VISIBLE);
				b_handwr=true;
				tv_handwriting.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_handwriting.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.seeking_textviewid) {

			if(b_seeking) {
				ly_seeking.setVisibility(View.GONE);
				b_seeking=false;
				tv_seeking.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_seeking.setVisibility(View.VISIBLE);
				b_seeking=true;
				tv_seeking.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_seeking.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.domicile_textviewid) {
			if(b_domicil) {
				ly_domiciled.setVisibility(View.GONE);
				b_domicil=false;
				tv_domiciled.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_domiciled.setVisibility(View.VISIBLE);
				b_domicil=true;
				tv_domiciled.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_domiciled.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.religion_textviewid) {
			if(b_religion) {
				ly_religion.setVisibility(View.GONE);
				b_religion=false;
				tv_religion.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_religion.setVisibility(View.VISIBLE);
				b_religion=true;
				tv_religion.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_religion.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.religios_govt_textviewid) {
			if(b_religion_gt) {
				ly_religion_govt.setVisibility(View.GONE);
				b_religion_gt=false;
				tv_religion_govt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_religion_govt.setVisibility(View.VISIBLE);
				b_religion_gt=true;
				tv_religion_govt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_religion_govt.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.ex_serviceman_textviewid) {
			if(b_ex_serman) {
				ly_ex_ser_man.setVisibility(View.GONE);
				b_ex_serman=false;
				tv_ex_ser_man.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_ex_ser_man.setVisibility(View.VISIBLE);
				b_ex_serman=true;
				tv_ex_ser_man.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_ex_ser_man.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.dependent_serviceman_textviewid) {
			if(b_dep_serman) {
				ly_dependent_serman.setVisibility(View.GONE);
				b_dep_serman=false;
				tv_dependent_serman.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_dependent_serman.setVisibility(View.VISIBLE);
				b_dep_serman=true;
				tv_dependent_serman.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_dependent_serman.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.dependent_ex_serviceman_textviewid) {
			if(b_dep_exserman) {
				ly_dep_ex_serman.setVisibility(View.GONE);
				b_dep_exserman=false;
				tv_dep_ex_serman.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_dep_ex_serman.setVisibility(View.VISIBLE);
				b_dep_exserman=true;
				tv_dep_ex_serman.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_dep_ex_serman.setVisibility(View.VISIBLE);
		}
		else if(v.getId()==R.id.child_freedomf_textviewid) {
			if(b_child_freed) {
				ly_child_freedomf.setVisibility(View.GONE);
				b_child_freed=false;
				tv_child_freedomf.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_child_freedomf.setVisibility(View.VISIBLE);
				b_child_freed=true;
				tv_child_freedomf.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
			//ly_child_freedomf.setVisibility(View.VISIBLE);
		}

//		else if(v.getId()==R.id.ly_h_genderid){
//			hindidialog("लिंग");
//		}
//		else if(v.getId()==R.id.ly_h_categoryid){
//			hindidialog("श्रेणी");
//		}
//		else if(v.getId()==R.id.ly_h_disabilityid){
//			hindidialog("आप विकलांगता के साथ एक व्यक्ति हैं?");
//		}
//		else if(v.getId()==R.id.ly_h_suggeringid){
//			hindidialog("क्या आप मस्तिष्क पक्षाघात से पीड़ित है और अपने लेखन की गति प्रभावित है?");
//		}
//		else if(v.getId()==R.id.ly_h_handwritingid){
//			hindidialog("क्या आपका प्रमुख हाथ(हाथ लेखन) प्रभावित और आपकी लेखन की गति प्रभावित है?");
//		}
//		else if(v.getId()==R.id.ly_h_seekingid){
//			hindidialog("क्या आप विधवा/ तलाकशुदा महिला/ महिला जो न्यायिक अपने पति से अलग रहती हैं और दोबारा शादी नहीं  की हैं, के तहत छूट की मांग कर रहे हैं?");
//		}
//		else if(v.getId()==R.id.ly_h_domocileid){
//			hindidialog("क्या आप, अवधि 31-12-1989 से 1980/01/01  के दौरान जम्मू एवं कश्मीर राज्य में अधिवासित रहे हैं?");
//		}
//		else if(v.getId()==R.id.ly_h_religionid){
//			hindidialog("आपका धर्म?");
//		}
//		else if(v.getId()==R.id.ly_h_religios_govtid){
//			hindidialog("क्या आप धार्मिक अल्पसंख्यक समुदाय में हैं ? (सरकार के आदेश के अनुसार) ");
//		}
//		else if(v.getId()==R.id.ly_h_ex_ser_manid){
//			hindidialog("क्या आप एक भूतपूर्व सैनिक हैं?");
//		}
//		else if(v.getId()==R.id.ly_h_dep_ser_manid){
//			hindidialog("क्या आप में मारे गए सैनिक पर निर्भर कर रहे हैं?");
//		}
//		else if(v.getId()==R.id.ly_h_dep_ex_ser_manid){
//			hindidialog("क्या आप ,अपने राज्य के पूर्व सेवा आदमी के आश्रित है?");
//		}
//		else if(v.getId()==R.id.ly_h_child_freedomfid){
//			hindidialog("अपने राज्य के स्वतंत्रता सेनानी के बच्चे / ग्रैंड बच्चा है?");
//		}


		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				gender=sp_gender.getSelectedItem().toString();

				if(gender.equalsIgnoreCase("Select")==true)
					gender="";

				category=sp_category.getSelectedItem().toString();

				if(category.equalsIgnoreCase("Select")==true)
					category="";

				sub_category=et_sub_category.getText().toString();


				disability=rb_disability_yes.isChecked()?"yes":"no";

				type_disability=sp_disability_cate.getSelectedItem().toString();
				if(type_disability.equalsIgnoreCase("Select")==true)
					type_disability="";

				per_disability=sp_disability_perc.getSelectedItem().toString();
				if(per_disability.equalsIgnoreCase("Select")==true)
					per_disability="";

				pwd_sub_category=et_pwd_subcate.getText().toString();

				pwd_date=sp_pwd_day.getSelectedItem().toString();
				pwd_month=sp_pwd_month.getSelectedItem().toString();
				pwd_year=sp_pwd_year.getSelectedItem().toString();

				pwd_datee=pwd_year+"-"+pwd_month+"-"+pwd_date;

				cerebral_palsy=rb_suffering_yes.isChecked()?"yes":"no";
				palsy_extra_time=rb_comp_time_yes.isChecked()?"yes":"no";
				dominant_writing_hand=rb_handwr_yes.isChecked()?"yes":"no";
				dominant_extra_time=rb_writing_time_yes.isChecked()?"yes":"no";
				relaxation_judicially=rb_seeking_yes.isChecked()?"yes":"no";
				domicile_jk=rb_domicile_yes.isChecked()?"yes":"no";

				religion=sp_religion.getSelectedItem().toString();

				if(religion.equalsIgnoreCase("Select")==true)
					religion="";

				religious_minor=rb_religion_govt_yes.isChecked()?"yes":"no";
				ex_serviceman=rb_ex_serman_yes.isChecked()?"yes":"no";
				disable_exman=rb_dis_ex_serman_yes.isChecked()?"yes":"no";
				period_of_service=et_service_time.getText().toString();

				start_service_date=sp_serv_from_day.getSelectedItem().toString();
				start_service_month=sp_serv_from_month.getSelectedItem().toString();
				start_service_year=sp_serv_from_year.getSelectedItem().toString();

				start_datee=start_service_year+"-"+start_service_month+"-"+start_service_date;

				service_to_date=sp_serv_to_day.getSelectedItem().toString();
				service_to_month=sp_serv_to_month.getSelectedItem().toString();
				service_to_year=sp_serv_to_year.getSelectedItem().toString();

				start_to_datee=service_to_year+"-"+service_to_month+"-"+service_to_date;

				end_date=sp_servdis_day.getSelectedItem().toString();
				end_month=sp_servdis_month.getSelectedItem().toString();
				end_year=sp_servdis_year.getSelectedItem().toString();

				end_datee=end_year+"-"+end_month+"-"+end_date;

				killed_in_action=rb_dep_serman_yes.isChecked()?"yes":"no";
				dependent_on_exman=rb_dep_exservman_yes.isChecked()?"yes":"no";
				relation_with_exman=relation_xman.getText().toString();
				freedom_fighter=rb_child_freedf_yes.isChecked()?"yes":"no";
				relation_with_fighter=et_relation_fighter.getText().toString();

                if(nw.isConnected())
				volley();
				else
				{
					final Snackbar snackbar = Snackbar.make(main_layout, "   No Internet Connection.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
				}
//					Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_LONG).show();
			}
		});
	}
	public void volley()
	{

		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(true);
		pd.setMessage("Loading Please Wait");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		detailedt=detailprf.edit();

		RequestQueue queue= Volley.newRequestQueue(getActivity());
		String url;

		if(gender.length()>0&category.length()>0&religion.length()>0)
			st_status="true";


       if(gender.length()>0&category.length()>0&sub_category.length()>0&religion.length()>0){
		   detailedt.putBoolean("category_info",true);
	   }
		else
		   detailedt.putBoolean("category_info",false);


		url= Constants.url+"user_catinfo.php?unique_id="+Constants.Unique_Id+"&gender="+gender+"&category="+category+
				"&sub_category="+sub_category+"&disability="+disability+"&type_disability="+type_disability+
				"&per_disability="+per_disability+"&pwd_subcat="+pwd_sub_category+"&pwd_date="+pwd_datee+
				"&cerabral="+cerebral_palsy+"&palsy_extra="+palsy_extra_time+"&dominant_writing="+
				dominant_writing_hand+"&dominant_extra="+dominant_extra_time+"&judicially_relax="+relaxation_judicially+
				"&jk_dis_exservice="+domicile_jk+"&religion="+religion+"&religious_minor="+religious_minor+"&ex_service="+
				ex_serviceman+"&dis_exservice="+disable_exman+"&p_o_service="+period_of_service+"&s_s_date="+start_datee+
				"&s_to_date="+start_to_datee+"&s_end_date="+end_datee+"&killed_action="+killed_in_action+"&dept_of_exman="+
				dependent_on_exman+"&relation_with_exser_man="+relation_with_exman+"&freedom_fighter="+freedom_fighter+
				"&relation_fighter="+relation_with_fighter+"&status="+st_status+"&date_of_issue="+et_dateof_issue.getText().toString();

		url = url.replace(" ", "%20");
		Log.e("emailcheckvolleyurl", url);
		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				pd.dismiss();
				Log.e("loginres", res.toString());
				try {
					//if(res.getString("scalar").equals("Successfully Inserted")) {

						editor=user_info.edit();

                      JSONObject jobj=new JSONObject();

						jobj.put("gender",gender);
						jobj.put("category",category);
						jobj.put("sub_cate",sub_category);
					    jobj.put("date_of_issue",et_dateof_issue.getText().toString());
						jobj.put("disability",disability);
						jobj.put("type_disability",type_disability);
						jobj.put("per_disability",per_disability);
						jobj.put("pwd_subcat",pwd_sub_category);
						jobj.put("pwd_date",pwd_datee);
						jobj.put("cerabral",cerebral_palsy);
						jobj.put("palsy_extra",palsy_extra_time);
						jobj.put("dominant_writing",dominant_writing_hand);
						jobj.put("dominant_extra",dominant_extra_time);
						jobj.put("judicially_relax",relaxation_judicially);
						jobj.put("domicile_jk",domicile_jk);
						jobj.put("religion",religion);
						jobj.put("religious_minor",religious_minor);
						jobj.put("ex_serviceman",ex_serviceman);
						jobj.put("disable_exman",disable_exman);
						jobj.put("period_of_service",period_of_service);
						jobj.put("ser_start_date",start_datee);
						jobj.put("ser_to_date",start_to_datee);
						jobj.put("ser_discharge_date",end_datee);
						jobj.put("dep_serman_killed",killed_in_action);
						jobj.put("dep_serman_state",dependent_on_exman);
						jobj.put("relation_with_exman",relation_with_exman);
						jobj.put("freedom_fighter",freedom_fighter);
						jobj.put("relation_with_fighter", relation_with_fighter);

						editor.putString("category_info", jobj.toString());
						editor.commit();


					if(st_status.equalsIgnoreCase("true")==true) {
						detailedt.putBoolean("category",true);
					}
					else
						detailedt.putBoolean("category",false);


					detailedt.commit();


						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,new Home_fragment() ).commit();
					final Snackbar snackbar = Snackbar.make(main_layout, "Successfully Inserted.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
//					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
//						Toast.makeText(getActivity(),"Successfully Inserted",Toast.LENGTH_SHORT).show();
					//}

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("exceptionlogin",e.toString());
					pd.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError e) {

				pd.dismiss();

				final Snackbar snackbar = Snackbar.make(main_layout, "   Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();


//				Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
				Log.e("error", e.toString());
			}
		});
		int socketTimeout = 50000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		jsonreq.setRetryPolicy(policy);
		queue.add(jsonreq);
	}

	public void hindidialog(String text) {

		final Dialog dialog=new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.hindi_dialog);

		//dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		dialog.show();

		TextView tv_head=(TextView)dialog.findViewById(R.id.hindi_textid);

		TextView ok=(TextView)dialog.findViewById(R.id.dialog_ok);

		tv_head.setText(text);

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});
	}

}