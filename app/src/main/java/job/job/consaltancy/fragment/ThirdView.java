package job.job.consaltancy.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import job.job.consaltancy.R;

/**
 * Created by Dinesh Kukreja on 29-09-2015.
 */
public class ThirdView extends Fragment
{
    ImageView im;
    boolean b;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.firstview,container,false);
        im=(ImageView)view.findViewById(R.id.imageView7);
        im.setImageResource(R.drawable.banner3);

//        if(Constants.B3!=null) {
//            Log.e("bitmap", Constants.B3.toString());
//            im.setImageBitmap(Constants.B3);
//        }
//
//        im.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if(Constants.Link.size()>0) {
//                    String url = Constants.Link.get(0);
//
//                    if (url.length() > 0)
//                        b = URLIsReachable(url);
//
//                    if (b) {
//                        Intent in = new Intent(Intent.ACTION_VIEW);
//                        in.setData(Uri.parse(url));
//                        startActivity(in);
//                    } else {
//                        //Toast.makeText(getActivity(), "Link not valid", Toast.LENGTH_LONG).show();
//                    }
//                }
//
//            }
//        });

        return view;
    }

    public boolean URLIsReachable(String urlString)
    {

        try
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            int responseCode = urlConnection.getResponseCode();
            urlConnection.disconnect();
            // Toast.makeText(c,"code= "+responseCode,Toast.LENGTH_LONG).show();
            if(responseCode==200)
                return true;
            else
                return false;
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
            return false;
        } catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
