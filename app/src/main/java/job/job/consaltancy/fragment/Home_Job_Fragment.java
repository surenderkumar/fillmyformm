package job.job.consaltancy.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Job_home_adapter;
import job.job.consaltancy.getset.job_getset;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Home_Job_Fragment extends Fragment {

    Job_home_adapter adapter;
    RecyclerView recyclerView;
    RelativeLayout processing_lay, applied_lay;
    ArrayList<job_getset> jobarray;
    String cate_info,unique_id;
    SharedPreferences user_info, pref;
    SharedPreferences.Editor user_editor;
    Boolean expire;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.job_home_fragment, container, false);

        setHasOptionsMenu(true);

        user_info = getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
        pref = getActivity().getSharedPreferences("notification", Context.MODE_PRIVATE);
        cate_info = user_info.getString("category_info", "");
        unique_id=user_info.getString("unique_id","");
        Log.e("unique_id",unique_id+"");

        processing_lay = (RelativeLayout) v.findViewById(R.id.processing_layoutid);
        applied_lay = (RelativeLayout) v.findViewById(R.id.applied_layoutid);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);


        // HomeActivity.tv_point.setText("Rs. "+String.valueOf(Constants.Wallet_Money));

//        adapter=new Job_home_adapter(getActivity(),jobarray);
//        recyclerView.setAdapter(adapter);

        // Constants.Job_Query=pref.getString("job_query","");

        Constants.fragment = 1;
        volley();


        processing_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // HomeActivity.et_search.setVisibility(View.GONE);
                HomeActivity.tv_appname.setVisibility(View.VISIBLE);

                Jobs_in_processing acc_frag = new Jobs_in_processing();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, acc_frag).addToBackStack(null).commit();

            }
        });

        applied_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // HomeActivity.et_search.setVisibility(View.GONE);
                HomeActivity.tv_appname.setVisibility(View.VISIBLE);

                Applied_job_fragment acc_frag = new Applied_job_fragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, acc_frag).addToBackStack(null).commit();

            }
        });


        String cate_info = user_info.getString("category_info", "");
        if (cate_info.length() > 0) {
            try {
                JSONObject jobj = new JSONObject(cate_info);
                Constants.Gender = jobj.getString("gender");
                Constants.Category = jobj.getString("category");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        inflater.inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.search);

        final SearchView searchView = new SearchView(((HomeActivity) getActivity()).getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search by Form name");

        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final ArrayList<job_getset> filteredModelList = filter(jobarray, newText);

                adapter.setFilter(filteredModelList);
                recyclerView.setVisibility((adapter.getItemCount() == 0) ? View.GONE : View.VISIBLE);
                return true;
            }
        });

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    private ArrayList<job_getset> filter(ArrayList<job_getset> models, String query) {
        query = query.toLowerCase();

        final ArrayList<job_getset> filteredModelList = new ArrayList<>();
        for (job_getset model : models) {
            final String text = model.getCompany_name().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void volley() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
//        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        String url = Constants.url + "jobs_api.php?unique_id="+unique_id;

        url = url.replaceAll(" ", "%20");

        Log.e("job_url= ", url);

        jobarray = new ArrayList<job_getset>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                try {
                    if (res.has("job")) {
                        JSONArray array = res.getJSONArray("job");

                        int len = array.length();

                        Log.e("length=", String.valueOf(len));

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            ArrayList<String> post = new ArrayList<>();
                            ArrayList<String> lastdate = new ArrayList<>();
                            ArrayList<String> examcenter = new ArrayList<>();
                            ArrayList<String> category = new ArrayList<>();
                            ArrayList<String> category_fee = new ArrayList<>();
                            ArrayList<String> our_charges = new ArrayList<>();
                            ArrayList<String> paid_wallet = new ArrayList<>();

                            String bc = obj.getString("post_name");
                            if (bc.contains(",")) {
                                String[] bcarr = bc.split(",");
                                for (int j = 0; j < bcarr.length; j++)
                                    post.add(bcarr[j]);
                            } else
                                post.add(bc);

//                              String prf = obj.getString("lastdate");
//                              if (prf.contains(",")) {
//                                  String[] prarr = prf.split(",");
//                                  for (int j = 0; j < prarr.length; j++)
//                                      lastdate.add(prarr[j]);
//                              } else
//                                  lastdate.add(prf);

                            String ec = obj.getString("reference");
                            if (ec.contains(",")) {
                                String[] prarr = ec.split(",");
                                for (int j = 0; j < prarr.length; j++)
                                    examcenter.add(prarr[j]);
                            } else
                                examcenter.add(ec);

                            String cc = obj.getString("category");
                            if (cc.contains(",")) {
                                String[] prarr = cc.split(",");
                                for (int j = 0; j < prarr.length; j++)
                                    category.add(prarr[j]);
                            } else
                                category.add(cc);

                            String cf = obj.getString("fees");
                            if (cf.contains(",")) {
                                String[] prarr = cf.split(",");
                                for (int j = 0; j < prarr.length; j++)
                                    category_fee.add(prarr[j]);
                            } else
                                category_fee.add(cf);

                            String oc = obj.getString("our_charges");
                            if (oc.contains(",")) {
                                String[] prarr = oc.split(",");
                                for (int j = 0; j < prarr.length; j++)
                                    our_charges.add(prarr[j]);
                            } else
                                our_charges.add(oc);

                            String pw = obj.getString("paid_wallet");
                            if (pw.contains(",")) {
                                String[] prarr = pw.split(",");
                                for (int j = 0; j < prarr.length; j++)
                                    paid_wallet.add(prarr[j]);
                            } else
                                paid_wallet.add(pw);




                            if (i == 0) {
                                user_editor = user_info.edit();
                                user_editor.putInt("job_lastid", obj.getInt("id"));
                                user_editor.commit();
                            }


                            expire = timediff(obj.getString("lastdate"));

                            jobarray.add(new job_getset(obj.getInt("id"), obj.getString("job_type"), obj.getString("company_name"),
                                    obj.getString("detail"), obj.getString("notification"),
                                    our_charges, paid_wallet, post, obj.getString("lastdate"), examcenter, expire,category, obj.getString("hint"), obj.getString("center_hint"),category_fee));

                            Log.e("job_type=", obj.getString("post_name") + " " + lastdate.toString()+" "+category);
                        }

                        adapter = new Job_home_adapter(getActivity(), jobarray);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setVisibility((adapter.getItemCount() == 0) ? View.GONE : View.VISIBLE);

//                        dialog.dismiss();

                    }
                    if (res.has("total")) {
                        JSONArray array = res.getJSONArray("total");
                        JSONObject obj = array.getJSONObject(0);
                        Constants.Count = Integer.parseInt(obj.getString("count"));

                    }
                } catch (JSONException e) {
                    Log.e("catch exp= ", e.toString());
//                    dialog.dismiss();
                    e.printStackTrace();
                }

            }

        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
//                dialog.dismiss();

                String error = arg0.toString();
                if (error.contains("NoConnectionError")) {
                    showSnackbar("Oops! No Internet Connection");
//                    Toast.makeText(getActivity(), "No Connection Error", Toast.LENGTH_LONG).show();
                } else
                    showSnackbar("No Data, We Update Soon");
//                    Toast.makeText(getActivity(), "No data, We Update Soon", Toast.LENGTH_LONG).show();

                Log.e("error", arg0.toString());
            }
        });
        queue.add(request);
    }


    private void showSnackbar(String text) {

//        InputMethodManager imm = (InputMethodManager) getActivity()).getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(login.getWindowToken(), 0);
        final MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.notify);

        final Snackbar snackbar = Snackbar.make(recyclerView, "  " + text, Snackbar.LENGTH_LONG);
//        snackbar.setAction("OK", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                    }
//                });
//        snackbar.setActionTextColor(Color.DKGRAY);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
        textView.setGravity(Gravity.CENTER);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
        textView.setCompoundDrawablePadding(0);

        snackbar.show(); // Don’t forget to show!
        mp.start();

    }

    public Boolean timediff(String val) {

        Calendar cal = Calendar.getInstance();

        String dt = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH);

//        Date d = new Date(dt);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
//        String time1 = sdf.format(d);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(dateFormat.format(cal.getTime()));

        String time1 = dateFormat.format(cal.getTime());

        if (time1.contains("-"))
            dt = time1.replaceAll("-", "");
        else
            dt = time1;

        String val1 = "0";
        if (val.contains("-"))
            val1 = val.replaceAll("-", "");

        Log.e("date_diff", val1 + "  " + dt);

        long pdt = Long.valueOf(val1);
        long cdt = Long.valueOf(dt);

        Log.e("date_difflong", pdt + "  " + cdt);

        if (pdt < cdt)
            return false;
        else
            return true;
    }

}
