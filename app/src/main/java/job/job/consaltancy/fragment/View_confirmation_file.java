package job.job.consaltancy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class View_confirmation_file extends AppCompatActivity {

    WebView wv;
    String urll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_confirmation_file);

        wv=(WebView)findViewById(R.id.webview_id);

        urll=getIntent().getStringExtra("url");
        wv.setWebViewClient(new Callback());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);

        //String pdf=Constants.url+"fee_receipt/"+"list-of-countries-in-the-world.pdf";

        wv.loadUrl("http://docs.google.com/gview?embedded=true&url="+urll);

    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v =inflater.inflate(R.layout.view_confirmation_file, container,false);
//
//        wv=(WebView)v.findViewById(R.id.webview_id);
//        wv.setWebViewClient(new Callback());
//        wv.getSettings().setJavaScriptEnabled(true);
//        wv.getSettings().setBuiltInZoomControls(true);
//        String pdf=Constants.url+"fee_receipt/"+"list-of-countries-in-the-world.pdf";
//        wv.loadUrl("http://docs.google.com/gview?embedded=true&url="+pdf);
//
//        return v;
//    }
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }

}
