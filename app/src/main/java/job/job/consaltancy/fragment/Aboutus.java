package job.job.consaltancy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Aboutus extends AppCompatActivity {

    WebView wv;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_confirmation_file);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About Us");


        wv=(WebView)findViewById(R.id.webview_id);

        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
        wv.loadData(Constants.About_US, "text/html; charset=UTF-8", null);
        wv.loadDataWithBaseURL(null, Constants.About_US, "text/html", "utf-8",null);

    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v =inflater.inflate(R.layout.webview_detail, container,false);
//
//        wv=(WebView)v.findViewById(R.id.webview_id);
//        tv_head=(TextView)v.findViewById(R.id.tv_heading_id);
//        ly_footer=(LinearLayout)v.findViewById(R.id.footer_layid);
//        ly_footer.setVisibility(View.GONE);
//        tv_head.setText("About Us");
//        String about="As the world's #1 job site, with over 18 crore unique visitors every month from over 60 different countries, Indeed has become the catalyst for putting the world to work. Indeed is intensely passionate about delivering the right fit for every hire. Indeed helps companies of all sizes hire the best talent and offers the best opportunity for job seekers to get hired.";
//        String data="This Privacy Policy covers only data that we collect through the Site, and not any other data collection or processing, including, without limitation, the data collection practices of any affiliate or other third party, including any third-party operators of web pages to which the Site links, and any information that we collect offline or through any websites, products, or services that do not display a direct link to this Privacy Policy. Occasionally, we may refer to this Privacy Policy in notices and consent requests related to special-purpose web pages, mobile applications, or other resources, for example, if we invite you to submit ideas to improve the Site; under such circumstances, this Privacy Policy applies to information collected by us through such special-purpose resources, as modified in the particular notice or consent request (e.g., with respect to the types of data collected or our uses or disclosures of such information)";
//
//        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        wv.loadData(Constants.About_US, "text/html; charset=UTF-8", null);
//        wv.loadDataWithBaseURL(null, Constants.About_US, "text/html", "utf-8",null);
//
//        return v;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);

    }

}
