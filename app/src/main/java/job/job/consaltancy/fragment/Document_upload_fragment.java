package job.job.consaltancy.fragment;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.ImageBase64;
import job.job.consaltancy.utils.NetworkConnection;

public class Document_upload_fragment extends Fragment {

	Spinner sp_doc_name;
	TextView tv_upload,tv_choose,progresstxt;
	ImageView iv_doc_view;
	LinearLayout main_layout;
	String st_base46="",st_imagename="";

	ArrayList<String> doc_name_list;

	String upLoadServerUri = null;
	String uploadFilePath = Environment.getExternalStorageDirectory()+"/";
	private Bitmap thumbnail;
	String imgPath="";
	String uploadFileName = "";
	Bitmap bitmap;
	Uri selectedImage, picUri;
	ProgressBar progress;

	NetworkConnection nw;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.fill_document_info, container,false);

		nw=new NetworkConnection(getActivity());

		tv_upload=(TextView)v.findViewById(R.id.tv_uploadid);
		tv_choose=(TextView)v.findViewById(R.id.tv_choosefileid);
		progresstxt=(TextView)v.findViewById(R.id.textView3);

		main_layout=(LinearLayout)v.findViewById(R.id.main_layout);

		iv_doc_view=(ImageView)v.findViewById(R.id.iv_documentviewid);

		sp_doc_name = (Spinner)v.findViewById(R.id.sp_doc_nameid);

		progress=(ProgressBar)v.findViewById(R.id.progressBar2);

		doc_name_list=new ArrayList<>();

		doc_name_list.add("10th Certificate");
		doc_name_list.add("12th Certificate");
		doc_name_list.add("Aadhar Card");
		doc_name_list.add("Graduation Certificate");
		doc_name_list.add("Post Graduation Certificate");
		doc_name_list.add("Diploma Certificate");

		ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, doc_name_list);
		dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_doc_name.setAdapter(dayAdapter);

		sp_doc_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				progress.setVisibility(View.VISIBLE);
				progresstxt.setVisibility(View.VISIBLE);
				String url=Constants.url + "document_images/" + doc_name_list.get(position)+"_"+Constants.Unique_Id + ".png";
				progresstxt.setText("Please wait.... we check wheater you already upload a "+doc_name_list.get(position)+" or not.");
				url=url.replace(" ","%20");
				Picasso.with(getActivity())
						.load(url)
						.memoryPolicy(MemoryPolicy.NO_CACHE)
						.networkPolicy(NetworkPolicy.NO_CACHE)
						.into(iv_doc_view , new Callback() {
							@Override
							public void onSuccess()
							{
								progress.setVisibility(View.GONE);
								progresstxt.setVisibility(View.GONE);
							}

							@Override
							public void onError() {
								progress.setVisibility(View.GONE);
								progresstxt.setText("This document is not available.Please upload it.");
							}
						});

				Log.e("image_url",url);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		tv_choose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SelectImage();
			}
		});

		tv_upload.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				st_imagename=sp_doc_name.getSelectedItem().toString()+"_"+Constants.Unique_Id;
				if (st_base46.length()>0) {

					if(nw.isConnected())
						imagevolley();
					else
					{
						final Snackbar snackbar = Snackbar.make(main_layout, "   Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

						snackbar.setActionTextColor(Color.WHITE);
						snackbar.setDuration(3500);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

						TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
						textView.setTextColor(Color.WHITE);//change Snackbar's text color;
						textView.setGravity(Gravity.CENTER_VERTICAL);
						textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
						textView.setCompoundDrawablePadding(0);

						snackbar.show();
					}
//						Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_LONG).show();
				}
				else
				{
					final Snackbar snackbar = Snackbar.make(main_layout, "   Please Select a Image.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
				}
//					Toast.makeText(getActivity(),"Please Select a Image",Toast.LENGTH_LONG).show();
			}
		});

		return v;
	}

	public void SelectImage()
	{
		final CharSequence[] options = {"Upload a good quality& clear photo. We will adjust size & font according to your form requirement.\n\n","Take Photo", "Choose from Gallery", "Cancel"};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {

				if (options[item].equals("Take Photo")) {


					File photo = null;
					Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
					if (android.os.Environment.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED)) {
						photo = new File(android.os.Environment
								.getExternalStorageDirectory(), "test.jpg");
					} else {
						photo = new File(android.os.Environment
								.getExternalStorageDirectory(), "test.jpg");
					}
					if (photo != null) {
						intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
						selectedImage = Uri.fromFile(photo);
						startActivityForResult(intent, 1);
					}



//					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//					if     (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//						startActivityForResult(takePictureIntent, 1);
//					}

				} else if (options[item].equals("Choose from Gallery")) {

					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(intent, 2);

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
		builder.setCancelable(false);

	}


	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Log.e("result", resultCode + "");

		if(resultCode==0) {
		}
		else if(resultCode==getActivity().RESULT_OK) {
			if (requestCode == 1)
			{

//				Bundle extras = data.getExtras();
//				bitmap= (Bitmap) extras.get("data");
				//Uri selectedImage = data.getData();
				Log.e("selected_ur",selectedImage+" ");

				ContentResolver cr = getActivity().getContentResolver();

				try {
					bitmap = MediaStore.Images.Media
                            .getBitmap(cr, selectedImage);
				} catch (IOException e) {
					e.printStackTrace();
				}

            	iv_doc_view.setImageBitmap(bitmap);

				st_base46 = ImageBase64.encodeTobase64(bitmap);

//				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//				bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//				byte[] byteArray = byteArrayOutputStream .toByteArray();

				//st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);

//				if(bitmap!=null) {
//					bitmap = null;
//				}

//				Log.e("Base64_image",st_base46);

//				Uri selectedImage = data.getData();
//				String[] filePathColumn = { MediaStore.Images.Media.DATA };
//
//				// Get the cursor
//				Cursor cursor = getActivity().getContentResolver().query(selectedImage,
//						filePathColumn, null, null, null);
//				// Move to first row
//				cursor.moveToFirst();
//
//				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//				imgPath = cursor.getString(columnIndex);
//				cursor.close();
//
//				iv_doc_view.setImageBitmap(BitmapFactory
//						.decodeFile(imgPath));
				//encodeImagetoString();

			} else if (requestCode == 2) {

				selectedImage = data.getData();
				iv_doc_view.setImageURI(selectedImage);//setImageBitmap(thumbnail);

				try {
					bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),selectedImage);
					//bitmap=decodeUri(getActivity(),selectedImage,50);
				} catch (IOException e) {
					e.printStackTrace();
				}

				st_base46 = ImageBase64.encodeTobase64(bitmap);

//				Log.e("bitmap", bitmap + "");
//				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//				bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//				byte[] byteArray = byteArrayOutputStream .toByteArray();
//				st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//				if(bitmap!=null) {
//					bitmap = null;
//				}
//				Log.e("Base64_image",st_base46);

//				Uri selectedImage = data.getData();
//
//				String[] filePathColumn = { MediaStore.Images.Media.DATA };
//
//				// Get the cursor
//				Cursor cursor = getActivity().getContentResolver().query(selectedImage,
//						filePathColumn, null, null, null);
//				// Move to first row
//				cursor.moveToFirst();
//
//				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//				imgPath = cursor.getString(columnIndex);
//				cursor.close();
//
//				iv_doc_view.setImageBitmap(BitmapHelper
//						.decodeFile(imgPath,1000,1500,true));
//				Bitmap bitmap = ((BitmapDrawable)iv_doc_view.getDrawable()).getBitmap();
//				st_base46 = ImageBase64.encodeTobase64(bitmap);
			}
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	public void encodeImagetoString() {
		new AsyncTask<Void, Void, String>() {
			ProgressDialog pd;
			protected void onPreExecute() {

				pd = new ProgressDialog(getActivity());
				pd.setCancelable(true);
				pd.setMessage("Please Wait...");
				pd.setCanceledOnTouchOutside(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();

			};

			@Override
			protected String doInBackground(Void... params) {
				BitmapFactory.Options options = null;
				options = new BitmapFactory.Options();
				options.inSampleSize = 3;
//				bitmap = BitmapFactory.decodeFile(imgPath,
//						options);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				// Must compress the Image to reduce image size to make upload easy
				bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
				byte[] byte_arr = stream.toByteArray();
				// Encode Image to String
				st_base46 = Base64.encodeToString(byte_arr, Base64.DEFAULT);
				return "";
			}

			@Override
			protected void onPostExecute(String msg) {

				Log.e("base_64",st_base46+" imagedata");
                 pd.dismiss();
				//prgDialog.setMessage("Calling Upload");
				// Put converted Image string into Async Http Post param
			//	params.put("image", encodedString);
				// Trigger Image upload
				//triggerImageUpload();
			}
		}.execute(null, null, null);
	}


	public Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
			throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

		int width_tmp = o.outWidth
				, height_tmp = o.outHeight;
		int scale = 1;

		while(true) {
			if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
	}


	public void imagevolley() {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(false);
		pd.setMessage("Uploading Please Wait...");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pd.show();

		RequestQueue queue = Volley.newRequestQueue(getActivity());
		String url="";
		url = Constants.url +"document_upload.php?";
		url = url.replace(" ", "%20");
		Log.e("name", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Log.e("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject obj=new JSONObject(s);
						s=obj.getString("scalar");
						if(s.equalsIgnoreCase("Successfully Inserted")==true)
						{
							final Snackbar snackbar = Snackbar.make(main_layout, "Upload Successfully.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
//							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//							Toast.makeText(getActivity(),"Upload Successfully",Toast.LENGTH_LONG).show();
						}
						else if(s.equalsIgnoreCase("Can't Inserted")==true)
						{
							final Snackbar snackbar = Snackbar.make(main_layout, "   Somthing is Wrong Can't Upload, Please Try Again.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//							Toast.makeText(getActivity(),"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
						}

						pd.dismiss();
						Log.e("response_string",s+" response");
					}
				}

				catch (Exception e)
				{
					pd.dismiss();
					Log.e("e","e",e);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {

				Log.e("imagevolley_error",volleyError.toString());
                pd.dismiss();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("st_base64", st_base46);
				params.put("uniqueid", Constants.Unique_Id);
				params.put("imagename", st_imagename);

				Log.e("img222222", st_base46);

				return params;
			}
		};

		int socketTimeout = 10000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);
	}
}