package job.job.consaltancy.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;


public class Address_Info_fragment extends Fragment implements View.OnClickListener {

	FloatingActionButton tv_submit;
	TextView tv_email,tv_contact,tv_addresscorsp,tv_addresspmnt;
	LinearLayout ly_email,ly_contact,ly_addresscorsp,ly_addresspmnt,lys_addresspmnt;
	RelativeLayout main_layout;

	EditText et_mob,et_mobalt,et_landline,et_email,et_emailalt,et_add1,et_add2,et_add3,et_town,et_distr,et_pin,et_near_rail,et_near_landm;
	EditText et_add1pr,et_add2pr,et_add3pr,et_townpr,et_distrpr,et_pinpr,et_near_railpr,et_near_landmpr;
	RadioButton rb_addressp_yes,rb_addressp_no;
	boolean b_email=false,b_contact=false,b_add_cors=false,b_add_pmnt=false,b_add_p_yes_no=false;
    Spinner sp_state,sp_statepr;
	String st_mob,st_mobalt,st_landline,st_email_alt,st_add1,st_add2,st_add3,st_town,st_dist,st_state,st_pin,st_near_rail,st_near_land;
	String st_p_add1,st_p_add2,st_p_add3,st_p_town,st_p_dist,st_p_state,st_p_pin,st_p_near_rail,st_p_near_land,st_status="false";

	SharedPreferences user_info,detailprf;
	SharedPreferences.Editor editor,detailedt;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.fill_address_info, container,false);

		user_info=getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		detailprf=getActivity().getSharedPreferences("detailprf",Context.MODE_PRIVATE);
		et_mob=(EditText)v.findViewById(R.id.et_mobilenoid);
		et_mobalt=(EditText)v.findViewById(R.id.et_mobileno_altrid);
		et_landline=(EditText)v.findViewById(R.id.et_landline_id);
		et_email=(EditText)v.findViewById(R.id.et_emailid);
		et_emailalt=(EditText)v.findViewById(R.id.et_email_altrid);
		et_add1=(EditText)v.findViewById(R.id.et_address1id);
		et_add2=(EditText)v.findViewById(R.id.et_address2id);
		et_add3=(EditText)v.findViewById(R.id.et_address3id);
		et_pin=(EditText)v.findViewById(R.id.et_pincodeid);
		et_town=(EditText)v.findViewById(R.id.et_townid);
		et_distr=(EditText)v.findViewById(R.id.et_districtid);

		sp_state=(Spinner)v.findViewById(R.id.sp_stateid);

		et_near_rail=(EditText)v.findViewById(R.id.et_near_rail_stid);
		et_near_landm=(EditText)v.findViewById(R.id.et_near_landmarkid);
		et_near_landm.setHint("Nearest Land-Mark(Optional)");

		et_add1pr=(EditText)v.findViewById(R.id.et_praddress1id);
		et_add2pr=(EditText)v.findViewById(R.id.et_praddress2id);
		et_add3pr=(EditText)v.findViewById(R.id.et_praddress3id);
		et_pinpr=(EditText)v.findViewById(R.id.et_prpincodeid);
		et_townpr=(EditText)v.findViewById(R.id.et_prtownid);
		et_distrpr=(EditText)v.findViewById(R.id.et_prdistrictid);
		sp_statepr=(Spinner)v.findViewById(R.id.sp_prstateid);
		et_near_railpr=(EditText)v.findViewById(R.id.et_prnear_rail_stid);
		et_near_landmpr=(EditText)v.findViewById(R.id.et_prnear_landmarkid);
		et_near_landmpr.setHint("Nearest Land-Mark(Optional)");

		tv_submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);
		tv_email=(TextView)v.findViewById(R.id.email_textviewid);
		tv_contact=(TextView)v.findViewById(R.id.mobileno_textviewid);
		tv_addresscorsp=(TextView)v.findViewById(R.id.address_textviewid);
		tv_addresspmnt=(TextView)v.findViewById(R.id.addresspr_textviewid);


		main_layout=(RelativeLayout)v.findViewById(R.id.main_layout);
		ly_email=(LinearLayout)v.findViewById(R.id.email_layoutid);
		ly_contact=(LinearLayout)v.findViewById(R.id.mobileno_layoutid);
		ly_addresscorsp=(LinearLayout)v.findViewById(R.id.address_layoutid);
		ly_addresspmnt=(LinearLayout)v.findViewById(R.id.addresspr_layoutid);

		lys_addresspmnt=(LinearLayout)v.findViewById(R.id.addresspr_detail_layoutid);
		rb_addressp_no=(RadioButton)v.findViewById(R.id.rb_permanent_add_noid);
		rb_addressp_yes=(RadioButton)v.findViewById(R.id.rb_permanent_add_yesid);

		et_email.setText(user_info.getString("email",""));
		et_email.setEnabled(false);

		//et_state.setText(user_info.getString("state",""));


		Constants.statelist.clear();
		Constants.statelist.add("Select state");
		Constants.statelist.add("Andaman and Nicobar Islands");
		Constants.statelist.add("Andhra Pradesh");
		Constants.statelist.add("Arunachal Pradesh");
		Constants.statelist.add("Assam");
		Constants.statelist.add("Bihar");
		Constants.statelist.add("Chandigarh");
		Constants.statelist.add("Chhattisgarh");
		Constants.statelist.add("Dadra and Nagar Haveli");
		Constants.statelist.add("Daman and Diu");
		Constants.statelist.add("Delhi – National Capital Territory");
		Constants.statelist.add("Goa");
		Constants.statelist.add("Gujarat");
		Constants.statelist.add("Haryana");
		Constants.statelist.add("Himachal Pradesh");
		Constants.statelist.add("Jammu & Kashmir");
		Constants.statelist.add("Jharkhand");
		Constants.statelist.add("Karnataka");
		Constants.statelist.add("Kerala");
		Constants.statelist.add("Lakshadweep");
		Constants.statelist.add("Madhya Pradesh");
		Constants.statelist.add("Maharashtra");
		Constants.statelist.add("Manipur");
		Constants.statelist.add("Meghalaya");
		Constants.statelist.add("Mizoram");
		Constants.statelist.add("Nagaland");
		Constants.statelist.add("Odisha");
		Constants.statelist.add("Puducherry");
		Constants.statelist.add("Punjab");
		Constants.statelist.add("Rajasthan");
		Constants.statelist.add("Sikkim");
		Constants.statelist.add("Tamil Nadu");
		Constants.statelist.add("Telangana");
		Constants.statelist.add("Tripura");
		Constants.statelist.add("Uttar Pradesh");
		Constants.statelist.add("Uttarakhand");
		Constants.statelist.add("West Bengal");


		ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_state.setAdapter(dayAdapter);
		sp_statepr.setAdapter(dayAdapter);

//		ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, day_list);
//		dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);




		String st_add = user_info.getString("address_info","");

		Log.e("address_info",st_add);

		if(st_add.length()>0) {
			try {

				JSONObject jobj = new JSONObject(st_add);

				et_email.setText(Constants.Email_Id);
				et_emailalt.setText(jobj.getString("email_alt"));
				et_mob.setText(jobj.getString("mobile_no"));
				et_mobalt.setText(jobj.getString("mobile_alt"));
				et_landline.setText(jobj.getString("landline"));
				et_add1.setText(jobj.getString("address1"));
				et_add2.setText(jobj.getString("address2"));
				et_add3.setText(jobj.getString("address3"));
				et_town.setText(jobj.getString("town"));
				et_distr.setText(jobj.getString("district"));
				//et_state.setText(jobj.getString("state"));
				et_pin.setText(jobj.getString("pincode"));
				et_near_rail.setText(jobj.getString("near_rail"));
				et_near_landm.setText(jobj.getString("near_land"));

				et_add1pr.setText(jobj.getString("address_p1"));
				et_add2pr.setText(jobj.getString("address_p2"));
				et_add3pr.setText(jobj.getString("address_p3"));
				et_townpr.setText(jobj.getString("town_p"));
				et_distrpr.setText(jobj.getString("district_p"));
				//et_statepr.setText(jobj.getString("state_p"));
				et_pinpr.setText(jobj.getString("pincode_p"));
				et_near_railpr.setText(jobj.getString("near_rail_p"));
				et_near_landmpr.setText(jobj.getString("near_land_p"));

				if(Constants.statelist.contains(jobj.getString("state"))){
					int ind=Constants.statelist.indexOf(jobj.getString("state"));

					sp_state.setSelection(ind);
				}

				if(Constants.statelist.contains(jobj.getString("state_p"))){
					int ind=Constants.statelist.indexOf(jobj.getString("state_p"));

					sp_statepr.setSelection(ind);
				}


			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("address_set_exception",e.toString());
			}
		}

		rb_addressp_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_addresspmnt.setVisibility(View.GONE);
					b_add_p_yes_no=true;

					st_p_add1=et_add1.getText().toString();
					st_p_add2=et_add2.getText().toString();
					st_p_add3=et_add3.getText().toString();
					st_p_town=et_town.getText().toString();
					st_p_dist=et_distr.getText().toString();
					st_p_state=sp_state.getSelectedItem().toString();
					st_p_pin=et_pin.getText().toString();
					st_p_near_rail=et_near_rail.getText().toString();
					st_p_near_land=et_near_landm.getText().toString();

				}
			}
		});
		rb_addressp_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_addresspmnt.setVisibility(View.VISIBLE);

					b_add_p_yes_no=false;

				}
			}
		});


		tv_email.setOnClickListener(this);
		tv_contact.setOnClickListener(this);
		tv_addresspmnt.setOnClickListener(this);
		tv_addresscorsp.setOnClickListener(this);

		tv_submit.setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==R.id.email_textviewid){
			if(b_email) {
				ly_email.setVisibility(View.GONE);
				b_email=false;
				tv_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_email.setVisibility(View.VISIBLE);
				b_email=true;
				tv_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.mobileno_textviewid){
			if(b_contact) {
				ly_contact.setVisibility(View.GONE);
				b_contact=false;
				tv_contact.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_contact.setVisibility(View.VISIBLE);
				b_contact=true;
				tv_contact.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.address_textviewid){
			if(b_add_cors) {
				ly_addresscorsp.setVisibility(View.GONE);
				b_add_cors=false;
				tv_addresscorsp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_addresscorsp.setVisibility(View.VISIBLE);
				b_add_cors=true;
				tv_addresscorsp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.addresspr_textviewid){
			if(b_add_pmnt) {
				ly_addresspmnt.setVisibility(View.GONE);
				b_add_pmnt=false;
				tv_addresspmnt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_addresspmnt.setVisibility(View.VISIBLE);
				b_add_pmnt=true;
				tv_addresspmnt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.submitbtn) {

			st_email_alt=et_emailalt.getText().toString();
			st_mob=et_mob.getText().toString();
			st_mobalt=et_mobalt.getText().toString();
			st_landline=et_landline.getText().toString();

			st_add1=et_add1.getText().toString();
			st_add2=et_add2.getText().toString();
			st_add3=et_add3.getText().toString();
			st_town=et_town.getText().toString();
			st_dist=et_distr.getText().toString();
			st_state=sp_state.getSelectedItem().toString();
			st_pin=et_pin.getText().toString();
			st_near_rail=et_near_rail.getText().toString();
			st_near_land=et_near_landm.getText().toString();

			if(b_add_p_yes_no==false) {
				st_p_add1 = et_add1pr.getText().toString();
				st_p_add2 = et_add2pr.getText().toString();
				st_p_add3 = et_add3pr.getText().toString();
				st_p_town = et_townpr.getText().toString();
				st_p_dist = et_distrpr.getText().toString();
				st_p_state = sp_statepr.getSelectedItem().toString();
				st_p_pin = et_pinpr.getText().toString();
				st_p_near_rail = et_near_railpr.getText().toString();
				st_p_near_land = et_near_landmpr.getText().toString();
			}

			volley();

		}
	}

	public void volley()
	{

		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(true);
		pd.setMessage("Loading Please Wait");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		detailedt=detailprf.edit();

		RequestQueue queue= Volley.newRequestQueue(getActivity());
		String url;

		if(st_add1.length()>0&st_pin.length()>0)
           st_status="true";

         if(st_mob.length()>0&st_add1.length()>0&st_town.length()>0&st_dist.length()>0&st_state.length()>0&st_pin.length()>0&
				 st_near_rail.length()>0){
			 detailedt.putBoolean("Address_info",true);
		 }
		else
			 detailedt.putBoolean("Address_info",false);

		url= Constants.url+"adress_info.php?unique_id="+Constants.Unique_Id+"&eamil_alt="+st_email_alt+"&mobile_no="+st_mob+"&mobile_alt="+st_mobalt+"&landline="+
				 st_landline+"&address1="+st_add1+"&address2="+st_add2+"&address3="+st_add3+"&town="+st_town+"&district="+st_dist+
				 "&state="+st_state+"&pincode="+st_pin+"&near_rail="+st_near_rail+"&near_land="+st_near_land+"&address_p1="+st_p_add1+
				"&address_p2="+st_p_add2+"&address_p3="+st_p_add3+"&town_p="+st_p_town+"&district_p="+st_p_dist+"&state_p="+st_p_state+
				"&pincode_p="+st_p_pin+"&near_rail_p="+st_p_near_rail+"&near_land_p="+st_p_near_land+"&status="+st_status;

		url = url.replace(" ", "%20");
		Log.e("emailcheckvolleyurl", url);
		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {

				pd.dismiss();

				Log.e("loginres", res.toString());

				try {

					//if(res.getString("scalar").equals("Successfully Inserted")) {
					Log.e("emailcheckvolley", "done");
					final Snackbar snackbar = Snackbar.make(main_layout, "Successfully Inserted.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
//					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
//						Toast.makeText(getActivity(), "Successfully Inserted", Toast.LENGTH_SHORT).show();


						editor=user_info.edit();

						JSONObject jobj=new JSONObject();

							jobj.put("email_alt", st_email_alt);
							jobj.put("mobile_no",st_mob);
							jobj.put("mobile_alt",st_mobalt);
							jobj.put("landline",st_landline);

							jobj.put("address1",st_add1);
							jobj.put("address2",st_add2);
							jobj.put("address3",st_add3);
							jobj.put("town",st_town);
							jobj.put("district",st_dist);
							jobj.put("state",st_state);
							jobj.put("pincode",st_pin);
							jobj.put("near_rail",st_near_rail);
							jobj.put("near_land",st_near_land);

							jobj.put("address_p1",st_p_add1);
							jobj.put("address_p2",st_p_add2);
							jobj.put("address_p3",st_p_add3);
							jobj.put("town_p",st_p_town);
							jobj.put("district_p",st_p_dist);
							jobj.put("state_p",st_p_state);
							jobj.put("pincode_p",st_p_pin);
							jobj.put("near_rail_p",st_p_near_rail);
							jobj.put("near_land_p",st_p_near_land);

							editor.putString("address_info", jobj.toString());

						editor.commit();


					if(st_status.equalsIgnoreCase("true")==true)
						detailedt.putBoolean("address",true);
					else
						detailedt.putBoolean("address",false);


					detailedt.commit();


					//}

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("exceptionlogin",e.toString());
					pd.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError e) {

				pd.dismiss();
				final Snackbar snackbar = Snackbar.make(main_layout, "   Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();
//				Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
				Log.e("error", e.toString());
			}
		});
		int socketTimeout = 50000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		jsonreq.setRetryPolicy(policy);
		queue.add(jsonreq);
	}

}