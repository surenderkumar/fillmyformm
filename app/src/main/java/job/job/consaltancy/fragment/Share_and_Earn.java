package job.job.consaltancy.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Job_in_processing_adapter;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Share_and_Earn extends AppCompatActivity {

    TextView wallet_money;
    WebView wv;
    LinearLayout ly_share;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_share_earn);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wv=(WebView)findViewById(R.id.webview_id);

        ly_share=(LinearLayout)findViewById(R.id.ly_share_id);

        wallet_money=(TextView)findViewById(R.id.tv_pointid);

        wallet_money.setText("₹ " + String.valueOf(Constants.Wallet_Money));

        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
        wv.loadData(Constants.Share_and_Earn_Text, "text/html; charset=UTF-8", null);
        wv.loadDataWithBaseURL(null, Constants.Share_and_Earn_Text , "text/html", "utf-8", null);

        ly_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String PACKAGE_NAME = getPackageName();
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                // share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Fill MY Form");

                share.putExtra(Intent.EXTRA_TEXT,Constants.Share_and_Earn1+Constants.Unique_Id+Constants.Share_and_Earn2+"\n Download Here: play.google.com/store/apps/details?id="+PACKAGE_NAME);
                share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });


    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v =inflater.inflate(R.layout.webview_share_earn, container,false);
//
//        wv=(WebView)v.findViewById(R.id.webview_id);
//
//        ly_share=(LinearLayout)v.findViewById(R.id.ly_share_id);
//
//        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        wv.loadData(Constants.Share_and_Earn_Text, "text/html; charset=UTF-8", null);
//        wv.loadDataWithBaseURL(null, Constants.Share_and_Earn_Text , "text/html", "utf-8", null);
//
//        ly_share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String PACKAGE_NAME = getActivity().getPackageName();
//							Intent share = new Intent(android.content.Intent.ACTION_SEND);
//							share.setType("text/plain");
//							// share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//							share.putExtra(Intent.EXTRA_SUBJECT, "Form Filler");
//
//							share.putExtra(Intent.EXTRA_TEXT,Constants.Share_and_Earn1+Constants.Unique_Id+Constants.Share_and_Earn2+"\n Download Here: play.google.com/store/apps/details?id="+PACKAGE_NAME);
//							share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//							startActivity(Intent.createChooser(share, "Share link!"));
//            }
//        });
//
//        return v;
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);

    }
}
