package job.job.consaltancy.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

import job.job.consaltancy.Link_Webview;
import job.job.consaltancy.R;
import job.job.consaltancy.adapter.PagerAdapter;
import job.job.consaltancy.utils.Constants;


public class Other_fragment extends Fragment
{

	SharedPreferences user_info;
	TextView tv_facebook,tv_twitter,tv_rateus,tv_offer,tv_howtopay,tv_useapp,tv_suggestion;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.other_tab_layout, container,false);

		tv_facebook=(TextView)v.findViewById(R.id.otr_facebookid);
		tv_twitter=(TextView)v.findViewById(R.id.otr_twitterid);
		tv_rateus=(TextView)v.findViewById(R.id.otr_rateusid);

		tv_offer=(TextView)v.findViewById(R.id.otr_offersid);
		tv_howtopay=(TextView)v.findViewById(R.id.otr_payid);
		tv_useapp=(TextView)v.findViewById(R.id.otr_use_appid);

		tv_suggestion=(TextView)v.findViewById(R.id.otr_numberid);

		Constants.fragment=2;
		
		tv_offer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(getActivity(), Link_Webview.class);
				in.putExtra("link","your_offer");
				startActivity(in);
			}
		});

		tv_howtopay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(getActivity(), Link_Webview.class);
				in.putExtra("link","how_to_pay");
				startActivity(in);
			}
		});

		tv_useapp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(getActivity(), Link_Webview.class);
				in.putExtra("link","how_to_use");
				startActivity(in);
			}
		});

		tv_facebook.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Fill-MY-Form-APP-1668916366686245"));
				getActivity().startActivity(browserIntent);
			}
		});

		tv_twitter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/fill_MY_form"));
				getActivity().startActivity(browserIntent);
			}
		});

		tv_rateus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String PACKAGE_NAME = getActivity().getPackageName();
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+PACKAGE_NAME));
				getActivity().startActivity(browserIntent);
			}
		});

		tv_suggestion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "fillmyformapp@gmail.com"));
				startActivity(intent);
			}
		});

		return v;
	}

}