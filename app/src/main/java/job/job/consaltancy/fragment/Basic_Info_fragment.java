package job.job.consaltancy.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.ImageBase64;
import job.job.consaltancy.utils.NetworkConnection;


public class Basic_Info_fragment extends Fragment implements View.OnClickListener {

	FloatingActionButton tv_submit;
	TextView tv_area,tv_state,tv_con_name,tv_fn_name,tv_mn_name,tv_hb_name,tv_dob,tv_dob_proof,tv_nationality,tv_adharno,tv_identity,tv_criminal,tv_photograph;
	TextView tv_userimage,tv_usersign;
	LinearLayout ly_con_name,ly_fn_name,ly_mn_name,ly_hb_name,ly_dob,ly_nationality,ly_adharno,ly_identity,ly_criminal,ly_photograph;
	LinearLayout ly_area,ly_state;
	RelativeLayout main_layout;

	LinearLayout ly_h_con_name,ly_h_fn_name,ly_h_mn_name,ly_h_hb_name,ly_h_dob,ly_h_nationality,ly_h_adharno,ly_h_identity,ly_h_criminal;
	LinearLayout ly_h_photograph,ly_h_area,ly_h_state;


	Spinner sp_day,sp_month,sp_year,sp_area,sp_state,sp_dob_proof;
	EditText et_criminal,c_first,c_middle,c_last,f_first,f_middle,f_last,m_first,m_middle,m_last,h_first,h_middle,h_last,dob_proof,nationality,aadhar,mark;

	ImageView iv_user,iv_sign;

	RadioButton rb_criminal_yes,rb_criminal_no,radioButton;
	RadioGroup rgroup;
    boolean b_con_name=false,b_fn_name=false,b_mn_name=false,b_hb_name=false,b_dob=false,b_nation=false,b_adhar=false;
	boolean b_identity=false,b_crim=false,b_photo=false,b_area=false,b_state=false;

	ArrayList<String> day_list,month_list,year_list,address_list,proof_list;

	String c_name="",f_name="",m_name="",h_name="",dob="",dobproof="",nationalitystr="",areastr="",domicilestr="";
	String aadharstr="",markstr="",criminalstr="",casestr="",st_userimage="",st_imagename="",st_base46,st_status="false";

	SharedPreferences user_info,detailprf;
	SharedPreferences.Editor editor,detailedt;

	Bitmap bitmap;
	Uri selectedImage, picUri;

	NetworkConnection nw;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.fill_basic_info, container,false);

		user_info=getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);

		detailprf=getActivity().getSharedPreferences("detailprf",Context.MODE_PRIVATE);

		tv_con_name=(TextView)v.findViewById(R.id.condidate_name_textviewid);
		tv_fn_name=(TextView)v.findViewById(R.id.fathername_textviewid);
		tv_mn_name=(TextView)v.findViewById(R.id.mothername_textviewid);
		tv_hb_name=(TextView)v.findViewById(R.id.hushbandname_textviewid);
		tv_dob=(TextView)v.findViewById(R.id.dob_textviewid);
		tv_nationality=(TextView)v.findViewById(R.id.nationality_textviewid);
		tv_adharno=(TextView)v.findViewById(R.id.aadharcard_textviewid);
		tv_identity=(TextView)v.findViewById(R.id.identitymark_textviewid);
		tv_criminal=(TextView)v.findViewById(R.id.criminal_textviewid);
		tv_photograph=(TextView)v.findViewById(R.id.photograph_textviewid);
		tv_submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);
		tv_area=(TextView)v.findViewById(R.id.area_textviewid);
		tv_state=(TextView)v.findViewById(R.id.state_textviewid);
		tv_userimage=(TextView)v.findViewById(R.id.tv_userimage_id);
		tv_usersign=(TextView)v.findViewById(R.id.tv_usersign_id);

		sp_dob_proof=(Spinner)v.findViewById(R.id.dob_sp_proof);
		sp_state=(Spinner)v.findViewById(R.id.sp_stateid);
		rgroup=(RadioGroup)v.findViewById(R.id.radiogroupid);

		et_criminal=(EditText)v.findViewById(R.id.ed_criminal_id);
		c_first=(EditText)v.findViewById(R.id.firstnameid);
		c_middle=(EditText)v.findViewById(R.id.middlenameid);
		c_last=(EditText)v.findViewById(R.id.lastnameid);
		f_first=(EditText)v.findViewById(R.id.father_firstnameid);
		f_middle=(EditText)v.findViewById(R.id.father_middlenameid);
		f_last=(EditText)v.findViewById(R.id.father_lastnameid);
		m_first=(EditText)v.findViewById(R.id.mother_firstnameid);
		m_middle=(EditText)v.findViewById(R.id.mother_middlenameid);
		m_last=(EditText)v.findViewById(R.id.mother_lastnameid);
		h_first=(EditText)v.findViewById(R.id.husband_firstnameid);
		h_middle=(EditText)v.findViewById(R.id.husband_middlenameid);
		h_last=(EditText)v.findViewById(R.id.husband_lastnameid);
		dob_proof=(EditText)v.findViewById(R.id.dob_proofid);
		nationality=(EditText)v.findViewById(R.id.nationality_id);
		aadhar=(EditText)v.findViewById(R.id.aadharcard_id);
		mark=(EditText)v.findViewById(R.id.identitymark_id);

		iv_user=(ImageView)v.findViewById(R.id.image_id);
		iv_sign=(ImageView)v.findViewById(R.id.signatureimage_id);

		main_layout=(RelativeLayout)v.findViewById(R.id.main_layout);
		ly_con_name=(LinearLayout)v.findViewById(R.id.condidate_namelayoutid);
		ly_fn_name=(LinearLayout)v.findViewById(R.id.father_namelayoutid);
		ly_mn_name=(LinearLayout)v.findViewById(R.id.mother_namelayoutid);
		ly_hb_name=(LinearLayout)v.findViewById(R.id.husband_namelayoutid);
		ly_dob=(LinearLayout)v.findViewById(R.id.dob_layoutid);
		ly_nationality=(LinearLayout)v.findViewById(R.id.nationality_layoutid);
		ly_adharno=(LinearLayout)v.findViewById(R.id.aadharcard_layoutid);
		ly_identity=(LinearLayout)v.findViewById(R.id.identitymark_layoutid);
		ly_criminal=(LinearLayout)v.findViewById(R.id.criminal_layoutid);
		ly_photograph=(LinearLayout)v.findViewById(R.id.photograph_layoutid);
		ly_area=(LinearLayout)v.findViewById(R.id.area_layoutid);
		ly_state=(LinearLayout)v.findViewById(R.id.state_layoutid);

//		ly_h_con_name=(LinearLayout)v.findViewById(R.id.ly_h_cnameid);
//		ly_h_fn_name=(LinearLayout)v.findViewById(R.id.ly_h_fnameid);
//		ly_h_mn_name=(LinearLayout)v.findViewById(R.id.ly_h_mnameid);
//		ly_h_hb_name=(LinearLayout)v.findViewById(R.id.ly_h_hnameid);
//		ly_h_dob=(LinearLayout)v.findViewById(R.id.ly_h_dobid);
//		ly_h_nationality=(LinearLayout)v.findViewById(R.id.ly_h_nationalityid);
//		ly_h_adharno=(LinearLayout)v.findViewById(R.id.ly_h_aadharnoid);
//		ly_h_identity=(LinearLayout)v.findViewById(R.id.ly_h_visibleidentyid);
//		ly_h_criminal=(LinearLayout)v.findViewById(R.id.ly_h_criminalcaseid);
//		ly_h_photograph=(LinearLayout)v.findViewById(R.id.ly_h_photoid);
//		ly_h_area=(LinearLayout)v.findViewById(R.id.ly_h_areaid);
//		ly_h_state=(LinearLayout)v.findViewById(R.id.ly_h_statedomicileid);

		rb_criminal_no=(RadioButton)v.findViewById(R.id.rb_criminal_noid);
		rb_criminal_yes=(RadioButton)v.findViewById(R.id.rb_criminal_yesid);

		tv_con_name.setOnClickListener(this);
		tv_fn_name.setOnClickListener(this);
		tv_mn_name.setOnClickListener(this);
		tv_hb_name.setOnClickListener(this);
		tv_dob.setOnClickListener(this);
		tv_nationality.setOnClickListener(this);
		tv_adharno.setOnClickListener(this);
		tv_identity.setOnClickListener(this);
		tv_criminal.setOnClickListener(this);
		tv_photograph.setOnClickListener(this);
		tv_area.setOnClickListener(this);
		tv_state.setOnClickListener(this);
		tv_usersign.setOnClickListener(this);
		tv_userimage.setOnClickListener(this);

//		ly_h_con_name.setOnClickListener(this);
//		ly_h_fn_name.setOnClickListener(this);
//		ly_h_mn_name.setOnClickListener(this);
//		ly_h_hb_name.setOnClickListener(this);
//		ly_h_dob.setOnClickListener(this);
//		ly_h_nationality.setOnClickListener(this);
//		ly_h_area.setOnClickListener(this);
//		ly_h_state.setOnClickListener(this);
//		ly_h_adharno.setOnClickListener(this);
//		ly_h_identity.setOnClickListener(this);
//		ly_h_criminal.setOnClickListener(this);
//		ly_h_photograph.setOnClickListener(this);






		day_list=new ArrayList<>();
		month_list=new ArrayList<>();
		year_list=new ArrayList<>();
		proof_list=new ArrayList<>();


		for (int i=1;i<32;i++)
			day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			month_list.add(String.valueOf(i));
		for (int i=1970;i<2020;i++)
			year_list.add(String.valueOf(i));

		sp_day=(Spinner)v.findViewById(R.id.dob_sp_dayid);
		sp_month=(Spinner)v.findViewById(R.id.dob_sp_monthsid);
		sp_year=(Spinner)v.findViewById(R.id.dob_sp_yearid);
		sp_area=(Spinner)v.findViewById(R.id.area_sp_id);

		ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, day_list);
		dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_day.setAdapter(dayAdapter);

		ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, month_list);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_month.setAdapter(monthAdapter);

		ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, year_list);
		yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_year.setAdapter(yearAdapter);

		address_list=new ArrayList<>();
		address_list.add("Select");
		address_list.add("Rural");
		address_list.add("Urban");

		ArrayAdapter<String> addressAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, address_list);
		addressAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_area.setAdapter(addressAdapter);

		ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_state.setAdapter(stateAdapter);

        proof_list.add("10th Certificate");
		proof_list.add("Other");

		ArrayAdapter<String> proofAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, proof_list);
		proofAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dob_proof.setAdapter(proofAdapter);

		rb_criminal_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					et_criminal.setVisibility(View.VISIBLE);
					criminalstr="Yes";
				}

			}
		});
		rb_criminal_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					et_criminal.setVisibility(View.GONE);
					criminalstr = "No";
				}

			}
		});

		sp_dob_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				if(position>0) {
					dob_proof.setVisibility(View.VISIBLE);
				}
				else {
					dob_proof.setVisibility(View.GONE);
				}
				dobproof=proof_list.get(position);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		sp_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				if (position > 0)
					areastr = sp_area.getSelectedItem().toString();

				Log.e("areastr", areastr);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				if (position > 0)
					domicilestr = sp_state.getSelectedItem().toString();

				Log.e("domicilestr", domicilestr);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		nw=new NetworkConnection(getActivity());

		tv_submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c_first.getText().toString().length() > 0) {
					c_name = c_first.getText().toString();
					if (c_middle.getText().toString().length() > 0) {
						c_name = c_name + " " + c_middle.getText().toString();
					}
					if (c_last.getText().toString().length() > 0) {
						c_name = c_name + " " + c_last.getText().toString();
					}
				}
				if (f_first.getText().toString().length() > 0) {
					f_name = f_first.getText().toString();
					if (f_middle.getText().toString().length() > 0) {
						f_name = f_name + " " + f_middle.getText().toString();
					}
					if (f_last.getText().toString().length() > 0) {
						f_name = f_name + " " + f_last.getText().toString();
					}
				}
				if (m_first.getText().toString().length() > 0) {
					m_name = m_first.getText().toString();
					if (m_middle.getText().toString().length() > 0) {
						m_name = m_name + " " + m_middle.getText().toString();
					}
					if (m_last.getText().toString().length() > 0) {
						m_name = m_name + " " + m_last.getText().toString();
					}
				}
				if (h_first.getText().toString().length() > 0) {
					h_name = h_first.getText().toString();
					if (h_middle.getText().toString().length() > 0) {
						h_name = h_name + " " + h_middle.getText().toString();
					}
					if (h_last.getText().toString().length() > 0) {
						h_name = h_name + " " + h_last.getText().toString();
					}
				}

				dob = sp_year.getSelectedItem().toString() + "-" + sp_month.getSelectedItem().toString() + "-" + sp_day.getSelectedItem().toString();
				Log.e("dob", dob);

				if(dobproof.equalsIgnoreCase("Other")==true)
				dobproof = dob_proof.getText().toString();

				Log.e("dobproof", dobproof);

				nationalitystr = "Indian";//nationality.getText().toString();
				Log.e("nationalitystr", nationalitystr);

				aadharstr = aadhar.getText().toString();
				Log.e("aadharstr", aadharstr);

				markstr = mark.getText().toString();
				Log.e("markstr", markstr);

				Log.e("criminalstr", criminalstr);


				casestr = et_criminal.getText().toString();
				Log.e("casestr", casestr);

				if(nw.isConnected())
				volley();
				else
				{
					final Snackbar snackbar = Snackbar.make(main_layout, "   No Internet Connection.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
				}
//				Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_LONG).show();

			}
		});


		String st_info= user_info.getString("basic_info","");
		if(st_info.length()>0) {

			try {
				JSONObject jobj=new JSONObject(st_info);
				String d,m,y,area,state;
				d=jobj.getString("dob_d");
				m=jobj.getString("dob_m");
				y=jobj.getString("dob_y");
				area=jobj.getString("area");
				state=jobj.getString("domicile_state");

				int d_ind=day_list.indexOf(d);
				int m_ind=month_list.indexOf(m);
				int y_ind=year_list.indexOf(y);
				if(address_list.contains(area)) {
					int ar_ind=address_list.indexOf(area);
					sp_area.setSelection(ar_ind);
				}

				if(Constants.statelist.contains(state)) {
					int st_ind=Constants.statelist.indexOf(state);
					sp_state.setSelection(st_ind);
				}

				sp_day.setSelection(d_ind);
				sp_month.setSelection(m_ind);
				sp_year.setSelection(y_ind);

				c_first.setText(jobj.getString("c_fname"));
				c_middle.setText(jobj.getString("c_mname"));
				c_last.setText(jobj.getString("c_lname"));
				f_first.setText(jobj.getString("f_fname"));
				f_middle.setText(jobj.getString("f_mname"));
				f_last.setText(jobj.getString("f_lname"));
				m_first.setText(jobj.getString("m_fname"));
				m_middle.setText(jobj.getString("m_mname"));
				m_last.setText(jobj.getString("m_lname"));
				h_first.setText(jobj.getString("h_fname"));
				h_middle.setText(jobj.getString("h_mname"));
				h_last.setText(jobj.getString("h_lname"));
				aadhar.setText(jobj.getString("aadhar_no"));
				mark.setText(jobj.getString("visible_id_mark"));
				et_criminal.setText(jobj.getString("criminal_case"));

				if(jobj.getString("criminal_case_yn").equalsIgnoreCase("Yes")==true)
					rb_criminal_yes.setChecked(true);
				else
				rb_criminal_no.setChecked(true);

				Log.e("dob_proof",jobj.getString("dob_proof")+" ");
				if(!jobj.getString("dob_proof").equalsIgnoreCase("10th Certificate")==true){
					sp_dob_proof.setSelection(1);
					dob_proof.setVisibility(View.VISIBLE);
					dob_proof.setText(jobj.getString("dob_proof"));
				}
				else{
					sp_dob_proof.setSelection(0);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		String url=Constants.url + "user_image/"+Constants.Unique_Id +"_"+"userimage"+ ".png";
		url=url.replace(" ","%20");
		Picasso.with(getActivity())
				.load(url)
				.networkPolicy(NetworkPolicy.NO_CACHE)
				.memoryPolicy(MemoryPolicy.NO_CACHE)
				.into(iv_user, new Callback() {
					@Override
					public void onSuccess() {
						iv_user.setVisibility(View.VISIBLE);
					}

					@Override
					public void onError() {
						iv_user.setVisibility(View.GONE);
					}
				});

		Log.e("image_url",url);


		String url1=Constants.url + "user_image/"+Constants.Unique_Id +"_"+"usersign"+ ".png";
		url=url.replace(" ","%20");
		Picasso.with(getActivity())
				.load(url1)
				.networkPolicy(NetworkPolicy.NO_CACHE)
				.memoryPolicy(MemoryPolicy.NO_CACHE)
				.into(iv_sign, new Callback() {

					@Override
					public void onSuccess() {
						iv_sign.setVisibility(View.VISIBLE);
					}

					@Override
					public void onError() {
						iv_sign.setVisibility(View.GONE);
					}
				});;

		Log.e("sign_url",url1);

		return v;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==R.id.condidate_name_textviewid) {
			if(b_con_name) {
				ly_con_name.setVisibility(View.GONE);
				b_con_name=false;
				tv_con_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
				else {
				ly_con_name.setVisibility(View.VISIBLE);
				b_con_name=true;
				tv_con_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.fathername_textviewid) {
			if(b_fn_name) {
				ly_fn_name.setVisibility(View.GONE);
				b_fn_name=false;
				tv_fn_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_fn_name.setVisibility(View.VISIBLE);
				b_fn_name=true;
				tv_fn_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.mothername_textviewid) {
			if(b_mn_name) {
				ly_mn_name.setVisibility(View.GONE);
				b_mn_name=false;
				tv_mn_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_mn_name.setVisibility(View.VISIBLE);
				b_mn_name=true;
				tv_mn_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}

		}
		else if(v.getId()==R.id.hushbandname_textviewid) {
			if(b_hb_name) {
			    ly_hb_name.setVisibility(View.GONE);
				b_hb_name=false;
				tv_hb_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
				}
			else {
				ly_hb_name.setVisibility(View.VISIBLE);
				b_hb_name=true;
				tv_hb_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.dob_textviewid) {
			if(b_dob) {
				ly_dob.setVisibility(View.GONE);
				b_dob=false;
				tv_dob.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_dob.setVisibility(View.VISIBLE);
				b_dob=true;
				tv_dob.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.nationality_textviewid) {
			if(b_nation) {
				ly_nationality.setVisibility(View.GONE);
				b_nation=false;
				tv_nationality.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_nationality.setVisibility(View.VISIBLE);
				b_nation=true;
				tv_nationality.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}

		}
		else if(v.getId()==R.id.area_textviewid) {
			if(b_area) {
				ly_area.setVisibility(View.GONE);
				b_area=false;
				tv_area.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_area.setVisibility(View.VISIBLE);
				b_area=true;
				tv_area.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.state_textviewid) {
			if(b_state) {
				ly_state.setVisibility(View.GONE);
				b_state=false;
				tv_state.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_state.setVisibility(View.VISIBLE);
				b_state=true;
				tv_state.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.aadharcard_textviewid)
		{
			if(b_adhar) {
				ly_adharno.setVisibility(View.GONE);
				b_adhar=false;
				tv_adharno.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_adharno.setVisibility(View.VISIBLE);
				b_adhar=true;
				tv_adharno.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.identitymark_textviewid) {
			if(b_identity) {
				ly_identity.setVisibility(View.GONE);
				b_identity=false;
				tv_identity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_identity.setVisibility(View.VISIBLE);
				b_identity=true;
				tv_identity.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}

		}
		else if(v.getId()==R.id.criminal_textviewid) {
			if(b_crim) {
				ly_criminal.setVisibility(View.GONE);
				b_crim=false;
				tv_criminal.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_criminal.setVisibility(View.VISIBLE);
				b_crim=true;
				tv_criminal.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}

		}
		else if(v.getId()==R.id.photograph_textviewid) {
			if(b_photo) {
				ly_photograph.setVisibility(View.GONE);
				b_photo=false;
				tv_photograph.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_photograph.setVisibility(View.VISIBLE);
				b_photo=true;
				tv_photograph.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.tv_userimage_id) {
           st_userimage="userimage";
			st_imagename=Constants.Unique_Id+"_"+st_userimage;
			SelectImage();
		}
		else if(v.getId()==R.id.tv_usersign_id) {
			st_userimage="usersign";
			st_imagename=Constants.Unique_Id+"_"+st_userimage;
			SelectImage();
		}
//		else if (v.getId()==R.id.ly_h_cnameid){
//          hindidialog("अभ्यर्थी का नाम");
//		}
//		else if (v.getId()==R.id.ly_h_fnameid){
//			hindidialog("पिता का नाम");
//		}
//		else if (v.getId()==R.id.ly_h_mnameid){
//			hindidialog("माता का नाम");
//		}
//		else if (v.getId()==R.id.ly_h_hnameid){
//			hindidialog("पति का नाम ( विवाहित महिलाओं के लिए)");
//		}
//		else if (v.getId()==R.id.ly_h_dobid){
//			hindidialog("दिनांक जन्म (10 वीं कक्षा के अंक पत्र के अनुसार)");
//		}
//		else if (v.getId()==R.id.ly_h_nationalityid){
//			hindidialog("राष्ट्रीयता");
//		}
//		else if (v.getId()==R.id.ly_h_areaid){
//			hindidialog("क्षेत्र, ग्रामीण, शहरी");
//		}
//		else if (v.getId()==R.id.ly_h_statedomicileid){
//			hindidialog("आपके अधिवास का राज्य");
//		}
//		else if (v.getId()==R.id.ly_h_aadharnoid){
//			hindidialog("आधार कार्ड संख्या (अगर कोई है तो )");
//		}
//		else if (v.getId()==R.id.ly_h_visibleidentyid){
//			hindidialog("अपने शरीर पर दृष्टिगोचर पहचान निशान");
//		}
//		else if (v.getId()==R.id.ly_h_criminalcaseid){
//			hindidialog("किसी भी आपराधिक मामले दायर की है या पहले से ही उम्मीदवार के खिलाफ सिद्ध ?\n" +
//					"     वर्णन करें\n");
//		}
//		else if (v.getId()==R.id.ly_h_photoid){
//			hindidialog("उम्मीदवार की तस्वीर\n" +
//					"& उम्मीदवार के हस्ताक्षर");
//		}
	}

	public void volley() {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(true);
		pd.setMessage("Loading Please Wait");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		detailedt=detailprf.edit();

		RequestQueue queue= Volley.newRequestQueue(getActivity());
		String url;

		if(c_name.length()>0&f_name.length()>0&m_name.length()>0&dob.length()>0)
			st_status="true";

		if(c_name.length()>0&f_name.length()>0&m_name.length()>0&areastr.length()>0&domicilestr.length()>0&aadharstr.length()>0){
		detailedt.putBoolean("basic_info",true);
		}
		else
			detailedt.putBoolean("basic_info",false);

		url= Constants.url+"basicprofile.php?unique_id="+Constants.Unique_Id+"&user_name="+c_name+"&father_name="+f_name+
				"&mother_name="+m_name+"&husband_name="+h_name+"&dob="+dob+"&proof="+dobproof+"&nationality="+nationalitystr+
				"&area="+areastr+"&domicile="+domicilestr+"&aadhar_number="+aadharstr+"&mark="+markstr+
				"&criminal_case="+criminalstr+"&case="+casestr+"&photo1="+""+"&photo2="+""+"&status="+st_status;

		url = url.replace(" ", "%20");
		Log.e("emailcheckvolleyurl", url);
		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {

				pd.dismiss();

				Log.e("loginres", res.toString());

				try {

					//if(res.getString("scalar").equals("Successfully Inserted")) {

						editor=user_info.edit();

						JSONObject jobj=new JSONObject();

						jobj.put("c_fname",c_first.getText().toString());
						jobj.put("c_mname",c_middle.getText().toString());
						jobj.put("c_lname",c_last.getText().toString());
						jobj.put("f_fname",f_first.getText().toString());
						jobj.put("f_mname",f_middle.getText().toString());
						jobj.put("f_lname",f_last.getText().toString());
						jobj.put("m_fname",m_first.getText().toString());
						jobj.put("m_mname",m_middle.getText().toString());
						jobj.put("m_lname",m_last.getText().toString());
						jobj.put("h_fname",h_first.getText().toString());
						jobj.put("h_mname",h_middle.getText().toString());
						jobj.put("h_lname",h_last.getText().toString());
						jobj.put("dob_d",sp_day.getSelectedItem().toString());
						jobj.put("dob_m",sp_month.getSelectedItem().toString());
						jobj.put("dob_y",sp_year.getSelectedItem().toString());
						jobj.put("dob_proof",dobproof);
						jobj.put("area",areastr);
						jobj.put("aadhar_no",aadharstr);
						jobj.put("visible_id_mark",markstr);
						jobj.put("criminal_case_yn", criminalstr);
						jobj.put("criminal_case", casestr);
						jobj.put("domicile_state", domicilestr);

						editor.putString("basic_info",jobj.toString());
						editor.commit();

						Log.e("emailcheckvolley", "done");

					  if(st_status.equalsIgnoreCase("true")==true) {
						  detailedt.putBoolean("basic",true);
					  }
					else
						  detailedt.putBoolean("basic",false);


					detailedt.commit();
					final Snackbar snackbar = Snackbar.make(main_layout, "Successfully Insert.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
//					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();

//						Toast.makeText(getActivity(),"Successfully Inserted",Toast.LENGTH_SHORT).show();

					//}

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("exceptionlogin",e.toString());
					pd.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError e) {

				pd.dismiss();
				final Snackbar snackbar = Snackbar.make(main_layout, "   Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();
//				Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
				Log.e("error", e.toString());
			}
		});
		int socketTimeout = 50000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		jsonreq.setRetryPolicy(policy);
		queue.add(jsonreq);
	}


	public void SelectImage()
	{
		final CharSequence[] options = {"Upload a good quality& clear photo. We will adjust size & font according to your form requirement.\n\n","Take Photo", "Choose from Gallery", "Cancel"};
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {

				if (options[item].equals("Take Photo")) {

//					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//					if     (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//						startActivityForResult(takePictureIntent, 1);
//					}


					File photo = null;
					Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
					if (android.os.Environment.getExternalStorageState().equals(
							android.os.Environment.MEDIA_MOUNTED)) {
						photo = new File(android.os.Environment
								.getExternalStorageDirectory(), "test.jpg");
					} else {
						photo = new File(android.os.Environment
								.getExternalStorageDirectory(), "test.jpg");
					}
					if (photo != null) {
						intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
						selectedImage = Uri.fromFile(photo);
						startActivityForResult(intent, 1);
					}

				} else if (options[item].equals("Choose from Gallery")) {

					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(intent, 2);

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
		builder.setCancelable(false);

	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Log.e("result", resultCode + "");

		if(resultCode==0) {
		}
		else if(resultCode==getActivity().RESULT_OK) {
			if (requestCode == 1)
			{
//				Bundle extras = data.getExtras();
//				Bitmap myBitmap= (Bitmap) extras.get("data");

				try {
					bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),selectedImage);
					//bitmap=decodeUri(getActivity(),selectedImage,100);
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(st_userimage.equalsIgnoreCase("userimage")==true) {
					iv_user.setImageBitmap(bitmap);
					iv_user.setVisibility(View.VISIBLE);
				}
				else {
					iv_sign.setImageBitmap(bitmap);
					iv_sign.setVisibility(View.VISIBLE);
				}

				st_base46 = ImageBase64.encodeTobase64(bitmap);

//				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//				myBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//				byte[] byteArray = byteArrayOutputStream .toByteArray();
//				st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);

				Log.e("Base64_image",st_base46);

				imagevolley();

			} else if (requestCode == 2) {

				selectedImage = data.getData();
				if(st_userimage.equalsIgnoreCase("userimage")==true) {
					iv_user.setImageURI(selectedImage);
					iv_user.setVisibility(View.VISIBLE);
				}
				else {
					iv_sign.setImageURI(selectedImage);
					iv_sign.setVisibility(View.VISIBLE);
				}


				//iv_doc_view.setImageURI(selectedImage);//setImageBitmap(thumbnail);

				try {
					bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),selectedImage);
					//bitmap=decodeUri(getActivity(),selectedImage,100);
				} catch (IOException e) {
					e.printStackTrace();
				}

				st_base46 = ImageBase64.encodeTobase64(bitmap);

//				Log.e("bitmap", bitmap + "");
//				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//				bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//				byte[] byteArray = byteArrayOutputStream .toByteArray();
//				st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);

				Log.e("Base64_image",st_base46);
			    imagevolley();
			}
		}
	}

	public Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
			throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

		int width_tmp = o.outWidth
				, height_tmp = o.outHeight;
		int scale = 1;

		while(true) {
			if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
	}

	public void imagevolley() {

		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(true);
		pd.setMessage("Uploading Please Wait...");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		pd.show();

		RequestQueue queue = Volley.newRequestQueue(getActivity());
		String url="";
		url = Constants.url +"upload_userimage.php?";
		url = url.replace(" ", "%20");
		Log.e("name", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Log.e("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject obj=new JSONObject(s);
						s=obj.getString("scalar");
						if(s.equalsIgnoreCase("Successfully Inserted")==true)
						{
							final Snackbar snackbar = Snackbar.make(main_layout, "Upload Successfully.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
//							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//							Toast.makeText(getActivity(),"Upload Successfully",Toast.LENGTH_LONG).show();
						}
						else if(s.equalsIgnoreCase("Can't Inserted")==true)
						{
							final Snackbar snackbar = Snackbar.make(main_layout, "   Somthing is Wrong Can't Upload, Please Try Again.", Snackbar.LENGTH_INDEFINITE);

							snackbar.setActionTextColor(Color.WHITE);
							snackbar.setDuration(3500);
							View snackbarView = snackbar.getView();
							snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

							TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
							textView.setTextColor(Color.WHITE);//change Snackbar's text color;
							textView.setGravity(Gravity.CENTER_VERTICAL);
							textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
							textView.setCompoundDrawablePadding(0);

							snackbar.show();
//							Toast.makeText(getActivity(),"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
						}

						pd.dismiss();
						Log.e("response_string",s+" response");
					}
				}

				catch (Exception e)
				{
					pd.dismiss();
					Log.e("e","e",e);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {

				Log.e("imagevolley_error",volleyError.toString());
				pd.dismiss();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("st_base64", st_base46);
				params.put("uniqueid", Constants.Unique_Id);
				params.put("imagename", st_imagename);
				params.put("imagetype", st_userimage);

				Log.e("img222222", st_base46);
				Log.e("st_userimagename", st_userimage);

				return params;
			}
		};

		int socketTimeout = 10000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);
	}


	public void hindidialog(String text) {

		final Dialog dialog=new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.hindi_dialog);

		//dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		dialog.show();

		TextView tv_head=(TextView)dialog.findViewById(R.id.hindi_textid);

		TextView ok=(TextView)dialog.findViewById(R.id.dialog_ok);

		tv_head.setText(text);

		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});
	}

}