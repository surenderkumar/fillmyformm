package job.job.consaltancy.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;
import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.R;
import job.job.consaltancy.adapter.PagerAdapter_tab;
import job.job.consaltancy.utils.Constants;

/**
 * Created by Surbhi on 03-03-2016.
 */
public class Tab_Fragment extends Fragment implements MaterialTabListener {
    View view;

    private ViewPager viewPager;
    private MaterialTabHost tabLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.tab_fragment, container, false);
        // b3=(FloatingActionButton)view.findViewById(R.id.fab32);

        tabLayout = (MaterialTabHost) view.findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText("Home").setTabListener(this));
        tabLayout.addTab(tabLayout.newTab().setText("Form Available").setTabListener(this));
        tabLayout.addTab(tabLayout.newTab().setText("Others").setTabListener(this));


        viewPager = (ViewPager) view.findViewById(R.id.pager);
        PagerAdapter_tab adapter = new PagerAdapter_tab(getChildFragmentManager(), 3);
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {

                    @Override
                    public void onPageSelected(int position) {
                        // when user do a swipe the selected tab change
                        tabLayout.setSelectedNavigationItem(position);
                        switch (position) {
                            case 0:
                                // HomeActivity.et_search.setVisibility(View.GONE);
                                Constants.fragment = 0;
                                HomeActivity.tv_appname.setVisibility(View.VISIBLE);
                                break;
                            case 1:
                                Constants.fragment = 1;
                                // HomeActivity.et_search.setVisibility(View.VISIBLE);
                                HomeActivity.tv_appname.setVisibility(View.VISIBLE);
                                break;
                            case 2:
                                Constants.fragment = 2;
                                // HomeActivity.et_search.setVisibility(View.GONE);
                                HomeActivity.tv_appname.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });


        return view;

    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        tabLayout.setSelectedNavigationItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }
}
