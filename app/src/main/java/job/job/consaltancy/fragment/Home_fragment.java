package job.job.consaltancy.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.R;
import job.job.consaltancy.adapter.PagerAdapter;
import job.job.consaltancy.getset.get_slide;
import job.job.consaltancy.utils.Constants;
import me.relex.circleindicator.CircleIndicator;


public class Home_fragment extends Fragment {

    TextView  tv_pr_noti, tv_apl_noti;
    ViewPager viewPager;
    PagerAdapter adapter;
    ImageView iv_bascheck, iv_catcheck, iv_addcheck, iv_educheck;
    CircleIndicator indicator;
    TimerTask timer;
    int i = 0;
    ArrayList<get_slide> data;
    LinearLayout tv_basic, tv_category, tv_education, tv_address, tv_experience, tv_document, tv_addmore, tv_other,processing_job, applied_job;
    SharedPreferences user_info, detailprf;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if(v==null) {
            v = inflater.inflate(R.layout.home_fragment, container, false);
            user_info = getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
            detailprf = getActivity().getSharedPreferences("detailprf", Context.MODE_PRIVATE);
            Constants.Unique_Id = user_info.getString("unique_id", "");
            Constants.Email_Id = user_info.getString("email", "");
            Constants.State = user_info.getString("state", "");
            Constants.District = user_info.getString("district", "");

            processing_job = (LinearLayout) v.findViewById(R.id.processing_jobid);
            applied_job = (LinearLayout) v.findViewById(R.id.applied_jobid);

            viewPager = (ViewPager) v.findViewById(R.id.viewPager);
            indicator = (CircleIndicator) v.findViewById(R.id.indicator);
            viewPager.setOffscreenPageLimit(2);
            volley();
//        adapter = new PagerAdapter(getChildFragmentManager());
//        viewPager.setAdapter(adapter);
//        indicator.setViewPager(viewPager);

            tv_basic = (LinearLayout) v.findViewById(R.id.tv_basicid);
            tv_category = (LinearLayout) v.findViewById(R.id.tv_categoryid);
            tv_education = (LinearLayout) v.findViewById(R.id.tv_educationid);
            tv_address = (LinearLayout) v.findViewById(R.id.tv_addressid);
            tv_experience = (LinearLayout) v.findViewById(R.id.tv_experienceid);
            tv_document = (LinearLayout) v.findViewById(R.id.tv_documentsid);
            tv_addmore = (LinearLayout) v.findViewById(R.id.tv_addmoreid);
            tv_other = (LinearLayout) v.findViewById(R.id.tv_otherid);
            tv_pr_noti = (TextView) v.findViewById(R.id.tv_processing_noti_id);
            tv_apl_noti = (TextView) v.findViewById(R.id.tv_applied_noti_id);

            iv_addcheck = (ImageView) v.findViewById(R.id.iv_addcheck_id);
            iv_bascheck = (ImageView) v.findViewById(R.id.iv_basiccheck_id);
            iv_educheck = (ImageView) v.findViewById(R.id.iv_educheck_id);
            iv_catcheck = (ImageView) v.findViewById(R.id.iv_catechecked_id);

            Log.e("wallet_money", String.valueOf(Constants.Wallet_Money) + " wallet money");
            Constants.fragment = 0;
            //HomeActivity.tv_point.setText("Rs. "+String.valueOf(Constants.Wallet_Money));


//		if(detailprf.getBoolean("basic_info",false))
//			iv_bascheck.setVisibility(View.VISIBLE);
//
//		if(detailprf.getBoolean("category_info",false))
//			iv_catcheck.setVisibility(View.VISIBLE);
//
//		if(detailprf.getBoolean("Address_info",false))
//			iv_addcheck.setVisibility(View.VISIBLE);


            if (Constants.Process_Job_Noti > 0) {
                tv_pr_noti.setVisibility(View.VISIBLE);
                tv_pr_noti.setText(String.valueOf(Constants.Process_Job_Noti));
            } else {
//            tv_pr_noti.setVisibility(View.GONE);
            }

            if (Constants.Applied_Job_Noti > 0) {
                tv_apl_noti.setVisibility(View.VISIBLE);
                tv_apl_noti.setText(String.valueOf(Constants.Applied_Job_Noti));
            } else {
//            tv_apl_noti.setVisibility(View.GONE);
            }

//		final Handler handler= new Handler() ;
//		Timer time=new Timer();
//		time.schedule(timer, 0, 5000);
//
//		timer=new TimerTask() {
//			@Override
//			public void run() {
//				handler.post(new Runnable() {
//
//					@Override
//					public void run() {
//						viewPager.setCurrentItem(i % 4);
//
//						i++;
//					}
//				});
//			}
//		};

//        final Handler handler = new Handler();
//
//        final Runnable update = new Runnable() {
//            public void run() {
//                if (i == 5) {
//                    i = 0;
//                }
//                viewPager.setCurrentItem(i++, true);
//            }
//        };
//
//        new Timer().schedule(new TimerTask() {
//
//            @Override
//            public void run() {
//                handler.post(update);
//            }
//        }, 2000, 5000);


            processing_job.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Jobs_in_processing acc_frag = new Jobs_in_processing();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, acc_frag).addToBackStack(null).commit();
                    Constants.Process_Job_Noti = 0;
                }
            });

            applied_job.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Applied_job_fragment acc_frag = new Applied_job_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, acc_frag).addToBackStack(null).commit();
                    Constants.Applied_Job_Noti = 0;
                }
            });

            tv_basic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Basic_Info_fragment basic_frag = new Basic_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });

            tv_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Category_Info_fragment basic_frag = new Category_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });

            tv_education.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Education_Info_fragment basic_frag = new Education_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });

            tv_address.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Address_Info_fragment basic_frag = new Address_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });

            tv_experience.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Experience_Info_fragment basic_frag = new Experience_Info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });
            tv_document.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Document_upload_fragment basic_frag = new Document_upload_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });
            tv_other.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Other_info_fragment basic_frag = new Other_info_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });
            tv_addmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.fragment = 3;
                    Addmore_info_insert basic_frag = new Addmore_info_insert();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).addToBackStack(null).commit();
                }
            });

        }
        return v;
    }

    public void volley()
    {

        data=new ArrayList<get_slide>();

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        Log.e("url= ", "jjava.lang.String[]iii");
        String url = Constants.url+"slider_images.php?";
        Log.e("url= ", url);

        JsonObjectRequest request = new JsonObjectRequest( url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
//                dialog.dismiss();
                try {
                    if(res.has("images")) {
                        JSONArray jr = res.getJSONArray("images");

//                    Log.e("response=", res.getJSONArray("data").toString());
                        int len = jr.length();
                        String data1[]=new String[jr.length()];
//                    Log.e("length=", String.valueOf(len));

                        for (int i = 0; i < jr.length(); i++) {
                            JSONObject jsonobj = jr.getJSONObject(i);           //String nom, String prenom, String email, String age, String deport, String actuel
                            data.add(new get_slide(jsonobj.getInt("id"), jsonobj.getString("image")));
                            data1[i]=Constants.url+"earnimages/"+jsonobj.getString("image");
                            Log.e("image",data1[i]+"");
//                        db.insertgroup(jsonobj.getInt("id"), jsonobj.getString("admin_name"), jsonobj.getString("group_name"), jsonobj.getString("about"), jsonobj.getString("alt_mobile"), jsonobj.getString("category"), jsonobj.getString("subcategory"), jsonobj.getString("email"), jsonobj.getString("weburl"), jsonobj.getString("state"), jsonobj.getString("city"), jsonobj.getString("address"), jsonobj.getString("date"),jsonobj.getString("admin_thumb"), jsonobj.getString("group_thumb"), jsonobj.getString("title"), jsonobj.getString("comment"), jsonobj.getString("follow"), jsonobj.getString("profile_name"));
                        }
                        final int p=data.size();
                        Log.e("imagelength",p+" aaa");
                        adapter= new PagerAdapter(getChildFragmentManager(),data1);
                        viewPager.setAdapter(adapter);
                        indicator.setViewPager(viewPager);

                        final Handler handler= new Handler() ;

                        timer=new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        viewPager.setCurrentItem(i % p);

                                        i++;
                                    }
                                });
                            }
                        };
                        Timer time=new Timer();
                        time.schedule(timer, 0, 5000);

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Log.e("catch exp= ", e.toString());
                    e.printStackTrace();
//                    dialog.dismiss();
                }

            }

        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {

//                Log.e("error", arg0.toString());
//                dialog.dismiss();
            }
        });
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }




}