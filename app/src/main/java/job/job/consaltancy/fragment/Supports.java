package job.job.consaltancy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 8/31/2016.
 */

public class Supports extends AppCompatActivity {

    WebView wv;
    TextView tv_head;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_confirmation_file);



        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Help \u0026 Support");

        wv=(WebView)findViewById(R.id.webview_id);


        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
        wv.loadData(Constants.Support, "text/html; charset=UTF-8", null);
        wv.loadDataWithBaseURL(null, Constants.Support, "text/html", "utf-8",null);

    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v =inflater.inflate(R.layout.webview_detail, container,false);
//
//        wv=(WebView)v.findViewById(R.id.webview_id);
//        tv_head=(TextView)v.findViewById(R.id.tv_heading_id);
//
//        tv_head.setText("Supports");
//
//        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        wv.loadData(Constants.Support, "text/html; charset=UTF-8", null);
//        wv.loadDataWithBaseURL(null, Constants.Support, "text/html", "utf-8",null);
//
//        return v;
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);

    }
}
