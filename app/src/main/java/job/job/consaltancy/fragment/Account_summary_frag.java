package job.job.consaltancy.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Account_adapter;
import job.job.consaltancy.adapter.Job_home_adapter;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */

public class Account_summary_frag extends Fragment {

    Account_adapter adapter;
    ListView accountlist;
    TextView tv_earn,tv_email,tv_uname,tv_point;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.account_summary, container,false);

        accountlist = (ListView)v. findViewById(R.id.account_listid);
        tv_earn=(TextView)v.findViewById(R.id.tv_earnid);
        tv_email=(TextView)v.findViewById(R.id.emailid);
        tv_uname=(TextView)v.findViewById(R.id.usernameid);
        tv_point=(TextView)v.findViewById(R.id.tv_pointid);

        adapter=new Account_adapter(getActivity());
        accountlist.setAdapter(adapter);

        tv_point.setText("Rs."+ Constants.Wallet_Money);

        Typeface cf = Typeface.createFromAsset(getActivity().getAssets(), "PTN77F.ttf");
        tv_uname.setTypeface(cf);

        Typeface cf1 = Typeface.createFromAsset(getActivity().getAssets(), "droidsans_nor.ttf");
        tv_email.setTypeface(cf1);

        tv_earn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Share_and_Earn acc_frag=new Share_and_Earn();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout,acc_frag).addToBackStack(null).commit();

            }
        });

        return v;
    }
}
