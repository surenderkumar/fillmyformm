package job.job.consaltancy.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.parse.ParseInstallation;
import com.parse.PushService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.ParseApplication;
import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Job_set_noti_adap;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Job_set_notification extends Fragment {

    ListView list;
    Job_set_noti_adap adap;
    ArrayList<String> data;
    public static ArrayList<String> selection,selectionname;
    String selection_id;
    CheckBox cb_all;
    FloatingActionButton submit;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.job_set_notification, container,false);

        data=new ArrayList<>();
        selection=new ArrayList<>();
        selectionname=new ArrayList<>();
        selection.clear();
        selectionname.clear();
        pref=getActivity().getSharedPreferences("notification", Context.MODE_PRIVATE);

        list=(ListView)v.findViewById(R.id.list_id);
        cb_all=(CheckBox)v.findViewById(R.id.cb_selectall_id);
        submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);
        data.add("UPSC Jobs Form");
        data.add("SSC Jobs Form");
        data.add("State Govt.Jobs Form");
        data.add("Banking Jobs Form");
        data.add("Teaching Jobs Form");
        data.add("Engineering Jobs Form");
        data.add("General Purpose Form");
        data.add("PSUs Jobs Form");
        data.add("Admission Form");
        data.add("IT Sector Jobs Form");
        data.add("Diploma Jobs Form");
        data.add("Other Graduate Jobs Form");
        data.add("12th based Jobs Form");
        data.add("10 based Jobs Form");
        data.add("ITI Jobs Form");
        data.add("Pharma Jobs Form");
        data.add("Medical and Health Jobs Form");
        data.add("Railway Jobs Form");
        data.add("Defence Jobs Form");
        data.add("Police Jobs Form");
        data.add("Army Jobs Form");
        data.add("Navy Jobs Form");
        data.add("Other Govt Jobs Form");
        data.add("Other Private Jobs Form");

        for(int i=0;i<data.size();i++){
            String vl= pref.getString("value"+i,"-1");
            Log.e("selection_vl",vl+" value"+i);
            if(Integer.parseInt(vl)<0) {

            }
            else {
                selection.add(vl);
                Log.e("selection_id",vl+" value");
            }
        }
        adap=new Job_set_noti_adap(getActivity(),data);
        list.setAdapter(adap);


        cb_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                edit=pref.edit();

                if(isChecked) {
                    selection.clear();
                    for (int i=0;i<data.size();i++){
                        selection.add(String.valueOf(i));
                        selectionname.add(data.get(i));
                        edit.putString("value"+i,String.valueOf(i));
                    }
                }
                else {
                    selection.clear();
                    selectionname.clear();
                    edit.clear();
                }

                edit.commit();


                adap.notifyDataSetChanged();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("selection_value", selection.toString());

                edit=pref.edit();

                edit.clear();

                if (selectionname.size() > 0) {
                    selection_id=selectionname.get(0);
                    edit.putString("value",selection.get(0));


                    //Constants.Job_Query="job_type="+selectionname.get(0);

                    Log.e("selection_id",selection_id+" value");

                    if(selectionname.size()>1){
                        for(int i=0;i<selectionname.size();i++) {
                            selection_id = selection_id + "," + selectionname.get(i);
                           // Constants.Job_Query=Constants.Job_Query+" OR job_type= "+selectionname.get(i);
                            edit.putString("value"+i,selection.get(i));

                            Log.e("selection_id",selection_id+" value");
                        }

                        volley();
                    }
                    else

                        volley();

                } else
                    Toast.makeText(getActivity(),"Please Select Atleast On Field",Toast.LENGTH_LONG).show();

                //edit.putString("job_query",Constants.Job_Query);

                edit.commit();
            }
        });
        return v;
    }

    public void volley() {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(true);
        pd.setMessage("Loading Please Wait");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        pd.show();

        RequestQueue queue= Volley.newRequestQueue(getActivity());
        String url;

        url= Constants.url+"insertjobfield.php?unique_id="+Constants.Unique_Id +"&job_id="+selection_id;

        url = url.replace(" ", "%20");

        Log.e("emailcheckvolleyurl", url);

        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                pd.dismiss();

                Log.e("loginres", res.toString());

                try {

                    JSONObject obj=new JSONObject();
                    obj.put("abc","abc");

                    Toast.makeText(getActivity(),"Successfully Inserted",Toast.LENGTH_SHORT).show();
                    Intent in=new Intent(getActivity(),HomeActivity.class);
                    startActivity(in);

                    //}

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("exceptionlogin",e.toString());
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {

                pd.dismiss();
                Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
                Log.e("error", e.toString());
            }
        });
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }


}
