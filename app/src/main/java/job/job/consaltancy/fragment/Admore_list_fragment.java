package job.job.consaltancy.fragment;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Add_more_view_adapter;
import job.job.consaltancy.adapter.Other_info_adapter;
import job.job.consaltancy.getset.other_info_admin_getset;
import job.job.consaltancy.getset.other_info_user_getset;
import job.job.consaltancy.utils.Constants;


public class Admore_list_fragment extends Fragment
{

	LinearLayout main_layout;
	ListView list;
	Add_more_view_adapter adapter;
	ArrayList<String> info_list,info_type;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.add_more_list, container,false);

		main_layout=(LinearLayout)v.findViewById(R.id.main_layout);
		list=(ListView)v.findViewById(R.id.list_id);

		volley();

		return v;
	}

	public void volley() {
		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setMessage("Loading..Please wait.");
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCanceledOnTouchOutside(false);
		dialog.getWindow().setGravity(Gravity.CENTER);
		dialog.show();

		RequestQueue queue= Volley.newRequestQueue(getActivity());

		String url= Constants.url+"get_admore_info.php?unique_id="+Constants.Unique_Id;
		Log.e("other_url= ", url);

		info_list=new ArrayList<>();
		info_type=new ArrayList<>();

		url=url.replaceAll(" ","%20");

		JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST,url, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res)
			{

				try {

					if(res.has("user_info")) {
						JSONArray array = res.getJSONArray("user_info");
						int len = array.length();
						Log.e("length=", String.valueOf(len));

						for (int i = 0; i < array.length(); i++) {
							JSONObject obj = array.getJSONObject(i);

							info_list.add(obj.getString("info"));
							info_type.add(obj.getString("info_type"));
						}

					}
					else
					{
						final Snackbar snackbar = Snackbar.make(main_layout, "   Somthing is Wrong Can't get value, Please Try Again.", Snackbar.LENGTH_INDEFINITE);

						snackbar.setActionTextColor(Color.WHITE);
						snackbar.setDuration(3500);
						View snackbarView = snackbar.getView();
						snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

						TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
						textView.setTextColor(Color.WHITE);//change Snackbar's text color;
						textView.setGravity(Gravity.CENTER_VERTICAL);
						textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
						textView.setCompoundDrawablePadding(0);

						snackbar.show();
					}
//					Toast.makeText(getActivity(),"Somthing is Wrong Can't get value, Please Try Again",Toast.LENGTH_LONG).show();

					adapter=new Add_more_view_adapter(getActivity(),info_list,info_type);
					list.setAdapter(adapter);

					dialog.dismiss();

				} catch (JSONException e) {
					Log.e("catch exp= ", e.toString());
					dialog.dismiss();
					e.printStackTrace();
				}
			}

		}
				,new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				dialog.dismiss();

				String error=arg0.toString();
				if (error.contains("NoConnectionError")) {
					final Snackbar snackbar = Snackbar.make(main_layout, "   No Connection Error.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
//					Toast.makeText(getActivity(), "NoConnectionError", Toast.LENGTH_LONG).show();
				}
				else
				{
					final Snackbar snackbar = Snackbar.make(main_layout, "   Can't data, We Update Soon.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
				}
//					Toast.makeText(getActivity(), "Can't data, We Update Soon", Toast.LENGTH_LONG).show();

				Log.e("error", arg0.toString());
			}
		});
		queue.add(request);
	}



}