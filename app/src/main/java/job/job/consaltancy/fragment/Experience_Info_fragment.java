package job.job.consaltancy.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;


public class Experience_Info_fragment extends Fragment implements View.OnClickListener {

	FloatingActionButton tv_submit;
	LinearLayout  ly_govt_org,ly_work_exp,ly_other;
	LinearLayout  lys_govt_org,lys_work_exp,lys_other;
	TextView tv_govt_org,tv_work_exp,tv_other;
	RadioButton rb_govt_org_yes,rb_govt_org_no,rb_work_exp_yes,rb_work_exp_no,rb_other_yes,rb_other_no;
	RelativeLayout main_layout;
	Spinner sp_go_str_from_mth,sp_go_str_from_yr,sp_we_str_from_mth,sp_we_str_from_yr,sp_we_str_to_mth,sp_we_str_to_yr;
	Spinner sp_otr_str_from_mth,sp_otr_str_from_yr,sp_otr_str_to_mth,sp_otr_str_to_yr;

	EditText et_go_name,et_go_type,et_go_place,et_go_empid,et_go_postname,et_go_dutytype;
	EditText et_we_name,et_we_type,et_we_place,et_we_empid,et_we_postname,et_we_dutytype,et_we_ser_time;
	EditText et_otr_name,et_otr_type,et_otr_place,et_otr_empid,et_otr_postname,et_otr_dutytype,et_otr_ser_time;

	boolean b_go=false,b_we=false,b_otr=false;
	String b_go_yes_no="no",b_we_yes_no="no",b_otr_yes_no="no",st_status="false";

	ArrayList<String> month_list,year_list;

	String st_go_name,st_go_type,st_go_place,st_go_empid,st_go_post,st_go_duty,st_go_ser_str;
	String st_we_name,st_we_type,st_we_place,st_we_empid,st_we_post,st_we_duty,st_we_ser_time,st_we_ser_from,st_we_ser_to;
	String st_otr_name,st_otr_type,st_otr_place,st_otr_empid,st_otr_post,st_otr_duty,st_otr_ser_time,st_otr_ser_from,st_otr_ser_to;

	SharedPreferences user_info,detailprf;
	SharedPreferences.Editor editor,detailedt;

	NetworkConnection nw;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v =inflater.inflate(R.layout.fill_experience_info, container,false);

		nw=new NetworkConnection(getActivity());

		user_info=getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		detailprf=getActivity().getSharedPreferences("detailprf",Context.MODE_PRIVATE);


		rb_govt_org_yes=(RadioButton)v.findViewById(R.id.rb_govt_org_yesid);
		rb_govt_org_no=(RadioButton)v.findViewById(R.id.rb_govt_org_noid);
		rb_work_exp_yes=(RadioButton)v.findViewById(R.id.rb_work_exp_yesid);
		rb_work_exp_no=(RadioButton)v.findViewById(R.id.rb_work_exp_noid);
		rb_other_no=(RadioButton)v.findViewById(R.id.rb_otr_exp_noid);
		rb_other_yes=(RadioButton)v.findViewById(R.id.rb_otr_exp_yesid);

		sp_go_str_from_mth=(Spinner)v.findViewById(R.id.sp_ser_str_monthid);
		sp_go_str_from_yr=(Spinner)v.findViewById(R.id.sp_ser_str_yearid);

		sp_we_str_from_mth=(Spinner)v.findViewById(R.id.sp_we_ser_from_monthid);
		sp_we_str_from_yr=(Spinner)v.findViewById(R.id.sp_we_ser_from_yearid);
		sp_we_str_to_mth=(Spinner)v.findViewById(R.id.sp_we_ser_to_monthid);
		sp_we_str_to_yr=(Spinner)v.findViewById(R.id.sp_we_ser_to_yearid);

		sp_otr_str_from_mth=(Spinner)v.findViewById(R.id.sp_otr_ser_from_monthid);
		sp_otr_str_from_yr=(Spinner)v.findViewById(R.id.sp_otr_ser_from_yearid);
		sp_otr_str_to_mth=(Spinner)v.findViewById(R.id.sp_otr_ser_to_monthid);
		sp_otr_str_to_yr=(Spinner)v.findViewById(R.id.sp_otr_ser_to_yearid);

		main_layout=(RelativeLayout)v.findViewById(R.id.main_layout);
		ly_govt_org=(LinearLayout)v.findViewById(R.id.govt_org_layoutid);
		ly_work_exp=(LinearLayout)v.findViewById(R.id.work_exp_layoutid);
		ly_other=(LinearLayout)v.findViewById(R.id.other_layoutid);

		lys_govt_org=(LinearLayout)v.findViewById(R.id.govt_org_detail_layoutid);
		lys_work_exp=(LinearLayout)v.findViewById(R.id.work_exp_detail_layoutid);
		lys_other=(LinearLayout)v.findViewById(R.id.other_detail_layoutid);

		tv_govt_org=(TextView)v.findViewById(R.id.govt_org_textviewid);
		tv_work_exp=(TextView)v.findViewById(R.id.work_exp_textviewid);
		tv_other=(TextView)v.findViewById(R.id.other_exp_textviewid);
		tv_submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);

		et_go_name=(EditText)v.findViewById(R.id.et_org_nameid);
		et_go_type=(EditText)v.findViewById(R.id.et_org_typeid);
		et_go_place=(EditText)v.findViewById(R.id.et_org_placeid);
		et_go_empid=(EditText)v.findViewById(R.id.et_org_empid_id);
		et_go_postname=(EditText)v.findViewById(R.id.et_org_postnameid);
		et_go_dutytype=(EditText)v.findViewById(R.id.et_org_dutytypeid);

		et_we_name=(EditText)v.findViewById(R.id.et_we_org_nameid);
		et_we_type=(EditText)v.findViewById(R.id.et_we_org_typeid);
		et_we_place=(EditText)v.findViewById(R.id.et_we_org_placeid);
		et_we_empid=(EditText)v.findViewById(R.id.et_we_empid_id);
		et_we_postname=(EditText)v.findViewById(R.id.et_we_postnameid);
		et_we_dutytype=(EditText)v.findViewById(R.id.et_we_typeofdutyid);
		et_we_ser_time=(EditText)v.findViewById(R.id.et_we_experid);

		et_otr_name=(EditText)v.findViewById(R.id.et_otr_org_nameid);
		et_otr_type=(EditText)v.findViewById(R.id.et_otr_org_typeid);
		et_otr_place=(EditText)v.findViewById(R.id.et_otr_org_placeid);
		et_otr_empid=(EditText)v.findViewById(R.id.et_otr_empid_id);
		et_otr_postname=(EditText)v.findViewById(R.id.et_otr_postnameid);
		et_otr_dutytype=(EditText)v.findViewById(R.id.et_otr_typeofdutyid);
		et_otr_ser_time=(EditText)v.findViewById(R.id.et_otr_experid);

		rb_work_exp_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_work_exp.setVisibility(View.VISIBLE);
					b_we_yes_no="yes";
				}
			}
		});
		rb_work_exp_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_work_exp.setVisibility(View.GONE);
					b_we_yes_no="no";

//					st_we_name = "";
//					st_we_type = "";
//					st_we_place = "";
//					st_we_empid = "";
//					st_we_post = "";
//					st_we_duty ="";
//					st_we_ser_time = "";

				}
			}
		});
		rb_govt_org_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_govt_org.setVisibility(View.VISIBLE);
					b_go_yes_no="yes";
				}
			}
		});
		rb_govt_org_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_govt_org.setVisibility(View.GONE);
					b_go_yes_no="no";
//					st_go_name = "";
//					st_go_type = "";
//					st_go_place = "";
//					st_go_empid = "";
//					st_go_post = "";
//					st_go_duty = "";
//					st_go_ser_str ="";
				}
			}
		});
		rb_other_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_other.setVisibility(View.VISIBLE);
					b_otr_yes_no="yes";
				}
			}
		});
		rb_other_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					lys_other.setVisibility(View.GONE);
					b_otr_yes_no="no";

//					st_otr_name = "";
//					st_otr_type = "";
//					st_otr_place = "";
//					st_otr_empid = "";
//					st_otr_post = "";
//					st_otr_duty = "";
//					st_otr_ser_time = "";

				}
			}
		});

		month_list=new ArrayList<>();
		year_list=new ArrayList<>();

		for (int i=1;i<13;i++)
			month_list.add(String.valueOf(i));
		for (int i=1970;i<2020;i++)
			year_list.add(String.valueOf(i));

		ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, month_list);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_go_str_from_mth.setAdapter(monthAdapter);
		sp_we_str_from_mth.setAdapter(monthAdapter);
		sp_we_str_to_mth.setAdapter(monthAdapter);
		sp_otr_str_from_mth.setAdapter(monthAdapter);
		sp_otr_str_to_mth.setAdapter(monthAdapter);

		ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, year_list);
		yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_go_str_from_yr.setAdapter(yearAdapter);
		sp_we_str_from_yr.setAdapter(yearAdapter);
		sp_we_str_to_yr.setAdapter(yearAdapter);
		sp_otr_str_from_yr.setAdapter(yearAdapter);
		sp_otr_str_to_yr.setAdapter(yearAdapter);


		String jstr=user_info.getString("experience_info","");

		Log.e("experience_info",jstr);

        String temp;
		if(jstr.length()>0) {
			try {
				JSONObject jobj=new JSONObject(jstr);

				if(jobj.getString("go_exp").equalsIgnoreCase("yes")==true)
					rb_govt_org_yes.setChecked(true);
				else
					rb_govt_org_no.setChecked(true);

				if(jobj.getString("we_exp").equalsIgnoreCase("yes")==true)
					rb_work_exp_yes.setChecked(true);
				else
					rb_work_exp_no.setChecked(true);

				if(jobj.getString("otr_exp").equalsIgnoreCase("yes")==true)
					rb_other_yes.setChecked(true);
				else
					rb_other_no.setChecked(true);


				et_go_name.setText(jobj.getString("go_name"));
				et_go_type.setText(jobj.getString("go_type"));
				et_go_place.setText(jobj.getString("go_place"));
				et_go_empid.setText(jobj.getString("go_empid"));
				et_go_postname.setText(jobj.getString("go_post"));
				et_go_dutytype.setText(jobj.getString("go_duty"));
				//et_go_.setText(jobj.getString("go_ser_start"));

				et_we_name.setText(jobj.getString("we_name"));
				et_we_type.setText(jobj.getString("we_type"));
				et_we_place.setText(jobj.getString("we_place"));
				et_we_empid.setText(jobj.getString("we_empid"));
				et_we_postname.setText(jobj.getString("we_post"));
				et_we_dutytype.setText(jobj.getString("we_duty"));
				//et_we_ser_time.setText(jobj.getString("go_ser_time"));

				et_otr_name.setText(jobj.getString("otr_name"));
				et_otr_type.setText(jobj.getString("otr_type"));
				et_otr_place.setText(jobj.getString("otr_place"));
				et_otr_empid.setText(jobj.getString("otr_empid"));
				et_otr_postname.setText(jobj.getString("otr_post"));
				et_otr_dutytype.setText(jobj.getString("otr_duty"));
				//et_otr_ser_time.setText(jobj.getString("otr_ser_time"));



				temp=jobj.getString("go_ser_start");
				if(temp.contains("/")) {
					String[] arr = temp.split("/");
					sp_go_str_from_mth.setSelection(month_list.indexOf(arr[0]));
					sp_go_str_from_yr.setSelection(year_list.indexOf(arr[1]));
				}

				temp=jobj.getString("we_ser_from");
				String [] arr1=temp.split("/");

				sp_we_str_from_mth.setSelection(month_list.indexOf(arr1[0]));
				sp_we_str_from_yr.setSelection(year_list.indexOf(arr1[1]));

				temp=jobj.getString("we_ser_to");

				if(temp.contains("/")) {
					String[] arr2 = temp.split("/");
					sp_we_str_to_mth.setSelection(month_list.indexOf(arr2[0]));
					sp_we_str_to_yr.setSelection(year_list.indexOf(arr2[1]));
				}

				temp=jobj.getString("otr_ser_from");
				if(temp.contains("/")) {
					String[] arr3 = temp.split("/");
					sp_otr_str_from_mth.setSelection(month_list.indexOf(arr3[0]));
					sp_otr_str_from_yr.setSelection(year_list.indexOf(arr3[1]));
				}

				temp=jobj.getString("otr_ser_to");
				if(temp.contains("/")) {
					String[] arr4 = temp.split("/");
					sp_otr_str_to_mth.setSelection(month_list.indexOf(arr4[0]));
					sp_otr_str_to_yr.setSelection(year_list.indexOf(arr4[1]));
				}



			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("experience_infor_exp",e.toString());
			}
		}

		tv_govt_org.setOnClickListener(this);
		tv_work_exp.setOnClickListener(this);
		tv_other.setOnClickListener(this);
		tv_submit.setOnClickListener(this);

		return v;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==R.id.govt_org_textviewid){
			if(b_go) {
				ly_govt_org.setVisibility(View.GONE);
				b_go=false;
				tv_govt_org.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else{
				ly_govt_org.setVisibility(View.VISIBLE);
				b_go=true;
				tv_govt_org.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.work_exp_textviewid){
			if(b_we) {
				ly_work_exp.setVisibility(View.GONE);
				b_we=false;
				tv_work_exp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else{
				ly_work_exp.setVisibility(View.VISIBLE);
				b_we=true;
				tv_work_exp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.other_exp_textviewid){
			if(b_otr) {
				ly_other.setVisibility(View.GONE);
				b_otr=false;
				tv_other.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else{
				ly_other.setVisibility(View.VISIBLE);
				b_otr=true;
				tv_other.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}

		else if(v.getId()==R.id.submitbtn){
//			EditText et_go_name,et_go_type,et_go_place,et_go_empid,et_go_postname,et_go_dutytype;
//			EditText et_we_name,et_we_type,et_we_place,et_we_empid,et_we_postname,et_we_dutytype,et_we_ser_time;
//			EditText et_otr_name,et_otr_type,et_otr_place,et_otr_empid,et_otr_postname,et_otr_dutytype,et_otr_ser_time;

			//if(b_go_yes_no) {
				st_go_name = et_go_name.getText().toString();
				st_go_type = et_go_type.getText().toString();
				st_go_place = et_go_place.getText().toString();
				st_go_empid = et_go_empid.getText().toString();
				st_go_post = et_go_postname.getText().toString();
				st_go_duty = et_go_dutytype.getText().toString();
				st_go_ser_str = sp_go_str_from_mth.getSelectedItem() + "/" + sp_go_str_from_yr.getSelectedItem();
			//}

			//if(b_we_yes_no) {
				st_we_name = et_we_name.getText().toString();
				st_we_type = et_we_type.getText().toString();
				st_we_place = et_we_place.getText().toString();
				st_we_empid = et_we_empid.getText().toString();
				st_we_post = et_we_postname.getText().toString();
				st_we_duty = et_we_dutytype.getText().toString();
				st_we_ser_from = sp_we_str_from_mth.getSelectedItem() + "/" + sp_we_str_from_yr.getSelectedItem();
				st_we_ser_to = sp_we_str_to_mth.getSelectedItem() + "/" + sp_we_str_to_yr.getSelectedItem();

			//}

			//if(b_otr_yes_no) {
				st_otr_name = et_otr_name.getText().toString();
				st_otr_type = et_otr_type.getText().toString();
				st_otr_place = et_otr_place.getText().toString();
				st_otr_empid = et_otr_empid.getText().toString();
				st_otr_post = et_otr_postname.getText().toString();
				st_otr_duty = et_otr_dutytype.getText().toString();
				st_otr_ser_from = sp_otr_str_from_mth.getSelectedItem() + "/" + sp_otr_str_from_yr.getSelectedItem();
				st_otr_ser_to = sp_otr_str_to_mth.getSelectedItem() + "/" + sp_otr_str_to_yr.getSelectedItem();
			//}

			if(nw.isConnected())
				volley();
			else
			{
				final Snackbar snackbar = Snackbar.make(main_layout, "     Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();
			}
//				Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_LONG).show();
		}

	}


	public void volley()
	{

		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(true);
		pd.setMessage("Loading Please Wait");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		RequestQueue queue= Volley.newRequestQueue(getActivity());
		String url;

		if(b_go_yes_no.equalsIgnoreCase("yes")==true|b_we_yes_no.equalsIgnoreCase("yes")==true|b_otr_yes_no.equalsIgnoreCase("yes")==true)
            st_status="true";

		url= Constants.url+"experience_info.php?unique_id="+Constants.Unique_Id+"&go_name="+st_go_name+"&go_type="+st_go_type+"&go_place="+st_go_place+"&go_empid="+
				st_go_empid+"&go_post="+st_go_post+"&go_duty="+st_go_duty+"&go_ser_start="+st_go_ser_str+"&we_name="+st_we_name+
				"&we_type="+st_we_type+"&we_place="+st_we_place+"&we_empid="+st_we_empid+"&we_post="+st_we_post+
				"&we_duty="+st_we_duty+"&we_ser_from="+st_we_ser_from+"&we_ser_to="+st_we_ser_to+"&we_ser_time="+st_we_ser_time+
				"&otr_name="+st_otr_name+"&otr_type="+st_otr_type+"&otr_place="+st_otr_place+"&otr_empid="+st_otr_empid+
				"&otr_post="+st_otr_post+"&otr_duty="+st_otr_duty+"&otr_ser_time="+st_otr_ser_time+
				"&otr_ser_from="+st_otr_ser_from+"&otr_ser_to="+st_otr_ser_to+"&govt_exp="+b_go_yes_no+"&work_exp="+b_we_yes_no+
		        "&other_exp="+b_otr_yes_no+"&status="+st_status;

		url = url.replace(" ", "%20");
		Log.e("emailcheckvolleyurl", url);
		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {

				pd.dismiss();

				Log.e("loginres", res.toString());

				try {

					//if(res.getString("scalar").equals("Successfully Inserted")) {

						editor=user_info.edit();

						JSONObject jobj=new JSONObject();
						//try {
							jobj.put("go_exp",b_go_yes_no);
							jobj.put("go_name",st_go_name);
							jobj.put("go_type",st_go_type);
							jobj.put("go_place",st_go_place);
							jobj.put("go_empid",st_go_empid);
							jobj.put("go_post",st_go_post);
							jobj.put("go_duty",st_go_duty);
							jobj.put("go_ser_start",st_go_ser_str);


							jobj.put("we_exp",b_we_yes_no);
							jobj.put("we_name",st_we_name+"");
							jobj.put("we_type",st_we_type+"");
							jobj.put("we_place",st_we_place+"");
							jobj.put("we_empid",st_we_empid+"");
							jobj.put("we_post",st_we_post+"");
							jobj.put("we_duty",st_we_duty+"");
					        jobj.put("we_ser_from",st_we_ser_from+"");
					        jobj.put("we_ser_to",st_we_ser_to+"");
							jobj.put("we_ser_start",st_we_ser_time+"");

							jobj.put("otr_exp",b_otr_yes_no);
							jobj.put("otr_name",st_otr_name+"");
							jobj.put("otr_type",st_otr_type+"");
							jobj.put("otr_place",st_otr_place+"");
							jobj.put("otr_empid",st_otr_empid+"");
							jobj.put("otr_post",st_otr_post+"");
							jobj.put("otr_duty",st_otr_duty+"");
							jobj.put("otr_ser_from",st_otr_ser_from+"");
							jobj.put("otr_ser_to",st_otr_ser_to+"");
							jobj.put("otr_ser_start",st_otr_ser_time+"");

							editor.putString("experience_info",jobj.toString());

//						} catch (JSONException e) {
//							e.printStackTrace();
//						}

						editor.commit();



					detailedt=detailprf.edit();
					if(st_status.equalsIgnoreCase("true")==true) {
						detailedt.putBoolean("experience",true);
					}
					else
						detailedt.putBoolean("experience",false);


					detailedt.commit();


						Log.e("emailcheckvolley", "done");
					final Snackbar snackbar = Snackbar.make(main_layout, "Successfully Inserted.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
//					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
//						Toast.makeText(getActivity(), "Successfully Inserted", Toast.LENGTH_SHORT).show();

					//}

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("exceptionlogin",e.toString());
					pd.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError e) {

				pd.dismiss();
				final Snackbar snackbar = Snackbar.make(main_layout, "    Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();
//				Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
				Log.e("error", e.toString());
			}
		});
		int socketTimeout = 50000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		jsonreq.setRetryPolicy(policy);
		queue.add(jsonreq);
	}

}