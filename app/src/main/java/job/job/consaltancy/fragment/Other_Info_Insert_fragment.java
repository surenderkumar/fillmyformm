package job.job.consaltancy.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import job.job.consaltancy.Apply_job_activity;
import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.ImageBase64;

/**
 * Created by Rohan on 2/9/2017.
 */
public class Other_Info_Insert_fragment extends Fragment
{
    TextView tv_head,tv_submit;
    EditText et_other;
    ImageView iv_other;
    Spinner sp_other;
    String value,st_base46,st_imagename;
    Bitmap bitmap;
    Uri selectedImage;
    LinearLayout main_layout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v =inflater.inflate(R.layout.other_info_insert, container,false);
        getActivity().setResult(getActivity().RESULT_OK);

        tv_head=(TextView)v.findViewById(R.id.tv_heading_id);
        et_other=(EditText)v.findViewById(R.id.et_other_id);
        iv_other=(ImageView)v.findViewById(R.id.iv_other_id);
        tv_submit=(TextView)v.findViewById(R.id.tv_submit_id);
        sp_other=(Spinner)v.findViewById(R.id.sp_other_id);
        main_layout=(LinearLayout)v.findViewById(R.id.main_layout);

        tv_head.setText(Constants.Heading);



        if(Constants.Value_Type.equalsIgnoreCase("selection")) {
            sp_other.setVisibility(View.VISIBLE);
            ArrayList<String> value=new ArrayList<>();
            value.add("Select");
            if(Constants.Value_Selection.contains(",")) {
                String[] arr = Constants.Value_Selection.split(",");
                for(int i=0;i<arr.length;i++)
                    value.add(arr[i]);
            }
            else
                value.add(Constants.Value_Selection);

            ArrayAdapter<String> sp_Adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, value);
            sp_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_other.setAdapter(sp_Adapter);

        }
        else if(Constants.Value_Type.equalsIgnoreCase("image")) {
            iv_other.setVisibility(View.VISIBLE);

            //Toast.makeText(c,"else if image",Toast.LENGTH_LONG).show();
        }
        else {
            et_other.setVisibility(View.VISIBLE);
            //Toast.makeText(c,"else text",Toast.LENGTH_LONG).show();
        }

        iv_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImage();
            }
        });


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.Value_Type.equalsIgnoreCase("selection")) {
                    value=sp_other.getSelectedItem().toString();
                    if(value.equals("Select"))
                    {
                        final Snackbar snackbar = Snackbar.make(main_layout, "   Please select one option from drop-down.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                    }
                    else {
                        volley();
                    }
                }
                else if(Constants.Value_Type.equalsIgnoreCase("image")) {
                    imagevolley();
                }
                else {
                    value=et_other.getText().toString();
                    if(value.length()>0)
                        volley();
                    else
                    {
                        final Snackbar snackbar = Snackbar.make(main_layout, "   Please enter text.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                    }
//						Toast.makeText(getActivity(),"Plase enter text",Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;

    }


    public void SelectImage()
    {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {

//					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//					if     (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//						startActivityForResult(takePictureIntent, 1);
//					}

                    File photo = null;
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    if (android.os.Environment.getExternalStorageState().equals(
                            android.os.Environment.MEDIA_MOUNTED)) {
                        photo = new File(android.os.Environment
                                .getExternalStorageDirectory(), "test.jpg");
                    } else {
                        photo = new File(android.os.Environment
                                .getExternalStorageDirectory(), "test.jpg");
                    }
                    if (photo != null) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                        selectedImage = Uri.fromFile(photo);
                        startActivityForResult(intent, 1);
                    }

                } else if (options[item].equals("Choose from Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        builder.setCancelable(false);

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("result", resultCode + "");

        if(resultCode==0) {
        }
        else if(resultCode==getActivity().RESULT_OK) {
            if (requestCode == 1)
            {
//				Bundle extras = data.getExtras();
//				bitmap= (Bitmap) extras.get("data");

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),selectedImage);
                    //bitmap=decodeUri(getActivity(),selectedImage,100);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                iv_other.setImageBitmap(bitmap);

                st_base46 = ImageBase64.encodeTobase64(bitmap);

//                selectedImage=data.getData();
//
//
//				try {
//					bitmap=decodeUri(getActivity(),selectedImage,100);
//				} catch (FileNotFoundException e) {
//					e.printStackTrace();
//					Log.e("image_exception",e.toString());
//				}

//				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//				myBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//				byte[] byteArray = byteArrayOutputStream .toByteArray();
//				st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                Log.e("Base64_image",st_base46);

            } else if (requestCode == 2) {

                selectedImage = data.getData();
                iv_other.setImageURI(selectedImage);//setImageBitmap(thumbnail);

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),selectedImage);

                    //bitmap=decodeUri(getActivity(),selectedImage,100);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("image_exception",e.toString());
                }
                Log.e("bitmap", bitmap + "");

                st_base46 = ImageBase64.encodeTobase64(bitmap);

//				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//				bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//				byte[] byteArray = byteArrayOutputStream .toByteArray();
//				st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                Log.e("Base64_image",st_base46);
            }
        }
    }

    public Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }


    public void imagevolley() {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(true);
        pd.setMessage("Uploading Please Wait...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pd.show();

        st_imagename=Constants.Unique_Id+"_"+Constants.Heading;

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url="";
        url = Constants.url +"upload_other_image.php?";
        url = url.replace(" ", "%20");
        Log.e("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.e("response", s);
                try {
                    if(s.length()>0)
                    {
                        JSONObject obj=new JSONObject(s);
                        s=obj.getString("scalar");
                        if(s.equalsIgnoreCase("Successfully Inserted")==true)
                        {
                            final Snackbar snackbar = Snackbar.make(main_layout, "Upload Successfully.", Snackbar.LENGTH_INDEFINITE);

                            snackbar.setActionTextColor(Color.WHITE);
                            snackbar.setDuration(500);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                            textView.setGravity(Gravity.CENTER_VERTICAL);
//                            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                            textView.setCompoundDrawablePadding(0);

                            snackbar.show();
                            Constants.other_count=Constants.other_count++;
                            getActivity().getSupportFragmentManager().popBackStack();
//                            if(Constants.other_count==Constants.other_adapter_count)
//                            {
//                                Apply_job_activity basic_frag = new Apply_job_activity();
//                                ((FragmentActivity) getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
//                                        addToBackStack(null).commit();
//                            }
//							Toast.makeText(getActivity(),"Upload Successfully",Toast.LENGTH_LONG).show();
                        }
                        else if(s.equalsIgnoreCase("Can't Inserted")==true)
                        {
                            final Snackbar snackbar = Snackbar.make(main_layout, "   Somthing is Wrong Can't Upload.", Snackbar.LENGTH_INDEFINITE);

                            snackbar.setActionTextColor(Color.WHITE);
                            snackbar.setDuration(3500);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                            textView.setGravity(Gravity.CENTER_VERTICAL);
                            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                            textView.setCompoundDrawablePadding(0);

                            snackbar.show();
//							Toast.makeText(getActivity(),"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
                        }

                        pd.dismiss();
                        Log.e("response_string",s+" response");
                    }
                }

                catch (Exception e)
                {
                    pd.dismiss();
                    Log.e("e","e",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.e("imagevolley_error",volleyError.toString());
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("st_base64", st_base46);
                params.put("uniqueid", Constants.Unique_Id);
                params.put("imagename", st_imagename);
                params.put("value_type", Constants.Value_Type);
                params.put("info_type", Constants.Info_Type);
                params.put("psid", Constants.PSID);

                //Log.e("img222222", st_base46);

                return params;
            }
        };

        int socketTimeout = 10000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }


    public void volley()
    {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(true);
        pd.setMessage("Loading Please Wait");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        pd.show();

        RequestQueue queue= Volley.newRequestQueue(getActivity());
        String url;

        url= Constants.url+"other_info_insert.php?unique_id="+Constants.Unique_Id+"&psid="+Constants.PSID+"&value_type="+
                Constants.Value_Type+"&value="+value+"&info_type="+Constants.Info_Type;

        url = url.replace(" ", "%20");
        Log.e("emailcheckvolleyurl", url);
        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                pd.dismiss();

                Log.e("loginres", res.toString());

                try {

                    String st=res.getString("scalar");

                    if(st.equalsIgnoreCase("OK")) {
                        Log.e("hello","hello");
//                        Toast.makeText(getActivity(), Constants.other_count + " hello", Toast.LENGTH_SHORT).show();
                        final Snackbar snackbar = Snackbar.make(main_layout, "Successfully Inserted.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
//                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                        try {
//                            Toast.makeText(getActivity(), Constants.other_count + " abccc", Toast.LENGTH_SHORT).show();
                            Log.e("sizooo", Constants.other_count + " abccc");
                            Constants.other_count = Constants.other_count + 1;
                            getActivity().getSupportFragmentManager().popBackStack();

//                            Log.e("sizooo", Constants.other_count + " abccc");
//                            int a = Constants.other_count;
//                            int b = Constants.other_adapter_count;
//                            if (a == b) {
//                                Log.e("sizrrrr", Constants.other_count + "" + a + "," + b);
//                                Apply_job_activity basic_frag = new Apply_job_activity();
//                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
//                                        addToBackStack(null).commit();
//                            }
                        }catch (Exception e)
                        {
                            Log.e("excep",e.getMessage());
                            e.printStackTrace();
                        }
//						Toast.makeText(getActivity(), "Successfully Inserted", Toast.LENGTH_SHORT).show();
                    }
                    else if(st.equalsIgnoreCase("ERROR")) {
                        final Snackbar snackbar = Snackbar.make(main_layout, "   Can't Inserted.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
//						Toast.makeText(getActivity(), "Can't Inserted", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        final Snackbar snackbar = Snackbar.make(main_layout, "   Somthing is wrong, Please try again.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
//						Toast.makeText(getActivity(), "Somthing is wrong can't, Please try again", Toast.LENGTH_SHORT).show();
                    }

                    //}

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("exception_other_info",e.toString());
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {

                pd.dismiss();
                final Snackbar snackbar = Snackbar.make(main_layout, "   Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                snackbar.setDuration(3500);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                textView.setGravity(Gravity.CENTER_VERTICAL);
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                textView.setCompoundDrawablePadding(0);

                snackbar.show();
//				Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
                Log.e("other_info_error", e.toString());
            }
        });
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }

}