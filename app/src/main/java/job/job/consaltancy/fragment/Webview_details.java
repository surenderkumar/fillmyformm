package job.job.consaltancy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Webview_details extends Fragment {

    WebView wv;
    ProgressBar bar;
    LinearLayout back_layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.webview_detail, container,false);

        wv=(WebView)v.findViewById(R.id.webview_id);
        bar=(ProgressBar)v.findViewById(R.id.progressBar);
        back_layout=(LinearLayout)v.findViewById(R.id.back_layout);
        back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });

//        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        wv.loadData(Constants.Job_Detail, "text/html; charset=UTF-8", null);
//        wv.loadDataWithBaseURL(null, Constants.Job_Detail, "text/html", "utf-8",null);

        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.loadUrl("http://docs.google.com/gview?embedded=true&url="+ Constants.url+"notification/"+Constants.Job_Detail);
        wv.setWebViewClient(new Callback());

        return v;
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            //view.loadUrl(url);
            return(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            bar.setVisibility(View.GONE);
        }
    }
}
