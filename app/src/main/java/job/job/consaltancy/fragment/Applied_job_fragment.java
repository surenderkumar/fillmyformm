package job.job.consaltancy.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Applied_job_adapter;
import job.job.consaltancy.adapter.Job_in_processing_adapter;
import job.job.consaltancy.getset.job_processing_getset;
import job.job.consaltancy.utils.Constants;

/**
 * Created by surender on 6/7/2016.
 */
public class Applied_job_fragment extends Fragment {

    ImageView iv_head;
    TextView tv_head,refresh;
    LinearLayout no_connection,main_layout;
    Applied_job_adapter adapter;
    RecyclerView recyclerView;
    ArrayList<job_processing_getset> data;
    SharedPreferences user_info;
    SharedPreferences.Editor user_editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.job_in_processing, container,false);

        user_info=getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);

        iv_head=(ImageView)v.findViewById(R.id.iv_heading_id);
        tv_head=(TextView)v.findViewById(R.id.tv_heading_id);

        main_layout=(LinearLayout)v.findViewById(R.id.main_layout);
        no_connection=(LinearLayout)v.findViewById(R.id.ly_noconnection_id);

        recyclerView = (RecyclerView)v. findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        iv_head.setImageResource(R.drawable.formapplied);
        tv_head.setText("Applied Forms");

//        data=new ArrayList<>();
//        adapter=new Applied_job_adapter(getActivity(),data);
//        recyclerView.setAdapter(adapter);

        refresh=(TextView)v.findViewById(R.id.tv_refreshid);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volley();
            }
        });


        volley();

        return v;
    }

    public void volley() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        data=new ArrayList<>();

        RequestQueue queue= Volley.newRequestQueue(getActivity());

        String url= Constants.url+"get_processing_jobs.php?unique_id="+Constants.Unique_Id+"&status=Applied";
        Log.e("url= ", url);
        url=url.replaceAll(" ","%20");

        //jobarray=new ArrayList<job_getset>();

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST,url, null,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res)
            {

                try {
                    JSONArray array=res.getJSONArray("job");

                    int len=array.length();

                    Log.e("length=", String.valueOf(len));

                    for(int i=0;i<array.length();i++)
                    {
                        JSONObject obj=array.getJSONObject(i);

                        if(i==0){
                            user_editor=user_info.edit();
                            user_editor.putInt("applied_job_lastid",obj.getInt("id"));
                            user_editor.commit();
                        }


                        data.add(new job_processing_getset(obj.getInt("id"),obj.getString("job_id"),obj.getString("post_name"),
                                obj.getString("fees"),obj.getString("exam_center"),obj.getString("preference"),
                                obj.getString("company_name"),obj.getString("job_category"),obj.getString("file_link"),obj.getString("fee_receipt"),obj.getString("date")));
                        Log.e("fee_receipt",obj.getString("fee_receipt")+"");
                    }


                    recyclerView.setVisibility(View.VISIBLE);
                    no_connection.setVisibility(View.GONE);

                    adapter=new Applied_job_adapter(getActivity(),data);
                    recyclerView.setAdapter(adapter);

                    dialog.dismiss();

                } catch (JSONException e) {
                    Log.e("catch exp= ", e.toString());
                    dialog.dismiss();
                    e.printStackTrace();
                }

            }

        }
                ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                dialog.dismiss();

                String error=arg0.toString();
                if (error.contains("NoConnectionError")) {
                    final Snackbar snackbar = Snackbar.make(main_layout, "   No Connection Error.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
//                    Toast.makeText(getActivity(), "NoConnectionError", Toast.LENGTH_LONG).show();
                    recyclerView.setVisibility(View.GONE);
                    no_connection.setVisibility(View.VISIBLE);
                }
                else
                {
                    final Snackbar snackbar = Snackbar.make(main_layout, "   No Any Applied Job.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
                }
//                    Toast.makeText(getActivity(), "No Any Applied Job", Toast.LENGTH_LONG).show();

                Log.e("error", arg0.toString());
            }
        });
        queue.add(request);
    }
}
