package job.job.consaltancy.fragment;


import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import job.job.consaltancy.HomeActivity;
import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;


public class MyProfileActivity extends AppCompatActivity {

    private static int RESULT_LOAD_IMAGE = 2;
    private File imgFile;
    final int PIC_CROP = 4044;
    Bitmap thePic;
    String img1 = "";
    private final static int ACTIVITY_PICK_IMAGE = 0;
    private final static int ACTIVITY_TAKE_PHOTO = 1;
    String path;
    SharedPreferences user_info;
    SharedPreferences.Editor editor;
    CircleImageView user_image;
    String name = "", id = "", email = "", dist = "", state = "", phone = "",image="";

    LinearLayout main_layout;
    EditText tv_name,tv_email;
    ImageView camera;
    TextView  tv_dist, tv_state, tv_phone, tv_uniqueid,update,tv_pointid;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        main_layout=(LinearLayout)findViewById(R.id.main_layout);

        tv_dist = (TextView) findViewById(R.id.inf_districtid);
        tv_email = (EditText) findViewById(R.id.inf_emailid);
        tv_phone = (TextView) findViewById(R.id.inf_phoneid);
        tv_name = (EditText) findViewById(R.id.inf_nameid);
        tv_state = (TextView) findViewById(R.id.inf_stateid);
        tv_uniqueid = (TextView) findViewById(R.id.inf_uniqueid);
        tv_pointid = (TextView) findViewById(R.id.tv_pointid);
        update=(TextView)findViewById(R.id.textView7);

        user_image=(CircleImageView)findViewById(R.id.imageView3);
        camera=(ImageView)findViewById(R.id.imageView4);

        user_info = getSharedPreferences("user_info", Context.MODE_PRIVATE);

        email = user_info.getString("email", "");
        id = user_info.getString("unique_id", "");
        dist = user_info.getString("district", "");
        state = user_info.getString("state", "");

        tv_pointid.setText("₹ " + String.valueOf(Constants.Wallet_Money));

        if(id.length()>1)
        {
            volley();
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatevolley();
            }
        });

        String basic_info = user_info.getString("basic_info", "");
        String st_add = user_info.getString("address_info", "");

        try {
            JSONObject jobj = new JSONObject(basic_info);

            name = jobj.getString("c_fname");
            name = name + " " + jobj.getString("c_mname");
            name = name + " " + jobj.getString("c_lname");

            jobj = new JSONObject(st_add);

            phone = jobj.getString("mobile_no");

        } catch (JSONException e) {
            e.printStackTrace();
        }



        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Upload_Image();
            }
        });

       camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Upload_Image();
            }
        });

//        if (email.length() > 0)
//            tv_email.setText(email);
//        else
//            tv_email.setText("Not Available");
//
//        if (id.length() > 0)
//            tv_uniqueid.setText(id);
//        else
//            tv_uniqueid.setText("Not Available");
//
//        if (name.length() > 0)
//            tv_name.setText(name);
//        else
//            tv_name.setText("Not Available");
//
//        if (dist.length() > 0)
//            tv_dist.setText(dist);
//        else
//            tv_dist.setText("Not Available");
//
//        if (state.length() > 0)
//            tv_state.setText(state);
//        else
//            tv_state.setText("Not Available");
//
//        if (phone.length() > 0)
//            tv_phone.setText(phone);
//        else
//            tv_phone.setText("Not Available");


    }
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//		View v =inflater.inflate(R.layout.activity_my_profile, container,false);
//
//		tv_dist=(TextView)v.findViewById(R.id.inf_districtid);
//		tv_email=(TextView)v.findViewById(R.id.inf_emailid);
//		tv_phone=(TextView)v.findViewById(R.id.inf_phoneid);
//		tv_name=(TextView)v.findViewById(R.id.inf_nameid);
//		tv_state=(TextView)v.findViewById(R.id.inf_stateid);
//		tv_uniqueid=(TextView)v.findViewById(R.id.inf_uniqueid);
//
//
//		user_info=getActivity().getSharedPreferences("user_info", 1);
//
//		email=user_info.getString("email","");
//		id=user_info.getString("unique_id","");
//		dist=user_info.getString("district","");
//		state=user_info.getString("state","");
//
//		String basic_info=user_info.getString("basic_info","");
//		String st_add = user_info.getString("address_info","");
//
//		try {
//			JSONObject jobj=new JSONObject(basic_info);
//
//			name=jobj.getString("c_fname");
//			name=" "+jobj.getString("c_mname");
//			name=" "+jobj.getString("c_lname");
//
//			jobj=new JSONObject(st_add);
//
//			phone=jobj.getString("mobile_no");
//
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//
//		if(email.length()>0)
//		tv_email.setText(email);
//		else
//		tv_email.setText("Email Not Available");
//
//		if(id.length()>0)
//			tv_uniqueid.setText(id);
//		else
//		tv_uniqueid.setText("Id Not Available");
//
//		if(name.length()>0)
//			tv_name.setText(name);
//		else
//			tv_name.setText("Name Not Available");
//
//		if(dist.length()>0)
//			tv_dist.setText(dist);
//		else
//			tv_dist.setText("District Not Available");
//
//		if(state.length()>0)
//			tv_state.setText(state);
//		else
//			tv_state.setText("State Not Available");
//
//		if(phone.length()>0)
//			tv_phone.setText(phone);
//		else
//			tv_phone.setText("Phone No. Not Available");
//
//		return v;
//	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {
            editor=user_info.edit();
            editor.putString("email", tv_email.getText().toString());
            editor.putString("unique_id", id);
            editor.putString("name", tv_name.getText().toString());
            editor.putString("image", image);
            editor.commit();
//            Log.e("username",Constants.username);
            Intent in=new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(in);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        editor=user_info.edit();
        editor.putString("email", tv_email.getText().toString());
        editor.putString("unique_id", id);
        editor.putString("name", tv_name.getText().toString());
        editor.putString("image", image);
        editor.commit();
        Intent in=new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(in);
        finish();
        super.onBackPressed();
    }

    public void Upload_Image() {
        try {

            final CharSequence[] items = {"Take Photo",
                    "Choose from Gallery"
                    , "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
            builder.setTitle("Add Photo!");

            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {


                        case 0:
                            Uri outputFileUri = Uri.fromFile(getTempFile(MyProfileActivity.this));
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                            startActivityForResult(cameraIntent, ACTIVITY_TAKE_PHOTO);
                            break;

                        case 1:


                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, RESULT_LOAD_IMAGE);

                            break;


                        case 2:
                            dialog.dismiss();
                    }


                    dialog.dismiss();


                }
            });


            builder.show();
        }catch (Exception e)
        {
            Log.e("Exception",e.getMessage());
        }

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            String url = data.getData().toString();
            Bitmap bitmap = null;
            InputStream is = null;
            if (url.startsWith("content://com.google.android.apps.photos.content")) {

                Uri myUri = Uri.parse(url);
                Log.e("url", myUri.getAuthority());
                picturePath = getImageUrlWithAuthority(MyProfileActivity.this,
                        myUri);


                Uri ur = Uri.parse(picturePath);
                String path = getRealPathFromURI(ur);
//                compressImage(ur);
                imgFile = new File(path);
                Log.e("url", picturePath);


            } else

                imgFile = new File(picturePath);

            try {
                // call the standard crop action intent (the user device may not
                // support it)
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                // indicate image type and Uri
                cropIntent.setDataAndType(Uri.fromFile(imgFile), "image/*");
                // set crop properties
                cropIntent.putExtra("crop", "true");
                // indicate aspect of desired crop
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                // indicate output X and Y
                cropIntent.putExtra("outputX", 500);
                cropIntent.putExtra("outputY", 500);
                // retrieve data on return
                cropIntent.putExtra("return-data", true);

                Uri mCropImagedUri = Uri
                        .fromFile(getTempFile(MyProfileActivity.this));
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
                startActivityForResult(cropIntent, PIC_CROP);
            }
            // respond to users whose devices do not support the crop action
            catch (ActivityNotFoundException anfe) {
                // display an error message
                String errorMessage = "Whoops - your device doesn't support the crop action!";
                Toast toast = Toast.makeText(MyProfileActivity.this, errorMessage,
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ACTIVITY_PICK_IMAGE:

                    break;
                case ACTIVITY_TAKE_PHOTO:

                    try {

                        Log.e("inside", "crop");
                        // call the standard crop action intent (the user device may
                        // not support it)
                        Intent cropIntent = new Intent(
                                "com.android.camera.action.CROP");
                        // indicate image type and Uri
                        cropIntent.setDataAndType(
                                Uri.fromFile(getTempFile(MyProfileActivity.this)),
                                "image/*");
                        // set crop properties
                        cropIntent.putExtra("crop", "true");
                        // indicate aspect of desired crop
                        cropIntent.putExtra("aspectX", 1);
                        cropIntent.putExtra("aspectY", 1);

                        cropIntent.putExtra("scale", true);

                        cropIntent.putExtra("outputX", 500);
                        cropIntent.putExtra("outputY", 500);

                        cropIntent
                                .putExtra(MediaStore.EXTRA_OUTPUT, Uri
                                        .fromFile(getFilePath(
                                                MyProfileActivity.this,
                                                "image.jpeg")));

                        // retrieve data on return
                        cropIntent.putExtra("return-data", true);

                        // start the activity - we handle returning in
                        // onActivityResult
                        startActivityForResult(cropIntent, 1221);
                    }
                    // respond to users whose devices do not support the crop action
                    catch (Exception anfe) {
                        // display an error message
                        String errorMessage = "Whoops - your device doesn't support the crop action!";
                        Toast toast = Toast.makeText(MyProfileActivity.this, errorMessage,
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }

                    break;
                default:
                    break;
            }
        }

        if (requestCode == 1221 && resultCode == RESULT_OK && null != data) {
            Bundle extras = data.getExtras();

            Log.e("Aaaa", data.toString());
            Uri selectedImage = data.getData();
            if (selectedImage == null) {
                thePic = extras.getParcelable("data");
            } else {

                Log.e("image", selectedImage.getPath());


                try {
                    thePic = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);


                    if (thePic != null) {

                    }


                    user_image.setImageBitmap(thePic);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                thePic.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();


                img1 = Base64.encodeToString(byteArray, Base64.DEFAULT);


//                    img1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    HiHelloConstant.imgg1=img1;
//                    Log.e("base64",img1);


                // Log.e("base64",base64(thePic));
                //new UploadPdf().execute(base64(),cno,"Image"+timeStamp + ".pdf",pref.getString("name2",""),myFile.getTotalSpace()+"","0","0","pdf",shared.getString("username",""),pref.getString("id",""),shared.getString("name1",""),"0");
            }

        }

        File f = getFilePath(getApplicationContext(),
                "image.jpeg");


        // final Bitmap thePic = extras.getParcelable("dat");




        if(requestCode==PIC_CROP&&resultCode==RESULT_OK&&null!=data)

        {
            Bundle extras = data.getExtras();


            Uri selectedImage = data.getData();

            Log.e("image", "IO");
            if (selectedImage == null) {
                thePic = extras.getParcelable("data");


                path = getFilePath(MyProfileActivity.this, "image.jpg").getAbsolutePath();

            } else {

                Log.e("image", selectedImage.getPath());
                try {
                    thePic = MediaStore.Images.Media.getBitmap(
                            getApplicationContext().getContentResolver(), selectedImage);


                    path = selectedImage.getPath();


                    if (thePic != null) {
                        user_image.setImageBitmap(thePic);
                    }



                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    thePic.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();


                    img1 = Base64.encodeToString(byteArray, Base64.DEFAULT);


//                    Log.e("base64",img1);
                    //new UploadPdf().execute(base64(),cno,"Image"+timeStamp + ".pdf",username,myFile.getTotalSpace()+"","0","0","pdf",shared.getString("username",""),pref.getString("id",""),shared.getString("name1",""),"0");


                } catch (Exception e) {


                    Log.e("ui", "e", e);


                }
            }

            // final Bitmap thePic = extras.getParcelable("dat");


        }




    }

    public static File getFilePath(Context context, String filename) {
        File path = new File(Environment.getExternalStorageDirectory(),
                "HiHello");
        if (!path.exists()) {
            path.mkdir();
        }
        return new File(path, filename);
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static Uri writeToTempImageAndGetPathUri(Context inContext,
                                                    Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public static String getImageUrlWithAuthority(Context context, Uri uri) {
        InputStream is = null;
        if (uri.getAuthority() != null) {
            try {
                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return writeToTempImageAndGetPathUri(context, bmp).toString();
            } catch (Exception e) {
                Log.e("e", "e", e);

            } finally {
                try {
                    is.close();
                } catch (IOException e) {


                }
            }
        }
        return null;
    }

    private File getTempFile(Context context) {
        String fileName = "image.jpg";
        File path = new File(Environment.getExternalStorageDirectory(),
                "Doctornxt");
        if (!path.exists()) {
            path.mkdir();
        }
        return new File(path, fileName);
    }






    public void volley()
    {
        final ProgressDialog dialog = new ProgressDialog(MyProfileActivity.this);
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(MyProfileActivity.this);
        Log.e("url= ", "jjava.lang.String[]iii");
        String url = Constants.url+"get_profile.php?unique_id="+id;
        Log.e("url= ", url);

        JsonObjectRequest request = new JsonObjectRequest( url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                dialog.dismiss();
                try {
                    if(res.has("user")) {
                        JSONArray jr = res.getJSONArray("user");

//                    Log.e("response=", res.getJSONArray("data").toString());
                        int len = jr.length();

//                    Log.e("length=", String.valueOf(len));

                        for (int i = 0; i < jr.length(); i++) {
                            JSONObject jsonobj = jr.getJSONObject(i);           //String nom, String prenom, String email, String age, String deport, String actuel
//                            data.add(new History_get_set(jsonobj.getInt("id"), jsonobj.getString("email"), jsonobj.getString("problem_type"), jsonobj.getString("problem_desc"), jsonobj.getString("date"), jsonobj.getString("time"), jsonobj.getString("request"), jsonobj.getString("status"), jsonobj.getString("name"), jsonobj.getString("address"), jsonobj.getString("distance")));
                            email = jsonobj.getString("email");
                            id = jsonobj.getString("unique_id");
                            dist = jsonobj.getString("district");
                            state = jsonobj.getString("state");
                            name = jsonobj.getString("name");
                            image = jsonobj.getString("image");
                            if (email.length() > 0)
                                tv_email.setText(email);
                            else
                                tv_email.setText("Not Available");

                            if (id.length() > 0)
                                tv_uniqueid.setText(id);
                            else
                                tv_uniqueid.setText("Not Available");

                            if (name.length() > 0)
                                tv_name.setText(name);
                            else
                                tv_name.setText("Not Available");

                            if (dist.length() > 0)
                                tv_dist.setText(dist);
                            else
                                tv_dist.setText("Not Available");

                            if (state.length() > 0)
                                tv_state.setText(state);
                            else
                                tv_state.setText("Not Available");

                            if (phone.length() > 0)
                                tv_phone.setText(phone);
                            else
                                tv_phone.setText("Not Available");
                            if(image.length()>1)
                            {
                                Picasso.with(MyProfileActivity.this)
                                        .load(Constants.url+"user_image/img/"+image)
                                        .error(R.drawable.ic_upload_pic).placeholder(R.drawable.ic_upload_pic)
                                        .into(user_image);
                            }

                        }


                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Log.e("catch exp= ", e.toString());
                    e.printStackTrace();
                    dialog.dismiss();
                }

            }

        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {

//                Log.e("error", arg0.toString());
//                dialog.dismiss();
            }
        });
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public void updatevolley() {
        final ProgressDialog pd = new ProgressDialog(MyProfileActivity.this);
        pd.setCancelable(false);
        pd.setMessage("Loading Please Wait");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        pd.show();


        RequestQueue queue = Volley.newRequestQueue(MyProfileActivity.this);
        String url = Constants.url + "update_profile.php";
        url = url.replace(" ", "%20");
        Log.e("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                pd.dismiss();
                Log.e("response", s);
//                Toast.makeText(Signup_activity.this,s+"", Toast.LENGTH_SHORT).show();
                try {
//                    Toast.makeText(Signup_activity.this, "Signup Successfull", Toast.LENGTH_SHORT).show();
                    String str=s;
                    Log.e("response", str);
                  if(str.contains("Success"))
                    {
                        Log.e("spdate","Success");
//                        Toast.makeText(MyProfileActivity.this, "Update Successfull", Toast.LENGTH_SHORT).show();
//                        Intent in1 = new Intent(getApplicationContext(), Manu_activity.class);
//                        startActivity(in1);
//                        finish();

                        final Snackbar snackbar = Snackbar.make(main_layout, "Update Successfull.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
//					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();


                    }
                }

                catch (Exception e)
                {
                    pd.dismiss();
                    Log.e("e","e",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("unique_id", id);
                params.put("email", tv_email.getText().toString());
                params.put("name", tv_name.getText().toString());
                params.put("image", img1);
                Log.e("img11111", img1);


                return params;
            }

        };

        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

}