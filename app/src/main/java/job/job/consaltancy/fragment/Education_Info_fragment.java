package job.job.consaltancy.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.R;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;


public class Education_Info_fragment extends Fragment implements View.OnClickListener
{
	TextView tv_matric,tv_intermed,tv_diploma,tv_graduation,tv_postgraduation,tv_language;
	LinearLayout ly_matric,ly_intermed,ly_diploma,ly_graduation,ly_postgraduation,ly_language;
	LinearLayout lys_diploma,ly_matric_rb,ly_intermed_rb,ly_graduation_rb,ly_postgraduation_rb;
	RelativeLayout main_layout;
	RadioButton rb_dipl_yes,rb_dipl_no,rb_mtr_yes,rb_mtr_no,rb_intermed_yes,rb_intermed_no,rb_grd_yes,rb_grd_no,rb_postgrd_yes,rb_postgrd_no;
	boolean b_matric=false,b_intermed=false,b_diploma=false,b_graduation=false,b_postgraduation=false,b_language=false;

// matric widget

	Spinner sp_matric_result,sp_mtr_passing_day,sp_mtr_passing_month,sp_mtr_passing_year,sp_mtr_grade,sp_mtr_mode,sp_matric_brd_state;
	EditText et_matric_sub,et_matric_brd,et_matric_school,et_matric_rollno,et_matric_obt_mark,et_matric_ttl_mark,et_matric_per_mark;
	ArrayList<String> mtr_rslt_list,mtr_day_list,mtr_month_list,mtr_year_list,mtr_grade_list,mtr_mode_list;

// intermediate widget

	Spinner sp_12th_stream,sp_12th_result,sp_12th_passing_day,sp_12th_passing_month,sp_12th_passing_year,sp_12th_grade,sp_12th_brd_state,sp_12th_mode;
	EditText et_12th_sub,et_12th_brd,et_12th_school,et_12th_rollno,et_12th_obt_mark,et_12th_ttl_mark,et_12th_per_mark;
	ArrayList<String> intermed_stream_list,intermed_rslt_list,intermed_day_list,intermed_month_list,intermed_year_list,intermed_grade_list,intermed_mode_list;

// diploma widget

	Spinner sp_dipl_type,sp_dipl_result,sp_dipl_passing_day,sp_dipl_passing_month,sp_dipl_passing_year,sp_dipl_grade,sp_dipl_brd_state,sp_dipl_mode;
	EditText et_dipl_sub,et_dipl_duration,et_dipl_brd,et_dipl_school,et_dipl_rollno,et_dipl_obt_mark,et_dipl_ttl_mark,et_dipl_per_mark;
	ArrayList<String> dipl_type_list,dipl_rslt_list,dipl_day_list,dipl_month_list,dipl_year_list,dipl_grade_list,dipl_mode_list;

    // degree widget

	Spinner sp_grd_deg_name,sp_grd_deg_result,sp_grd_deg_passing_day,sp_grd_deg_passing_month,sp_grd_deg_passing_year,sp_grd_deg_grade,sp_grd_deg_state,sp_grd_deg_mode;
	EditText et_grd_deg_sub,et_grd_deg_duration,et_grd_deg_unver,et_grd_deg_place,et_grd_deg_college,et_grd_deg_rollno;
	EditText et_grd_deg_obt_mark,et_grd_deg_ttl_mark,et_grd_deg_per_mark,et_grd_deg_cgpa,et_grd_deg_max_cgpa;
	ArrayList<String> grd_deg_name_list,grd_deg_rslt_list,grd_deg_day_list,grd_deg_month_list,grd_deg_year_list,grd_deg_grade_list;
	ArrayList<String> grd_deg_mode_list;

	Spinner sp_postgrd_deg_name,sp_postgrd_deg_result,sp_postgrd_deg_passing_day,sp_postgrd_deg_passing_month,sp_postgrd_deg_passing_year;
	Spinner sp_postgrd_deg_grade,sp_postgrd_deg_mode,sp_postgrd_deg_state;
	EditText et_postgrd_deg_sub,et_postgrd_deg_duration,et_postgrd_deg_unver,et_postgrd_deg_place;
	EditText et_postgrd_deg_college,et_postgrd_deg_rollno;
	EditText et_postgrd_deg_obt_mark,et_postgrd_deg_ttl_mark,et_postgrd_deg_per_mark,et_postgrd_deg_cgpa,et_postgrd_deg_max_cgpa;
	ArrayList<String> postgrd_deg_name_list,postgrd_deg_rslt_list,postgrd_deg_day_list,postgrd_deg_month_list,postgrd_deg_year_list;
	ArrayList<String> postgrd_deg_mode_list,postgrd_deg_grade_list;

	CheckBox h_read,h_write,h_speak,e_read,e_write,e_speak,o_read,o_write,o_speak;
	EditText other_lan;

	FloatingActionButton submit;
	String all_sub=" ",nameofboard=" ",stateboard=" ",name_of_school=" ",result=" ",roll_number=" ",obtanined_mrks=" ",total_marks=" ",percentage_mrks=" ",date_of_passing=" ",grade=" ",mode=" ";
	String stream=" ",all_sb12=" ",name_baord=" ",state_of_board12=" ",name_of_school12=" ",result12=" ",roll_number12=" ",obtanied_mrks12=" ",total_mrks12=" ",p_marks=" ",d_of_pass=" ",division=" ",mode12=" ";
	String d_holder=" ",deploma=" ",d_stream=" ",dur_of_dep=" ",nam_of_board=" ",d_state=" ",d_name_school=" ",d_res=" ",roll_no=" ",obt_marks=" ",tot_marks=" ",per_marksd=" ",d_of_pass_d=" ",d_grade=" ",d_mode=" ";
	String graduate=" ",g_stream=" ",g_duration=" ",g_name_uni=" ",g_place=" ",g_state=" ",g_name_of_clg=" ",g_result=" ",g_marks_obt=" ",g_total_mrks=" ",g_percent=" ",g_d_of_pass=" ",g_grade=" ",g_mode=" ",cgpa=" ",mrks_cgpa=" ";
	String postgraduate=" ",pg_streamv=" ",pg_dur=" ",pg_name_of_uni=" ",pg_uni_place=" ",pg_uni_state=" ",pg_clgname=" ",pg_result=" ",pg_obt_marks=" ",pg_total_marks=" ",pg_perc_of_mrks=" ",pd_d_pass=" ",pg_garde=" ",pg_modee=" ",pg_cgpa=" ",pg_max=" ";
	String other_lang=" ",hindi_read=" ",eng_read=" ",other_read=" ",hindi_write=" ",hindi_speak=" ",eng_write=" ",eng_speak=" ",other_write=" ",other_speak=" ";

	String postgraduation_yn,graduation_yn,matric_yn,intermed_yn;

//	String mat_sub="",mat_board=" ",mat_board_state=" ",mat_school=" ",mat_result=" ",mat_roll=" ",mat_marks=" ",mat_total=" ",mat_percentage=" ",mat_day=" ",mat_month=" ",mat_year=" ",mat_date=" ",mat_grade=" ",mat_mode=" ";
//	String inter_sub="",inter_board=" ",inter_board_state=" ",inter_school=" ",inter_result=" ",inter_roll=" ",inter_marks=" ",inter_total=" ",inter_percentage=" ",inter_day=" ",inter_month=" ",inter_year=" ",inter_date=" ",inter_grade=" ",inter_mode=" ";

	SharedPreferences user_info,detailprf;
	SharedPreferences.Editor editor,detailedt;

	NetworkConnection nw;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v =inflater.inflate(R.layout.fill_education_info, container,false);

		nw=new NetworkConnection(getActivity());

		user_info=getActivity().getSharedPreferences("user_info", Context.MODE_PRIVATE);
		detailprf=getActivity().getSharedPreferences("detailprf",Context.MODE_PRIVATE);

  //Matric  widget

		main_layout=(RelativeLayout)v.findViewById(R.id.main_layout);

		tv_matric=(TextView)v.findViewById(R.id.matrict_textviewid);

		submit=(FloatingActionButton)v.findViewById(R.id.submitbtn);
		ly_matric=(LinearLayout)v.findViewById(R.id.matric_layoutid);
		ly_matric_rb=(LinearLayout)v.findViewById(R.id.matric_rb_layoutid);

		et_matric_sub=(EditText)v.findViewById(R.id.et_matric_subjectid);
		et_matric_brd=(EditText)v.findViewById(R.id.et_matric_boardid);
		sp_matric_brd_state=(Spinner)v.findViewById(R.id.sp_matric_board_stateid);
		et_matric_school=(EditText)v.findViewById(R.id.et_matric_schoolid);
		et_matric_rollno=(EditText)v.findViewById(R.id.et_matric_rollnoid);
		et_matric_obt_mark=(EditText)v.findViewById(R.id.et_matric_obt_markid);
		et_matric_ttl_mark=(EditText)v.findViewById(R.id.et_matric_ttl_markid);
		et_matric_per_mark=(EditText)v.findViewById(R.id.et_matric_perc_markid);

		rb_mtr_yes=(RadioButton)v.findViewById(R.id.rb_matric_yesid);
		rb_mtr_no=(RadioButton)v.findViewById(R.id.rb_matric_noid);
		rb_intermed_yes=(RadioButton)v.findViewById(R.id.rb_intermed_yesid);
		rb_intermed_no=(RadioButton)v.findViewById(R.id.rb_intermed_noid);
		rb_grd_yes=(RadioButton)v.findViewById(R.id.rb_grd_yesid);
		rb_grd_no=(RadioButton)v.findViewById(R.id.rb_grd_noid);
		rb_postgrd_yes=(RadioButton)v.findViewById(R.id.rb_postgrd_yesid);
		rb_postgrd_no=(RadioButton)v.findViewById(R.id.rb_postgrd_noid);


		Constants.statelist.clear();
		Constants.statelist.add("Select state");
		Constants.statelist.add("Andaman and Nicobar Islands");
		Constants.statelist.add("Andhra Pradesh");
		Constants.statelist.add("Arunachal Pradesh");
		Constants.statelist.add("Assam");
		Constants.statelist.add("Bihar");
		Constants.statelist.add("Chandigarh");
		Constants.statelist.add("Chhattisgarh");
		Constants.statelist.add("Dadra and Nagar Haveli");
		Constants.statelist.add("Daman and Diu");
		Constants.statelist.add("Delhi – National Capital Territory");
		Constants.statelist.add("Goa");
		Constants.statelist.add("Gujarat");
		Constants.statelist.add("Haryana");
		Constants.statelist.add("Himachal Pradesh");
		Constants.statelist.add("Jammu & Kashmir");
		Constants.statelist.add("Jharkhand");
		Constants.statelist.add("Karnataka");
		Constants.statelist.add("Kerala");
		Constants.statelist.add("Lakshadweep");
		Constants.statelist.add("Madhya Pradesh");
		Constants.statelist.add("Maharashtra");
		Constants.statelist.add("Manipur");
		Constants.statelist.add("Meghalaya");
		Constants.statelist.add("Mizoram");
		Constants.statelist.add("Nagaland");
		Constants.statelist.add("Odisha");
		Constants.statelist.add("Puducherry");
		Constants.statelist.add("Punjab");
		Constants.statelist.add("Rajasthan");
		Constants.statelist.add("Sikkim");
		Constants.statelist.add("Tamil Nadu");
		Constants.statelist.add("Telangana");
		Constants.statelist.add("Tripura");
		Constants.statelist.add("Uttar Pradesh");
		Constants.statelist.add("Uttarakhand");
		Constants.statelist.add("West Bengal");


		ArrayAdapter<String> stateten = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		stateten.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_matric_brd_state.setAdapter(stateten);

		sp_matric_result=(Spinner)v.findViewById(R.id.sp_matric_resultid);
        mtr_rslt_list=new ArrayList<>();
		mtr_rslt_list.add("Select");
		mtr_rslt_list.add("Pass");
		mtr_rslt_list.add("Fail");
		mtr_rslt_list.add("Result Awaited");

		final ArrayAdapter<String> resAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mtr_rslt_list);
		resAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_matric_result.setAdapter(resAdapter);


		mtr_day_list=new ArrayList<>();
		mtr_month_list=new ArrayList<>();
		mtr_year_list=new ArrayList<>();

		for (int i=1;i<32;i++)
			mtr_day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			mtr_month_list.add(String.valueOf(i));
		for (int i=1970;i<2021;i++)
			mtr_year_list.add(String.valueOf(i));


		sp_mtr_passing_day=(Spinner)v.findViewById(R.id.sp_matric_passing_dayid);
		sp_mtr_passing_month=(Spinner)v.findViewById(R.id.sp_matric_passing_monthid);
		sp_mtr_passing_year=(Spinner)v.findViewById(R.id.sp_matric_passing_yearid);

		ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mtr_day_list);
		dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_mtr_passing_day.setAdapter(dayAdapter);

		ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mtr_month_list);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_mtr_passing_month.setAdapter(monthAdapter);

		ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mtr_year_list);
		yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_mtr_passing_year.setAdapter(yearAdapter);


		sp_mtr_grade=(Spinner)v.findViewById(R.id.sp_matric_grade_id);
		sp_mtr_mode=(Spinner)v.findViewById(R.id.sp_matric_mode_id);
		mtr_grade_list=new ArrayList<>();
		mtr_grade_list.add("Select");
		mtr_grade_list.add("1st Devision");
		mtr_grade_list.add("2nd Devision");
		mtr_grade_list.add("3rd Devision");


		mtr_mode_list=new ArrayList<>();
		mtr_mode_list.add("Select");
		mtr_mode_list.add("Regular");
		mtr_mode_list.add("Correspondence/Distance");
		mtr_mode_list.add("Other");

		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, mtr_grade_list);
		sp_mtr_grade.setAdapter(gradeAdapter);

		ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, mtr_mode_list);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_mtr_mode.setAdapter(modeAdapter);

		tv_matric.setOnClickListener(this);


		et_matric_obt_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				String tot_mk = et_matric_ttl_mark.getText().toString();
				String obt_mk = String.valueOf(s);

				try {
					int t_m = Integer.parseInt(tot_mk);
					int o_m = Integer.parseInt(obt_mk);

					int per=0;
					if(t_m>0)
					per = (Integer) (o_m * 100) / t_m;

					//Log.e("percentage_bf", String.valueOf(per) + " t_m " + t_m + " o_m " + o_m + " s " + s);
					et_matric_per_mark.setText(String.valueOf(per));
				} catch (NumberFormatException e) {
					Log.e("Numberformatexce", e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk = et_matric_ttl_mark.getText().toString();
				String obt_mk = String.valueOf(s);  //et_matric_ttl_mark.getText().toString();
				try {
					int t_m = Integer.parseInt(tot_mk);
					int o_m = Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per = (Integer) (o_m * 100) / t_m;

					//Log.e("percentage_af", String.valueOf(per) + " t_m " + t_m + " o_m " + o_m + " s " + s);
					et_matric_per_mark.setText(String.valueOf(per));
				} catch (NumberFormatException e) {
					Log.e("Numberformatexce", e.toString());
				}


			}
		});

		et_matric_ttl_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String tot_mk = String.valueOf(s);
				String obt_mk = et_matric_obt_mark.getText().toString();

				try {
					int t_m = Integer.parseInt(tot_mk);
					int o_m = Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					per = (Integer) (o_m * 100) / t_m;

					//Log.e("percentage_bf", String.valueOf(per) + " t_m " + t_m + " o_m " + o_m + " s " + s);
					et_matric_per_mark.setText(String.valueOf(per));
				} catch (NumberFormatException e) {
					Log.e("Numberformatexce", e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk = String.valueOf(s);
				String obt_mk = et_matric_obt_mark.getText().toString();

				try {
					int t_m = Integer.parseInt(tot_mk);
					int o_m = Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per = (Integer) (o_m * 100) / t_m;

					//Log.e("percentage_af", String.valueOf(per) + " t_m " + t_m + " o_m " + o_m + " s " + s);
					et_matric_per_mark.setText(String.valueOf(per));
				} catch (NumberFormatException e) {
					Log.e("Numberformatexce", e.toString());
				}

			}
		});


		tv_postgraduation=(TextView)v.findViewById(R.id.postgraduation_textviewid);
		tv_graduation=(TextView)v.findViewById(R.id.graduation_textviewid);
		tv_diploma=(TextView)v.findViewById(R.id.diploma_textviewid);
		tv_intermed=(TextView)v.findViewById(R.id.intermed_textviewid);
		tv_matric=(TextView)v.findViewById(R.id.matrict_textviewid);

		ly_matric=(LinearLayout)v.findViewById(R.id.matric_layoutid);
		ly_intermed=(LinearLayout)v.findViewById(R.id.intermed_layoutid);
		ly_diploma=(LinearLayout)v.findViewById(R.id.diploma_layoutid);
		lys_diploma=(LinearLayout)v.findViewById(R.id.dipl_detail_layoutid);
		ly_graduation=(LinearLayout)v.findViewById(R.id.graducation_layoutid);
		ly_postgraduation=(LinearLayout)v.findViewById(R.id.postgraducation_layoutid);

		ly_intermed_rb=(LinearLayout)v.findViewById(R.id.intermed_rb_layoutid);
		ly_graduation_rb=(LinearLayout)v.findViewById(R.id.grd_rb_layoutid);
		ly_postgraduation_rb=(LinearLayout)v.findViewById(R.id.postgrd_rb_layoutid);

		et_12th_sub=(EditText)v.findViewById(R.id.et_12th_subjectid);
		et_12th_brd=(EditText)v.findViewById(R.id.et_12th_boardid);
		sp_12th_brd_state=(Spinner)v.findViewById(R.id.sp_12th_board_stateid);
		et_12th_school=(EditText)v.findViewById(R.id.et_12th_schoolid);
		et_12th_rollno=(EditText)v.findViewById(R.id.et_12th_rollnoid);
		et_12th_obt_mark=(EditText)v.findViewById(R.id.et_12th_obt_markid);
		et_12th_ttl_mark=(EditText)v.findViewById(R.id.et_12th_ttl_markid);
		et_12th_per_mark=(EditText)v.findViewById(R.id.et_12th_perc_markid);




		et_12th_obt_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				String tot_mk=et_12th_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);

					int per=0;
					if(t_m>0)
						per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_12th_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=et_12th_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);  //et_matric_ttl_mark.getText().toString();
				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

				//	Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_12th_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}


			}
		});

		et_12th_ttl_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_12th_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_12th_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_12th_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_12th_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}
		});

		ArrayAdapter<String> stateint = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		stateint.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_brd_state.setAdapter(stateint);

		sp_12th_stream=(Spinner)v.findViewById(R.id.sp_12th_stream_id);

		intermed_stream_list=new ArrayList<>();
		intermed_stream_list.add("Select");
		intermed_stream_list.add("Art");
		intermed_stream_list.add("Commerce");
		intermed_stream_list.add("Science");

		ArrayAdapter<String> strAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_stream_list);
		strAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_stream.setAdapter(strAdapter);


		sp_12th_result=(Spinner)v.findViewById(R.id.sp_12th_resultid);

        intermed_rslt_list=new ArrayList<>();
		intermed_rslt_list.add("Select");
		intermed_rslt_list.add("Pass");
		intermed_rslt_list.add("Fail");
		intermed_rslt_list.add("Result Awaited");

		ArrayAdapter<String> inresAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_rslt_list);
		inresAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_result.setAdapter(inresAdapter);


		intermed_day_list=new ArrayList<>();
		intermed_month_list=new ArrayList<>();
		intermed_year_list=new ArrayList<>();

		for (int i=1;i<32;i++)
			intermed_day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			intermed_month_list.add(String.valueOf(i));
		for (int i=1970;i<2021;i++)
			intermed_year_list.add(String.valueOf(i));


		sp_12th_passing_day=(Spinner)v.findViewById(R.id.sp_12th_passing_dayid);
		sp_12th_passing_month=(Spinner)v.findViewById(R.id.sp_12th_passing_monthid);
		sp_12th_passing_year=(Spinner)v.findViewById(R.id.sp_12th_passing_yearid);

		ArrayAdapter<String> indayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_day_list);
		indayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_passing_day.setAdapter(indayAdapter);

		ArrayAdapter<String> inmonthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_month_list);
		inmonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_passing_month.setAdapter(monthAdapter);

		ArrayAdapter<String> inyearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_year_list);
		inyearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_passing_year.setAdapter(inyearAdapter);


		sp_12th_grade=(Spinner)v.findViewById(R.id.sp_12th_grade_id);
		sp_12th_mode=(Spinner)v.findViewById(R.id.sp_12th_mode_id);
		intermed_grade_list=new ArrayList<>();
		intermed_grade_list.add("Select");
		intermed_grade_list.add("1st Devision");
		intermed_grade_list.add("2nd Devision");
		intermed_grade_list.add("3rd Devision");


		intermed_mode_list=new ArrayList<>();
		intermed_mode_list.add("Select");
		intermed_mode_list.add("Regular");
		intermed_mode_list.add("Correspondence/Distance");
		intermed_mode_list.add("Other");

		ArrayAdapter<String> ingradeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_grade_list);
		ingradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_grade.setAdapter(ingradeAdapter);

		ArrayAdapter<String> inmodeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, intermed_mode_list);
		inmodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_12th_mode.setAdapter(inmodeAdapter);

		tv_intermed.setOnClickListener(this);



		et_dipl_sub=(EditText)v.findViewById(R.id.et_dipl_streamid);
		et_dipl_brd=(EditText)v.findViewById(R.id.et_dipl_boardid);
		sp_dipl_brd_state=(Spinner)v.findViewById(R.id.sp_dipl_board_stateid);
		et_dipl_school=(EditText)v.findViewById(R.id.et_dipl_schoolid);
		et_dipl_rollno=(EditText)v.findViewById(R.id.et_dipl_rollnoid);
		et_dipl_obt_mark=(EditText)v.findViewById(R.id.et_dipl_obt_markid);
		et_dipl_ttl_mark=(EditText)v.findViewById(R.id.et_dipl_ttl_markid);
		et_dipl_per_mark=(EditText)v.findViewById(R.id.et_dipl_perc_markid);
		et_dipl_duration=(EditText)v.findViewById(R.id.et_dipl_duretionid);

        rb_dipl_yes=(RadioButton)v.findViewById(R.id.rb_dipl_yesid);
		rb_dipl_no=(RadioButton)v.findViewById(R.id.rb_dipl_noid);

		et_dipl_obt_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				String tot_mk=et_dipl_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_dipl_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=et_dipl_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);  //et_matric_ttl_mark.getText().toString();
				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_dipl_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}


			}
		});

		et_dipl_ttl_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_dipl_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_dipl_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_dipl_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_dipl_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}
			}
		});


		ArrayAdapter<String> stated = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		stated.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_brd_state.setAdapter(stated);

		sp_dipl_type=(Spinner)v.findViewById(R.id.sp_dipl_type_id);

		dipl_type_list=new ArrayList<>();
		dipl_type_list.add("Select");
		dipl_type_list.add("Diploma after 10th (Equivalent to 12th )");
		dipl_type_list.add("Diploma after 12th");


		ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_type_list);
		typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_type.setAdapter(typeAdapter);


		sp_dipl_result=(Spinner)v.findViewById(R.id.sp_dipl_resultid);

		dipl_rslt_list=new ArrayList<>();
		dipl_rslt_list.add("Select");
		dipl_rslt_list.add("Pass");
		dipl_rslt_list.add("Fail");
		dipl_rslt_list.add("Result Awaited");

		ArrayAdapter<String> dresAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_rslt_list);
		dresAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_result.setAdapter(dresAdapter);


		dipl_day_list=new ArrayList<>();
		dipl_month_list=new ArrayList<>();
		dipl_year_list=new ArrayList<>();

		for (int i=1;i<32;i++)
			dipl_day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			dipl_month_list.add(String.valueOf(i));
		for (int i=1970;i<2021;i++)
			dipl_year_list.add(String.valueOf(i));


		sp_dipl_passing_day=(Spinner)v.findViewById(R.id.sp_dipl_passing_dayid);
		sp_dipl_passing_month=(Spinner)v.findViewById(R.id.sp_dipl_passing_monthid);
		sp_dipl_passing_year=(Spinner)v.findViewById(R.id.sp_dipl_passing_yearid);

		ArrayAdapter<String> ddayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_day_list);
		ddayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_passing_day.setAdapter(ddayAdapter);

		ArrayAdapter<String> dmonthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_month_list);
		dmonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_passing_month.setAdapter(dmonthAdapter);

		ArrayAdapter<String> dyearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_year_list);
		dyearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_passing_year.setAdapter(dyearAdapter);


		sp_dipl_grade=(Spinner)v.findViewById(R.id.sp_dipl_grade_id);
		sp_dipl_mode=(Spinner)v.findViewById(R.id.sp_dipl_mode_id);
		dipl_grade_list=new ArrayList<>();
		dipl_grade_list.add("Select");
		dipl_grade_list.add("1st Devision");
		dipl_grade_list.add("2nd Devision");
		dipl_grade_list.add("3rd Devision");


		dipl_mode_list=new ArrayList<>();
		dipl_mode_list.add("Select");
		dipl_mode_list.add("Regular");
		dipl_mode_list.add("Correspondence/Distance");
		dipl_mode_list.add("Other");

		ArrayAdapter<String> dgradeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_grade_list);
		dgradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_grade.setAdapter(dgradeAdapter);

		ArrayAdapter<String> dmodeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dipl_mode_list);
		dmodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_dipl_mode.setAdapter(dmodeAdapter);

		tv_diploma.setOnClickListener(this);

		rb_dipl_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_diploma.setVisibility(View.VISIBLE);
			}
		});
		rb_dipl_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					lys_diploma.setVisibility(View.GONE);
			}
		});

		rb_mtr_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_matric.setVisibility(View.VISIBLE);
			}
		});
		rb_mtr_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_matric.setVisibility(View.GONE);
			}
		});

		rb_intermed_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_intermed.setVisibility(View.VISIBLE);
			}
		});
		rb_intermed_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_intermed.setVisibility(View.GONE);
			}
		});

		rb_grd_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_graduation.setVisibility(View.VISIBLE);
			}
		});
		rb_grd_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_graduation.setVisibility(View.GONE);
			}
		});

		rb_postgrd_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_postgraduation.setVisibility(View.VISIBLE);
			}
		});
		rb_postgrd_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					ly_postgraduation.setVisibility(View.GONE);
			}
		});


		tv_diploma.setOnClickListener(this);

    	et_grd_deg_sub=(EditText)v.findViewById(R.id.et_grd_deg_streamid);
		et_grd_deg_duration=(EditText)v.findViewById(R.id.et_grd_deg_duretionid);
		et_grd_deg_unver=(EditText)v.findViewById(R.id.et_grd_deg_unverid);
		et_grd_deg_place=(EditText)v.findViewById(R.id.et_grd_deg_unver_placeid);
		sp_grd_deg_state=(Spinner)v.findViewById(R.id.sp_grd_deg_unver_stateid);
		et_grd_deg_college=(EditText)v.findViewById(R.id.et_grd_deg_collegeid);

		et_grd_deg_obt_mark=(EditText)v.findViewById(R.id.et_grd_deg_obt_markid);
		et_grd_deg_ttl_mark=(EditText)v.findViewById(R.id.et_grd_deg_ttl_markid);
		et_grd_deg_per_mark=(EditText)v.findViewById(R.id.et_grd_deg_perc_markid);
		et_grd_deg_cgpa=(EditText)v.findViewById(R.id.et_grd_deg_cgpaid);
		et_grd_deg_max_cgpa=(EditText)v.findViewById(R.id.et_grd_deg_max_cgpaid);


		et_grd_deg_obt_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				String tot_mk=et_grd_deg_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

				//	Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_grd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=et_grd_deg_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);  //et_matric_ttl_mark.getText().toString();
				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_grd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}


			}
		});

		et_grd_deg_ttl_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_grd_deg_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);

					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_grd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_grd_deg_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

				//	Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_grd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}
			}
		});



		ArrayAdapter<String> stateg = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		stateg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_state.setAdapter(stateg);

		sp_grd_deg_name=(Spinner)v.findViewById(R.id.sp_grd_deg_name_id);

		//B.Arch, B.Com, B.Pharm,B.A., BBA,BBM, BCA,BDS,BE/B.Tech., BEd, BHM, BSc,BVSc, CA, CS,ICWA,LLB, MBBS.
		grd_deg_name_list=new ArrayList<>();
		grd_deg_name_list.add("Select");
		grd_deg_name_list.add("B.Arch");
		grd_deg_name_list.add("B.Pharm");
		grd_deg_name_list.add("B.A.");
		grd_deg_name_list.add("B.Com.");
		grd_deg_name_list.add("BBA");
		grd_deg_name_list.add("BBM");
		grd_deg_name_list.add("BCA");
		grd_deg_name_list.add("BDS");
		grd_deg_name_list.add("BE/B.Tech.");
		grd_deg_name_list.add("BEd");
		grd_deg_name_list.add("BHM");
		grd_deg_name_list.add("BSc");
		grd_deg_name_list.add("BVSc");
		grd_deg_name_list.add("CA");
		grd_deg_name_list.add("CS");
		grd_deg_name_list.add("ICWA");
		grd_deg_name_list.add("LLB");
		grd_deg_name_list.add("MBBS");
		grd_deg_name_list.add("Other");

		ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, grd_deg_name_list);
		nameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_name.setAdapter(nameAdapter);


		sp_grd_deg_result=(Spinner)v.findViewById(R.id.sp_grd_deg_resultid);

		grd_deg_rslt_list=new ArrayList<>();
		grd_deg_rslt_list.add("Select");
		grd_deg_rslt_list.add("Pass");
		grd_deg_rslt_list.add("Fail");
		grd_deg_rslt_list.add("Result Awaited");

		ArrayAdapter<String> gresAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, grd_deg_rslt_list);
		gresAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_result.setAdapter(gresAdapter);

		grd_deg_day_list=new ArrayList<>();
		grd_deg_month_list=new ArrayList<>();
		grd_deg_year_list=new ArrayList<>();

		for (int i=1;i<32;i++)
			grd_deg_day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			grd_deg_month_list.add(String.valueOf(i));
		for (int i=1970;i<2021;i++)
			grd_deg_year_list.add(String.valueOf(i));


		sp_grd_deg_passing_day=(Spinner)v.findViewById(R.id.sp_grd_deg_passing_dayid);
		sp_grd_deg_passing_month=(Spinner)v.findViewById(R.id.sp_grd_deg_passing_monthid);
		sp_grd_deg_passing_year=(Spinner)v.findViewById(R.id.sp_grd_deg_passing_yearid);

		ArrayAdapter<String> gdayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, grd_deg_day_list);
		gdayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_passing_day.setAdapter(gdayAdapter);

		ArrayAdapter<String> gmonthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, grd_deg_month_list);
		gmonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_passing_month.setAdapter(gmonthAdapter);

		ArrayAdapter<String> gyearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, grd_deg_year_list);
		gyearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_passing_year.setAdapter(gyearAdapter);


		sp_grd_deg_grade=(Spinner)v.findViewById(R.id.sp_grd_deg_grade_id);
		sp_grd_deg_mode=(Spinner)v.findViewById(R.id.sp_grd_deg_mode_id);
		grd_deg_grade_list=new ArrayList<>();
		grd_deg_grade_list.add("Select");
		grd_deg_grade_list.add("1st Devision");
		grd_deg_grade_list.add("2nd Devision");
		grd_deg_grade_list.add("3rd Devision");


		grd_deg_mode_list=new ArrayList<>();
		grd_deg_mode_list.add("Select");
		grd_deg_mode_list.add("Regular");
		grd_deg_mode_list.add("Correspondence/Distance");
		grd_deg_mode_list.add("Other");

		ArrayAdapter<String> ggradeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, grd_deg_grade_list);
		ggradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_grd_deg_grade.setAdapter(ggradeAdapter);

		ArrayAdapter<String> gmodeAdapter = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item, grd_deg_mode_list);
		sp_grd_deg_mode.setAdapter(gmodeAdapter);

		tv_graduation.setOnClickListener(this);

		et_postgrd_deg_sub=(EditText)v.findViewById(R.id.et_postgrd_deg_streamid);
		et_postgrd_deg_duration=(EditText)v.findViewById(R.id.et_postgrd_deg_duretionid);
		et_postgrd_deg_unver=(EditText)v.findViewById(R.id.et_postgrd_deg_unverid);
		et_postgrd_deg_place=(EditText)v.findViewById(R.id.et_postgrd_deg_unver_placeid);
		sp_postgrd_deg_state=(Spinner)v.findViewById(R.id.sp_postgrd_deg_unver_stateid);
		et_postgrd_deg_college=(EditText)v.findViewById(R.id.et_postgrd_deg_collegeid);

		et_postgrd_deg_obt_mark=(EditText)v.findViewById(R.id.et_postgrd_deg_obt_markid);
		et_postgrd_deg_ttl_mark=(EditText)v.findViewById(R.id.et_postgrd_deg_ttl_markid);
		et_postgrd_deg_per_mark=(EditText)v.findViewById(R.id.et_postgrd_deg_perc_markid);
		et_postgrd_deg_cgpa=(EditText)v.findViewById(R.id.et_postgrd_deg_cgpaid);
		et_postgrd_deg_max_cgpa=(EditText)v.findViewById(R.id.et_postgrd_deg_max_cgpaid);



		et_postgrd_deg_obt_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				String tot_mk=et_postgrd_deg_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_postgrd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=et_postgrd_deg_ttl_mark.getText().toString();
				String obt_mk=String.valueOf(s);  //et_matric_ttl_mark.getText().toString();
				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_postgrd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}


			}
		});

		et_postgrd_deg_ttl_mark.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_postgrd_deg_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);

					int per=0;
					if(t_m>0)
					per=(Integer)(o_m * 100)/t_m;

					//Log.e("percentage_bf",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_postgrd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				String tot_mk=String.valueOf(s);
				String obt_mk=et_postgrd_deg_obt_mark.getText().toString();

				try {
					int t_m=Integer.parseInt(tot_mk);
					int o_m=Integer.parseInt(obt_mk);
					int per=0;
					if(t_m>0)
					 per=(Integer)(o_m * 100)/t_m;

				//	Log.e("percentage_af",String.valueOf(per)+" t_m "+t_m+" o_m "+o_m+" s "+s);
					et_postgrd_deg_per_mark.setText(String.valueOf(per));
				}catch (NumberFormatException e){
					Log.e("Numberformatexce",e.toString());
				}
			}
		});




		ArrayAdapter<String> statepg = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Constants.statelist);
		statepg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_state.setAdapter(statepg);



		sp_postgrd_deg_name=(Spinner)v.findViewById(R.id.sp_postgrd_deg_name_id);

//		LLB, M.Phil, M.Arch,M.Com,M.Pharm, M.A.,MBA, PGDM, MCA, MD, MDS, ME/M.Tech., MEd, MHM,MS, BSc., MSW, PG Diploma
		postgrd_deg_name_list=new ArrayList<>();
		postgrd_deg_name_list.add("Select");
		postgrd_deg_name_list.add("M.Phil");
		postgrd_deg_name_list.add("M.Arch");
		postgrd_deg_name_list.add("M.Com");
		postgrd_deg_name_list.add("M.Pharm");
		postgrd_deg_name_list.add("M.A.");
		postgrd_deg_name_list.add("MBA");
		postgrd_deg_name_list.add("PGDM");
		postgrd_deg_name_list.add("MCA");
		postgrd_deg_name_list.add("MD");
		postgrd_deg_name_list.add("MDS");
		postgrd_deg_name_list.add("ME/M.Tech.");
		postgrd_deg_name_list.add("MEd");
		postgrd_deg_name_list.add("MHM");
		postgrd_deg_name_list.add("MS");
		postgrd_deg_name_list.add("MSW");
		postgrd_deg_name_list.add("PG Diploma");
		postgrd_deg_name_list.add("Other");


		ArrayAdapter<String> postnameAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_name_list);
		postnameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_name.setAdapter(postnameAdapter);


		sp_postgrd_deg_result=(Spinner)v.findViewById(R.id.sp_postgrd_deg_resultid);

		postgrd_deg_rslt_list=new ArrayList<>();
		postgrd_deg_rslt_list.add("Select");
		postgrd_deg_rslt_list.add("Pass");
		postgrd_deg_rslt_list.add("Fail");
		postgrd_deg_rslt_list.add("Result Awaited");

		ArrayAdapter<String> pgresAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_rslt_list);
		pgresAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_result.setAdapter(pgresAdapter);


		postgrd_deg_day_list=new ArrayList<>();
		postgrd_deg_month_list=new ArrayList<>();
		postgrd_deg_year_list=new ArrayList<>();

		for (int i=1;i<32;i++)
			postgrd_deg_day_list.add(String.valueOf(i));
		for (int i=1;i<13;i++)
			postgrd_deg_month_list.add(String.valueOf(i));
		for (int i=1970;i<2021;i++)
			postgrd_deg_year_list.add(String.valueOf(i));


		sp_postgrd_deg_passing_day=(Spinner)v.findViewById(R.id.sp_postgrd_deg_passing_dayid);
		sp_postgrd_deg_passing_month=(Spinner)v.findViewById(R.id.sp_postgrd_deg_passing_monthid);
		sp_postgrd_deg_passing_year=(Spinner)v.findViewById(R.id.sp_postgrd_deg_passing_yearid);

		ArrayAdapter<String> pgdayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_day_list);
		pgdayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_passing_day.setAdapter(pgdayAdapter);

		ArrayAdapter<String> pgmonthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_month_list);
		pgmonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_passing_month.setAdapter(pgmonthAdapter);

		ArrayAdapter<String> pgyearAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_year_list);
		pgyearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_passing_year.setAdapter(pgyearAdapter);


		sp_postgrd_deg_grade=(Spinner)v.findViewById(R.id.sp_postgrd_deg_grade_id);
		sp_postgrd_deg_mode=(Spinner)v.findViewById(R.id.sp_postgrd_deg_mode_id);
		postgrd_deg_grade_list=new ArrayList<>();
		postgrd_deg_grade_list.add("Select");
		postgrd_deg_grade_list.add("1st Devision");
		postgrd_deg_grade_list.add("2nd Devision");
		postgrd_deg_grade_list.add("3rd Devision");


		postgrd_deg_mode_list=new ArrayList<>();
		postgrd_deg_mode_list.add("Select");
		postgrd_deg_mode_list.add("Regular");
		postgrd_deg_mode_list.add("Correspondence/Distance");
		postgrd_deg_mode_list.add("Other");

		ArrayAdapter<String> pggradeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_grade_list);
		pggradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_grade.setAdapter(pggradeAdapter);

		ArrayAdapter<String> pgmodeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, postgrd_deg_mode_list);
		pgmodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_postgrd_deg_mode.setAdapter(pgmodeAdapter);


		h_read=(CheckBox)v.findViewById(R.id.h_read);
		h_write=(CheckBox)v.findViewById(R.id.h_write);
		h_speak=(CheckBox)v.findViewById(R.id.h_speak);

		e_read=(CheckBox)v.findViewById(R.id.e_read);
		e_write=(CheckBox)v.findViewById(R.id.e_write);
		e_speak=(CheckBox)v.findViewById(R.id.e_speak);

		o_read=(CheckBox)v.findViewById(R.id.o_read);
		o_write=(CheckBox)v.findViewById(R.id.o_write);
		o_speak=(CheckBox)v.findViewById(R.id.o_speak);

		other_lan=(EditText)v.findViewById(R.id.et_other_languageid);

		tv_postgraduation.setOnClickListener(this);



		tv_language=(TextView)v.findViewById(R.id.language_textviewid);
		ly_language=(LinearLayout)v.findViewById(R.id.language_layoutid);

		tv_language.setOnClickListener(this);

       String edu_info,temp;
		String[] st_ar;
		int ind=0;
		edu_info=user_info.getString("education_info","");

		Log.e("education_info",edu_info);

		if(edu_info.length()>0) {

			try {
				JSONObject	jobj = new JSONObject(edu_info);


				if(jobj.getString("matric_yn").equalsIgnoreCase("yes")==true)
					rb_mtr_yes.setChecked(true);

				et_matric_sub.setText(jobj.getString("sub10"));
				et_matric_brd.setText(jobj.getString("board10"));
				et_matric_school.setText(jobj.getString("school10"));
				et_matric_rollno.setText(jobj.getString("rollno10"));
				et_matric_obt_mark.setText(jobj.getString("obtainmark10"));
				et_matric_ttl_mark.setText(jobj.getString("totalmark10"));
				et_matric_per_mark.setText(jobj.getString("percentagemark10"));

				temp=jobj.getString("boardstate10");
				if(Constants.statelist.contains(temp)){
					ind=Constants.statelist.indexOf(temp);
					sp_matric_brd_state.setSelection(ind);
				}

				temp=jobj.getString("result10");
				if(mtr_rslt_list.contains(temp)){
					ind=mtr_rslt_list.indexOf(temp);
					sp_matric_result.setSelection(ind);

					}

				temp=jobj.getString("passingdate10");
				st_ar=temp.split("-");

				sp_mtr_passing_year.setSelection(mtr_year_list.indexOf(st_ar[0]));
				sp_mtr_passing_month.setSelection(mtr_month_list.indexOf(st_ar[1]));
				sp_mtr_passing_day.setSelection(mtr_day_list.indexOf(st_ar[2]));

				temp=jobj.getString("grade10");
				if(mtr_grade_list.contains(temp))
				sp_mtr_grade.setSelection(mtr_grade_list.indexOf(temp));

				temp=jobj.getString("mode10");
				if(mtr_mode_list.contains(temp))
					sp_mtr_mode.setSelection(mtr_mode_list.indexOf(temp));

// Inter Mediate


				if(jobj.getString("intermed_yn").equalsIgnoreCase("yes")==true)
					rb_intermed_yes.setChecked(true);


				et_12th_sub.setText(jobj.getString("sub12"));
				et_12th_brd.setText(jobj.getString("board12"));
				et_12th_school.setText(jobj.getString("school12"));
				et_12th_rollno.setText(jobj.getString("rollno12"));
				et_12th_obt_mark.setText(jobj.getString("obtainmark12"));
				et_12th_ttl_mark.setText(jobj.getString("totalmark12"));
				et_12th_per_mark.setText(jobj.getString("percentagemark12"));

				temp=jobj.getString("stream12");
				if(intermed_stream_list.contains(temp)){
					ind=intermed_stream_list.indexOf(temp);
					sp_12th_stream.setSelection(ind);
				}


				temp=jobj.getString("boardstate12");
				if(Constants.statelist.contains(temp)){
					ind=Constants.statelist.indexOf(temp);
					sp_12th_brd_state.setSelection(ind);
				}

				temp=jobj.getString("result12");
				if(intermed_rslt_list.contains(temp)){
					ind=intermed_rslt_list.indexOf(temp);
					sp_12th_result.setSelection(ind);
                }

				temp=jobj.getString("passingdate12");
				st_ar=temp.split("-");

				sp_12th_passing_year.setSelection(intermed_year_list.indexOf(st_ar[0]));
				sp_12th_passing_month.setSelection(intermed_month_list.indexOf(st_ar[1]));
				sp_12th_passing_day.setSelection(intermed_day_list.indexOf(st_ar[2]));


				temp=jobj.getString("grade12");
				if(intermed_grade_list.contains(temp))
					sp_12th_grade.setSelection(intermed_grade_list.indexOf(temp));

				temp=jobj.getString("mode12");
				if(intermed_mode_list.contains(temp))
					sp_12th_mode.setSelection(intermed_mode_list.indexOf(temp));


				if(jobj.getString("d_holder").equalsIgnoreCase("yes")==true)
					rb_dipl_yes.setChecked(true);
				else
				rb_dipl_no.setChecked(true);

				et_dipl_sub.setText(jobj.getString("d_stream"));
				et_dipl_duration.setText(jobj.getString("d_duretion"));
				et_dipl_brd.setText(jobj.getString("d_board"));
				et_dipl_school.setText(jobj.getString("d_school"));
				et_dipl_rollno.setText(jobj.getString("d_rollno"));
				et_dipl_obt_mark.setText(jobj.getString("d_obt_mark"));
				et_dipl_ttl_mark.setText(jobj.getString("d_total_mark"));
				et_dipl_per_mark.setText(jobj.getString("d_per_mark"));

				temp=jobj.getString("d_deploma");
				if(dipl_type_list.contains(temp)){
					ind=dipl_type_list.indexOf(temp);
					sp_dipl_type.setSelection(ind);
				}


				temp=jobj.getString("d_board_state");
				if(Constants.statelist.contains(temp)){
					ind=Constants.statelist.indexOf(temp);
					sp_dipl_brd_state.setSelection(ind);
				}

				temp=jobj.getString("d_result");
				if(dipl_rslt_list.contains(temp)){
					ind=dipl_rslt_list.indexOf(temp);
					sp_dipl_result.setSelection(ind);
				}

				temp=jobj.getString("d_date_passing");
				st_ar=temp.split("-");

				sp_dipl_passing_year.setSelection(dipl_year_list.indexOf(st_ar[0]));
				sp_dipl_passing_month.setSelection(dipl_month_list.indexOf(st_ar[1]));
				sp_dipl_passing_day.setSelection(dipl_day_list.indexOf(st_ar[2]));


				temp=jobj.getString("d_grade");
				if(dipl_grade_list.contains(temp))
					sp_dipl_grade.setSelection(dipl_grade_list.indexOf(temp));

				temp=jobj.getString("d_mode");
				if(dipl_mode_list.contains(temp))
					sp_dipl_mode.setSelection(dipl_mode_list.indexOf(temp));



				if(jobj.getString("graduation_yn").equalsIgnoreCase("yes")==true)
					rb_grd_yes.setChecked(true);


				et_grd_deg_sub.setText(jobj.getString("g_stream"));
				et_grd_deg_duration.setText(jobj.getString("g_duration"));
				et_grd_deg_unver.setText(jobj.getString("g_univercity"));
				et_grd_deg_place.setText(jobj.getString("g_univercity_place"));
				et_grd_deg_college.setText(jobj.getString("g_collage"));
				et_grd_deg_obt_mark.setText(jobj.getString("g_obt_mark"));
				et_grd_deg_ttl_mark.setText(jobj.getString("g_total_mark"));
				et_grd_deg_per_mark.setText(jobj.getString("g_percentage"));
				et_grd_deg_cgpa.setText(jobj.getString("g_cgpa"));
				et_grd_deg_max_cgpa.setText(jobj.getString("g_max_cgpa"));

				temp=jobj.getString("g_graduate");
				if(grd_deg_name_list.contains(temp)){
					ind=grd_deg_name_list.indexOf(temp);
					sp_grd_deg_name.setSelection(ind);
				}

				temp=jobj.getString("g_univercity_state");
				if(Constants.statelist.contains(temp)){
					ind=Constants.statelist.indexOf(temp);
					sp_grd_deg_state.setSelection(ind);
				}

				temp=jobj.getString("g_result");
				if(grd_deg_rslt_list.contains(temp)){
					ind=grd_deg_rslt_list.indexOf(temp);
					sp_grd_deg_result.setSelection(ind);
				}

				temp=jobj.getString("g_date_passing");

				st_ar=temp.split("-");

				if(st_ar.length>1) {
					sp_grd_deg_passing_year.setSelection(grd_deg_year_list.indexOf(st_ar[0]));
					sp_grd_deg_passing_month.setSelection(grd_deg_month_list.indexOf(st_ar[1]));
					sp_grd_deg_passing_day.setSelection(grd_deg_day_list.indexOf(st_ar[2]));
				}

				temp=jobj.getString("g_grade");
				if(grd_deg_grade_list.contains(temp))
					sp_grd_deg_grade.setSelection(grd_deg_grade_list.indexOf(temp));

				temp=jobj.getString("g_mode");
				if(grd_deg_mode_list.contains(temp))
					sp_grd_deg_mode.setSelection(grd_deg_mode_list.indexOf(temp));


				if(jobj.getString("postgraduation_yn").equalsIgnoreCase("yes")==true)
					rb_postgrd_yes.setChecked(true);

				et_postgrd_deg_sub.setText(jobj.getString("pg_stream"));
				et_postgrd_deg_duration.setText(jobj.getString("pg_duration"));
				et_postgrd_deg_unver.setText(jobj.getString("pg_univercity"));
				et_postgrd_deg_place.setText(jobj.getString("pg_univercity_place"));
				et_postgrd_deg_college.setText(jobj.getString("pg_collage"));
				et_postgrd_deg_obt_mark.setText(jobj.getString("pg_obt_mark"));
				et_postgrd_deg_ttl_mark.setText(jobj.getString("pg_total_mark"));
				et_postgrd_deg_per_mark.setText(jobj.getString("pg_percentage"));
				et_postgrd_deg_cgpa.setText(jobj.getString("pg_cgpa"));
				et_postgrd_deg_max_cgpa.setText(jobj.getString("pg_max_cgpa"));

				temp=jobj.getString("pg_graduate");
				if(postgrd_deg_name_list.contains(temp)){
					ind=postgrd_deg_name_list.indexOf(temp);
					sp_postgrd_deg_name.setSelection(ind);
				}

				temp=jobj.getString("pg_univercity_state");
				if(Constants.statelist.contains(temp)){
					ind=Constants.statelist.indexOf(temp);
					sp_postgrd_deg_state.setSelection(ind);
				}

				temp=jobj.getString("pg_result");
				if(postgrd_deg_rslt_list.contains(temp)){
					ind=postgrd_deg_rslt_list.indexOf(temp);
					sp_postgrd_deg_result.setSelection(ind);
				}

				temp=jobj.getString("pg_date_passing");

				st_ar=temp.split("-");

				if(st_ar.length>1) {
					sp_postgrd_deg_passing_year.setSelection(postgrd_deg_year_list.indexOf(st_ar[0]));
					sp_postgrd_deg_passing_month.setSelection(postgrd_deg_month_list.indexOf(st_ar[1]));
					sp_postgrd_deg_passing_day.setSelection(postgrd_deg_day_list.indexOf(st_ar[2]));
				}

				temp=jobj.getString("pg_grade");
				if(postgrd_deg_grade_list.contains(temp))
					sp_postgrd_deg_grade.setSelection(postgrd_deg_grade_list.indexOf(temp));

				temp=jobj.getString("pg_mode");
				if(postgrd_deg_mode_list.contains(temp))
					sp_postgrd_deg_mode.setSelection(postgrd_deg_mode_list.indexOf(temp));

				if(jobj.getString("hin_read").equalsIgnoreCase("Read")==true)
					h_read.setChecked(true);

				if(jobj.getString("hin_write").equalsIgnoreCase("Write")==true)
					h_write.setChecked(true);

				if(jobj.getString("hin_speak").equalsIgnoreCase("Speak")==true)
					h_speak.setChecked(true);

				if(jobj.getString("eng_read").equalsIgnoreCase("Read")==true)
					e_read.setChecked(true);

				if(jobj.getString("eng_write").equalsIgnoreCase("Write")==true)
					e_write.setChecked(true);

				if(jobj.getString("eng_speak").equalsIgnoreCase("Speak")==true)
					e_speak.setChecked(true);

				other_lan.setText(jobj.getString("oth_lang"));

				if(jobj.getString("oth_read").equalsIgnoreCase("Read")==true)
					o_read.setChecked(true);

				if(jobj.getString("oth_write").equalsIgnoreCase("Write")==true)
					o_write.setChecked(true);

				if(jobj.getString("oth_speak").equalsIgnoreCase("Speak")==true)
					o_speak.setChecked(true);


			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				all_sub=et_matric_sub.getText().toString();
				nameofboard=et_matric_brd.getText().toString();

				stateboard=sp_matric_brd_state.getSelectedItem().toString();
				if(stateboard.equalsIgnoreCase("Select"))
					stateboard="";

				name_of_school=et_matric_school.getText().toString();
				result=sp_matric_result.getSelectedItem().toString();

				if(result.equalsIgnoreCase("Select"))
					result="";

				roll_number=et_matric_rollno.getText().toString();
				obtanined_mrks=et_matric_obt_mark.getText().toString();
				total_marks=et_matric_ttl_mark.getText().toString();
				percentage_mrks=et_matric_per_mark.getText().toString();
				date_of_passing=sp_mtr_passing_year.getSelectedItem().toString()+"-"+sp_mtr_passing_month.getSelectedItem().toString()+"-"+sp_mtr_passing_day.getSelectedItem().toString();
				grade=sp_mtr_grade.getSelectedItem().toString();

				if(grade.equalsIgnoreCase("Select"))
					grade="";

				mode=sp_mtr_mode.getSelectedItem().toString();

				matric_yn=rb_mtr_yes.isChecked()?"yes":"no";

				intermed_yn=rb_intermed_yes.isChecked()?"yes":"no";

				stream=sp_12th_stream.getSelectedItem().toString();
				all_sb12=et_12th_sub.getText().toString();
				name_baord=et_12th_brd.getText().toString();
				state_of_board12=sp_12th_brd_state.getSelectedItem().toString();
				name_of_school12=et_12th_school.getText().toString();
				result12=sp_12th_result.getSelectedItem().toString();
				roll_number12=et_12th_rollno.getText().toString();
				obtanied_mrks12=et_12th_obt_mark.getText().toString();
				total_mrks12=et_12th_ttl_mark.getText().toString();
				p_marks=et_12th_per_mark.getText().toString();

				d_of_pass=sp_12th_passing_year.getSelectedItem().toString()+"-"+sp_12th_passing_month.getSelectedItem().toString()+"-"+sp_12th_passing_day.getSelectedItem().toString();

				division=sp_12th_grade.getSelectedItem().toString();
				mode12=sp_12th_mode.getSelectedItem().toString();
				d_holder=rb_dipl_yes.isChecked()?"yes":"no";

				deploma=sp_dipl_type.getSelectedItem().toString();
				d_stream=et_dipl_sub.getText().toString();
				dur_of_dep=et_dipl_duration.getText().toString();
				nam_of_board=et_dipl_brd.getText().toString();
				d_state=sp_dipl_brd_state.getSelectedItem().toString();
				d_name_school=et_dipl_school.getText().toString();
				d_res=sp_dipl_result.getSelectedItem().toString();
				roll_no=et_dipl_rollno.getText().toString();
				obt_marks=et_dipl_obt_mark.getText().toString();
				tot_marks=et_dipl_ttl_mark.getText().toString();
				per_marksd=et_dipl_per_mark.getText().toString();

				d_of_pass_d=sp_dipl_passing_year.getSelectedItem().toString()+"-"+sp_dipl_passing_month.getSelectedItem().toString()+"-"+sp_dipl_passing_day.getSelectedItem().toString();

				d_grade=sp_dipl_grade.getSelectedItem().toString();
				d_mode=sp_dipl_mode.getSelectedItem().toString();

				graduation_yn=rb_grd_yes.isChecked()?"yes":"no";
				graduate=sp_grd_deg_name.getSelectedItem().toString();
				g_stream=et_grd_deg_sub.getText().toString();
				g_duration=et_grd_deg_duration.getText().toString();
				g_name_uni=et_grd_deg_unver.getText().toString();
				g_state=sp_grd_deg_state.getSelectedItem().toString();
				g_place=et_grd_deg_place.getText().toString();
				g_name_of_clg=et_grd_deg_college.getText().toString();
				g_result=sp_grd_deg_result.getSelectedItem().toString();
				g_marks_obt=et_grd_deg_obt_mark.getText().toString();
				g_total_mrks=et_grd_deg_ttl_mark.getText().toString();
				g_percent=et_grd_deg_per_mark.getText().toString();

				g_d_of_pass=sp_grd_deg_passing_year.getSelectedItem().toString()+"-"+sp_grd_deg_passing_month.getSelectedItem().toString()+"-"+sp_grd_deg_passing_day.getSelectedItem().toString();

				g_grade=sp_grd_deg_grade.getSelectedItem().toString();
				g_mode=sp_grd_deg_mode.getSelectedItem().toString();
				cgpa=et_grd_deg_cgpa.getText().toString();
				mrks_cgpa=et_grd_deg_max_cgpa.getText().toString();

				postgraduation_yn=rb_postgrd_yes.isChecked()?"yes":"no";
				postgraduate=sp_postgrd_deg_name.getSelectedItem().toString();
				pg_streamv=et_postgrd_deg_sub.getText().toString();
				pg_dur=et_postgrd_deg_duration.getText().toString();
				pg_name_of_uni=et_postgrd_deg_unver.getText().toString();
				pg_uni_place=et_postgrd_deg_place.getText().toString();
				pg_uni_state=sp_postgrd_deg_state.getSelectedItem().toString();
				pg_clgname=et_postgrd_deg_college.getText().toString();
				pg_result=sp_postgrd_deg_result.getSelectedItem().toString();
				pg_obt_marks=et_postgrd_deg_obt_mark.getText().toString();
				pg_total_marks=et_postgrd_deg_ttl_mark.getText().toString();
				pg_perc_of_mrks=et_postgrd_deg_per_mark.getText().toString();

				pd_d_pass=sp_postgrd_deg_passing_year.getSelectedItem().toString()+"-"+sp_postgrd_deg_passing_month.getSelectedItem().toString()+"-"+sp_postgrd_deg_passing_day.getSelectedItem().toString();

				pg_garde=sp_postgrd_deg_grade.getSelectedItem().toString();
				pg_modee=sp_postgrd_deg_mode.getSelectedItem().toString();
				pg_cgpa=et_postgrd_deg_cgpa.getText().toString();
				pg_max=et_postgrd_deg_max_cgpa.getText().toString();

				if(h_read.isChecked())
				{
					hindi_read=h_read.getText().toString();
				}
				if(h_write.isChecked())
				{
					hindi_write=h_write.getText().toString();
				}
				if(h_speak.isChecked())
				{
					hindi_speak=h_speak.getText().toString();
				}
				if(e_read.isChecked())
				{
					eng_read=e_read.getText().toString();
				}
				if(e_write.isChecked())
				{
					eng_write=e_write.getText().toString();
				}
				if(e_speak.isChecked())
				{
					eng_speak=e_speak.getText().toString();
				}
				if(o_write.isChecked())
				{
					other_write=o_write.getText().toString();
				}
				if(o_speak.isChecked())
				{
					other_speak=o_speak.getText().toString();
				}
				if(o_read.isChecked())
				{
					other_read=o_read.getText().toString();
				}
				other_lang=other_lan.getText().toString();

				if(nw.isConnected())
					volley();
				else
				{
					final Snackbar snackbar = Snackbar.make(main_layout, "   No Internet Connection.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
				}
//					Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_LONG).show();

			}
		});

		return v;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==R.id.matrict_textviewid){
			if(b_matric) {
				ly_matric_rb.setVisibility(View.GONE);
				b_matric=false;
				tv_matric.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_matric_rb.setVisibility(View.VISIBLE);
				b_matric=true;
				tv_matric.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}

		}
		else if(v.getId()==R.id.intermed_textviewid){
			if(b_intermed) {
				ly_intermed_rb.setVisibility(View.GONE);
				b_intermed=false;
				tv_intermed.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_intermed_rb.setVisibility(View.VISIBLE);
				b_intermed=true;
				tv_intermed.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}

		else if(v.getId()==R.id.diploma_textviewid){
			if(b_diploma) {
				ly_diploma.setVisibility(View.GONE);
				b_diploma=false;
				tv_diploma.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_diploma.setVisibility(View.VISIBLE);
				b_diploma=true;
				tv_diploma.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}

		else if(v.getId()==R.id.graduation_textviewid){
			if(b_graduation) {
				ly_graduation_rb.setVisibility(View.GONE);
				b_graduation=false;
				tv_graduation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_graduation_rb.setVisibility(View.VISIBLE);
				b_graduation=true;
				tv_graduation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}

		else if(v.getId()==R.id.postgraduation_textviewid){
			if(b_postgraduation) {
				ly_postgraduation_rb.setVisibility(View.GONE);
				b_postgraduation=false;
				tv_postgraduation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_postgraduation_rb.setVisibility(View.VISIBLE);
				b_postgraduation=true;
				tv_postgraduation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}
		else if(v.getId()==R.id.language_textviewid){
			if(b_language) {
				ly_language.setVisibility(View.GONE);
				b_language=false;
				tv_language.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0);
			}
			else {
				ly_language.setVisibility(View.VISIBLE);
				b_language=true;
				tv_language.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse, 0);
			}
		}

	}
	public void volley()
	{

		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setCancelable(true);
		pd.setMessage("Loading Please Wait");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		detailedt=detailprf.edit();

		RequestQueue queue= Volley.newRequestQueue(getActivity());
		String url;

		if(all_sub.length()>0&nameofboard.length()>0&stateboard.length()>0&name_of_school.length()>0&roll_number.length()>0
				&obtanined_mrks.length()>0&total_marks.length()>0&grade.length()>0&mode.length()>0){
		detailedt.putBoolean("Education_info",true);
	}

		url= Constants.url+"edu_info.php?unique_id="+Constants.Unique_Id+"&all_sub="+all_sub+"&nameofboard="+nameofboard+"&stateboard="+stateboard+
				"&name_of_school="+name_of_school+"&result="+result+"&roll_number="+roll_number+"&obtanined_mrks="+
				obtanined_mrks+"&total_marks="+total_marks+"&percentage_mrks="+percentage_mrks+"&date_of_passing="+
				date_of_passing+"&grade="+grade+"&mode="+mode+"&stream="+stream+"&all_sb12="+all_sb12+"&name_baord="+
				name_baord+"&state_of_board12="+state_of_board12+"&name_of_school12="+name_of_school12+"&result12="+
				result12+"&roll_number12="+roll_number12+"&obtanied_mrks12="+obtanied_mrks12+"&total_mrks12="+total_mrks12+
				"&p_marks="+p_marks+"&d_of_pass="+d_of_pass+"&division="+division+"&mode12="+mode12+"&d_holder="+d_holder+
				"&deploma="+deploma+"&d_stream="+d_stream+"&dur_of_dep="+dur_of_dep+"&nam_of_board="+nam_of_board+
				"&d_state="+d_state+"&d_name_school="+d_name_school+"&d_res="+d_res+"&roll_no="+roll_no+"&obt_marks="+
				obt_marks+"&tot_marks="+tot_marks+"&per_marksd="+per_marksd+"&d_of_pass_d="+d_of_pass_d+"&d_grade="+d_grade+
				"&d_mode="+d_mode+"&graduate="+graduate+"&g_stream="+g_stream+"&g_duration="+g_duration+"&g_name_uni="+g_name_uni+
				"&g_place="+g_place+"&g_state="+g_state+"&g_name_of_clg="+g_name_of_clg+"&g_result="+g_result+
				"&g_marks_obt="+g_marks_obt+"&g_total_mrks="+g_total_mrks+"&g_percent="+g_percent+"&g_d_of_pass="+g_d_of_pass+
				"&g_grade="+g_grade+"&g_mode="+g_mode+"&cgpa="+cgpa+"&mrks_cgpa="+mrks_cgpa+"&postgraduate="+postgraduate+
				"&pg_streamv="+pg_streamv+"&pg_dur="+pg_dur+"&pg_name_of_uni="+pg_name_of_uni+"&pg_uni_place="+pg_uni_place+
				"&pg_uni_state="+pg_uni_state+"&pg_clgname="+pg_clgname+"&pg_result="+pg_result+"&pg_obt_marks="+pg_obt_marks+
				"&pg_total_marks="+pg_total_marks+"&pg_perc_of_mrks="+pg_perc_of_mrks+"&pd_d_pass="+pd_d_pass+"&pg_garde="+pg_garde+
				"&pg_modee="+pg_modee+"&pg_cgpa="+pg_cgpa+"&pg_max="+pg_max+"&other_lang="+other_lang+"&hindi_read="+hindi_read+
				"&eng_read="+eng_read+"&other_read="+other_read+"&hindi_write="+hindi_write+"&hindi_speak="+hindi_speak+
				"&eng_write="+eng_write+"&eng_speak="+eng_speak+"&other_write="+other_write+"&other_speak="+other_speak+
	  "&matric_yn="+matric_yn+"&intermed_yn="+intermed_yn+"&graduation_yn="+graduation_yn+"&postgraduation_yn="+postgraduation_yn;

		          url = url.replace(" ", "%20");

		Log.e("emailcheckvolleyurl", url);
		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				pd.dismiss();
				Log.e("loginres", res.toString());
				try {
					//if(res.getString("scalar").equals("Successfully Inserted")) {
						JSONObject jobj=new JSONObject();

					jobj.put("matric_yn",matric_yn);

						jobj.put("sub10",all_sub);
						jobj.put("board10",nameofboard);
						jobj.put("boardstate10",stateboard);
						jobj.put("school10",name_of_school);
						jobj.put("result10",result);
						jobj.put("rollno10",roll_number);
						jobj.put("obtainmark10",obtanined_mrks);
						jobj.put("totalmark10",total_marks);
						jobj.put("percentagemark10",percentage_mrks);
						jobj.put("passingdate10",date_of_passing);
						jobj.put("grade10",grade);
						jobj.put("mode10",mode);

					jobj.put("intermed_yn",intermed_yn);

					jobj.put("stream12",stream);
						jobj.put("sub12",all_sb12);
						jobj.put("board12",name_baord);
						jobj.put("boardstate12",state_of_board12);
						jobj.put("school12",name_of_school12);
						jobj.put("result12",result12);
						jobj.put("rollno12",roll_number12);
						jobj.put("obtainmark12",obtanied_mrks12);
						jobj.put("totalmark12",total_mrks12);
						jobj.put("percentagemark12",p_marks);
						jobj.put("passingdate12",d_of_pass);
						jobj.put("grade12",division);
						jobj.put("mode12",mode12);

						jobj.put("d_holder",d_holder);
						jobj.put("d_deploma",deploma);
						jobj.put("d_stream",d_stream);
						jobj.put("d_duretion",dur_of_dep);
						jobj.put("d_board",nam_of_board);
						jobj.put("d_board_state",d_state);
						jobj.put("d_school",d_name_school);
						jobj.put("d_result",d_res);
						jobj.put("d_rollno",roll_no);
						jobj.put("d_obt_mark",obt_marks);
						jobj.put("d_total_mark",tot_marks);
						jobj.put("d_per_mark",per_marksd);
						jobj.put("d_date_passing",d_of_pass_d);
						jobj.put("d_grade",d_grade);
						jobj.put("d_mode",d_mode);


					jobj.put("graduation_yn",graduation_yn);
						jobj.put("g_graduate",graduate);
						jobj.put("g_stream",g_stream);
						jobj.put("g_duration",g_duration);
						jobj.put("g_univercity",g_name_uni);
						jobj.put("g_univercity_place",g_place);
						jobj.put("g_univercity_state",g_state);
						jobj.put("g_collage",g_name_of_clg);
						jobj.put("g_result",g_result);
						jobj.put("g_obt_mark",g_marks_obt);
						jobj.put("g_total_mark",g_total_mrks);
						jobj.put("g_percentage",g_percent);
						jobj.put("g_date_passing",g_d_of_pass);
						jobj.put("g_grade",g_grade);
						jobj.put("g_mode",g_mode);
						jobj.put("g_cgpa",cgpa);
						jobj.put("g_max_cgpa",mrks_cgpa);

					jobj.put("postgraduation_yn",postgraduation_yn);
					jobj.put("pg_graduate",postgraduate);
						jobj.put("pg_stream",pg_streamv);
						jobj.put("pg_duration",pg_dur);
						jobj.put("pg_univercity",pg_name_of_uni);
						jobj.put("pg_univercity_place",pg_uni_place);
						jobj.put("pg_univercity_state",pg_uni_state);
						jobj.put("pg_collage",pg_clgname);
						jobj.put("pg_result",pg_result);
						jobj.put("pg_obt_mark",pg_obt_marks);
						jobj.put("pg_total_mark",pg_total_marks);
						jobj.put("pg_percentage",pg_perc_of_mrks);
						jobj.put("pg_date_passing",pd_d_pass);
						jobj.put("pg_grade",pg_garde);
						jobj.put("pg_mode",pg_modee);
						jobj.put("pg_cgpa",pg_cgpa);
						jobj.put("pg_max_cgpa",pg_max);

						jobj.put("hin_read",hindi_read);
						jobj.put("hin_write",hindi_write);
						jobj.put("hin_speak",hindi_speak);

						jobj.put("eng_read",eng_read);
						jobj.put("eng_write",eng_write);
						jobj.put("eng_speak",eng_speak);

						jobj.put("oth_lang",other_lang);
						jobj.put("oth_read",other_read);
						jobj.put("oth_write",other_write);
						jobj.put("oth_speak",other_speak);

						editor = user_info.edit();

						editor.putString("education_info", jobj.toString());

						editor.commit();



					if(matric_yn.equalsIgnoreCase("yes")==true)
						detailedt.putBoolean("metric",true);
					else
						detailedt.putBoolean("metric",false);

					if(intermed_yn.equalsIgnoreCase("yes")==true)
						detailedt.putBoolean("intermed",true);
					else
						detailedt.putBoolean("intermed",false);

					if(d_holder.equalsIgnoreCase("yes")==true)
						detailedt.putBoolean("diploma",true);
					else
						detailedt.putBoolean("diploma",false);

					if(graduation_yn.equalsIgnoreCase("yes")==true)
						detailedt.putBoolean("graduation",true);
					else
						detailedt.putBoolean("graduation",false);

					if(postgraduation_yn.equalsIgnoreCase("yes")==true)
						detailedt.putBoolean("postgraduation",true);
					else
						detailedt.putBoolean("postgraduation",false);


					detailedt.commit();



						Log.e("educationvolley", "done");

					final Snackbar snackbar = Snackbar.make(main_layout, "Successfully Inserted.", Snackbar.LENGTH_INDEFINITE);

					snackbar.setActionTextColor(Color.WHITE);
					snackbar.setDuration(3500);
					View snackbarView = snackbar.getView();
					snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

					TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					textView.setTextColor(Color.WHITE);//change Snackbar's text color;
					textView.setGravity(Gravity.CENTER_VERTICAL);
//					textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
					textView.setCompoundDrawablePadding(0);

					snackbar.show();
//						Toast.makeText(getActivity(),"Successfully Inserted",Toast.LENGTH_SHORT).show();

					//}

				} catch (JSONException e) {
					e.printStackTrace();
					Log.e("exceptionlogin",e.toString());
					pd.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError e) {

				pd.dismiss();
				final Snackbar snackbar = Snackbar.make(main_layout, "     Please check your internet connection.", Snackbar.LENGTH_INDEFINITE);

				snackbar.setActionTextColor(Color.WHITE);
				snackbar.setDuration(3500);
				View snackbarView = snackbar.getView();
				snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

				TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
				textView.setTextColor(Color.WHITE);//change Snackbar's text color;
				textView.setGravity(Gravity.CENTER_VERTICAL);
				textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
				textView.setCompoundDrawablePadding(0);

				snackbar.show();
//				Toast.makeText(getActivity(),"Please check your internet connection", Toast.LENGTH_SHORT).show();
				Log.e("error", e.toString());
			}
		});
		int socketTimeout = 50000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		jsonreq.setRetryPolicy(policy);
		queue.add(jsonreq);
	}
}