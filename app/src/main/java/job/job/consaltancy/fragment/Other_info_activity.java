package job.job.consaltancy.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import job.job.consaltancy.Apply_job_activity;
import job.job.consaltancy.R;
import job.job.consaltancy.adapter.Other_Info_Adapterr;
import job.job.consaltancy.adapter.Other_info_adapter;
import job.job.consaltancy.getset.other_info_admin_getset;
import job.job.consaltancy.getset.other_info_user_getset;
import job.job.consaltancy.utils.Constants;

/**
 * Created by Rohan on 2/9/2017.
 */
public class Other_info_activity extends Fragment
{
    ListView list;
    ArrayList<other_info_admin_getset> admin_list;
    ArrayList<other_info_user_getset> user_list;
    Other_Info_Adapterr adapter;
    ArrayList<String> psid_arr;
    LinearLayout main_layout;
    TextView nextbtn;
    boolean b;
    SharedPreferences other_pref;
    SharedPreferences.Editor other_edit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.other_info1, container,false);
        getActivity().setResult(getActivity().RESULT_OK);

        other_pref=getActivity().getSharedPreferences("other_pref",Context.MODE_PRIVATE);
        b=other_pref.getBoolean("boolean",false);

        main_layout=(LinearLayout)v.findViewById(R.id.main_layout);
        list=(ListView)v.findViewById(R.id.list_other_id);

        nextbtn=(TextView)v.findViewById(R.id.nextbtn);

        volley();

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b)
                {
                    Apply_job_activity basic_frag = new Apply_job_activity();
                    ((FragmentActivity) getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
                            addToBackStack(null).commit();
                }
                else if(Constants.other_adapter_count==Constants.other_count)
                {
                    other_edit=other_pref.edit();
                    other_edit.putBoolean("boolean",true);
                    other_edit.commit();

                        Apply_job_activity basic_frag = new Apply_job_activity();
                        ((FragmentActivity) getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, basic_frag).
                                addToBackStack(null).commit();
                }
                else
                {
                    final Snackbar snackbar = Snackbar.make(main_layout, " Please fill all fields.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
                }
            }
        });

        return v;
    }

    public void volley() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue= Volley.newRequestQueue(getActivity());

        String url= Constants.url+"get_other_info1.php?unique_id="+Constants.Unique_Id+"&job_id="+Constants.Job_id;
        Log.e("other_url= ", url);

        admin_list=new ArrayList<>();
        user_list=new ArrayList<>();
        psid_arr=new ArrayList<>();


        url=url.replaceAll(" ","%20");

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST,url, null,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res)
            {

                try {

                    if(res.has("info")) {
                        JSONArray array = res.getJSONArray("info");
                        int len = array.length();
                        Log.e("length=", String.valueOf(len));

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);

                            admin_list.add(new other_info_admin_getset(obj.getInt("id"), obj.getString("heading"), obj.getString("value_selection"),
                                    obj.getString("value_type"), obj.getString("info_type")));
                        }

                        Constants.other_adapter_count=admin_list.size();
                        Log.e("sizeee",Constants.other_adapter_count+"");

                        if(res.has("user_info")){

                            JSONArray user_arr = res.getJSONArray("user_info");

                            for (int i = 0; i < user_arr.length(); i++) {
                                JSONObject obj = user_arr.getJSONObject(i);

                                psid_arr.add(obj.getString("psid"));

                                user_list.add(new other_info_user_getset(obj.getInt("id"), obj.getString("psid"), obj.getString("info"),
                                        obj.getString("value_type"), obj.getString("info_type")));
                            }

                        }

                    }
                    else if(res.has("wrong")) {
                        final Snackbar snackbar = Snackbar.make(main_layout, " Can't value for other.", Snackbar.LENGTH_INDEFINITE);

                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);
                        snackbar.show();
//						Toast.makeText(getActivity(),"Can't value for other",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        final Snackbar snackbar = Snackbar.make(main_layout, " Somthing is Wrong Can't get value, Please Try Again.", Snackbar.LENGTH_INDEFINITE);
                        snackbar.setActionTextColor(Color.WHITE);
                        snackbar.setDuration(3500);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                        textView.setCompoundDrawablePadding(0);

                        snackbar.show();
                    }
//					Toast.makeText(getActivity(),"Somthing is Wrong Can't get value, Please Try Again",Toast.LENGTH_LONG).show();

                    adapter=new Other_Info_Adapterr(getActivity(),user_list,admin_list,psid_arr);
                    list.setAdapter(adapter);

                    dialog.dismiss();

                } catch (JSONException e) {
                    Log.e("catch exp= ", e.toString());
                    dialog.dismiss();
                    e.printStackTrace();
                }
            }

        }
                ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                dialog.dismiss();

                String error=arg0.toString();
                if (error.contains("NoConnectionError")) {
                    final Snackbar snackbar = Snackbar.make(main_layout, " No Connection Error.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
//					Toast.makeText(getActivity(), "NoConnectionError", Toast.LENGTH_LONG).show();
                }
                else
                {
                    final Snackbar snackbar = Snackbar.make(main_layout, " Can't data, We Update Soon.", Snackbar.LENGTH_INDEFINITE);

                    snackbar.setActionTextColor(Color.WHITE);
                    snackbar.setDuration(3500);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);//change Snackbar's text color;
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
                    textView.setCompoundDrawablePadding(0);

                    snackbar.show();
                }
//					Toast.makeText(getActivity(), "Can't data, We Update Soon", Toast.LENGTH_LONG).show();

                Log.e("error", arg0.toString());
            }
        });
        queue.add(request);
    }


}
