package job.job.consaltancy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import job.job.consaltancy.fragment.Tab_Fragment;
import job.job.consaltancy.utils.Constants;
import job.job.consaltancy.utils.NetworkConnection;

public class ForgotPassword extends AppCompatActivity {

    Toolbar toolbar;
    private EditText email;
    private TextView submit;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern pattern;
    private Matcher matcher;
    private TextView signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final NetworkConnection nw = new NetworkConnection(this);
        pattern = Pattern.compile(EMAIL_PATTERN);

        email = (EditText) findViewById(R.id.user_email);
        submit = (TextView) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                matcher = pattern.matcher(email.getText());

                if (email.getText().length() < 1) {
                    showSnackbar("Please enter registered Email");

                } else if (!matcher.matches()) {
                    showSnackbar("It not seems a valid Email address");
                } else if (!nw.isConnected()) {
                    showSnackbar("Oops! Not Connected to Internet");
                } else {
//                    login_volley();
//                    showSnackbar("Web Service not available or Implemented");
                    Volley(email.getText().toString());
                }
            }
        });
    }

    private void setSubmitSuccessView() {
        setContentView(R.layout.activity_forgot_password2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        signin = (TextView) findViewById(R.id.btn_signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
    }

    private void showSnackbar(String text) {

        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(email.getWindowToken(), 0);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.notify);

        final Snackbar snackbar = Snackbar.make(email, "  " + text, Snackbar.LENGTH_LONG);
//        snackbar.setAction("OK", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                    }
//                });
        snackbar.setActionTextColor(Color.DKGRAY);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#ff4c4c"));//change Snackbar's background color;

        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);//change Snackbar's text color;
        textView.setGravity(Gravity.CENTER);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.causion, 0, 0, 0);
        textView.setCompoundDrawablePadding(0);

        snackbar.show(); // Don’t forget to show!
        mp.start();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void Volley(String email)
    {
        final ProgressDialog dialog = new ProgressDialog(ForgotPassword.this);
        dialog.setMessage("Sending.. please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(ForgotPassword.this);

        String url = Constants.url + "forget_email.php?email=" + email;
        Log.e("url= ", url);
        url = url.replaceAll(" ", "%20");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                try {

                    Log.e("response=", res.toString());

                    if (res.getString("message").equals("Mail Sent")) {
                        setSubmitSuccessView();
                    }
                    if(res.getString("message").equals("Wrong Email"))
                    {
                        showSnackbar("Wrong Email");
                    }
                    if(res.getString("message").equals("Mail Not Sent"))
                    {
                        showSnackbar("Mail Not Sent");
                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    Log.e("catch exp= ", e.toString());
                    dialog.dismiss();
                    e.printStackTrace();
                }
            }

        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                dialog.dismiss();
                String error = arg0.toString();
                if (error.contains("NoConnectionError")) {
                    showSnackbar("No Internet Connection found");
//                    Toast.makeText(HomeActivity.this, "No Network Connection", Toast.LENGTH_LONG).show();
                }
//                else
//                    Toast.makeText(HomeActivity.this,"No any job in processing", Toast.LENGTH_LONG).show();

                Log.e("error", arg0.toString());
            }
        });
        int socketTimeOut=3000;
        RetryPolicy policy= new DefaultRetryPolicy(socketTimeOut,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }
}
