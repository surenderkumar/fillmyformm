package job.job.consaltancy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import consultancy.login;
import consultancy.slider.WelcomeActivity;

public class SplashActivity extends AppCompatActivity
{
   Boolean b=false;
    SharedPreferences user_info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        user_info = getSharedPreferences("user_info", Context.MODE_PRIVATE);
        b=user_info.getBoolean("login_bool",false);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {

                if(b)
                {
                    Intent in = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(in);
                    finish();
                }
                else {
                    Intent in = new Intent(SplashActivity.this, WelcomeActivity.class);
                    startActivity(in);
                    finish();
                }

            }

        },3000);

    }
}
